﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GraphicSettings : MonoBehaviour
{
    public Resolution[] resolutions;
    public Dropdown resolutionDropDown;

    void Start()
    {
        resolutions = Screen.resolutions;
        resolutionDropDown.ClearOptions();

        List<string> optionsList = new List<string>();

        int currentResolutionIndex = 0;

        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + " X " + resolutions[i].height;
            optionsList.Add(option);

            if ((resolutions[i].width == Screen.currentResolution.width)&&(resolutions[i].height == Screen.currentResolution.height))
            {
                currentResolutionIndex = i;
            }
        }

        resolutionDropDown.AddOptions(optionsList);
        resolutionDropDown.value = currentResolutionIndex;
        resolutionDropDown.RefreshShownValue();
    }

    public void SetResolution(int resolutionIdex)
    {
        Resolution resolution = resolutions[resolutionIdex];

        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    public void QualitySet(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    public void FullScreenSet(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
    }

    public void ReturnGO()
    {
        this.gameObject.SetActive(false);
    }
}
