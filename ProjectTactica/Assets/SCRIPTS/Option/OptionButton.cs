﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OptionButton : MonoBehaviour
{
    [SerializeField] GameObject graphic;

    public void GraphicGO()
    {
        GameObject graphicGameObject = Instantiate(graphic, transform.position, Quaternion.identity);
        graphicGameObject.transform.SetParent(this.transform, false);
        graphicGameObject.transform.localPosition = Vector3.zero;
    }

    public void ReturnGO()
    {
        Destroy(this.gameObject);
    }
}
