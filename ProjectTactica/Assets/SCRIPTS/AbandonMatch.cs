﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AbandonMatch : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Quit()
    {
        PhotonNetwork.Disconnect();
        Application.Quit();
    }
    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
        //SceneManager.LoadScene("MENU");
    }
}
