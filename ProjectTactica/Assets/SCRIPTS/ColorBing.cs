﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorBing : MonoBehaviour
{
    [SerializeField]
    private Color bing_color;
    private Color starting_color;
    [SerializeField]
    private int bing_repeat;
    public bool isBing;
    [SerializeField]
    private bool isUI;
    private int bing_times;
    [SerializeField]
    private float bing_delay;
    // Start is called before the first frame update
    void Start()
    {
        bing_times = 0;
        setStartingColor();
        StartCoroutine(waitUntilBing());
    }

    void setStartingColor()
    {
        if (isUI)
        {
            starting_color = GetComponent<Image>().color;
        }
        else
        {
            starting_color = GetComponent<SpriteRenderer>().color;
        }
    }

    IEnumerator waitUntilBing()
    {
        yield return new WaitUntil(() => isBing);
        isBing = false;
        bing_times++;
        for(int i = 0; i < bing_repeat; i++)
        {
            StartCoroutine(waitForBing(i));
        }
        StartCoroutine(waitUntilBing());
    }

    IEnumerator waitForBing(int i)
    {
        int time = bing_times;
        yield return new WaitForSecondsRealtime(bing_delay * i);
        if (time == bing_times)
        {
            if (i % 2 == 0)
            {
                if (isUI)
                {
                    GetComponent<Image>().color = bing_color;
                }
                else
                {
                    GetComponent<SpriteRenderer>().color = bing_color;
                }
            }
            else
            {
                if (isUI)
                {
                    GetComponent<Image>().color = starting_color;
                }
                else
                {
                    GetComponent<SpriteRenderer>().color = starting_color;
                }
            }
        }
    }
}
