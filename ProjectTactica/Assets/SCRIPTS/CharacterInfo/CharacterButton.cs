﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class CharacterButton : MonoBehaviour
{
    public string unitName;
    public Sprite unitSprite;
    public string unitStat;
    public string unitPrice;
    public string unitSpeed;
    public Sprite unitAttackType;
    public Sprite unitSkill;
    public Sprite unitRace;
    public string unitHealth;
    public Sprite baseAttackDes;
    public Sprite skillDes;
    public string unitBackground;
    private CharacterInfoManager _characterInfoManager;
    private VideoPlayer video;
    // Start is called before the first frame update
    void Start()
    {
        _characterInfoManager = FindObjectOfType<CharacterInfoManager>();
        video = GetComponent<VideoPlayer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void PressUnit()
    {
        _characterInfoManager.bigCanvasFrame.SetActive(true);
        _characterInfoManager.showInfo.SetSelectingCharacter(this);
        for(int i = 0; i < _characterInfoManager.button.Length; i++)
        {
            _characterInfoManager.button[i].SetActive(true);
            if(i == 0)
            {
                _characterInfoManager.button2[i].SetActive(true);
            }
            
        }
        //_characterInfoManager.isFirst = true;
        _characterInfoManager.raw.enabled = true;
        _characterInfoManager.secondCheck = true;
    }
}
