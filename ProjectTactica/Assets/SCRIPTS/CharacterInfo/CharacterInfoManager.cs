﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CharacterInfoManager : MonoBehaviour
{
    public List<VideoPlayer> video;
    public GameObject[] button;
    public GameObject[] button2;
    [SerializeField]
    public GameObject BGText;
    public bool isFirst = true;
    public bool secondCheck = true;
    public GameObject bigCanvasFrame;
    public ShowInfo showInfo;
    [SerializeField]
    private string sceneName;
    public RawImage raw;
    [SerializeField]
    private GameObject cover;
    [SerializeField]
    private List<GameObject> allButton;
    // Start is called before the first frame update
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {
        if(isFirst && secondCheck)
        {
            button2[0].SetActive(true);
            button2[1].SetActive(false);
            cover.SetActive(false);
        }
        else if(!isFirst && secondCheck)
        {
            button2[0].SetActive(false);
            button2[1].SetActive(true);
            cover.SetActive(true);
        }
    }
    public void GoBackMenuSecne()
    {
        SceneManager.LoadScene(sceneName);
    }
    public void AllFilter()
    {
        for(int i = 0; i < allButton.Count; i++)
        {
            allButton[i].SetActive(true);
        }
    }
    public void HumanFilter()
    {
        for(int i = 0; i < allButton.Count;i++)
        {
            if (i % 3 == 0)
            {
                allButton[i].SetActive(true);
            }
            else
            {
                allButton[i].SetActive(false);
            }
        }
    }
    public void ElfFilter()
    {
        for (int i = 0; i < allButton.Count; i++)
        {
            if (i % 3 == 2)
            {
                allButton[i].SetActive(true);
            }
            else
            {
                allButton[i].SetActive(false);
            }
        }
    }
    public void BeastFilter()
    {
        for (int i = 0; i < allButton.Count; i++)
        {
            if (i % 3 == 1)
            {
                allButton[i].SetActive(true);
            }
            else
            {
                allButton[i].SetActive(false);
            }
        }
    }
    public void ClickPhase()
    {
        if (isFirst)
        {
            button[0].GetComponent<Image>().color = new Color(1f, 1f, 0f, 1f);
            button[1].GetComponent<Image>().color = new Color(173f/255f, 109f/255f, 0f, 1f);
            //button2[1].SetActive(true);
            //button2[0].SetActive(false);
            button[0].GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
            button[1].GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
            isFirst = false;
            BGText.SetActive(true);
        }
    }
    public void ClickPhase2()
    {
        if(!isFirst)
        {
            button[0].GetComponent<Image>().color = new Color(1f, 1f, 0f, 1f);
            button[1].GetComponent<Image>().color = new Color(173f / 255f, 109f / 255f, 0f, 1f);
            //button2[1].SetActive(false);
            //button2[0].SetActive(true);
            button[0].GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
            button[1].GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
            isFirst = true;
            BGText.SetActive(false);
        }
    }
}