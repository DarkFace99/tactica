﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ShowDescription : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField]
    private CharacterSelectManager2 characterSelectManager2;
    [SerializeField]
    private GameObject panel;
    [SerializeField]
    private Vector2 offsetPos; //( +- 0.5f, - 1.95f)
    private GameObject spawn_panel;
    private float mouseOverTime = 0;
    private float mouseOverTime_max = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("YAY");
        for (int i = 0; i < characterSelectManager2.panel.Length; i++)
        {
            if (characterSelectManager2.playerSelecting[i])
            {
                panel = characterSelectManager2.panel[i];
                StartCoroutine(waitUntilSpawnPanel());
            }
        }
        mouseOverTime += 0.5f;
        //Debug.Log(mouseOverTime);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //Debug.Log("YAY2");
        mouseOverTime = 0f;
        StopCoroutine(waitUntilSpawnPanel());
        if (spawn_panel)
        {
            Destroy(spawn_panel);
            spawn_panel = null;
        }
    }
    IEnumerator waitUntilSpawnPanel()
    {
        yield return new WaitUntil(() => mouseOverTime >= mouseOverTime_max);
        if (spawn_panel)
        {
            Destroy(spawn_panel);
        }
        spawn_panel = Instantiate(panel, new Vector2(transform.position.x + offsetPos.x, transform.position.y + offsetPos.y), Quaternion.identity);
    }
    private void OnDestroy()
    {
        if (spawn_panel)
        {
            Destroy(spawn_panel);
        }
    }
}
