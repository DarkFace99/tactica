﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonScript : MonoBehaviour
{
    [SerializeField]
    private int index;
    public List<bool> isWatching;
    [SerializeField]
    private GameObject BuyButton;
    [SerializeField]
    private Text BuyButtonText;
    private bool canBuy;
    [SerializeField]
    private Text moneyText;
    [SerializeField]
    private GameObject NotEnoughMoney;
    [SerializeField]
    private float appearTime;
    [SerializeField]
    private CharacterInfoManager characterInfo;
    public void Anna()
    {
        for(int i = 0; i < isWatching.Count; i++)
        {
            if(i == 0)
            {
                isWatching[i] = true;
                BuyButton.SetActive(true);
            }
            else
            {
                isWatching[i] = false;
            }
        }
    }
    public void CorcBoi()
    {
        for (int i = 0; i < isWatching.Count; i++)
        {
            if (i == 1)
            {
                isWatching[i] = true;
                BuyButton.SetActive(true);
            }
            else
            {
                isWatching[i] = false;
            }
        }
    }
    public void Neiga()
    {
        for (int i = 0; i < isWatching.Count; i++)
        {
            if (i == 2)
            {
                isWatching[i] = true;
                BuyButton.SetActive(true);
            }
            else
            {
                isWatching[i] = false;
            }
        }
    }
    public void John()
    {
        for (int i = 0; i < isWatching.Count; i++)
        {
            if (i == 3)
            {
                isWatching[i] = true;
                BuyButton.SetActive(true);
            }
            else
            {
                isWatching[i] = false;
            }
        }
    }
    public void Diora()
    {
        for (int i = 0; i < isWatching.Count; i++)
        {
            if (i == 5)
            {
                isWatching[i] = true;
                BuyButton.SetActive(true);
            }
            else
            {
                isWatching[i] = false;
            }
        }
    }
    public void Ekak()
    {
        for (int i = 0; i < isWatching.Count; i++)
        {
            if (i == 4)
            {
                isWatching[i] = true;
                BuyButton.SetActive(true);
            }
            else
            {
                isWatching[i] = false;
            }
        }
    }
    public void Unit7()
    {
        for (int i = 0; i < isWatching.Count; i++)
        {
            if (i == 6)
            {
                isWatching[i] = true;
                BuyButton.SetActive(true);
            }
            else
            {
                isWatching[i] = false;
            }
        }
    }
    public void Unit8()
    {
        for (int i = 0; i < isWatching.Count; i++)
        {
            if (i == 8)
            {
                isWatching[i] = true;
                BuyButton.SetActive(true);
            }
            else
            {
                isWatching[i] = false;
            }
        }
    }
    public void Unit9()
    {
        for (int i = 0; i < isWatching.Count; i++)
        {
            if (i == 7)
            {
                isWatching[i] = true;
                BuyButton.SetActive(true);
            }
            else
            {
                isWatching[i] = false;
            }
        }
    }
    public void Unit10()
    {
        for (int i = 0; i < isWatching.Count; i++)
        {
            if (i == 9)
            {
                isWatching[i] = true;
                BuyButton.SetActive(true);
            }
            else
            {
                isWatching[i] = false;
            }
        }
    }
    public void Unit11()
    {
        for (int i = 0; i < isWatching.Count; i++)
        {
            if (i == 10)
            {
                isWatching[i] = true;
                BuyButton.SetActive(true);
            }
            else
            {
                isWatching[i] = false;
            }
        }
    }
    public void Unit12()
    {
        for (int i = 0; i < isWatching.Count; i++)
        {
            if (i == 11)
            {
                isWatching[i] = true;
                BuyButton.SetActive(true);
            }
            else
            {
                isWatching[i] = false;
            }
        }
    }
    public void Buy()
    {
        if (isWatching[0] == true && PlayerPrefs.GetInt("Money" + index) >= 250 && PlayerPrefs.GetInt("UnlockAnna" + index) == 0)
        {
            PlayerPrefs.SetInt("UnlockAnna" + index ,1);
            PlayerPrefs.SetInt("Money" + index, PlayerPrefs.GetInt("Money" + index) - 250);
            Debug.Log("Anna Buy ,Remaining money " + PlayerPrefs.GetInt("Money" + index));
            KeepUpdate();
        }
        else if (isWatching[0] == true && PlayerPrefs.GetInt("Money" + index) < 250 && PlayerPrefs.GetInt("UnlockAnna" + index) == 0)
        {
            NotEnoughMoney.SetActive(true);
            StartCoroutine(NotEnough());
        }
        if (isWatching[1] == true && PlayerPrefs.GetInt("Money" + index) >= 250 && PlayerPrefs.GetInt("UnlockCroc-Boi" + index) == 0)
        {
            PlayerPrefs.SetInt("UnlockCroc-Boi" + index, 1);
            PlayerPrefs.SetInt("Money" + index, PlayerPrefs.GetInt("Money" + index) - 250);
            Debug.Log("Croc-Boi Buy ,Remaining money " + PlayerPrefs.GetInt("Money" + index));
            KeepUpdate();
        }
        else if (isWatching[1] == true && PlayerPrefs.GetInt("Money" + index) < 250 && PlayerPrefs.GetInt("UnlockCroc-Boi" + index) == 0)
        {
            NotEnoughMoney.SetActive(true);
            StartCoroutine(NotEnough());
        }
        if (isWatching[2] == true && PlayerPrefs.GetInt("Money" + index) >= 250 && PlayerPrefs.GetInt("UnlockNeiga" + index) == 0)
        {
            PlayerPrefs.SetInt("UnlockNeiga" + index, 1);
            PlayerPrefs.SetInt("Money" + index, PlayerPrefs.GetInt("Money" + index) - 250);
            Debug.Log("Neiga Buy ,Remaining money " + PlayerPrefs.GetInt("Money" + index));
            KeepUpdate();
        }
        else if (isWatching[2] == true && PlayerPrefs.GetInt("Money" + index) < 250 && PlayerPrefs.GetInt("UnlockNeiga" + index) == 0)
        {
            NotEnoughMoney.SetActive(true);
            StartCoroutine(NotEnough());
        }
        if (isWatching[3] == true && PlayerPrefs.GetInt("Money" + index) >= 250 && PlayerPrefs.GetInt("UnlockCharacter4" + index) == 0)
        {
            PlayerPrefs.SetInt("UnlockCharacter4" + index, 1);
            PlayerPrefs.SetInt("Money" + index, PlayerPrefs.GetInt("Money" + index) - 250);
            Debug.Log("John Buy ,Remaining money " + PlayerPrefs.GetInt("Money" + index));
            PlayerPrefs.SetInt("CharacterCount" + index,(PlayerPrefs.GetInt("CharacterCount" + index)) + 1);
            KeepUpdate();
        }
        else if (isWatching[3] == true && PlayerPrefs.GetInt("Money" + index) < 250 && PlayerPrefs.GetInt("UnlockCharacter4" + index) == 0)
        {
            NotEnoughMoney.SetActive(true);
            StartCoroutine(NotEnough());
        }
        if (isWatching[4] == true && PlayerPrefs.GetInt("Money" + index) >= 250 && PlayerPrefs.GetInt("UnlockCharacter5" + index) == 0)
        {
            PlayerPrefs.SetInt("UnlockCharacter5" + index, 1);
            PlayerPrefs.SetInt("Money" + index, PlayerPrefs.GetInt("Money" + index) - 250);
            Debug.Log("Diora Buy ,Remaining money " + PlayerPrefs.GetInt("Money" + index));
            PlayerPrefs.SetInt("CharacterCount" + index, (PlayerPrefs.GetInt("CharacterCount" + index)) + 1);
            KeepUpdate();
        }
        else if (isWatching[4] == true && PlayerPrefs.GetInt("Money" + index) < 250 && PlayerPrefs.GetInt("UnlockCharacter5" + index) == 0)
        {
            NotEnoughMoney.SetActive(true);
            StartCoroutine(NotEnough());
        }
        if (isWatching[5] == true && PlayerPrefs.GetInt("Money" + index) >= 250 && PlayerPrefs.GetInt("UnlockCharacter6" + index) == 0)
        {
            PlayerPrefs.SetInt("UnlockCharacter6" + index, 1);
            PlayerPrefs.SetInt("Money" + index, PlayerPrefs.GetInt("Money" + index) - 250);
            Debug.Log("Unit 6 Buy ,Remaining money " + PlayerPrefs.GetInt("Money" + index));
            PlayerPrefs.SetInt("CharacterCount" + index, (PlayerPrefs.GetInt("CharacterCount" + index)) + 1);
            KeepUpdate();
        }
        else if (isWatching[5] == true && PlayerPrefs.GetInt("Money" + index) < 250 && PlayerPrefs.GetInt("UnlockCharacter6" + index) == 0)
        {
            NotEnoughMoney.SetActive(true);
            StartCoroutine(NotEnough());
        }
        if (isWatching[6] == true && PlayerPrefs.GetInt("Money" + index) >= 250 && PlayerPrefs.GetInt("UnlockCharacter7" + index) == 0)
        {
            PlayerPrefs.SetInt("UnlockCharacter7" + index, 1);
            PlayerPrefs.SetInt("Money" + index, PlayerPrefs.GetInt("Money" + index) - 250);
            Debug.Log("Unit 7 Buy ,Remaining money " + PlayerPrefs.GetInt("Money" + index));
            PlayerPrefs.SetInt("CharacterCount" + index, (PlayerPrefs.GetInt("CharacterCount" + index)) + 1);
            KeepUpdate();
        }
        else if (isWatching[6] == true && PlayerPrefs.GetInt("Money" + index) < 250 && PlayerPrefs.GetInt("UnlockCharacter7" + index) == 0)
        {
            NotEnoughMoney.SetActive(true);
            StartCoroutine(NotEnough());
        }
        if (isWatching[7] == true && PlayerPrefs.GetInt("Money" + index) >= 250 && PlayerPrefs.GetInt("UnlockCharacter8" + index) == 0)
        {
            PlayerPrefs.SetInt("UnlockCharacter8" + index, 1);
            PlayerPrefs.SetInt("Money" + index, PlayerPrefs.GetInt("Money" + index) - 250);
            Debug.Log("Unit 8 Buy ,Remaining money " + PlayerPrefs.GetInt("Money" + index));
            PlayerPrefs.SetInt("CharacterCount" + index, (PlayerPrefs.GetInt("CharacterCount" + index)) + 1);
            KeepUpdate();
        }
        else if (isWatching[7] == true && PlayerPrefs.GetInt("Money" + index) < 250 && PlayerPrefs.GetInt("UnlockCharacter8" + index) == 0)
        {
            NotEnoughMoney.SetActive(true);
            StartCoroutine(NotEnough());
        }
        if (isWatching[8] == true && PlayerPrefs.GetInt("Money" + index) >= 250 && PlayerPrefs.GetInt("UnlockCharacter9" + index) == 0)
        {
            PlayerPrefs.SetInt("UnlockCharacter9" + index, 1);
            PlayerPrefs.SetInt("Money" + index, PlayerPrefs.GetInt("Money" + index) - 250);
            Debug.Log("Unit 9 Buy ,Remaining money " + PlayerPrefs.GetInt("Money" + index));
            PlayerPrefs.SetInt("CharacterCount" + index, (PlayerPrefs.GetInt("CharacterCount" + index)) + 1);
            KeepUpdate();
        }
        else if (isWatching[8] == true && PlayerPrefs.GetInt("Money" + index) < 250 && PlayerPrefs.GetInt("UnlockCharacter9" + index) == 0)
        {
            NotEnoughMoney.SetActive(true);
            StartCoroutine(NotEnough());
        }
    }
    void Start()
    {
        KeepUpdate();
        index = DataLogin.Instance.IdIndex;
    }
    void Update()
    {
        CheckBuy();
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            Debug.Log(PlayerPrefs.GetString("User" + index) + " Money:" + PlayerPrefs.GetInt("Money" + index));
        }
        for(int i = 0; i < characterInfo.video.Count; i++)
        {
            if (isWatching[i])
            {
                if(characterInfo.isFirst)
                {
                    characterInfo.raw.texture = characterInfo.video[i].texture;
                    StartCoroutine(playVideo());
                }
                else
                {
                    characterInfo.video[i].Stop();
                }
            }
        }
        
    }
    void CheckBuy()
    {
        if (isWatching[0] == true)
        {
            if (PlayerPrefs.GetInt("UnlockAnna" + index) == 1)
            {
                canBuy = false;
            }
            else
            {
                canBuy = true;
            }
        }
        else if (isWatching[1] == true)
        {
            if (PlayerPrefs.GetInt("UnlockCroc-Boi" + index) == 1)
            {
                canBuy = false;
            }
            else
            {
                canBuy = true;
            }
        }
        else if (isWatching[2] == true)
        {
            if (PlayerPrefs.GetInt("UnlockNeiga" + index) == 1)
            {
                canBuy = false;
            }
            else
            {
                canBuy = true;
            }
        }
        else if (isWatching[3] == true)
        {
            if (PlayerPrefs.GetInt("UnlockCharacter4" + index) == 1)
            {
                canBuy = false;
            }
            else
            {
                canBuy = true;
            }
        }
        else if (isWatching[4] == true)
        {
            if (PlayerPrefs.GetInt("UnlockCharacter5" + index) == 1)
            {
                canBuy = false;
            }
            else
            {
                canBuy = true;
            }
        }
        else if (isWatching[5] == true)
        {
            if (PlayerPrefs.GetInt("UnlockCharacter6" + index) == 1)
            {
                canBuy = false;
            }
            else
            {
                canBuy = true;
            }
        }
        else if (isWatching[6] == true)
        {
            if (PlayerPrefs.GetInt("UnlockCharacter7" + index) == 1)
            {
                canBuy = false;
            }
            else
            {
                canBuy = true;
            }
        }
        else if (isWatching[7] == true)
        {
            if (PlayerPrefs.GetInt("UnlockCharacter8" + index) == 1)
            {
                canBuy = false;
            }
            else
            {
                canBuy = true;
            }
        }
        else if (isWatching[8] == true)
        {
            if (PlayerPrefs.GetInt("UnlockCharacter9" + index) == 1)
            {
                canBuy = false;
            }
            else
            {
                canBuy = true;
            }
        }
        if (canBuy)
        {
            BuyButton.GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
            BuyButtonText.text = "BUY";
        }
        else
        {
            BuyButton.GetComponent<Image>().color = new Color(100f / 255f, 100f / 255f, 100f / 255f, 100f / 255f);
            BuyButtonText.text = "OWNED";
        }
    }
    void KeepUpdate()
    {
        moneyText.text = PlayerPrefs.GetInt("Money" + DataLogin.Instance.IdIndex).ToString("0");
    }
    IEnumerator NotEnough()
    {
        yield return new WaitForSecondsRealtime(appearTime);
        NotEnoughMoney.SetActive(false);
    }
    IEnumerator playVideo()
    {
        for(int i = 0; i < characterInfo.video.Count; i++)
        {
            if(isWatching[i])
            {
                characterInfo.video[i].Prepare();
                WaitForSeconds wait = new WaitForSeconds(1f);
                while (!characterInfo.video[i].isPrepared)
                {
                    yield return wait;
                    break;
                }
                characterInfo.raw.texture = characterInfo.video[i].texture;
                characterInfo.video[i].Play();
            }
            else
            {
                characterInfo.video[i].Stop();
            }
            //player.Prepare();
            
        }
        
    }
}