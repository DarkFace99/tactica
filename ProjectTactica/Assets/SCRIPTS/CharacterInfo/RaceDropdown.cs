﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaceDropdown : MonoBehaviour
{
    [SerializeField]
    private CharacterInfoManager _characterInfoManager;
    public void Dropdown(int val)
    {
        if (val == 0)
        {
            _characterInfoManager.AllFilter();
        }
        if (val == 1)
        {
            _characterInfoManager.HumanFilter();
        }
        if (val == 2)
        {
            _characterInfoManager.ElfFilter();
        }
        if (val == 3)
        {
            _characterInfoManager.BeastFilter();
        }
    }
}
