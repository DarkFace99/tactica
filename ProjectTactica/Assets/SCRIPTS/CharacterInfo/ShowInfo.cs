﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowInfo : MonoBehaviour
{
    public Image UnitImage;
    public Text UnitName;
    public Text UnitPrice;
    public Text UnitSpeed;
    public Text UnitHealth;
    public Text UnitBG;
    public Image UnitType;
    public Image UnitSkill;
    public Image UnitRace;
    public Image Unitdes1;
    public Image Unitdes2;
    public Image UnitTri;
    public CharacterInfoManager _characterInfoManager;
    private CharacterButton selectingCharacter;
    public void ShowStat(Sprite picture, string status, string price, string speed, Sprite type, Sprite skill, Sprite race,string health, Sprite baseAtkdes, Sprite skilldes, string bg)
    {
        UnitName.enabled = true;
        UnitImage.enabled = true;
        UnitHealth.enabled = true;
        UnitSpeed.enabled = true;
        UnitType.enabled = true;
        UnitSkill.enabled = true;
        UnitRace.enabled = true;
        UnitPrice.enabled = true;
        Unitdes1.enabled = true;
        Unitdes2.enabled = true;
        UnitImage.sprite = picture;
        UnitName.text = status;
        UnitPrice.text = price;
        UnitSpeed.text = speed;
        UnitType.sprite = type;
        UnitSkill.sprite = skill;
        UnitRace.sprite = race;
        UnitHealth.text = health;
        Unitdes1.sprite = baseAtkdes;
        Unitdes2.sprite = skilldes;
        UnitBG.text = bg;
    }
    public void SetSelectingCharacter(CharacterButton character)
    {
        selectingCharacter = character;
        _characterInfoManager.showInfo.ShowStat(selectingCharacter.unitSprite, selectingCharacter.unitStat, selectingCharacter.unitPrice, selectingCharacter.unitSpeed, selectingCharacter.unitAttackType, selectingCharacter.unitSkill, selectingCharacter.unitRace, selectingCharacter.unitHealth, selectingCharacter.baseAttackDes, selectingCharacter.skillDes, selectingCharacter.unitBackground);
        //selectingCharacterSelect = characterSelectButton;
        //_characterSelectManager2.CharacterStatDisplay.SetDisplay(selectingCharacterSelect.unitSprite, selectingCharacterSelect.unitStat, selectingCharacterSelect.unitHealth, selectingCharacterSelect.unitSpeed, selectingCharacterSelect.unitAttackType, selectingCharacterSelect.unitSkill);
    }
}