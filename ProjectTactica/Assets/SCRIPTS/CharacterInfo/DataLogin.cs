﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataLogin : MonoBehaviour
{
    public static DataLogin Instance { get; private set; }
    public int IdIndex;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    
}
