﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandleControl : MonoBehaviour
{
    public Sprite afterPress;
    public GameObject button;
    public void AfterClick()
    {
       button.GetComponent<Image>().sprite  = afterPress;
    }
}
