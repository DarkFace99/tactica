﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Text.RegularExpressions;
using UnityEngine.UI;
using Photon.Pun;

public class Register : MonoBehaviour
{
    [SerializeField]
    private GameObject RegisterUI;
    [SerializeField]
    private InputField RegisterField;
    [SerializeField]
    private InputField EnterField;
    private string Username;
    [SerializeField]
    private string NextScene;
    private int userCount;
    //private bool
    void Start()
    {
        userCount = PlayerPrefs.GetInt("UserCount");

    }
    void Update()
    {
        //SetRegisterUI(FirstTime());
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            Debug.Log(PlayerPrefs.GetInt("UserCount"));
            userCount = PlayerPrefs.GetInt("UserCount");
            for (int index = 0; index < userCount; index++)
            {
                Debug.Log(PlayerPrefs.GetString("User" + index));
            }
        }
        if (Input.GetKeyDown(KeyCode.Equals))
        {
            userCount = PlayerPrefs.GetInt("UserCount");
            for (int index = userCount; index > userCount; index--)
            {
                PlayerPrefs.DeleteKey("User" + index);
                PlayerPrefs.DeleteKey("UserCount" + index);
                
            }
            //DeleteAccount();
        }
    }
    public void PressRegister()
    {
        userCount = PlayerPrefs.GetInt("UserCount");
        for (int index = 0; index < userCount; index++)
        {
            if (RegisterField.text != "" && RegisterField.text != PlayerPrefs.GetString("User" + index))
            {
                if(index == userCount)
                {
                    PlayerPrefs.SetString("User" + (index + 1), RegisterField.text);
                    Debug.Log("Register Success " + PlayerPrefs.GetString("User" + (index + 1)));
                }
                //RegisterUI.SetActive(false);
                //Debug.Log(PlayerPrefs.GetString("Player_Username", Username));
                //PlayerPrefs.DeleteAll();
            }
        }
    }
    public void DeleteAccount()
    {
        PlayerPrefs.DeleteAll();
       // PlayerPrefs.SetInt("UserCount", 0);
    }
    public void PressLogin()
    {
        userCount = PlayerPrefs.GetInt("UserCount");
        for (int index = 0; index < userCount; index++)
        {
            if (EnterField.text == PlayerPrefs.GetString("User" + index))
            {
                PhotonNetwork.NickName = EnterField.text;
                SceneManager.LoadScene(NextScene);
            }
        }
    }
    bool FirstTime()
    {
        if(PlayerPrefs.HasKey("User"))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    void SetRegisterUI(bool isFirst)
    {
        if(isFirst)
        {
            RegisterUI.SetActive(true);
        }
        else
        {
            RegisterUI.SetActive(false);
        }
    }
}