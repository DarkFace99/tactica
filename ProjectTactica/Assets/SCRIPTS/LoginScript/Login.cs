﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Photon.Pun;
using UnityEngine.EventSystems;

public class Login : MonoBehaviour, IPointerEnterHandler
{
    public int width, height;
    [SerializeField]
    private InputField RegisterPanel;
    [SerializeField]
    private InputField LoginPanel;
    [SerializeField]
    private string nextScene;
    private int userCount;
    [SerializeField]
    private float appearTime;
    [SerializeField]
    private GameObject NoUserText;
    [SerializeField]
    private GameObject UsedNameText;
    [SerializeField]
    private string Selectserver;
    [SerializeField]
    private AudioSource audio;
    private void Awake()
    {
        if (PlayerPrefs.HasKey("Width") == false || PlayerPrefs.HasKey("Height") == false)
        {
            PlayerPrefs.SetInt("Width", 1920);
            PlayerPrefs.SetInt("Height", 1080);
        }
        if (PlayerPrefs.HasKey("FullScreen") == false)
        {
            PlayerPrefs.SetInt("FullScreen", 1);

        }
        if(PlayerPrefs.HasKey("Quality") == false)
        {
            PlayerPrefs.SetInt("Quality", 3);
        }
        else
        {
            QualitySettings.SetQualityLevel(PlayerPrefs.GetInt("Quality"));
        }
        if ((PlayerPrefs.HasKey("Width") || PlayerPrefs.HasKey("Height") && (PlayerPrefs.HasKey("FullScreen") == false)))
        {
            Screen.SetResolution(PlayerPrefs.GetInt("Width"), PlayerPrefs.GetInt("Height"), !Screen.fullScreen);
        }
        if ((PlayerPrefs.HasKey("Width") || PlayerPrefs.HasKey("Height") && (PlayerPrefs.HasKey("FullScreen") == true)))
        {
            Screen.SetResolution(PlayerPrefs.GetInt("Width"), PlayerPrefs.GetInt("Height"), Screen.fullScreen);
        }
        audio = GetComponent<AudioSource>();
    }
    // Start is called before the first frame update
    void Start()
    {
        userCount = PlayerPrefs.GetInt("UserCount");
    }
    public void PressRegister()
    {
        if (RegisterPanel.text != "" && userCount == 0)
        {
            PlayerPrefs.SetString("User" + PlayerPrefs.GetInt("UserCount"), RegisterPanel.text);
            userCount++;
            PlayerPrefs.SetInt("UserCount", userCount);
            PlayerPrefs.SetInt("Money" + 0, 50);
            PlayerPrefs.SetInt("UnlockAnna" + 0, 1);
            PlayerPrefs.SetInt("UnlockCroc-Boi" + 0, 1);
            PlayerPrefs.SetInt("UnlockNeiga" + 0, 1);
            PlayerPrefs.SetInt("UnlockCharacter4" + 0, 0);
            PlayerPrefs.SetInt("UnlockCharacter5" + 0, 0);
            PlayerPrefs.SetInt("UnlockCharacter6" + 0, 0);
            PlayerPrefs.SetInt("UnlockCharacter7" + 0, 0);
            PlayerPrefs.SetInt("UnlockCharacter8" + 0, 0);

        }
        else if(RegisterPanel.text != "" && userCount > 0)
        {
            for(int i = 0; i < PlayerPrefs.GetInt("UserCount"); i++)
            { 
                if(PlayerPrefs.GetString("User" + i) != RegisterPanel.text)
                {
                    PlayerPrefs.SetString("User" + PlayerPrefs.GetInt("UserCount"), RegisterPanel.text);
                    PlayerPrefs.SetInt("Money" + PlayerPrefs.GetInt("UserCount"), 50);
                    PlayerPrefs.SetInt("UnlockAnna" + PlayerPrefs.GetInt("UserCount"), 1);
                    PlayerPrefs.SetInt("UnlockCroc-Boi" + PlayerPrefs.GetInt("UserCount"), 1);
                    PlayerPrefs.SetInt("UnlockNeiga" + PlayerPrefs.GetInt("UserCount"), 1);
                    PlayerPrefs.SetInt("UnlockCharacter4" + PlayerPrefs.GetInt("UserCount"), 0);
                    PlayerPrefs.SetInt("UnlockCharacter5" + PlayerPrefs.GetInt("UserCount"), 0);
                    PlayerPrefs.SetInt("UnlockCharacter6" + PlayerPrefs.GetInt("UserCount"), 0);
                    PlayerPrefs.SetInt("UnlockCharacter7" + PlayerPrefs.GetInt("UserCount"), 0);
                    PlayerPrefs.SetInt("UnlockCharacter8" + PlayerPrefs.GetInt("UserCount"), 0);
                    userCount++;
                    PlayerPrefs.SetInt("UserCount", userCount);
                    break;
                }
                else
                {
                    //Debug.Log("TryAgain");
                    UsedNameText.SetActive(true);
                    StartCoroutine(AppearTimeUsed());
                }
            }
        }
        
    }
    public void DeleteAccount()
    {
        PlayerPrefs.DeleteAll();
        // PlayerPrefs.SetInt("UserCount", 0);
    }
    public void PressLogin()
    {
        for (int i = 0; i < PlayerPrefs.GetInt("UserCount"); i++)
        {
            if (PlayerPrefs.GetString("User" + i) == LoginPanel.text)
            {
                PhotonNetwork.NickName = LoginPanel.text;
                DataLogin.Instance.IdIndex = i;
                SceneManager.LoadScene(nextScene);
            }
            else
            {
                if(i == PlayerPrefs.GetInt("UserCount") - 1)
                {
                    NoUserText.SetActive(true);
                    StartCoroutine(AppearTimeNoName());
                }
            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            Debug.Log(PlayerPrefs.GetInt("UserCount"));
            for (int i = 0; i < PlayerPrefs.GetInt("UserCount"); i++)
            {
                Debug.Log(PlayerPrefs.GetString("User" + i) + " Money:" +  PlayerPrefs.GetInt("Money" + i));
            }
        }
        if(Input.GetKeyDown(KeyCode.Delete))
        {
            DeleteAccount();
        }
    }
    public void SetWidt(int W)
    {
        width = W;
    }
    public void SetHeight(int H)
    {
        height = H;
    }

    public void SetReso()
    {
        Screen.SetResolution(width, height, Screen.fullScreen);
        PlayerPrefs.SetInt("Width", width);
        PlayerPrefs.SetInt("Height", height);
    }

    public void SetFull(bool isfull)
    {
        Screen.fullScreen = isfull;
        if (isfull)
        {
            PlayerPrefs.SetInt("FullScreen", 1);
        }
        else
        {
            PlayerPrefs.SetInt("FullScreen", 0);
        }
    }
    IEnumerator AppearTimeUsed()
    {
        yield return new WaitForSecondsRealtime(appearTime);
        UsedNameText.SetActive(false);
    }
    IEnumerator AppearTimeNoName()
    {
        yield return new WaitForSecondsRealtime(appearTime);
        NoUserText.SetActive(false);
    }
    public void GoToServer()
    {
        SceneManager.LoadScene(Selectserver);
        PhotonNetwork.Disconnect();
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        print("OnMouseEnter");
        audio.Play();
    }
}