﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Photon.Pun;

public class SaveSlot : MonoBehaviour,IPointerEnterHandler,IPointerClickHandler
{
    [SerializeField]
    private int ID;
    [SerializeField]
    private GameObject CreateSaveText;
    [SerializeField]
    private GameObject Set1;
    [SerializeField]
    private Text NameText;
    [SerializeField]
    private Text MoneyText;
    [SerializeField]
    private Text UnlockNumberCharacterText;
    [SerializeField]
    private GameObject Set2;
    [SerializeField]
    private InputField fillName;
    [SerializeField]
    private GameObject askingPanel;
    private bool isRegister = false;
    private bool Checks = false;
    [SerializeField]
    private AudioSource audio;
    [SerializeField]
    private AudioSource audio2;
    [SerializeField]
    private float time;
    private bool isGo = false;
    private bool canclick = true;
    // Start is called before the first frame update
    void Start()
    {
        Check();
        audio = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        if(isGo && time > 0.8f)
        {
            SceneManager.LoadScene("MENU");
        }
        else if(isGo && time <= 0.8f)
        {
            time += Time.deltaTime;
        }
    }
    void Check()
    {
        if (PlayerPrefs.HasKey("User" + ID))
        {
            Set2.SetActive(true);
            NameText.text = PlayerPrefs.GetString("User" + ID);
            UnlockNumberCharacterText.text = PlayerPrefs.GetInt("CharacterCount" + ID).ToString("0");
            MoneyText.text = (PlayerPrefs.GetInt("Money" + ID)).ToString("0");
            //Debug.Log("Money = " + PlayerPrefs.GetInt("Money" + ID) + " ID = " + ID);
        }
        else if(!PlayerPrefs.HasKey("User" + ID))
        {
            CreateSaveText.SetActive(true);
        }
    }
    public void ClickButton()
    {
        if(canclick)
        {
            if (PlayerPrefs.HasKey("User" + ID))
            {
                DataLogin.Instance.IdIndex = ID;
                isGo = true;
                canclick = false;
                PhotonNetwork.NickName = PlayerPrefs.GetString("User" + ID);
            }
            else if (!PlayerPrefs.HasKey("User" + ID))
            {
                //Debug.Log("Registering...");
                Set1.SetActive(true);
                CreateSaveText.SetActive(false);
            }
        }
    }
    void SetRegisterField()
    {
        isRegister = !isRegister;
        if(isRegister)
        {
            Set1.SetActive(true);
        }
        else
        {
            Set1.SetActive(false);
        }
        
    }
    public void PressRegister()
    {
        if (fillName.text != " ")
        {
            PlayerPrefs.SetString("User" + ID,fillName.text);
            PlayerPrefs.SetInt("Money" + ID, 200);
            PlayerPrefs.SetInt("UnlockAnna" + ID, 1);
            PlayerPrefs.SetInt("UnlockCroc-Boi" + ID, 1);
            PlayerPrefs.SetInt("UnlockNeiga" + ID, 1);
            PlayerPrefs.SetInt("UnlockCharacter4" + ID, 0);
            PlayerPrefs.SetInt("UnlockCharacter5" + ID, 0);
            PlayerPrefs.SetInt("UnlockCharacter6" + ID, 0);
            PlayerPrefs.SetInt("UnlockCharacter7" + ID, 0);
            PlayerPrefs.SetInt("UnlockCharacter8" + ID, 0);
            PlayerPrefs.SetInt("UnlockCharacter9" + ID, 0);
            PlayerPrefs.SetInt("CharacterCount" + ID, 3);
            //Debug.Log("RegisterComplete");
            Set1.SetActive(false);
            Set2.SetActive(true);
            CreateSaveText.SetActive(false);
            Check();
        }
    }
    public void PressCancel()
    {
        Set1.SetActive(false);
        CreateSaveText.SetActive(true);
    }
    public void DeleteSaveSlot()
    {
        askingPanel.SetActive(true);
        Set2.SetActive(false);
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        //print("OnMouseEnter");
        if(canclick)
        {
            audio.Play();
        }
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        //print("OnMouseEnter");
        audio2.Play();
    }
    public void Sure_Delete()
    {
        PlayerPrefs.DeleteKey("User" + ID);
        PlayerPrefs.DeleteKey("Money" + ID);
        PlayerPrefs.DeleteKey("UnlockAnna" + ID);
        PlayerPrefs.DeleteKey("UnlockCroc-Boi" + ID);
        PlayerPrefs.DeleteKey("UnlockNeiga" + ID);
        PlayerPrefs.DeleteKey("UnlockCharacter4" + ID);
        PlayerPrefs.DeleteKey("UnlockCharacter5" + ID);
        PlayerPrefs.DeleteKey("UnlockCharacter6" + ID);
        PlayerPrefs.DeleteKey("UnlockCharacter7" + ID);
        PlayerPrefs.DeleteKey("UnlockCharacter8" + ID);
        PlayerPrefs.DeleteKey("UnlockCharacter9" + ID);
        PlayerPrefs.DeleteKey("CharacterCount" + ID);
        //Debug.Log("DeletSlot " + ID);
        CreateSaveText.SetActive(true);
        askingPanel.SetActive(false);

    }
    public void Not_Delete()
    {
        askingPanel.SetActive(false);
        Set2.SetActive(true);
    }
}
