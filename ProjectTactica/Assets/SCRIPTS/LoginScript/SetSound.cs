﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SetSound : MonoBehaviour
{
    [SerializeField]
    private AudioMixer audioMixer;
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey("master"))
        {
            PlayerPrefs.SetFloat("master", PlayerPrefs.GetFloat("master"));

        }
        else
        {
            PlayerPrefs.SetFloat("master", -10);
        }
        if (PlayerPrefs.HasKey("music"))
        {
            PlayerPrefs.SetFloat("music", PlayerPrefs.GetFloat("music"));
        }
        else
        {
            PlayerPrefs.SetFloat("music", -10);
        }
        if (PlayerPrefs.HasKey("sfx"))
        {
            PlayerPrefs.SetFloat("sfx", PlayerPrefs.GetFloat("sfx"));
            audioMixer.SetFloat("sfx", PlayerPrefs.GetFloat("sfx"));

        }
        else
        {
            PlayerPrefs.SetFloat("sfx", -10);
        }
    }
}
