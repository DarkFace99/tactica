﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitSelectButton : MonoBehaviour
{
    private CharacterSelectButton selectingCharacterSelect;
    //private CharacterSelectManager _characterSelectManager;
    private CharacterSelectManager2 _characterSelectManager2;

    // Start is called before the first frame update
    void Start()
    {
        //==================== Because of FindObjectOfType at characterSelectManager has only one object ====================
        //_characterSelectManager = FindObjectOfType<CharacterSelectManager>();
        _characterSelectManager2 = FindObjectOfType<CharacterSelectManager2>();
    }
    //==================== This fuction use for press Select button ====================
    //public void PressSelectButton()
    //{
    //    //==================== Add unit member after press select button for player 1 ====================
    //    if (_characterSelectManager.isPlayer1turn == true)
    //    {
    //        _characterSelectManager2.teamdisplay.AddMember(selectingCharacterSelect);
    //        _characterSelectManager2.selectRound++;
    //        _characterSelectManager2.CharacterStatDisplay.NotDisplay(selectingCharacterSelect.unitSprite, selectingCharacterSelect.unitStat);
    //        _characterSelectManager2.timeCounter.ReCountdown();
    //    }
    //    //==================== Add unit member after press select button for player 2 ====================
    //    else
    //    {
    //        _characterSelectManager2.teamdisplay2.AddMember(selectingCharacterSelect);
    //        _characterSelectManager2.selectRound++;
    //        _characterSelectManager2.CharacterStatDisplay.NotDisplay(selectingCharacterSelect.unitSprite, selectingCharacterSelect.unitStat);
    //        _characterSelectManager2.timeCounter.ReCountdown();
    //    }
    //}
    //==================== This function use after press unit button ====================
    public void SetSelectingCharacter(CharacterSelectButton characterSelectButton)
    {
        selectingCharacterSelect = characterSelectButton;
        _characterSelectManager2.CharacterStatDisplay.SetDisplay(selectingCharacterSelect.unitSprite, selectingCharacterSelect.unitStat, selectingCharacterSelect.unitHealth,selectingCharacterSelect.unitSpeed,selectingCharacterSelect.unitAttackType,selectingCharacterSelect.unitSkill, selectingCharacterSelect.unitRace, selectingCharacterSelect.unitTri);
    }
    //==================== This function use for random selecting character ====================
    public void SetSelectingCharacter2(CharacterSelectButton characterSelectButton)
    {
        selectingCharacterSelect = characterSelectButton;
    }
    //public void SelectPress()
    //{
    //    if(_characterSelectManager2.player == 1)
    //    {
    //        _characterSelectManager2.teamdisplay.AddingMember(_characterSelectManager2.teamdisplay.teamMemberDisplay,_characterSelectManager2.teamdisplay.layoutGroup.transform.localPosition ,selectingCharacterSelect);
    //    }
    //    else if(_characterSelectManager2.player == 2)
    //    {

    //    }
    //}
}
