﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DisconnectHand3 : MonoBehaviourPunCallbacks
{
    private string return_scene_connect = "MENU";
    public override void OnLeftRoom()
    {
        PhotonNetwork.LeaveRoom();
        SceneManager.LoadScene(return_scene_connect);
    }
}
