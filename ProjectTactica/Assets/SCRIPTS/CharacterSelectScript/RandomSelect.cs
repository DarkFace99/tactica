﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSelect : MonoBehaviour
{
    public string unitName;
    public Sprite unitSprite;
    public string unitStat;
    public string unitRandom;
    public Sprite unitRandomSprite;
    private CharacterSelectManager _characterSelectManager;
    private CharacterSelectButton selectingCharacterSelect;
    // Start is called before the first frame update
    void Start()
    {
        _characterSelectManager = FindObjectOfType<CharacterSelectManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void RandomHuman1()
    {
        if (_characterSelectManager.isPlayer1turn == true)
        {
            _characterSelectManager.teamdisplay.AddMember(selectingCharacterSelect);
            _characterSelectManager.selectRound++;
            _characterSelectManager.CharacterStatDisplay.NotDisplay(selectingCharacterSelect.unitSprite, selectingCharacterSelect.unitStat);
        }
        else
        {
            _characterSelectManager.teamdisplay2.AddMember(selectingCharacterSelect);
            _characterSelectManager.selectRound++;
            _characterSelectManager.CharacterStatDisplay.NotDisplay(selectingCharacterSelect.unitSprite, selectingCharacterSelect.unitStat);
        }
    }
}
