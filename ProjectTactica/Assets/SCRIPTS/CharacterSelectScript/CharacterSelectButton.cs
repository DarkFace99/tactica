﻿using System.Collections.Generic;
using UnityEngine;

public class CharacterSelectButton : MonoBehaviour
{
    public string unitName;
    public Sprite unitSprite;
    public string unitStat;
    public string unitHealth;
    public string unitSpeed;
    public Sprite unitAttackType;
    public Sprite unitSkill;
    public Sprite unitRace;
    public Sprite unitTri;
    private CharacterSelectManager2 _characterSelectManager;

    // Start is called before the first frame updateg
    void Start()
    {
        //==================== Because of FindObjectOfType at characterSelectManager has only one object ====================
        _characterSelectManager = FindObjectOfType<CharacterSelectManager2>();
    }

    // Update is called once per frame

    public void PressUnitButton()
    {
        _characterSelectManager.UnitSelectButton.SetSelectingCharacter(this);
    }
    //public void RandomUnitButton()
    //{
    //    for(int i = 0; i < _characterSelectManager.player1Select.Count; i++)
    //    {
    //        if(i == _characterSelectManager.timeCounter.randomNumber)
    //        {
    //            if (_characterSelectManager.isPlayer1turn == true)
    //            {
    //                if (_characterSelectManager.player1Select[i] == false)
    //                {
    //                    _characterSelectManager.UnitSelectButton.SetSelectingCharacter2(_characterSelectManager.characterselectbutton[i]);
    //                    _characterSelectManager.teamdisplay.AddMember(_characterSelectManager.characterselectbutton[i]);
    //                    _characterSelectManager.player1Select[i] = true;
    //                    DataSelect.Instance.p1Select[i] = true;
    //                    break;
    //                }
    //            }
    //            else
    //            {
    //                if (_characterSelectManager.player2Select[i] == false)
    //                {
    //                    _characterSelectManager.UnitSelectButton.SetSelectingCharacter2(_characterSelectManager.characterselectbutton[i]);
    //                    _characterSelectManager.teamdisplay2.AddMember(_characterSelectManager.characterselectbutton[i]);
    //                    _characterSelectManager.player2Select[i] = true;
    //                    DataSelect.Instance.p2Select[i] = true;
    //                    break;
    //                }
    //            }
    //        }
    //    }
        //if (_characterSelectManager.isPlayer1turn == true && _characterSelectManager.player1Select[0] == false)
        //{
        //    _characterSelectManager.UnitSelectButton.SetSelectingCharacter2(_characterSelectManager.characterselectbutton[0]);
        //    _characterSelectManager.teamdisplay.AddMember(_characterSelectManager.characterselectbutton[0]);
        //    _characterSelectManager.player1Select[0] = true;
        //}
        //else if (_characterSelectManager.isPlayer1turn == false && _characterSelectManager.player2Select[0] == false)
        //{
        //    _characterSelectManager.UnitSelectButton.SetSelectingCharacter2(_characterSelectManager.characterselectbutton[0]);
        //    _characterSelectManager.teamdisplay2.AddMember(_characterSelectManager.characterselectbutton[0]);
        //    _characterSelectManager.player2Select[0] = true;
        //    // _characterSelectManager.selectRound++;
        //}
        //else if (_characterSelectManager.isPlayer1turn == true && _characterSelectManager.player1Select[3] == false)
        //{
        //    _characterSelectManager.UnitSelectButton.SetSelectingCharacter2(_characterSelectManager.characterselectbutton[3]);
        //    _characterSelectManager.teamdisplay.AddMember(_characterSelectManager.characterselectbutton[3]);
        //    _characterSelectManager.player1Select[3] = true;
        //}
        //else if (_characterSelectManager.isPlayer1turn == false && _characterSelectManager.player2Select[3] == false)
        //{
        //    _characterSelectManager.UnitSelectButton.SetSelectingCharacter2(_characterSelectManager.characterselectbutton[3]);
        //    _characterSelectManager.teamdisplay2.AddMember(_characterSelectManager.characterselectbutton[3]);
        //    _characterSelectManager.player2Select[3] = true;
        //    // _characterSelectManager.selectRound++;
        //}
        //else if (_characterSelectManager.isPlayer1turn == true && _characterSelectManager.player1Select[6] == false)
        //{
        //    _characterSelectManager.UnitSelectButton.SetSelectingCharacter2(_characterSelectManager.characterselectbutton[6]);
        //    _characterSelectManager.teamdisplay.AddMember(_characterSelectManager.characterselectbutton[6]);
        //    _characterSelectManager.player1Select[6] = true;
        //}
        //else if (_characterSelectManager.isPlayer1turn == false && _characterSelectManager.player2Select[6] == false)
        //{
        //    _characterSelectManager.UnitSelectButton.SetSelectingCharacter2(_characterSelectManager.characterselectbutton[6]);
        //    _characterSelectManager.teamdisplay2.AddMember(_characterSelectManager.characterselectbutton[6]);
        //    _characterSelectManager.player2Select[6] = true;
        //    // _characterSelectManager.
        //    _characterSelectManager.selectRound++;
        //}
    //    _characterSelectManager.selectRound++;
    //}
}