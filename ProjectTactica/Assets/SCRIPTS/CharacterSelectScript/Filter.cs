﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Filter : MonoBehaviour
{
    public CharacterSelectManager2 _characterSelectManager2;
    // Start is called before the first frame update
    public void Dropdown(int val)
    {
        if (val == 0)
        {
            _characterSelectManager2.AllFilter();
        }
        else if (val == 1)
        {
            _characterSelectManager2.HumanFilter();
        }
        else if (val == 2)
        {
            _characterSelectManager2.ElfFilter();
        }
        else if (val == 3)
        {
            _characterSelectManager2.BeastFilter();
        }
    }
}
