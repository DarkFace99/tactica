﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DisconnectHandle2 : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private string return_scene_connect;
    [SerializeField]
    private string return_scene_disconnect;
    [SerializeField]
    private int maxPing;
    [SerializeField]
    private float maxOverPingTime;
    private float overPingTime;
    // Start is called before the first frame update
    private void Awake()
    {
        overPingTime = 0f;
        StartCoroutine(waitUntilNotConnect());
    }
    // Update is called once per frame
    void Update()
    {
        pingCheck();
    }

    IEnumerator waitUntilNotConnect()
    {
        yield return new WaitUntil(() => !PhotonNetwork.IsConnected);
        SceneManager.LoadScene(return_scene_disconnect);
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        PhotonNetwork.LeaveRoom();
        SceneManager.LoadScene(return_scene_connect);
    }
    void pingCheck()
    {
        if (PhotonNetwork.GetPing() > maxPing)
        {
            overPingTime += Time.deltaTime;
            if(overPingTime > maxOverPingTime)
            {
                PhotonNetwork.Disconnect();
            }
        }
        else
        {
            overPingTime = 0f;
        }
    }
}
