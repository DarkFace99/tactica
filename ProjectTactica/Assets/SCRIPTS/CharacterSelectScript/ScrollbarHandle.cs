﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollbarHandle : MonoBehaviour
{
    public CharacterSelectManager2 characterSelectManager2;
    [SerializeField]
    private GameObject Text;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(FirstWait());
    }

    // Update is called once per frame
    void Update()
    {

    }
    IEnumerator FirstWait()
    {
        yield return new WaitForSecondsRealtime(characterSelectManager2.delaytime);
        Text.SetActive(true);
    }
}