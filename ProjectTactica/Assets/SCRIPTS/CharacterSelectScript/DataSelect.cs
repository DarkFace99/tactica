﻿using System.Collections.Generic;
using UnityEngine;

public class DataSelect : MonoBehaviour
{
    public static DataSelect Instance { get; private set; }
    public List<int> playerChoose;
    public int T, L;

    private void Awake()
    { 
        //==================== Singleton for Not spawn more than 1 and the keep value between scene ====================
        //_characterSelectManager2 = FindObjectOfType<CharacterSelectManager2>();
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}