﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class RandomMap : MonoBehaviour
{
    [SerializeField]
    private GameObject map;
    [SerializeField]
    private List<GameObject> DesertLayout;
    [SerializeField]
    private List<GameObject> IceLayout;
    private GridSpawn gridSpawn;
    public int theme;
    public int layout;

    void Awake()
    {
        theme = 0;
        layout = 0;
        StartCoroutine(GetMap());
        if (PhotonNetwork.IsMasterClient == false)
        {
            theme = Random.Range(1, 3);
            layout = Random.Range(1, 5);
            //theme = 2;
            //layout = 1;
            GetComponent<PhotonView>().RPC("RPC_setTypeOfMap", RpcTarget.All, theme, layout);
        }
    }
    [PunRPC]
    void RPC_setTypeOfMap(int theme_num, int layout_num)
    {
        DataSelect.Instance.T = theme_num;
        DataSelect.Instance.L = layout_num;
        theme = theme_num;
        layout = layout_num;
    }
    private void SetMapLayout(int t, int l)
    {
        if(t == 1)
        {
            map.GetComponent<Image>().sprite = DesertLayout[l - 1].GetComponent<Image>().sprite;
        }
        else
        {
            map.GetComponent<Image>().sprite = IceLayout[l - 1].GetComponent<Image>().sprite;
        }
    }
    IEnumerator GetMap()
    {
        yield return new WaitUntil(() => theme != 0 && layout != 0);
        SetMapLayout(theme, layout);
    }
}
