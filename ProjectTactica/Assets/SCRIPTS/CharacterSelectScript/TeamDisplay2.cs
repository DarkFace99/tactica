﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TeamDisplay2 : MonoBehaviour
{
    public VerticalLayoutGroup layoutGroup;
    public TeamMemberDisplay baseTeamMemberDisplay;
    private CharacterSelectManager2 _characterSelectManager2;


    private Dictionary<string, TeamMemberDisplay> units;

    public void AddMember(CharacterSelectButton characterSelectButton)
    {
        TeamMemberDisplay newMemberDisplay = Instantiate(baseTeamMemberDisplay, layoutGroup.transform) as TeamMemberDisplay;
        newMemberDisplay.SetDisplay(characterSelectButton.unitSprite, characterSelectButton.unitName);
        units.Add(characterSelectButton.unitName, newMemberDisplay);
    }

    // Start is called before the first frame update
    void Start()
    {
        units = new Dictionary<string, TeamMemberDisplay>();
        _characterSelectManager2 = FindObjectOfType<CharacterSelectManager2>();
    }

    [PunRPC]
    void RPC_AddMember2(int i)
    {
        //picture.SetActive(true);
        _characterSelectManager2.rightSide[i].SetActive(true);

    }
    public void AddMember2()
    {
        for (int i = 0; i < _characterSelectManager2.playerSelect.Count; i++)
        {
            if (_characterSelectManager2.playerSelect[i] > 0 && _characterSelectManager2.player == 2)
            {
                GetComponent<PhotonView>().RPC("RPC_AddMember2", RpcTarget.Others, i);
                _characterSelectManager2.leftSide[i].SetActive(true);
            }
        }
    }
}