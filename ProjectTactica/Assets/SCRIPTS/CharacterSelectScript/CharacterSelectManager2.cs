﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using System;
//using System.IO;

public class CharacterSelectManager2 : MonoBehaviour
{
    //public Text TimeText;
    public CharacterStatDisplay CharacterStatDisplay;
    public TimeCounter timeCounter;

    public UnitSelectButton UnitSelectButton;
    public AllButton allButton;
    public int nextSceneIndex; 
    public List<int> playerSelect;
    public List<bool> playerSelecting;
    public List<bool> playerSelectAble;
    public List<GameObject> leftSide;
    public List<GameObject> rightSide;
    public List<GameObject> ChooseDisplay;
    public List<GameObject> allFilter;
    public List<GameObject> allUnitButtons;
    public List<GameObject> allUnitImage;
    public List<GameObject> leftSlot1;
    public List<GameObject> leftSlot2;
    public List<GameObject> leftSlot3;
    public List<GameObject> rightSlot1;
    public List<GameObject> rightSlot2;
    public List<GameObject> rightSlot3;
    public int player;
    public DataSelect DataSelect;
    public GameObject optionUI;
    public GameObject optionAlphaBack;
    public List<CharacterSelectButton> unitButton;
    [SerializeField]
    private Text youText;
    [SerializeField]
    private Text opponentText;
    public float delaytime;
    //private bool isSelecting = false;
    //==================== For Select all 6 Characters to prevent of select more than 6 Characters ====================
    public bool ableToSelect = true;
    public int selectRound = 1;
    private bool oneTime;
    public AudioSource clock;
    [SerializeField]
    private AudioSource chooseWisely;
    public GameObject[] panel;
    public GameObject[] panel2;

    private void Awake()
    {
        oneTime = true;
        if(PhotonNetwork.IsMasterClient)
        {
            player = 1;
        }
        else
        {
            player = 2;
        }
        if(player == 1)
        {
            ChooseDisplay[0].SetActive(true);
        }
        else
        {
            ChooseDisplay[1].SetActive(true);
        }
        SetName();
    }
    // Start is called before the first frame update
    void Start()
    {
        SoundController.Instance.audio[1].Play();
        SoundController.Instance.audio[0].Stop();
        optionAlphaBack.SetActive(false);
        allButton = GetComponent<AllButton>();
        StartCoroutine(WaitForStart());
        StartCoroutine(WaitForTransition());
        StartCoroutine(Disable());
        SetAvailableUnit();
    }
    void Update()
    {
        if(selectRound > 6 && oneTime)
        {
            oneTime = false;
            StartGame();
        }
        if ((player == 1 && selectRound % 2 != 0) || (player == 2 && selectRound % 2 == 0))
        {
            if(timeCounter.currentTime <= 6.0f && timeCounter.currentTime > 0.5f)
            {
                if(!clock.isPlaying)
                {
                    clock.Play();
                    //Debug.Log("Tikkkkkkkkkk");
                }
            }
            else if(timeCounter.currentTime <= 0.5f)
            {
                if (clock.isPlaying)
                {
                    //Debug.Log("Tikkkkkkkkkk2");
                    clock.Stop();
                }
            }
        }
    }
    public void StartGame()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        if(PhotonNetwork.IsMasterClient)
        {
            //Debug.Log("LOAD_LEVEL");
            PhotonNetwork.LoadLevel(nextSceneIndex);
        }
    }

    // put in the button
    [PunRPC]
    void RPC_SetRound()
    {
        selectRound++;
    }
    [PunRPC]
    void RPC_PlayerChoose()
    {
        if(selectRound % 2 != 0)
        {
            if(player == 1)
            { 
                if(selectRound !=  1)
                {
                    for (int i = 0; i < ChooseDisplay.Count; i++)
                    {
                        if (i == 0)
                        {
                            ChooseDisplay[i].SetActive(true);
                        }
                        else
                        {
                            ChooseDisplay[i].SetActive(false);
                        }
                    }
                }
               
            }
            else
            { 
                if(selectRound != 1)
                {
                    for (int i = 0; i < ChooseDisplay.Count; i++)
                    {
                        if (i == 1)
                        {
                            ChooseDisplay[i].SetActive(true);
                        }
                        else
                        {
                            ChooseDisplay[i].SetActive(false);
                        }
                    }
                }
            }
        }
        else
        {
            if (player == 1)
            {
                for (int i = 0; i < ChooseDisplay.Count; i++)
                {
                    if (i == 2)
                    {
                        ChooseDisplay[i].SetActive(true);
                    }
                    else
                    {
                        ChooseDisplay[i].SetActive(false);
                    }
                }
            }
            else
            {
                for (int i = 0; i < ChooseDisplay.Count; i++)
                {
                    if (i == 3 )
                    {
                        ChooseDisplay[i].SetActive(true);
                    }
                    else
                    {
                        ChooseDisplay[i].SetActive(false);
                    }
                }
            }
        }
    }
    public void SetRound()
    {
        if ((player == 1 && selectRound % 2 != 0) || (player == 2 && selectRound % 2 == 0))
        {
            GetComponent<PhotonView>().RPC("RPC_SetRound", RpcTarget.All);
            GetComponent<PhotonView>().RPC("RPC_PlayerChoose", RpcTarget.All);
        }
    }

    IEnumerator Disable()
    {
        yield return new WaitUntil(() => selectRound == 7);
        timeCounter.timeNumber.SetActive(false);
        ableToSelect = false;
    }
    public void AllFilter()
    {
        for(int i = 0; i < allUnitButtons.Count; i++)
        {
            allUnitButtons[i].SetActive(true);
        }
    }
    public void HumanFilter()
    {
        for (int i = 0; i < allUnitButtons.Count; i++)
        {
            if(i % 3 == 0)
            {
                allUnitButtons[i].SetActive(true);
            }
            else
            {
                allUnitButtons[i].SetActive(false);
            }
        }
    }
    public void ElfFilter()
    {
        for (int i = 0; i < allUnitButtons.Count; i++)
        {
            if (i % 3 == 2)
            {
                allUnitButtons[i].SetActive(true);
            }
            else
            {
                allUnitButtons[i].SetActive(false);
            }
        }
    }
    public void BeastFilter()
    {
        for (int i = 0; i < allUnitButtons.Count; i++)
        {
            if (i % 3 == 1)
            {
                allUnitButtons[i].SetActive(true);
            }
            else
            {
                allUnitButtons[i].SetActive(false);
            }
        }
    }
    void Waiting()
    {
        GetComponent<PhotonView>().RPC("RPC_Waiting", RpcTarget.All);
    }
    [PunRPC]
    void RPC_Waiting()
    {
        //ableToSelect = true;
        timeCounter.enabled = true;
        timeCounter.currentTime = timeCounter.startingTime;
    }
    IEnumerator WaitForStart()
    {
        yield return new WaitForSecondsRealtime(delaytime);
        Waiting();
    }
    void SetAvailableUnit()
    {
        if(PlayerPrefs.GetInt("UnlockAnna"+ DataLogin.Instance.IdIndex) == 1)
        {
            playerSelectAble[0] = true;
        }
        else
        {
            playerSelectAble[0] = false;
            Destroy(unitButton[0]);
            allUnitImage[0].GetComponent<Image>().color = new Color(0f, 0f, 0f, 100f / 255f);
        }
        if(PlayerPrefs.GetInt("UnlockCroc-Boi" + DataLogin.Instance.IdIndex) == 1)
        {
            playerSelectAble[1] = true;
        }
        else
        {
            playerSelectAble[1] = false;
            Destroy(unitButton[1]);
            allUnitImage[1].GetComponent<Image>().color = new Color(0f, 0f, 0f, 100f / 255f);
        }
        if (PlayerPrefs.GetInt("UnlockNeiga" + DataLogin.Instance.IdIndex) == 1)
        {
            playerSelectAble[2] = true;
        }
        else
        {
            playerSelectAble[2] = false;
            Destroy(unitButton[2]);
            allUnitImage[2].GetComponent<Image>().color = new Color(0f, 0f, 0f, 100f / 255f);
        }
        if (PlayerPrefs.GetInt("UnlockCharacter4" + DataLogin.Instance.IdIndex) == 1)
        {
            playerSelectAble[3] = true;
        }
        else
        {
            playerSelectAble[3] = false;
            Destroy(unitButton[3]);
            allUnitImage[3].GetComponent<Image>().color = new Color(0f, 0f, 0f, 100f / 255f);
        }
        if (PlayerPrefs.GetInt("UnlockCharacter5" + DataLogin.Instance.IdIndex) == 1)
        {
            playerSelectAble[4] = true;
        }
        else
        {
            playerSelectAble[4] = false;
            Destroy(unitButton[4]);
            allUnitImage[4].GetComponent<Image>().color = new Color(0f, 0f, 0f, 100f / 255f);
        }
        if (PlayerPrefs.GetInt("UnlockCharacter6" + DataLogin.Instance.IdIndex) == 1)
        {
            playerSelectAble[5] = true;
        }
        else
        {
            playerSelectAble[5] = false;
            Destroy(unitButton[5]);
            allUnitImage[5].GetComponent<Image>().color = new Color(0f, 0f, 0f, 100f / 255f);
        }
        if (PlayerPrefs.GetInt("UnlockCharacter7" + DataLogin.Instance.IdIndex) == 1)
        {
            playerSelectAble[6] = true;
        }
        else
        {
            playerSelectAble[6] = false;
            Destroy(unitButton[6]);
            allUnitImage[6].GetComponent<Image>().color = new Color(0f, 0f, 0f, 100f / 255f);
        }
        if (PlayerPrefs.GetInt("UnlockCharacter8" + DataLogin.Instance.IdIndex) == 1)
        {
            playerSelectAble[7] = true;
        }
        else
        {
            playerSelectAble[7] = false;
            Destroy(unitButton[7]);
            allUnitImage[7].GetComponent<Image>().color = new Color(0f, 0f, 0f, 100f / 255f);
        }
        if (PlayerPrefs.GetInt("UnlockCharacter9" + DataLogin.Instance.IdIndex) == 1)
        {
            playerSelectAble[8] = true;
        }
        else
        {
            playerSelectAble[8] = false;
            Destroy(unitButton[8]);
            allUnitImage[8].GetComponent<Image>().color = new Color(0f, 0f, 0f, 100f / 255f);
        }
    }
    void SetName()
    {
        youText.text = PhotonNetwork.NickName;
        GetComponent<PhotonView>().RPC("RPC_SetName", RpcTarget.Others, PhotonNetwork.NickName);
    }
    [PunRPC]
    void RPC_SetName(string name)
    {
        opponentText.text = name;
    }
    IEnumerator WaitForTransition()
    {
        yield return new WaitForSecondsRealtime(3.5f);
        chooseWisely.Play();
    }
}