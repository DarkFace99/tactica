﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterStatDisplay : MonoBehaviour
{
    public Image UnitImage;
    public Text UnitStat;
    public Text UnitHealth;
    public Text UnitSpeed;
    public Image UnitType;
    public Image UnitSkill;
    public Image UnitRace;
    public Image UnitTriangle;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetDisplay(Sprite picture,string status,string health,string speed,Sprite type,Sprite skill, Sprite race,Sprite tri)
    {
        UnitStat.enabled = true;
        UnitImage.enabled = true;
        UnitHealth.enabled = true;
        UnitSpeed.enabled = true;
        UnitType.enabled = true;
        UnitSkill.enabled = true;
        UnitRace.enabled = true;
        UnitImage.sprite = picture;
        UnitStat.text = status;
        UnitHealth.text = health;
        UnitSpeed.text = speed;
        UnitType.sprite = type;
        UnitSkill.sprite = skill;
        UnitRace.sprite = race;
        UnitTriangle.sprite = tri;
    } 
    public void NotDisplay(Sprite picture, string status)
    {
        UnitStat.enabled = false;
        UnitImage.enabled = false;
        UnitImage.sprite = picture;
        UnitStat.text = status;
    }
}
