﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class Char_GraphicSetting : MonoBehaviour
{
    [SerializeField]
    private AudioMixer audioMixer;
    public Resolution[] resolutions;
    public Dropdown resolutionDropdown;
    public Dropdown qualityDropdown;
    public Toggle yes;
    private bool isFull;
    [SerializeField]
    private int[] width;
    [SerializeField]
    private int[] height;
    [SerializeField]
    private Slider volume1;
    [SerializeField]
    private Slider volume2;
    [SerializeField]
    private Slider volume3;
    // Start is called before the first frame update
    private void Awake()
    {
        resolutions = Screen.resolutions;
        resolutionDropdown.ClearOptions();
        List<string> optionsList = new List<string>();
        int currentResolutionIndex = 0;

        for (int i = 0; i < width.Length; i++)
        {
            string option = width[i] + " X " + height[i];
            optionsList.Add(option);

            if ((resolutions[i].width == Screen.currentResolution.width) && (resolutions[i].height == Screen.currentResolution.height))
            {
                currentResolutionIndex = i;
            }
            
        }
        resolutionDropdown.AddOptions(optionsList);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();

        //if (PlayerPrefs.HasKey("Width") == false || PlayerPrefs.HasKey("Height") == false)
        //{
        //    PlayerPrefs.SetInt("Width", 1920);
        //    PlayerPrefs.SetInt("Height", 1080);
        //}
        //if (PlayerPrefs.HasKey("FullScreen") == false)
        //{
        //    PlayerPrefs.SetInt("FullScreen", 1);

        //}
        //if ((PlayerPrefs.HasKey("Width") || PlayerPrefs.HasKey("Height") && (PlayerPrefs.HasKey("FullScreen") == false)))
        //{
        //    Screen.SetResolution(PlayerPrefs.GetInt("Width"), PlayerPrefs.GetInt("Height"), !Screen.fullScreen);
        //}
        //if ((PlayerPrefs.HasKey("Width") || PlayerPrefs.HasKey("Height") && (PlayerPrefs.HasKey("FullScreen") == true)))
        //{
        //    Screen.SetResolution(PlayerPrefs.GetInt("Width"), PlayerPrefs.GetInt("Height"), Screen.fullScreen);
        //}
        if (PlayerPrefs.GetInt("FullScreen") == 0)
        {
            isFull = false;
        }
        else
        {
            isFull = true;
        }
        QualitySettings.SetQualityLevel(PlayerPrefs.GetInt("Quality"),true);
        if (PlayerPrefs.HasKey("master"))
        {
            PlayerPrefs.SetFloat("music", PlayerPrefs.GetFloat("master"));
        }
        else
        {
            PlayerPrefs.SetFloat("master", -10);
        }
        if (PlayerPrefs.HasKey("music"))
        {
            PlayerPrefs.SetFloat("music", PlayerPrefs.GetFloat("music"));
        }
        else
        {
            PlayerPrefs.SetFloat("music", -10);
        }
        if (PlayerPrefs.HasKey("sfx"))
        {
            PlayerPrefs.SetFloat("sfx", PlayerPrefs.GetFloat("sfx"));
        }
        else
        {
            PlayerPrefs.SetFloat("sfx", -10);
        }
    }
    void Start()
    {
        if (isFull)
        {
            yes.isOn = true;
        }
        else
        {
            yes.isOn = false;
        }
        volume1.value = PlayerPrefs.GetFloat("master");
        volume2.value = PlayerPrefs.GetFloat("music");
        volume3.value = PlayerPrefs.GetFloat("sfx");
        qualityDropdown.value = PlayerPrefs.GetInt("Quality");
    }

    public void SetResolution(int resolutionIndex)
    {
        if (resolutionIndex == 0)
        {
            Screen.SetResolution(1920, 1080, Screen.fullScreen);
            PlayerPrefs.SetInt("Height", 1080);
            PlayerPrefs.SetInt("Width", 1920);
        }
        //Resolution resolution = resolutions[resolutionIndex];
        else if (resolutionIndex == 1)
        {
            Screen.SetResolution(1600, 900, Screen.fullScreen);
            PlayerPrefs.SetInt("Height", 900);
            PlayerPrefs.SetInt("Width", 1600);
        }
        else if (resolutionIndex == 2)
        {
            Screen.SetResolution(1280, 720, Screen.fullScreen);
            PlayerPrefs.SetInt("Height", 720);
            PlayerPrefs.SetInt("Width", 1280);
        }
    }

    public void QualitySet(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex,true);
        PlayerPrefs.SetInt("Quality", qualityIndex);
    }

    public void FullScreenSet(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
    }
    public void SetVolumn(float volume)
    {
        audioMixer.SetFloat("volume", volume);
        PlayerPrefs.SetFloat("master", volume);
    }
    public void SetVolme2(float volume)
    {
        audioMixer.SetFloat("music", volume);
        PlayerPrefs.SetFloat("music", volume);
    }
    public void SetVolme3(float volume)
    {
        audioMixer.SetFloat("sfx", volume);
        PlayerPrefs.SetFloat("sfx", volume);
    }
}
