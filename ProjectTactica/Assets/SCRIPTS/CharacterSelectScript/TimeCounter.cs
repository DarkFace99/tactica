﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeCounter : MonoBehaviour
{
    public Text text;
    public float currentTime;
    public float startingTime;
    //private CharacterSelectManager _characterSelectManager;
    public CharacterSelectManager2 _characterSelectManager2;
    public int randomNumber;
    public GameObject timeNumber;
    [SerializeField]
    private List<int> numberPool;
    public Text leftPlayerText;
    public Text rightPlayerText;
   
    // Start is called before the first frame update
    void Start()
    {
        //currentTime = startingTime;
        SetNumberPool();
        //TextChangeForPlayer2();
    }
    // Update is called once per frame  
    void Update()
    {
        //Debug.Log(_characterSelectManager2.ableToSelect);
        if(currentTime < 0f)
        {
            currentTime = 0f;
        }
        //==================== Start countdown ====================
        if(_characterSelectManager2.ableToSelect == true)
        {
            currentTime -= Time.deltaTime;
        }
        text.text = currentTime.ToString("0");
        if(currentTime <= 0f && _characterSelectManager2.ableToSelect == true)
        {
            //==================== Random number to array ====================
            //randomNumber = Random.Range(0, 8);
            //Debug.Log("RandomNumber:" + randomNumber);
            ////==================== Checking that is not true to choose other ====================
            //while (true)
            //{
            //    for(int i = 0; i < 9; i++)
            //    {
            //        _characterSelectManager2.playerSelecting[i] = false;
            //    }
            //    if (_characterSelectManager2.playerSelect[randomNumber] != 0)
            //    {
            //        randomNumber = Random.Range(0, 8);
            //        Debug.Log("Re : " + randomNumber);
            //    }
            //    else
            //    {
            //        Debug.Log("RandomNuber:" + randomNumber + " Break");
            //        _characterSelectManager2.playerSelecting[randomNumber] = true;
            //        _characterSelectManager2.allButton.Select();
            //        break;
            //    }
            //}

            for (int i = 0; i < 9; i++)
            {
                randomNumber = GetRandomFromNumberpool();
                //Debug.Log("RandomNumber = " + randomNumber);
                if (_characterSelectManager2.playerSelect[randomNumber] == 0 && _characterSelectManager2.playerSelectAble[randomNumber] == true)
                {
                    //Debug.Log("Yay...." + numberPool.Count);
                    SetNumberPool();
                    break;
                }
            }
            for (int i = 0; i < 9; i++)
            {
                _characterSelectManager2.playerSelecting[i] = false;
            }
            _characterSelectManager2.playerSelecting[randomNumber] = true;
            _characterSelectManager2.allButton.unitNumber = randomNumber;
            _characterSelectManager2.allButton.Select();
            //==================== It is random and Add random character int the team ====================
            //==================== Make the currenttime == starting ====================
        }
    }
    [PunRPC]
    void RPC_Recountdown(float number)
    {
        currentTime = number;
    }
    public void ReCountdown()
    {
        GetComponent<PhotonView>().RPC("RPC_Recountdown", RpcTarget.All,startingTime);
    }
    void SetNumberPool()
    {
        numberPool.Clear();
        for(int i = 0; i < 9; i++)
        {
            numberPool.Add(i);
        }
    }
    int GetRandomFromNumberpool()
    {
        if (numberPool.Count <= 0)
        {

            // Debug.Log(numberPool.Count);
            return 0 ;
        }
        //int selectNumber = numberPool[Random.Range(0, 2)];
        int selectNumber = numberPool[Random.Range(0, numberPool.Count)];
        numberPool.Remove(selectNumber);
        return selectNumber;
    }
    //void TextChangeForPlayer2()
    //{
    //    if (_characterSelectManager2.player == 2)
    //    {
    //        leftPlayerText.text = "COMRADE";
    //        leftPlayerText.GetComponent<Text>().text = "COMRADE";
    //        rightPlayerText.text = "RIVAL";
    //        rightPlayerText.GetComponent<Text>().text = "RIVAL";
    //    }
    //}
}