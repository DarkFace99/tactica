﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    public static EventManager instance;

    public delegate void NoArgs();
    public static NoArgs onEndDrop;

    private void Awake()
    {
        if (instance != null)
            Destroy(instance);

        instance = this;
    }

    public void OnEndDrop()
    {
        if (onEndDrop != null)
            onEndDrop.Invoke();
    }


}
