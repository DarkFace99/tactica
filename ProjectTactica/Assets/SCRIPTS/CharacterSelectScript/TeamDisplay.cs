﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using System.IO;

public class TeamDisplay : MonoBehaviour
{
    //---------------------***** Here use for trnasform the Location *****---------------------
    public VerticalLayoutGroup layoutGroup;
    public TeamMemberDisplay baseTeamMemberDisplay;
    public GameObject teamMemberDisplay;
    private CharacterSelectManager2 _characterSelectManager2;


    private Dictionary<string, TeamMemberDisplay> units;

    public void AddMember(CharacterSelectButton characterSelectButton)
    {
        //-------------------------*****
        TeamMemberDisplay newMemberDisplay = Instantiate(baseTeamMemberDisplay, layoutGroup.transform) as TeamMemberDisplay;
        newMemberDisplay.SetDisplay(characterSelectButton.unitSprite, characterSelectButton.unitName);
        units.Add(characterSelectButton.unitName, newMemberDisplay);
    }
    // Start is called before the first frame update
    void Start()
    {
        units = new Dictionary<string, TeamMemberDisplay>();
        _characterSelectManager2 = FindObjectOfType<CharacterSelectManager2>();
       // GetComponent<PhotonView>().RPC("Test1",RpcTarget.All);
    }

    // Update is called once per frame
    //public void AddingMember(GameObject prefab,Vector3 vector3,CharacterSelectButton characterSelectButton)
    //{
        //TeamMemberDisplay obj = PhotonNetwork.Instantiate(Path.Combine("Prefab2", prefab.name), vector3, Quaternion.identity).GetComponent<TeamMemberDisplay>();
        //Debug.Log("obj name:" + obj);
        //obj.transform.parent = teamMemberDisplay.transform;
        //obj.SetDisplay(characterSelectButton.unitSprite, characterSelectButton.unitName);
        //units.Add(characterSelectButton.unitName, obj);
    //}
    [PunRPC]
    void RPC_AddMember(int i)
    {
        //picture.SetActive(true);
        _characterSelectManager2.rightSide[i].SetActive(true);
        
    }
    // For Swap side
    public void AddMember()
    {
        for(int i = 0; i < _characterSelectManager2.playerSelect.Count; i++)
        {
            if(_characterSelectManager2.playerSelect[i] > 0 && _characterSelectManager2.player == 1)
            {
                GetComponent<PhotonView>().RPC("RPC_AddMember", RpcTarget.Others,i);
                _characterSelectManager2.leftSide[i].SetActive(true);
            }
        }
    }
}