﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TeamMemberDisplay : MonoBehaviour
{
    public Image UnitImage;
    public Text UnitName;
    // Update is called once per frame
    public void SetDisplay(Sprite picture, string name)
    {
        UnitImage.sprite = picture;
        UnitName.text = name;
    }
}
