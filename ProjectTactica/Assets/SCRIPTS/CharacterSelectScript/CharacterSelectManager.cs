﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using System.IO;

public class CharacterSelectManager : MonoBehaviour
{
    public DataSelect dataSelect;
    public CharacterStatDisplay CharacterStatDisplay;
    public TeamDisplay teamdisplay;
    public TeamDisplay2 teamdisplay2;
    public List<GameObject> allCharacterStat;
    public UnitSelectButton UnitSelectButton;
    public TimeCounter timeCounter;
    public List<CharacterSelectButton> characterselectbutton;
    private int indexAllShader = 6;
    public List<bool> player1Select;
    public List<bool> player1Selectable;
    public List<bool> player2Selectable;
    public List<bool> player1Selecting;
    public List<bool> player2Select;
    public List<bool> player2Selecting;
    public List<GameObject> eachButtonShader;
    public List<GameObject> allShader;
    public GameObject BG;
    public int selectRound = 0;
    //private bool ableToSelect = true;
    private bool isFiltering = false;
    //private bool player1Select[];
    public bool isPlayer1turn;
    public int totalCharacter;
    //Keep info for player after select the character
    //public GameObject[,] keepInfo;

    public GameObject prefab;
    private GameObject test;
    private List<bool> SlotSelection;
    public Transform position;
    
    //==================== Another function use for random selecting character (with the reason that keep value to another script) ====================
    public void Select()
    {
        //object_list.Add()
        //for(int i = 0i < object_list.Count; i++)
        //{
        //    object_list[i].get
        //}
        //==================== This is player 1 turn ====================
        if (isPlayer1turn == true)
        {
            //==================== Each of these for availabe shader avoid raycast of button ====================
            if (player1Selecting[0] == true)
            {
                player1Selecting[0] = false;
                player2Selecting[0] = false;
                //BG.SetActive(false);
                //allCharacterStat[0].SetActive(false);
                player1Select[0] = true;
                //DataSelect.Instance.p1Select[0] = true;
                //test = Instantiate(prefab,position.transform.localPosition,Quaternion.identity);

            }
            if (player1Selecting[1] == true)
            {
                player1Selecting[1] = false;
                player2Selecting[1] = false;
                //BG.SetActive(false);
                //allCharacterStat[1].SetActive(false);
                player1Select[1] = true;
                //DataSelect.Instance.p1Select[1] = true;
            }
            if (player1Selecting[2] == true)
            {
                player1Selecting[2] = false;
                player2Selecting[2] = false;
                //BG.SetActive(false);
                //allCharacterStat[2].SetActive(false);
                player1Select[2] = true;
               // DataSelect.Instance.p1Select[2] = true;
            }
            if (player1Selecting[3] == true)
            {
                player1Selecting[3] = false;
                player2Selecting[3] = false;
                //BG.SetActive(false);
                //allCharacterStat[3].SetActive(false);
                player1Select[3] = true;
               // DataSelect.Instance.p1Select[3] = true;
                //Debug.Log("This is Round: " + selectRound + " Player1 Select Elf1 :" + player1SelectElf1);
            }
            if (player1Selecting[4] == true)
            {
                player1Selecting[4] = false;
                player2Selecting[4] = false;
                //BG.SetActive(false);
                //allCharacterStat[4].SetActive(false);
                player1Select[4] = true;
               // DataSelect.Instance.p1Select[4] = true;
            }
            if (player1Selecting[5] == true)
            {
                player1Selecting[5] = false;
                player2Selecting[5] = false;
                //BG.SetActive(false);
                //allCharacterStat[5].SetActive(false);
                player1Select[5] = true;
              //  DataSelect.Instance.p1Select[5] = true;
            }
            if (player1Selecting[6] == true)
            {
                player1Selecting[6] = false;
                player2Selecting[6] = false;
                //BG.SetActive(false);
                //allCharacterStat[6].SetActive(false);
                player1Select[6] = true;
               // DataSelect.Instance.p1Select[6] = true;
            }
            if (player1Selecting[7] == true)
            {
                player1Selecting[7] = false;
                player2Selecting[7] = false;
                //BG.SetActive(false);
                //allCharacterStat[7].SetActive(false);
                player1Select[7] = true;
               // DataSelect.Instance.p1Select[7] = true;
            }
            if (player1Selecting[8] == true)
            {
                player1Selecting[8] = false;
                player2Selecting[8] = false;
                //BG.SetActive(false);
                //allCharacterStat[8].SetActive(false);
                player1Select[8] = true;
               // DataSelect.Instance.p1Select[8] = true;
            }
        }
        //==================== This is player 2 turn ====================
        else
        {
            if (player2Selecting[0] == true)
            {
                player2Selecting[0] = false;
                player1Selecting[0] = false;
                //BG.SetActive(false);
                //allCharacterStat[0].SetActive(false);
                player2Select[0] = true;
               // DataSelect.Instance.p2Select[0] = true;
            }
            if (player2Selecting[1] == true)
            {
                player2Selecting[1] = false;
                player1Selecting[1] = false;
                //BG.SetActive(false);
                //allCharacterStat[1].SetActive(false);
                player2Select[1] = true;
               // DataSelect.Instance.p2Select[1] = true;
            }
            if (player2Selecting[2] == true)
            {
                player2Selecting[2] = false;
                player1Selecting[2] = false;
                //BG.SetActive(false);
                //allCharacterStat[2].SetActive(false);
                player2Select[2] = true;
               // DataSelect.Instance.p2Select[2] = true;
            }
            if (player2Selecting[3] == true)
            {
                player2Selecting[3] = false;
                player1Selecting[3] = false;
                //BG.SetActive(false);
                //allCharacterStat[3].SetActive(false);
                player2Select[3] = true;
               // DataSelect.Instance.p2Select[3] = true;
            }
            if (player2Selecting[4] == true)
            {
                player2Selecting[4] = false;
                player1Selecting[4] = false;
                //BG.SetActive(false);
                //allCharacterStat[4].SetActive(false);
                player2Select[4] = true;
               // DataSelect.Instance.p2Select[4] = true;
            }
            if (player2Selecting[5] == true)
            {
                player2Selecting[5] = false;
                player1Selecting[5] = false;
                //BG.SetActive(false);
                //allCharacterStat[5].SetActive(false);
                player2Select[5] = true;
                //DataSelect.Instance.p2Select[5] = true;
            }
            if (player2Selecting[6] == true)
            {
                player2Selecting[6] = false;
                player1Selecting[6] = false;
                //BG.SetActive(false);
                //allCharacterStat[6].SetActive(false);
                player2Select[6] = true;
               // DataSelect.Instance.p2Select[6] = true;
            }
            if (player2Selecting[7] == true)
            {
                player2Selecting[7] = false;
                player1Selecting[7] = false;
                //BG.SetActive(false);
                //allCharacterStat[7].SetActive(false);
                player2Select[7] = true;
                //DataSelect.Instance.p2Select[7] = true;
            }
            if (player2Selecting[8] == true)
            {
                player2Selecting[8] = false;
                player1Selecting[8] = false;
                //BG.SetActive(false);
                //allCharacterStat[8].SetActive(false);
                player2Select[8] = true;
                //DataSelect.Instance.p2Select[8] = true;
            }
        }
    }
    //==================== This function use for check that each turn that will be player 1 or player 2 ====================
    private void TurnCheck()
    {
        if (selectRound % 2 == 0)
        {
            isPlayer1turn = true;
            //SelectButton1.SetActive(true);
            //SelectButton2.SetActive(false);
        }
        else
        {
            isPlayer1turn = false;
            //SelectButton2.SetActive(true);
            //SelectButton1.SetActive(false);
        }
        if (selectRound > 5)
        {
            SceneManager.LoadScene("Fighting");
        }
    }
    //==================== Shade of button that use for raycast that close button ====================
    private void ShadeSelectCharacter()
    {
        if(isPlayer1turn == true)
        {
            for (int i = 0; i < player1Selecting.Count; i++)
            {
                eachButtonShader[i].SetActive(false);
            }
            for(int i = 0; i < player1Selecting.Count; i++)
            {
                if(player1Select[i] == true)
                {
                    eachButtonShader[i].SetActive(true);
                }
            }
        }
       else
       {
            for (int i = 0; i < player1Selecting.Count; i++)
            {
                eachButtonShader[i].SetActive(false);
            }
            for (int i = 0; i < player1Selecting.Count; i++)
            {
                if (player2Select[i] == true)
                {
                    eachButtonShader[i].SetActive(true);
                }
            }
        }
    }
    private void ShowSelectCharacter()
    {
        //for(int i = 0; i < player1Selecting.Count; i++)
        //{
        //    if(player1Select[i] == true)
        //    {
        //        if(selectRound == 0)
        //        {
                    
        //            choseCharacterSlot1[i].SetActive(true);
        //        }
        //        else if(selectRound == 2)
        //        {
        //            choseCharacterSlot3[i].SetActive(true);
        //        }
        //        else if (selectRound == 4)
        //        {
        //            choseCharacterSlot5[i].SetActive(true);
        //        }
        //    }
        //    if (player2Select[i] == true)
        //    {
        //        if (selectRound == 1)
        //        {
        //            choseCharacterSlot2[i].SetActive(true);
        //        }
        //        else if (selectRound == 3)
        //        {
        //            choseCharacterSlot4[i].SetActive(true);
        //        }
        //        else if (selectRound == 5)
        //        {
        //            choseCharacterSlot6[i].SetActive(true);
        //        }
        //    }
        //}
    }
    //==================== Just update function ====================
    void Update()
    {
        TurnCheck();
        ShadeSelectCharacter();
        ShowSelectCharacter();
        //Debug.Log("Now is Player1 turn: " + isPlayer1turn + "Player1 is selecting the elf1: " + player1IsSelectingElf1);
    }
    //==================== Human button 1 ====================
    public void Human1()
    {
        if (isPlayer1turn == true)
        {
            for (int i = 0; i < player1Selecting.Count; i++)
            {
                if (i == 0)
                {
                    player1Selecting[i] = true;
                }
                else
                {
                    player1Selecting[i] = false;
                }
            }
        }
        else
        {
            for (int i = 0; i < player1Selecting.Count; i++)
            {
                if (i == 0)
                {
                    player2Selecting[i] = true;
                }
                else
                {
                    player2Selecting[i] = false;
                }
            }
        }
        //if (player1Selecting[0] == true)
        //{
        //    for(int i = 0; i < player1Selecting.Count; i++)
        //    {
        //        if(i == 0)
        //        {
        //            allCharacterStat[i].SetActive(true);
        //        }
        //        else
        //        {
        //            allCharacterStat[i].SetActive(false);
        //        }
        //    }
        //    BG.SetActive(true);
        //}
        //if (player2Selecting[0] == true)
        //{
        //    for (int i = 0; i < player1Selecting.Count; i++)
        //    {
        //        if (i == 0)
        //        {
        //            allCharacterStat[i].SetActive(true);
        //        }
        //        else
        //        {
        //            allCharacterStat[i].SetActive(false);
        //        }
        //    }
        //    BG.SetActive(true);
        //}

    }
    //==================== Human button 2 ====================
    public void Human2()
    {
        if (isPlayer1turn == true)
        {
            for (int i = 0; i < player1Selecting.Count; i++)
            {
                if (i == 1)
                {
                    player1Selecting[i] = true;
                }
                else
                {
                    player1Selecting[i] = false;
                }
            }
        }
        else
        {
            for (int i = 0; i < player1Selecting.Count; i++)
            {
                if (i == 1)
                {
                    player2Selecting[i] = true;
                }
                else
                {
                    player2Selecting[i] = false;
                }
            }
        }
        //if (player1Selecting[1] == true)
        //{
        //    for (int i = 0; i < player1Selecting.Count; i++)
        //    {
        //        if (i == 1)
        //        {
        //            allCharacterStat[i].SetActive(true);
        //        }
        //        else
        //        {
        //            allCharacterStat[i].SetActive(false);
        //        }
        //    }
        //    BG.SetActive(true);
        //}
        //if (player2Selecting[1] == true)
        //{
        //    for (int i = 0; i < player1Selecting.Count; i++)
        //    {
        //        if (i == 1)
        //        {
        //            allCharacterStat[i].SetActive(true);
        //        }
        //        else
        //        {
        //            allCharacterStat[i].SetActive(false);
        //        }
        //    }
        //    BG.SetActive(true);
        //}
    }
    //==================== Human button 3 ====================
    public void Human3()
    {
        if (isPlayer1turn == true)
        {
            for (int i = 0; i < player1Selecting.Count; i++)
            {
                if (i == 2)
                {
                    player1Selecting[i] = true;
                }
                else
                {
                    player1Selecting[i] = false;
                }
            }
        }
        else
        {
            for (int i = 0; i < player1Selecting.Count; i++)
            {
                if (i == 2)
                {
                    player2Selecting[i] = true;
                }
                else
                {
                    player2Selecting[i] = false;
                }
            }
        }
        //if (player1Selecting[2] == true)
        //{
        //    for (int i = 0; i < player1Selecting.Count; i++)
        //    {
        //        if (i == 2)
        //        {
        //            allCharacterStat[i].SetActive(true);
        //        }
        //        else
        //        {
        //            allCharacterStat[i].SetActive(false);
        //        }
        //    }
        //    BG.SetActive(true);
        //}
        //if (player2Selecting[2] == true)
        //{
        //    for (int i = 0; i < player1Selecting.Count; i++)
        //    {
        //        if (i == 2)
        //        {
        //            allCharacterStat[i].SetActive(true);
        //        }
        //        else
        //        {
        //            allCharacterStat[i].SetActive(false);
        //        }
        //    }
        //    BG.SetActive(true);
        //}
    }
    //==================== Elf button 1 ====================
    public void Elf1()
    {
        if (isPlayer1turn == true)
        {
            for (int i = 0; i < player1Selecting.Count; i++)
            {
                if (i == 3)
                {
                    player1Selecting[i] = true;
                }
                else
                {
                    player1Selecting[i] = false;
                }
            }
        }
        else
        {
            for (int i = 0; i < player1Selecting.Count; i++)
            {
                if (i == 3)
                {
                    player2Selecting[i] = true;
                }
                else
                {
                    player2Selecting[i] = false;
                }
            }
        }
        //if (player1Selecting[3] == true)
        //{
        //    for (int i = 0; i < player1Selecting.Count; i++)
        //    {
        //        if (i == 3)
        //        {
        //            allCharacterStat[i].SetActive(true);
        //        }
        //        else
        //        {
        //            allCharacterStat[i].SetActive(false);
        //        }
        //    }
        //    BG.SetActive(true);
        //}
        //if (player2Selecting[3] == true)
        //{
        //    for (int i = 3; i < player1Selecting.Count; i++)
        //    {
        //        if (i == 3)
        //        {
        //            allCharacterStat[i].SetActive(true);
        //        }
        //        else
        //        {
        //            allCharacterStat[i].SetActive(false);
        //        }
        //    }
        //    BG.SetActive(true);
        //}
    }
    //==================== Elf button 2 ====================
    public void Elf2()
    {
        if (isPlayer1turn == true)
        {
            for (int i = 0; i < player1Selecting.Count; i++)
            {
                if (i == 4)
                {
                    player1Selecting[i] = true;
                }
                else
                {
                    player1Selecting[i] = false;
                }
            }
        }
        else
        {
            for (int i = 0; i < player1Selecting.Count; i++)
            {
                if (i == 4)
                {
                    player2Selecting[i] = true;
                }
                else
                {
                    player2Selecting[i] = false;
                }
            }
        }
        //if (player1Selecting[4] == true)
        //{
        //    for (int i = 0; i < player1Selecting.Count; i++)
        //    {
        //        if (i == 4)
        //        {
        //            allCharacterStat[i].SetActive(true);
        //        }
        //        else
        //        {
        //            allCharacterStat[i].SetActive(false);
        //        }
        //    }
        //    BG.SetActive(true);
        //}
        //if (player2Selecting[4] == true)
        //{
        //    for (int i = 0; i < player1Selecting.Count; i++)
        //    {
        //        if (i == 4)
        //        {
        //            allCharacterStat[i].SetActive(true);
        //        }
        //        else
        //        {
        //            allCharacterStat[i].SetActive(false);
        //        }
        //    }
        //    BG.SetActive(true);
        //}
    }
    //==================== Elf button 3 ====================
    public void Elf3()
    {
        if(isPlayer1turn == true)
        {
            for(int i = 0; i < player1Selecting.Count; i++)
            {
                if(i == 5)
                {
                    player1Selecting[i] = true;
                }
                else
                {
                    player1Selecting[i] = false;
                }
            }
        }
        else
        {
            for (int i = 0; i < player1Selecting.Count; i++)
            {
                if (i == 5)
                {
                    player2Selecting[i] = true;
                }
                else
                {
                    player2Selecting[i] = false;
                }
            }
        }
        //if (player1Selecting[5] == true)
        //{
        //    for (int i = 0; i < player1Selecting.Count; i++)
        //    {
        //        if (i == 5)
        //        {
        //            allCharacterStat[i].SetActive(true);
        //        }
        //        else
        //        {
        //            allCharacterStat[i].SetActive(false);
        //        }
        //    }
        //    BG.SetActive(true);
        //}
        //if (player2Selecting[5] == true)
        //{
        //    for (int i = 0; i < player1Selecting.Count; i++)
        //    {
        //        if (i == 5)
        //        {
        //            allCharacterStat[i].SetActive(true);
        //        }
        //        else
        //        {
        //            allCharacterStat[i].SetActive(false);
        //        }
        //    }
        //    BG.SetActive(true);
        //}
    }
    //==================== WereBeast button 1 ====================
    public void Werebeast1()
    {
        if (isPlayer1turn == true)
        {
            for (int i = 0; i < player1Selecting.Count; i++)
            {
                if (i == 6)
                {
                    player1Selecting[i] = true;
                }
                else
                {
                    player1Selecting[i] = false;
                }
            }
        }
        else
        {
            for (int i = 0; i < player1Selecting.Count; i++)
            {
                if (i == 6)
                {
                    player2Selecting[i] = true;
                }
                else
                {
                    player2Selecting[i] = false;
                }
            }
        }
        //if (player1Selecting[6] == true)
        //{
        //    for (int i = 0; i < player1Selecting.Count; i++)
        //    {
        //        if (i == 6)
        //        {
        //            allCharacterStat[i].SetActive(true);
        //        }
        //        else
        //        {
        //            allCharacterStat[i].SetActive(false);
        //        }
        //    }
        //    BG.SetActive(true);
        //}
        //if (player2Selecting[6] == true)
        //{
        //    for (int i = 0; i < player1Selecting.Count; i++)
        //    {
        //        if (i == 6)
        //        {
        //            allCharacterStat[i].SetActive(true);
        //        }
        //        else
        //        {
        //            allCharacterStat[i].SetActive(false);
        //        }
        //    }
        //    BG.SetActive(true);
        //}
    }
    //==================== WereBeast button 2 ====================
    public void Werebeast2()
    {
        if (isPlayer1turn == true)
        {
            for (int i = 0; i < player1Selecting.Count; i++)
            {
                if (i == 7)
                {
                    player1Selecting[i] = true;
                }
                else
                {
                    player1Selecting[i] = false;
                }
            }
        }
        else
        {
            for (int i = 0; i < player1Selecting.Count; i++)
            {
                if (i == 7)
                {
                    player2Selecting[i] = true;
                }
                else
                {
                    player2Selecting[i] = false;
                }
            }
        }
        //if (player1Selecting[7] == true)
        //{
        //    for (int i = 0; i < player1Selecting.Count; i++)
        //    {
        //        if (i == 7)
        //        {
        //            allCharacterStat[i].SetActive(true);
        //        }
        //        else
        //        {
        //            allCharacterStat[i].SetActive(false);
        //        }
        //    }
        //    BG.SetActive(true);
        //}
        //if (player2Selecting[7] == true)
        //{
        //    for (int i = 0; i < player1Selecting.Count; i++)
        //    {
        //        if (i == 7)
        //        {
        //            allCharacterStat[i].SetActive(true);
        //        }
        //        else
        //        {
        //            allCharacterStat[i].SetActive(false);
        //        }
        //    }
        //    BG.SetActive(true);
        //}
    }
    //==================== WereBeast button 3 ====================
    public void Werebeast3()
    {
        if (isPlayer1turn == true)
        {
            for (int i = 0; i < player1Selecting.Count; i++)
            {
                if (i == 8)
                {
                    player1Selecting[i] = true;
                }
                else
                {
                    player1Selecting[i] = false;
                }
            }
        }
        else
        {
            for (int i = 0; i < player1Selecting.Count; i++)
            {
                if (i == 8)
                {
                    player2Selecting[i] = true;
                }
                else
                {
                    player2Selecting[i] = false;
                }
            }
        }
        //if (player1Selecting[8] == true)
        //{
        //    for (int i = 0; i < player1Selecting.Count; i++)
        //    {
        //        if (i == 8)
        //        {
        //            allCharacterStat[i].SetActive(true);
        //        }
        //        else
        //        {
        //            allCharacterStat[i].SetActive(false);
        //        }
        //    }
        //    BG.SetActive(true);
        //}
        //if (player2Selecting[8] == true)
        //{
        //    for (int i = 0; i < player1Selecting.Count; i++)
        //    {
        //        if (i == 8)
        //        {
        //            allCharacterStat[i].SetActive(true);
        //        }
        //        else
        //        {
        //            allCharacterStat[i].SetActive(false);
        //        }
        //    }
        //    BG.SetActive(true);
        //}
    }
    //==================== HumanFilter button ====================
    public void HumanFilter()
    {
        isFiltering = !isFiltering;
        Debug.Log("isFiltering :" + isFiltering);
        //BG.SetActive(false);
        if (isFiltering == false)
        {
            for (int i = 0; i < indexAllShader; i++)
            {
                allShader[i].SetActive(false);
            }
        }
        else
        {
            for (int i = 0; i < indexAllShader; i++)
            {
                if (i == 1 || i == 2 || i == 4 || i == 5)
                {
                    allShader[i].SetActive(true);
                }
                else
                {
                    allShader[i].SetActive(false);
                }
                player1Selecting[i] = false;
                player2Selecting[i] = false;
            }
        }
    }
    //==================== ElfFilter button ====================
    public void ElfFilter()
    {
        isFiltering = !isFiltering;
        Debug.Log("isFiltering: " + isFiltering);
        //BG.SetActive(false);
        if (isFiltering == true)
        {
            for (int i = 0; i < indexAllShader; i++)
            {
                allShader[i].SetActive(false);
            }
        }
        else
        {
            for (int i = 0; i < indexAllShader; i++)
            {
                if (i == 0 || i == 2 || i == 3 || i == 5)
                {
                    allShader[i].SetActive(true);
                }
                else
                {
                    allShader[i].SetActive(false);
                }
                player1Selecting[i] = false;
                player2Selecting[i] = false;
            }
        }
    }
    //==================== WereBeastFilter Button ====================
    public void WerebeastFilter()
    {
        isFiltering = !isFiltering;
        Debug.Log("isFiltering :" + isFiltering);
        //BG.SetActive(false);
        if (isFiltering == true)
        {
            for (int i = 0; i < indexAllShader; i++)
            {
                allShader[i].SetActive(false);
            }
        }
        else
        {
            for (int i = 0; i < indexAllShader; i++)
            {
                if (i == 1 || i == 0 || i == 4 || i == 3)
                {
                    allShader[i].SetActive(true);
                }
                else
                {
                    allShader[i].SetActive(false);
                }
                player1Selecting[i] = false;
                player2Selecting[i] = false;
            }
        }
    }
}
