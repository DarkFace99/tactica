﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class AllButton : MonoBehaviour
{
    private CharacterSelectManager2 _characterSelectManager2;
    public int unitNumber;
    public GameObject lowerFrame;
    public GameObject selectButton;
    public Sprite availableButton;
    public Sprite notAvailableButton;
    public Text buttonText;
    [SerializeField]
    private ColorBing colorBing;
    private bool canClick;
    [SerializeField]
    private Color clickedColor;
    // Start is called before the first frame update
    void Start()
    {
        _characterSelectManager2 = GetComponent<CharacterSelectManager2>();
        StartCoroutine(WaitForCanclick());
    }
    void Update()
    {
        CanclickCheck();
    }
    public void Human1()
    {
        if(_characterSelectManager2.playerSelectAble[0] == true)
        {
            for (int i = 0; i < _characterSelectManager2.playerSelect.Count; i++)
            {
                if (i == 0 && _characterSelectManager2.playerSelectAble[i] == true)
                {
                    if (_characterSelectManager2.playerSelectAble[i] == true)
                    {
                        _characterSelectManager2.playerSelecting[i] = true;
                        unitNumber = i;
                        lowerFrame.SetActive(true);
                    }
                    else
                    {
                        _characterSelectManager2.playerSelecting[i] = false;
                    }
                }
                else
                {
                    _characterSelectManager2.playerSelecting[i] = false;
                }
            }
        }
    }
    public void Human2()
    {
        if (_characterSelectManager2.playerSelectAble[3] == true)
        {
            for (int i = 0; i < _characterSelectManager2.playerSelect.Count; i++)
            {
                if (i == 3 && _characterSelectManager2.playerSelectAble[i] == true)
                {
                    if (_characterSelectManager2.playerSelectAble[i] == true)
                    {
                        _characterSelectManager2.playerSelecting[i] = true;
                        unitNumber = i;
                        lowerFrame.SetActive(true);

                    }
                    else
                    {
                        _characterSelectManager2.playerSelecting[i] = false;
                    }
                }
                else
                {
                    _characterSelectManager2.playerSelecting[i] = false;
                }
            }
        }
    }
    public void Human3()
    {
        if(_characterSelectManager2.playerSelectAble[6] == true)
        {
            for (int i = 0; i < _characterSelectManager2.playerSelect.Count; i++)
            {
                if (i == 6 && _characterSelectManager2.playerSelectAble[i] == true)
                {
                    if (_characterSelectManager2.playerSelectAble[i] == true)
                    {
                        _characterSelectManager2.playerSelecting[i] = true;
                        unitNumber = i;
                        lowerFrame.SetActive(true);
                    }
                    else
                    {
                        _characterSelectManager2.playerSelecting[i] = false;
                    }
                }
                else
                {
                    _characterSelectManager2.playerSelecting[i] = false;
                }
            }
        }
    }
    public void Human4()
    {
        if (_characterSelectManager2.playerSelectAble[9] == true)
        {
            for (int i = 0; i < _characterSelectManager2.playerSelect.Count; i++)
            {
                if (i == 9 && _characterSelectManager2.playerSelectAble[i] == true)
                {
                    if (_characterSelectManager2.playerSelectAble[i] == true)
                    {
                        _characterSelectManager2.playerSelecting[i] = true;
                        unitNumber = i;
                        lowerFrame.SetActive(true);
                    }
                    else
                    {
                        _characterSelectManager2.playerSelecting[i] = false;
                    }
                }
                else
                {
                    _characterSelectManager2.playerSelecting[i] = false;
                }
            }
        }
    }
    public void Human5()
    {
        if (_characterSelectManager2.playerSelectAble[12] == true)
        {
            for (int i = 0; i < _characterSelectManager2.playerSelect.Count; i++)
            {
                if (i == 12 && _characterSelectManager2.playerSelectAble[i] == true)
                {
                    if (_characterSelectManager2.playerSelectAble[i] == true)
                    {
                        _characterSelectManager2.playerSelecting[i] = true;
                        unitNumber = i;
                        lowerFrame.SetActive(true);
                    }
                    else
                    {
                        _characterSelectManager2.playerSelecting[i] = false;
                    }
                }
                else
                {
                    _characterSelectManager2.playerSelecting[i] = false;
                }
            }
        }
    }
    public void Human6()
    {
        if (_characterSelectManager2.playerSelectAble[15] == true)
        {
            for (int i = 0; i < _characterSelectManager2.playerSelect.Count; i++)
            {
                if (i == 15 && _characterSelectManager2.playerSelectAble[i] == true)
                {
                    if (_characterSelectManager2.playerSelectAble[i] == true)
                    {
                        _characterSelectManager2.playerSelecting[i] = true;
                        unitNumber = i;
                        lowerFrame.SetActive(true);
                    }
                    else
                    {
                        _characterSelectManager2.playerSelecting[i] = false;
                    }
                }
                else
                {
                    _characterSelectManager2.playerSelecting[i] = false;
                }
            }
        }
    }
    public void Elf1()
    {
        if (_characterSelectManager2.playerSelectAble[2] == true)
        {
            for (int i = 0; i < _characterSelectManager2.playerSelect.Count; i++)
            {
                if (i == 2 && _characterSelectManager2.playerSelectAble[i] == true)
                {
                    if (_characterSelectManager2.playerSelectAble[i] == true)
                    {
                        _characterSelectManager2.playerSelecting[i] = true;
                        unitNumber = i;
                        lowerFrame.SetActive(true);
                    }
                    else
                    {
                        _characterSelectManager2.playerSelecting[i] = false;
                    }
                }
                else
                {
                    _characterSelectManager2.playerSelecting[i] = false;
                }
            }
        }
    }
    public void Elf2()
    {
        if (_characterSelectManager2.playerSelectAble[5] == true)
        {
            for (int i = 0; i < _characterSelectManager2.playerSelect.Count; i++)
            {
                if (i == 5 && _characterSelectManager2.playerSelectAble[i] == true)
                {
                    if (_characterSelectManager2.playerSelectAble[i] == true)
                    {
                        _characterSelectManager2.playerSelecting[i] = true;
                        unitNumber = i;
                        lowerFrame.SetActive(true);
                    }
                    else
                    {
                        _characterSelectManager2.playerSelecting[i] = false;
                    }
                }
                else
                {
                    _characterSelectManager2.playerSelecting[i] = false;
                }
            }
        }
    }
    public void Elf3()
    {
        if (_characterSelectManager2.playerSelectAble[8] == true)
        {
            for (int i = 0; i < _characterSelectManager2.playerSelect.Count; i++)
            {
                if (i == 8 && _characterSelectManager2.playerSelectAble[i] == true)
                {
                    if (_characterSelectManager2.playerSelectAble[i] == true)
                    {
                        _characterSelectManager2.playerSelecting[i] = true;
                        unitNumber = i;
                        lowerFrame.SetActive(true);
                    }
                    else
                    {
                        _characterSelectManager2.playerSelecting[i] = false;
                    }
                }
                else
                {
                    _characterSelectManager2.playerSelecting[i] = false;
                }
            }
        }
    }
    public void Elf4()
    {
        if (_characterSelectManager2.playerSelectAble[11] == true)
        {
            for (int i = 0; i < _characterSelectManager2.playerSelect.Count; i++)
            {
                if (i == 11 && _characterSelectManager2.playerSelectAble[i] == true)
                {
                    if (_characterSelectManager2.playerSelectAble[i] == true)
                    {
                        _characterSelectManager2.playerSelecting[i] = true;
                        unitNumber = i;
                        lowerFrame.SetActive(true);
                    }
                    else
                    {
                        _characterSelectManager2.playerSelecting[i] = false;
                    }
                }
                else
                {
                    _characterSelectManager2.playerSelecting[i] = false;
                }
            }
        }
    }
    public void Elf5()
    {
        if (_characterSelectManager2.playerSelectAble[14] == true)
        {
            for (int i = 0; i < _characterSelectManager2.playerSelect.Count; i++)
            {
                if (i == 14 && _characterSelectManager2.playerSelectAble[i] == true)
                {
                    if (_characterSelectManager2.playerSelectAble[i] == true)
                    {
                        _characterSelectManager2.playerSelecting[i] = true;
                        unitNumber = i;
                        lowerFrame.SetActive(true);
                    }
                    else
                    {
                        _characterSelectManager2.playerSelecting[i] = false;
                    }
                }
                else
                {
                    _characterSelectManager2.playerSelecting[i] = false;
                }
            }
        }
    }
    public void Elf6()
    {
        if (_characterSelectManager2.playerSelectAble[17] == true)
        {
            for (int i = 0; i < _characterSelectManager2.playerSelect.Count; i++)
            {
                if (i == 17 && _characterSelectManager2.playerSelectAble[i] == true)
                {
                    if (_characterSelectManager2.playerSelectAble[i] == true)
                    {
                        _characterSelectManager2.playerSelecting[i] = true;
                        unitNumber = i;
                        lowerFrame.SetActive(true);
                    }
                    else
                    {
                        _characterSelectManager2.playerSelecting[i] = false;
                    }
                }
                else
                {
                    _characterSelectManager2.playerSelecting[i] = false;
                }
            }
        }
    }
    public void Beast1()
    { 
        if (_characterSelectManager2.playerSelectAble[1] == true)
        {
            for (int i = 0; i < _characterSelectManager2.playerSelect.Count; i++)
            {
                if (i == 1 && _characterSelectManager2.playerSelectAble[i] == true)
                {
                    if (_characterSelectManager2.playerSelectAble[i] == true)
                    {
                        _characterSelectManager2.playerSelecting[i] = true;
                        unitNumber = i;
                        lowerFrame.SetActive(true);
                    }
                    else
                    {
                        _characterSelectManager2.playerSelecting[i] = false;
                    }
                }
                else
                {
                    _characterSelectManager2.playerSelecting[i] = false;
                }
            }
        }
    }
    public void Beast2()
    {
        if (_characterSelectManager2.playerSelectAble[4] == true)
        {
            for (int i = 0; i < _characterSelectManager2.playerSelect.Count; i++)
            {
                if (i == 4 && _characterSelectManager2.playerSelectAble[i] == true)
                {
                    if (_characterSelectManager2.playerSelectAble[i] == true)
                    {
                        _characterSelectManager2.playerSelecting[i] = true;
                        unitNumber = i;
                        lowerFrame.SetActive(true);
                    }
                    else
                    {
                        _characterSelectManager2.playerSelecting[i] = false;
                    }
                }
                else
                {
                    _characterSelectManager2.playerSelecting[i] = false;
                }
            }
        }
    }
    public void Beast3()
    {
        if (_characterSelectManager2.playerSelectAble[7] == true)
        {
            for (int i = 0; i < _characterSelectManager2.playerSelect.Count; i++)
            {
                if (i == 7 && _characterSelectManager2.playerSelectAble[i] == true)
                {
                    if (_characterSelectManager2.playerSelectAble[i] == true)
                    {
                        _characterSelectManager2.playerSelecting[i] = true;
                        unitNumber = i;
                        lowerFrame.SetActive(true);
                    }
                    else
                    {
                        _characterSelectManager2.playerSelecting[i] = false;
                    }
                }
                else
                {
                    _characterSelectManager2.playerSelecting[i] = false;
                }
            }
        }
    }
    public void Beast4()
    {
        if (_characterSelectManager2.playerSelectAble[10] == true)
        {
            for (int i = 0; i < _characterSelectManager2.playerSelect.Count; i++)
            {
                if (i == 10 && _characterSelectManager2.playerSelectAble[i] == true)
                {
                    if (_characterSelectManager2.playerSelectAble[i] == true)
                    {
                        _characterSelectManager2.playerSelecting[i] = true;
                        unitNumber = i;
                        lowerFrame.SetActive(true);
                    }
                    else
                    {
                        _characterSelectManager2.playerSelecting[i] = false;
                    }
                }
                else
                {
                    _characterSelectManager2.playerSelecting[i] = false;
                }
            }
        }
    }
    public void Beast5()
    {
        if (_characterSelectManager2.playerSelectAble[13] == true)
        {
            for (int i = 0; i < _characterSelectManager2.playerSelect.Count; i++)
            {
                if (i == 13 && _characterSelectManager2.playerSelectAble[i] == true)
                {
                    if (_characterSelectManager2.playerSelectAble[i] == true)
                    {
                        _characterSelectManager2.playerSelecting[i] = true;
                        unitNumber = i;
                        lowerFrame.SetActive(true);
                    }
                    else
                    {
                        _characterSelectManager2.playerSelecting[i] = false;
                    }
                }
                else
                {
                    _characterSelectManager2.playerSelecting[i] = false;
                }
            }
        }
    }
    public void Beast6()
    {
        if (_characterSelectManager2.playerSelectAble[16] == true)
        {
            for (int i = 0; i < _characterSelectManager2.playerSelect.Count; i++)
            {
                if (i == 16 && _characterSelectManager2.playerSelectAble[i] == true)
                {
                    if (_characterSelectManager2.playerSelectAble[i] == true)
                    {
                        _characterSelectManager2.playerSelecting[i] = true;
                        unitNumber = i;
                        lowerFrame.SetActive(true);
                    }
                    else
                    {
                        _characterSelectManager2.playerSelecting[i] = false;
                    }
                }
                else
                {
                    _characterSelectManager2.playerSelecting[i] = false;
                }
            }
        }
    }
    public void Select()
    {
        if(canClick)
        {
            if ((_characterSelectManager2.player == 1 && _characterSelectManager2.selectRound % 2 != 0) || (_characterSelectManager2.player == 2 && _characterSelectManager2.selectRound % 2 == 0))
            {
                if(_characterSelectManager2.selectRound == 1|| _characterSelectManager2.selectRound == 2)
                {
                    for (int i = 0; i < _characterSelectManager2.playerSelecting.Count; i++)
                    {
                        selectButton.GetComponent<Image>().sprite = availableButton;
                        if (_characterSelectManager2.playerSelecting[i] == true && _characterSelectManager2.playerSelectAble[i] == true)
                        {
                            _characterSelectManager2.playerSelecting[i] = false;
                            _characterSelectManager2.playerSelect[i]++;
                            _characterSelectManager2.leftSlot1[i].SetActive(true);
                            Slot1();
                            //_characterSelectManager2.DataSelect.playerChoose[i]++;
                            _characterSelectManager2.playerSelectAble[i] = false;
                            _characterSelectManager2.timeCounter.ReCountdown();
                            _characterSelectManager2.SetRound();
                            DataSelect.Instance.playerChoose.Add(unitNumber);
                            _characterSelectManager2.allUnitButtons[i].GetComponent<Image>().color = new Color(0.3f, 0.3f, 0.3f, 1f);
                            _characterSelectManager2.allUnitImage[i].GetComponent<Image>().color = new Color(0.3f, 0.3f, 0.3f, 1f);
                            Destroy(_characterSelectManager2.unitButton[i]);
                            if (_characterSelectManager2.clock.isPlaying)
                            {
                                Debug.Log("Tikkkkkkkkkk2");
                                _characterSelectManager2.clock.Stop();
                            }
                        }
                    }
                }
                if (_characterSelectManager2.selectRound == 3 || _characterSelectManager2.selectRound == 4)
                {
                    for (int i = 0; i < _characterSelectManager2.playerSelecting.Count; i++)
                    {
                        selectButton.GetComponent<Image>().sprite = availableButton;
                        if (_characterSelectManager2.playerSelecting[i] == true && _characterSelectManager2.playerSelectAble[i] == true)
                        {
                            _characterSelectManager2.playerSelecting[i] = false;
                            _characterSelectManager2.playerSelect[i]++;
                            _characterSelectManager2.leftSlot2[i].SetActive(true);
                            Slot2();
                            //_characterSelectManager2.DataSelect.playerChoose[i]++;
                            _characterSelectManager2.playerSelectAble[i] = false;
                            _characterSelectManager2.timeCounter.ReCountdown();
                            _characterSelectManager2.SetRound();
                            //_characterSelectManager2.teamdisplay.AddMember();
                            //_characterSelectManager2.teamdisplay2.AddMember2();
                            DataSelect.Instance.playerChoose.Add(unitNumber);
                            _characterSelectManager2.allUnitButtons[i].GetComponent<Image>().color = new Color(0.3f, 0.3f, 0.3f, 1f);
                            _characterSelectManager2.allUnitImage[i].GetComponent<Image>().color = new Color(0.3f, 0.3f, 0.3f, 1f);
                            Destroy(_characterSelectManager2.unitButton[i]);
                        }
                        if (_characterSelectManager2.clock.isPlaying)
                        {
                            Debug.Log("Tikkkkkkkkkk2");
                            _characterSelectManager2.clock.Stop();
                        }
                    }
                }
                if (_characterSelectManager2.selectRound == 5 || _characterSelectManager2.selectRound == 6)
                {
                    for (int i = 0; i < _characterSelectManager2.playerSelecting.Count; i++)
                    {
                        selectButton.GetComponent<Image>().sprite = availableButton;
                        if (_characterSelectManager2.playerSelecting[i] == true && _characterSelectManager2.playerSelectAble[i] == true)
                        {
                            _characterSelectManager2.playerSelecting[i] = false;
                            _characterSelectManager2.playerSelect[i]++;
                            _characterSelectManager2.leftSlot3[i].SetActive(true);
                            Slot3();
                            //_characterSelectManager2.DataSelect.playerChoose[i]++;
                            _characterSelectManager2.playerSelectAble[i] = false;
                            _characterSelectManager2.timeCounter.ReCountdown();
                            _characterSelectManager2.SetRound();
                            //_characterSelectManager2.teamdisplay.AddMember();
                            //_characterSelectManager2.teamdisplay2.AddMember2();
                            DataSelect.Instance.playerChoose.Add(unitNumber);
                            _characterSelectManager2.allUnitButtons[i].GetComponent<Image>().color = new Color(0.3f, 0.3f, 0.3f, 1f);
                            _characterSelectManager2.allUnitImage[i].GetComponent<Image>().color = new Color(0.3f, 0.3f, 0.3f, 1f);
                            Destroy(_characterSelectManager2.unitButton[i]);
                        }
                    }
                    if (_characterSelectManager2.clock.isPlaying)
                    {
                        Debug.Log("Tikkkkkkkkkk2");
                        _characterSelectManager2.clock.Stop();
                    }
                }

            }
            if ((_characterSelectManager2.player == 1 && !IsP1Turn()) || (_characterSelectManager2.player == 2 && IsP1Turn()))
            {
                //selectButton.GetComponent<Image>().color = new Color(0f, 0f, 0f, 100f / 255f);
                selectButton.GetComponent<Image>().sprite = notAvailableButton;
            }
        }
    }
    void Slot1()
    {
        GetComponent<PhotonView>().RPC("RPC_Slot1", RpcTarget.Others, unitNumber);
    }
    [PunRPC]
    void RPC_Slot1(int i)
    {
        _characterSelectManager2.rightSlot1[i].SetActive(true);
    }
    void Slot2()
    {
        GetComponent<PhotonView>().RPC("RPC_Slot2", RpcTarget.Others, unitNumber);
    }
    [PunRPC]
    void RPC_Slot2(int i)
    {
        _characterSelectManager2.rightSlot2[i].SetActive(true);
    }
    void Slot3()
    {
        GetComponent<PhotonView>().RPC("RPC_Slot3", RpcTarget.Others, unitNumber);
    }
    [PunRPC]
    void RPC_Slot3(int i)
    {
        _characterSelectManager2.rightSlot3[i].SetActive(true);
    }
    public void Option()
    {
        _characterSelectManager2.optionUI.SetActive(true);
        _characterSelectManager2.optionAlphaBack.SetActive(true);

    }
    public void ClosedOption()
    {
        _characterSelectManager2.optionUI.SetActive(false);
        _characterSelectManager2.optionAlphaBack.SetActive(false);
    }
    bool IsP1Turn()
    {
        if(_characterSelectManager2.selectRound % 2 != 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    void CanclickCheck()
    {
        if(_characterSelectManager2.player == 1 && _characterSelectManager2.selectRound % 2 != 0)
        {
            for(int i = 0; i < _characterSelectManager2.playerSelecting.Count; i++)
            {
                if (_characterSelectManager2.playerSelecting[i] == true)
                {
                    if (_characterSelectManager2.playerSelect[i] == 0)
                    {
                        canClick = true;
                        selectButton.GetComponent<Image>().sprite = availableButton;
                        buttonText.color = new Color(1f, 1f, 1f, 1f);
                    }
                    else
                    {
                        canClick = false;
                        selectButton.GetComponent<Image>().sprite = notAvailableButton;
                        buttonText.color = clickedColor;
                    }
                }
                    
            }

        }
        else if(_characterSelectManager2.player == 2 && _characterSelectManager2.selectRound % 2 == 0)
        {
            for (int i = 0; i < _characterSelectManager2.playerSelecting.Count; i++)
            {
                if(_characterSelectManager2.playerSelecting[i] == true)
                {
                    if (_characterSelectManager2.playerSelect[i] == 0)
                    {
                        canClick = true;
                        selectButton.GetComponent<Image>().sprite = availableButton;
                        buttonText.color = new Color(1f, 1f, 1f, 1f);
                    }
                    else
                    {
                        canClick = false;
                        selectButton.GetComponent<Image>().sprite = notAvailableButton;
                        buttonText.color = clickedColor;
                    }
                }
            }
        }
        else
        {
            canClick = false;
            selectButton.GetComponent<Image>().sprite = notAvailableButton;
            buttonText.color = clickedColor;
        }
    }
    IEnumerator WaitForCanclick()
    {
        if(!canClick)
        {
            yield return new WaitUntil(() => canClick);
            colorBing.isBing = canClick;
        }
        else
        {
            yield return new WaitUntil(() => !canClick);
        }
        StartCoroutine(WaitForCanclick());
    }
}