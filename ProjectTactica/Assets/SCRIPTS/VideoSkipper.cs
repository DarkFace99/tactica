﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VideoSkipper : MonoBehaviour
{
    [SerializeField]
    private VideoPlayer player;
    [SerializeField]
    private RawImage raw;
    [SerializeField]
    private Image barImage;
    [SerializeField]
    private float DecreaseAmount;
    [SerializeField]
    private float SkipAmount;
    [SerializeField]
    private float TotalAmount;
    [SerializeField]
    private GameObject Bar;
    [SerializeField]
    private GameObject obj;
    private bool isAnyKey = false;
    private float time = 0.0f;
    [SerializeField]
    private float videoLength;
    [SerializeField]
    private string sceneName;
    void Start()
    {
        StartCoroutine(playVideo());
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            obj.SetActive(true);
            SkipAmount += DecreaseAmount * Time.deltaTime;
        }
        else if(!Input.GetKey(KeyCode.Space) && SkipAmount >= 0.0f)
        {
            SkipAmount -= DecreaseAmount * Time.deltaTime;
        }
        //if(SkipAmount <= 0.0f)
        //{
        //    obj.SetActive(false);
        //}
        if (Input.anyKeyDown && SkipAmount <= 0.0f)
        {
            isAnyKey = true;
            StartCoroutine(AppearText());
        }
        else if(!Input.anyKeyDown && SkipAmount <= 0.0f && !isAnyKey)
        {
            obj.SetActive(false);
        }
        //else
        //{
        //    obj.SetActive(true);
        //}
        barImage.fillAmount = GetSneakNormailized();
        if(barImage.fillAmount == 1)
        {
            //if (PlayerPrefs.HasKey("User" + 0) || PlayerPrefs.HasKey("User" + 1) || PlayerPrefs.HasKey("User" + 2))
            //{
                SceneManager.LoadScene(sceneName);
            //}
            //else
            //{
            //    SceneManager.LoadScene("VideoGameplay");
            //}
            
        }
        
        
    }
    IEnumerator playVideo()
    {
        player.Prepare();
        WaitForSeconds wait = new WaitForSeconds(0.5f);
        while (!player.isPrepared)
        {
            yield return wait;
            break;
        }
        raw.enabled = true;
        raw.texture = player.texture;
        player.Play();
        StartCoroutine(ChangeScene());
    }
    private float GetSneakNormailized()
    {
        return SkipAmount / TotalAmount;
    }
    IEnumerator ChangeScene()
    {
        yield return new WaitForSeconds(videoLength);
        SceneManager.LoadScene("PressAnyKey");
    }
    IEnumerator AppearText()
    {
        obj.SetActive(true);
        yield return new WaitForSecondsRealtime(3f);
        obj.SetActive(false);
        isAnyKey = false;
        //oneTime = false;
        //Bar.SetActive(false);
    }
}
