﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadePanel : MonoBehaviour
{
    public bool isFade;
    private SpriteRenderer sr;
    private float alpha;
    [SerializeField]
    private float maxAlpha;
    [SerializeField]
    private float fadeAmount;
    [SerializeField]
    private float displayLength;
    private bool oneTimeChangeFade;
    private bool isIncrease;

    private void Awake()
    {
        isFade = false;
        isIncrease = true;
        alpha = 0f;
        oneTimeChangeFade = true;
        sr = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        fadeCheck();
    }

    void fadeCheck()
    {
        if (isFade)
        {
            RaycastCheck.canClickCharacter = false;
            if (isIncrease)
            {
                alpha += Time.deltaTime * fadeAmount;
                if (alpha >= maxAlpha)
                {
                    alpha = maxAlpha;
                    if (oneTimeChangeFade)
                    {
                        oneTimeChangeFade = false;
                        StartCoroutine(waitForChangeFade());
                    }
                }
            }
            else
            {
                alpha -= Time.deltaTime * fadeAmount;
                if (alpha <= 0f)
                {
                    alpha = 0f;
                    endFading();
                }
            }

            sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, alpha);
        }
    }

    IEnumerator waitForChangeFade()
    {
        yield return new WaitForSecondsRealtime(displayLength);
        isIncrease = false;
    }

    void endFading()
    {
        RaycastCheck.canClickCharacter = true;
        isFade = false;
        oneTimeChangeFade = true;
        isIncrease = true;
        gameObject.SetActive(false);
    }

}
