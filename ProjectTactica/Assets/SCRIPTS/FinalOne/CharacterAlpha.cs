﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;

public class CharacterAlpha : MonoBehaviour
{
    [SerializeField]
    private GameObject parent;
    [SerializeField]
    private List<Sprite> character_alpha_list;
    //========================================
    public int character_alpha_direction;
    public bool isLastOne;
    public bool isShow;
    private SpriteRenderer sr;
    private PhotonView pv;
    private SystemController systemController;
    // Start is called before the first frame update
    void Start()
    {
        isLastOne = true;
        isShow = true;
        pv = GetComponent<PhotonView>();
        sr = GetComponent<SpriteRenderer>();
        systemController = SystemController.systemController.GetComponent<SystemController>();
        if (pv.IsMine)
        {
            pv.RPC("rpc_setDirection", RpcTarget.Others, character_alpha_direction);
        }
        sr.sprite = character_alpha_list[character_alpha_direction - 1];
        StartCoroutine(waitUntilNotShow());
    }

    private void Update()
    {
        if(GetComponent<SpriteRenderer>().enabled != parent.GetComponent<SpriteRenderer>().enabled)
        {
            GetComponent<SpriteRenderer>().enabled = parent.GetComponent<SpriteRenderer>().enabled;
        }
    }

    IEnumerator waitUntilNotShow()
    {
        yield return new WaitUntil(() => !isLastOne);
        
        if (pv.IsMine)
        {
            pv.RPC("rpc_setIsShow", RpcTarget.Others, false);
        }
        sr.sprite = null;
    }

    [PunRPC]
    void rpc_setIsShow(bool set)
    {
        isLastOne = set;
    }
    [PunRPC]
    void rpc_setDirection(int direction)
    {
        character_alpha_direction = direction;
    }
}
