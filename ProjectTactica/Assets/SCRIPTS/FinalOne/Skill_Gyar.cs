﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Skill_Gyar : MonoBehaviour
{
    private GridBehaviour1 gridBehaviour;
    // Start is called before the first frame update
    void Start()
    {
        gridBehaviour = GridBehaviour1.gridBehaviour.GetComponent<GridBehaviour1>();
        if (GetComponent<PhotonView>().IsMine)
        {
            StartCoroutine(waitUntilControl());
            //StartCoroutine(waitUntilOnMountain());
        }
    }

    IEnumerator waitUntilControl()
    {
        CharacterController cc = GetComponent<CharacterController>();
        if (cc.isControl)
        {
            yield return new WaitUntil(() => !cc.isControl);
            turnCanVisitGrid(false);
        }
        else
        {
            yield return new WaitUntil(() => cc.isControl);
        }
        StartCoroutine(waitUntilControl());
    }

    IEnumerator waitUntilOnMountain()
    {
        CharacterController cc = GetComponent<CharacterController>();
        if(gridBehaviour.gridArr[cc.row, cc.col].tag == "STUN_TERRAIN")
        {
            yield return new WaitUntil(() => gridBehaviour.gridArr[cc.row, cc.col].tag != "STUN_TERRAIN" && !cc.isMove);
        }
        else
        {
            yield return new WaitUntil(() => gridBehaviour.gridArr[cc.row, cc.col].tag == "STUN_TERRAIN" && !cc.isMove);
        }
        GetComponent<PhotonView>().RPC("rpc_setStepOn", RpcTarget.All, cc.row, cc.col, gridBehaviour.gridArr[cc.row, cc.col].tag == "STUN_TERRAIN");
        StartCoroutine(waitUntilOnMountain());

    }

    public void turnCanVisitGrid(bool set)
    {
        for(int i = 0; i < gridBehaviour.rowTotal; i++)
        {
            for(int j = 0; j < gridBehaviour.colTotal; j++)
            {
                if(gridBehaviour.gridArr[i,j].tag == "STUN_TERRAIN")
                {
                    gridBehaviour.gridArr[i, j].GetComponent<GridStat1>().canVisit = set;
                }
            }
        }
    }

    [PunRPC]
    void rpc_setStepOn(int row, int col, bool set)
    {
        GridStat1 gridStat = gridBehaviour.gridArr[row, col].GetComponent<GridStat1>();
        if (set)
        {
            gridStat.stepOnCharacter = gameObject;
        }
        else
        {
            gridStat.stepOnCharacter = null;
        }
    }
}
