﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Skill_Diora : MonoBehaviour
{
    //character cannon
    [SerializeField]
    private float effect_delay;
    [SerializeField]
    private GameObject speedLine;
    [SerializeField]
    private GameObject character_cannon;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(waitUntilSkill());
    }

    IEnumerator waitUntilSkill()
    {
        CharacterController cc = GetComponent<CharacterController>();

        yield return new WaitUntil(() => cc.isSkill);

        spawnCharacterCannon();
    }

    void diora_skill_finish()
    {
        setSkillEffect();
        GetComponent<CharacterController>().clearAllAction();
        GetComponent<ActiveSkill>().SteadyCheck_call();
    }

    void setSkillEffect()
    {
        foreach (GameObject grid in GetComponent<CharacterController>().skill_grid_list_confirm)
        {
            GridStat1 gridStat = grid.GetComponent<GridStat1>();
            if (gridStat.stepOnCharacter)
            {
                CharacterController stepOnCC = gridStat.stepOnCharacter.GetComponent<CharacterController>();
                CharacterController cc = GetComponent<CharacterController>();
                if (stepOnCC.player != cc.player)
                {
                    stepOnCC.setDirection(cc.oppositeDirection(cc.current_direction));
                    stepOnCC.attacker = gameObject;
                    stepOnCC.setIsBeingAttack(true);
                    stepOnCC.setFreeze_call(true);
                }
            }
        }
    }

    void spawnCharacterCannon()
    {
        PhotonNetwork.Instantiate(Path.Combine("Prefab", speedLine.name), Vector2.zero, Quaternion.identity);
        PhotonNetwork.Instantiate(Path.Combine("Prefab", character_cannon.name), Vector2.zero, Quaternion.identity);
        StartCoroutine(waitForCharacterEffect());
    }

    IEnumerator waitForCharacterEffect()
    {
        yield return new WaitForSecondsRealtime(effect_delay);
        diora_skill_finish();
        StartCoroutine(waitUntilSkill());
    }
}
