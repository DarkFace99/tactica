﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GridBehaviour1 : MonoBehaviour
{
    public SystemController systemController;
    public GameObject disasterDESERT;
    public GameObject disasterICEAGE;
    private int randomRow;
    private int randomCol;
    private int twoNumber;
    public int rowTotal; //total of row
    public int colTotal; //total of column
    public float scaleX; //length of each grid
    public float scaleY; //length of each grid
    public Vector3 startingPos; //starting position to spawn grid through for loop
    public GameObject[,] gridArr; //array keeps GameObject of all grid
    public static GameObject gridBehaviour;
    public List<GameObject> list_disaster;
    //public int sandStromDisasterStun;
    public BigIcon bigIcon;

    // GRIDSPAWN
    public GridSpawn gridSpawn;
    public int theme;
    public int layout;
    // DROP ZONE
    public DropZone dropZone;

    // DISASTER
    public int turnSTUN;
    private BattlePhase battle;

    // Start is called before the first frame update
    void Awake()
    {
        gridBehaviour = gameObject;
        turnSTUN = 3;
        theme = DataSelect.Instance.T;
        layout = DataSelect.Instance.L;
        gridArr = new GameObject[rowTotal, colTotal];
        gridSpawn.GridGenerate(theme, layout);
        //dropZone.DropPoint(theme, layout);

    }
    void Start()
    {
        battle = BattlePhase.battlePhase.GetComponent<BattlePhase>();
        dropZone.DropPoint(theme, layout);
        StartCoroutine(dropZone.WaitAtFirst());
        StartCoroutine(dropZone.ChangeColourBack());
        setDisasterEfxLength();
    }

    void setDisasterEfxLength()
    {
        if (theme == 1)
        {
            battle.disaster_turn_amount = 3;
        }
        else if (theme == 2)
        {
            battle.disaster_turn_amount = 1;
        }
        else if (theme == 3)
        {

        }
    }


    //==================== Function to return true or false to check and random that is row or not ====================
    bool isRow()
    {
        if (twoNumber % 2 != 0) // if Random % 2 not equal to 0 return true
        {
            //Debug.Log("IsRow");
            return true;
        }
        else // if Random % 2 equal to 0 return false
        {
            //Debug.Log("IsColumn");
            return false;
        }
    }
    //==================== Function of Disaster ====================
    public void Disaster()
    {
        list_disaster_check();
        if (theme == 1)
        {
            int countMarkLeft = 8;
            while (countMarkLeft != 0)
            {
                int randomR, randomC;

                randomR = Random.Range(0, rowTotal);
                randomC = Random.Range(0, colTotal);

                if ((gridArr[randomR, randomC].GetComponent<GridStat1>().isDisaster == false) && (gridArr[randomR, randomC].tag == "grid_basic"))
                {
                    SetDisaster(randomR, randomC);
                    countMarkLeft--;
                }
            }
        }
        else if (theme == 2) // ++++ FOR SECOND THEME
        {

            int randomR, randomC;
            int keepR, keepC;

            randomR = Random.Range(1, rowTotal - 3);
            randomC = Random.Range(1, colTotal - 3);
            keepR = randomR + 3;
            keepC = randomC + 3;

            for (int i = randomR; i < keepR; i++)
            {
                for (int j = randomC; j < keepC; j++)
                {
                    if (((i < rowTotal - 1) && (j < colTotal - 1)) && ((i > - 1) && (j > - 1)))
                    {
                        if ((gridArr[i, j].GetComponent<GridStat1>().isDisaster == false) && (gridArr[i, j].tag == "grid_basic"))
                        {
                            SetDisaster(i, j);
                        }
                    }
                }
            }
        }
        else if (theme == 3)
        {

        }
    }

    void list_disaster_check()
    {
        if (list_disaster.Count > 0)
        {
            list_disaster.Clear();
        }
    }

    void SetDisaster(int row, int col)
    {
        if (theme == 1)
        {
            GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", disasterDESERT.name), gridArr[row, col].transform.localPosition, Quaternion.identity); //No RPC
            list_disaster.Add(obj);
            GetComponent<PhotonView>().RPC("RPC_SetDisaster", RpcTarget.All, row, col);
        }
        else if (theme == 2)
        {
            GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", disasterICEAGE.name), gridArr[row, col].transform.localPosition, Quaternion.identity); //No RPC
            list_disaster.Add(obj);
            GetComponent<PhotonView>().RPC("RPC_SetDisaster", RpcTarget.All, row, col);
        }
        else if (theme == 3)
        {

        }
    }
    [PunRPC]
    void RPC_SetDisaster(int row, int col)
    {
        gridArr[row, col].GetComponent<GridStat1>().isDisaster = true; //RPC
    }





    public void CheckCancel(int turnToRecover)
    {
        if (theme == 1)
        {
            for (int r = 0; r < rowTotal; r++)
            {
                for (int c = 0; c < colTotal; c++)
                {
                    //Debug.Log(r + " " + c);
                    if ((gridArr[r, c].GetComponent<GridStat1>().stepOnCharacter != null) && (gridArr[r, c].GetComponent<GridStat1>().isDisaster == true))
                    {
                        if (gridArr[r, c].GetComponent<GridStat1>().stepOnCharacter.GetComponent<PhotonView>().IsMine)
                        {
                            gridArr[r, c].GetComponent<GridStat1>().characterController.clearAllAction();
                            gridArr[r, c].GetComponent<GridStat1>().characterController.rpc_setStun(turnToRecover - SystemController.turn);
                        }
                        else
                        {
                            gridArr[r, c].GetComponent<GridStat1>().characterController.GetComponent<PhotonView>().RPC("rpc_clearAllAction", RpcTarget.All); ;
                            gridArr[r, c].GetComponent<GridStat1>().characterController.setStun_call(turnToRecover - SystemController.turn);
                        }
                    }
                }
            }
            foreach (GameObject disaster in list_disaster)
            {
                disaster.GetComponent<ChangeAlphaDisaster>().isChangeAlpha = true;
            }
        }
        else if (theme == 2)
        {
            for (int r = 0; r < rowTotal; r++)
            {
                for (int c = 0; c < colTotal; c++)
                {
                    //Debug.Log(r + " " + c);
                    if ((gridArr[r, c].GetComponent<GridStat1>().stepOnCharacter != null) && (gridArr[r, c].GetComponent<GridStat1>().isDisaster == true))
                    {
                        if (gridArr[r, c].GetComponent<GridStat1>().stepOnCharacter.GetComponent<PhotonView>().IsMine)
                        {
                            gridArr[r, c].GetComponent<GridStat1>().characterController.setIsBeingAttack(true);
                        }
                        else
                        {
                            gridArr[r, c].GetComponent<GridStat1>().characterController.setIsBeingAttack(true);
                        }
                    }
                }
            }
            foreach (GameObject disaster in list_disaster)
            {
                disaster.GetComponent<ChangeAlphaDisaster>().isChangeAlpha = true;
            }
        }
        else if (theme == 3)
        {

        }
    }





    public void AfterDisaster()
    {
        for (int i = 0; i < rowTotal; i++)
        {
            for (int j = 0; j < colTotal; j++)
            {
                AfterDisaster(i, j);
            }
        }
    }

    void AfterDisaster(int row, int col)
    {
        GetComponent<PhotonView>().RPC("RPC_AfterDisaster", RpcTarget.All, row, col);
    }

    public void RemoveDisasterGrid()
    {
        for (int i = 0; i < list_disaster.Count; i++)
        {
            PhotonNetwork.Destroy(list_disaster[i]);// No RPC
        }
    }

    [PunRPC]
    void RPC_AfterDisaster(int row, int col)
    {
        gridArr[row, col].GetComponent<GridStat1>().isDisaster = false;
    }
    public void RandomRowOrColumn()
    {
        twoNumber = Random.Range(0, 2); // random number that to be row or column
        //Debug.Log("Random twoNumber = " + twoNumber);
    }
}