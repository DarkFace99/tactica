﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;

public class MovementMark : MonoBehaviour
{

    public bool canVisit;
    //==========================
    public int row;
    public int col;
    public int direction;
    private SystemController systemController;
    private GridBehaviour1 gridBehavior1;
    private CharacterController control_cc;

    // Start is called before the first frame update
    void Start()
    {
        systemController = SystemController.systemController.GetComponent<SystemController>();
        gridBehavior1 = GridBehaviour1.gridBehaviour.GetComponent<GridBehaviour1>();
        control_cc = systemController.controllingCharacter().GetComponent<CharacterController>();
        /*if (canVisit)
        {
            GetComponent<Collider2D>().enabled = false;
            GetComponent<Collider2D>().enabled = true;
        }*/
    }

    void spawnCharacterSim()
    {
        GameObject character_sim = control_cc.character_sim;
        GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", character_sim.name), transform.localPosition, Quaternion.identity);
        obj.GetComponent<RendererDifferent>().characterIndex = control_cc.characterIndex;
        control_cc.character_sim_list.Add(obj);
        control_cc.path_list.Add(gridBehavior1.gridArr[row, col]);
        control_cc.direction_list.Add(direction);
        control_cc.currentStep++;
        control_cc.resetMovingGuide();
        if (control_cc.currentStep < control_cc.maxStep)
        {
            control_cc.spawnMovingGuide(row, col);
        }
        setCharacterAlpha(obj, control_cc);
    }

    public void onMouseDown()
    {
        if (canVisit)
        {
            spawnCharacterSim();
        }
    }

    void setCharacterAlpha(GameObject obj, CharacterController control_cc)
    {
        if (obj.GetComponentInChildren<CharacterAlpha>() != null)
        {
            obj.GetComponentInChildren<CharacterAlpha>().character_alpha_direction = direction;
        }
        if (control_cc.character_sim_list.Count > 1)
        {
            CharacterAlpha c_alpha = control_cc.character_sim_list[control_cc.currentStep - 2].GetComponentInChildren<CharacterAlpha>();
            if (c_alpha != null)
            {
                c_alpha.isLastOne = false;
            }
        }
    }
}
