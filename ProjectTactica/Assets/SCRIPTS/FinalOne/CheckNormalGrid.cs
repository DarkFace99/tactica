﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckNormalGrid : MonoBehaviour
{
    [SerializeField]
    float offset;
    [SerializeField]
    private GameObject arrow2;
    private GridStat1 gridStat;
    private SystemController systemController;
    // Start is called before the first frame update
    void Start()
    {
        systemController = SystemController.systemController.GetComponent<SystemController>();
        gridStat = GetComponent<GridStat1>();
        if (gridStat.isDropZone)
        {
            GameObject obj = Instantiate(arrow2,new Vector2(transform.position.x,transform.position.y + offset) , Quaternion.identity);
            obj.transform.SetParent(transform);
            systemController.arrow2.Add(obj);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
