﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;

public class SystemController : MonoBehaviour
{
    public float dropTime; //length of dropping phase (realtime seconds)
    public float startingDelay;
    [SerializeField]
    private GameObject spawn_grid;
    public bool isStartSpawn;
    public int maxCountDown;
    [SerializeField]
    private GameObject countDownUI;
    //============================================================
    public static GameObject systemController;
    // Put in here
    public List<GameObject> character_list; //list of all characters as GameObject
    public List<GameObject> icon_list; //list of all character's icons
    public List<Vector2> spawn_point; //list of all character's spawn points
    public List<CharacterController> characterControllers; //list of all character's CharacterController scripts
    public List<GameObject> keepAllUnit;
    public List<GameObject> spawn_grid_list;
    public enum GameState {Drop, P1_Action, P2_Action, Battle, Game_Over} //enum variable type for declaring game state
    public static GameState currentState; //tells current game state
    public static int player; //player 1 or 2 
    public static int turn; //current turn
    public static int firstTurnPlayer; //player number take an action first
    private bool oneTimeSpawn;
    public GameObject canvas;
    public GridBehaviour1 gridBehaviour;
    public bool canSurrender;
    public bool isCountDown;
    public bool isLosing;
    public BigIcon bigIcon;
    public static bool isEkakSkill;
    public List<GameObject> arrow1;
    public List<GameObject> arrow2;
    public DropZone dropZone;
    public int currentCountDown;
    private bool isP1Ready;
    private bool isP2Ready;

    // Start is called before the first frame update
    void Awake()
    {
        SetSelectUnit();
        systemController = gameObject;
        oneTimeSpawn = true;
        canSurrender = false;
        isStartSpawn = false;
        isEkakSkill = false;
        isCountDown = false;
        isP1Ready = false;
        isP2Ready = false;
        isLosing = false;
        currentCountDown = maxCountDown;
        turn = 1;
        currentState = GameState.Drop;
        setPlayer(PhotonNetwork.IsMasterClient); // master client -> 1 // non master client -> 2
        StartCoroutine(waitForStart());
    }

    private void Start()
    {
        SetCharacter();
        StartCoroutine(dropZone.GetSprite());
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("TURN" + turn);
        if(currentState != GameState.Drop && oneTimeSpawn) //end drop phase
        {
            oneTimeSpawn = false;
            spawnGridSpawning();
            StartCoroutine(waitForSpawnCharacter());
        }
        rayCastCheck();
    }

    //set player number // master client -> 1 // non master client -> 2
    void setPlayer(bool isMaster)
    {
        if (isMaster)
        {
            player = 1;
        }
        else
        {
            player = 2;
        }
    }

    //countdown for ending drop phase
    IEnumerator endDropPhase()
    {
        yield return new WaitForSecondsRealtime(dropTime);
        //GetComponent<PhotonView>().RPC("rpc_setReady", RpcTarget.All, player);
        StartCoroutine(waitUntilBothReady());
    }

    IEnumerator waitUntilBothReady()
    {
        yield return new WaitUntil(() => isP1Ready && isP2Ready);
        if (!PhotonNetwork.IsMasterClient)
        {
            GetComponent<PhotonView>().RPC("rpc_endDropPhase", RpcTarget.All);
        }
    }

    public void setReadyCall(int set_player)
    {
        GetComponent<PhotonView>().RPC("rpc_setReady", RpcTarget.All, set_player);
    }


    [PunRPC]
    void rpc_setReady(int set_player)
    {
        if(set_player == 1)
        {
            isP1Ready = true;
        }
        else
        {
            isP2Ready = true;
        }
    }
    
    
    //use rpc to change game state
    [PunRPC]
    void rpc_endDropPhase()
    {
        //firstTurnPlayer = Random.Range(1, 3);
        firstTurnPlayer = 2;
        if (firstTurnPlayer == 1)
        {
            currentState = GameState.P1_Action;
        }
        else
        {
            currentState = GameState.P2_Action;
        }
    }
    //public function returning controlling character as GameObject
    //if there's non -> return null
    public GameObject controllingCharacter()
    {
        for (int i = 0; i < character_list.Count; i++)
        {
            if (character_list[i].GetComponent<CharacterController>().isControl)
            {
                return character_list[i];
            }
        }
        return null;
    }

    //set all charaters' collider = set
    public void setColliider(bool set)
    {
        RaycastCheck.canClickCharacter = set;
    }

    public GameObject actionCharacter()
    {
        foreach (GameObject obj in character_list)
        {
            CharacterController cc = obj.GetComponent<CharacterController>();
            if (cc.turnToMove == turn)
            {
                return obj;
            }
        }
        return null;
    }

    //==================== Click at Icon to select and choose grid ====================
    public GameObject IconClick()
    {
        foreach(GameObject icon in icon_list)
        {
            if(icon.GetComponent<UnitDropIcon>().isSelecting)
            {
                return icon;
            }
        }
        return null;
    }
    
    void SetCharacter()
    {
        for (int i = 0; i < character_list.Count; i++)
        {
            characterControllers.Add(character_list[i].GetComponent<CharacterController>());
        }
    }
    //check game over
    public bool isGameOverCheck()
    {
        int diedCharacter = 0;
        for(int i = 0; i < character_list.Count; i++)
        {
            CharacterController cc = character_list[i].GetComponent<CharacterController>();
            if (cc.health <= 0)
            {
                diedCharacter++;
            }
        }

        if(diedCharacter == character_list.Count)
        {
            return true;
        }
        else
        {
            if (diedCharacter == character_list.Count - 1)
            {
                canSurrender = true;
                if (!isCountDown)
                {
                    isLosing = true;
                    GetComponent<PhotonView>().RPC("rpc_setIsCountDown", RpcTarget.All);
                }
            }
            return false;
        }
    }

    [PunRPC]
    void rpc_setIsCountDown()
    {
        isCountDown = true;
        countDownUI.SetActive(true);
    }

    //set end condition as int // -1 -> lose // 0 -> draw // 1 -> win
    public void setEndCondition(int endCondition)
    {
        PlayerPrefs.SetInt("END_CONDITION" + DataLogin.Instance.IdIndex, endCondition);
    }
    void SetSelectUnit()
    {
        for(int i = 0; i < DataSelect.Instance.playerChoose.Count; i++)
        {
            for(int j = 0; j < keepAllUnit.Count; j++)
            {
                if(DataSelect.Instance.playerChoose[i] == j)
                {
                    character_list.Add(keepAllUnit[j]);
                    break;
                }
            }
        }
    }
    //set characterIndex via rpc
    [PunRPC]
    void rpc_setCharacterIndex(int characterID, int index)
    {
        PhotonView pv = PhotonView.Find(characterID);
        pv.gameObject.GetComponent<CharacterController>().characterIndex = index;
    }

    IEnumerator waitForStart()
    {
        yield return new WaitForSecondsRealtime(startingDelay);

        StartCoroutine(endDropPhase());
        StartCoroutine(dropZone.Waiting()); // Run only a time to wait for time
        StartCoroutine(dropZone.DestroyAll());
        dropZone.CanDrop1();
        StartCoroutine(dropZone.GoToNextPhase());
    }
    IEnumerator waitForSpawnCharacter()
    {
        yield return new WaitForSecondsRealtime(startingDelay);
        //yield return new WaitForSecondsRealtime(10f);
        clearSpawnGridList();
        //loop through list of character and spawn as networking object
        for (int i = 0; i < character_list.Count; i++)
        {
            GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", character_list[i].name), spawn_point[i], Quaternion.identity);
            character_list[i] = obj; //set character list to be clone object in scene instead
            GetComponent<PhotonView>().RPC("rpc_setCharacterIndex", RpcTarget.All, obj.GetComponent<PhotonView>().ViewID, i);
        }
        isStartSpawn = true;
    }

    void spawnGridSpawning()
    {
        for(int i = 0; i < character_list.Count; i++)
        {
            GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", spawn_grid.name), spawn_point[i], Quaternion.identity);
            obj.GetComponent<RendererDifferent>().characterIndex = i;
            spawn_grid_list.Add(obj);
        }
    }

    void clearSpawnGridList()
    {
        foreach (GameObject obj in spawn_grid_list)
        {
            PhotonNetwork.Destroy(obj);
        }
        spawn_grid_list.Clear();
    }

    void rayCastCheck()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform != null)
                {
                    Debug.Log(hit.transform.name);
                }
                else
                {
                    Debug.Log("NONE");
                }
            }
        }
    }

    public void decreaseCooldown()
    {
        foreach(GameObject obj in character_list)
        {
            ActiveSkill activeSkill = obj.GetComponent<ActiveSkill>();
            if (activeSkill)
            {
                if (activeSkill.current_cooldown > 0)
                {
                    activeSkill.current_cooldown--;
                }
            }
        }
    }
}