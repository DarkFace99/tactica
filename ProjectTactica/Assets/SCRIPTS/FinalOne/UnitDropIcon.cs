﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class UnitDropIcon : MonoBehaviour
{
    /*[SerializeField]
    private bool isOpponent;*/
    //==================================
    public bool alreadyDeploy = false;
    public bool isSelecting = false;
    public Vector2 spawnLocation;
    private SystemController systemController;
    private GridBehaviour1 gridBehaviour;
    private BigIcon bigIcon;
    private void Start()
    {
        //systemController = SystemController.systemController.GetComponent<SystemController>();
        gridBehaviour = GridBehaviour1.gridBehaviour.GetComponent<GridBehaviour1>();
        //gridBehaviour.unitDropIcons.Add(this); // Add it into List of unitDropIcon in girdBehaviour scripts
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse1) && SystemController.currentState == SystemController.GameState.Drop /*&& !isOpponent*/) // For right click to cancel the unit that choosing
        {
            Cancel(); // Call Function to cancel action
        }
    }
    //=================== This Function is about when you use cursor to point on Something ====================
    //public void OnMouseOver()
    //{
    //    if (Input.GetKey(KeyCode.Mouse0) && SystemController.currentState == SystemController.GameState.Drop && alreadyDeploy == false /*&& !isOpponent*/) // Left click when drop Phase and that grid is not already deploy
    //    {
    //        isSelecting = true; // Assign isSelecting to true
    //        Selecting(); // Hardcode to not select more than one unit
    //    }
    //}
    public void Selecting()
    {
        if(gridBehaviour.dropZone.unitDropIcons[0].isSelecting == true)
        {
            gridBehaviour.dropZone.unitDropIcons[1].isSelecting = false;
            gridBehaviour.dropZone.unitDropIcons[2].isSelecting = false;
        }
        if(gridBehaviour.dropZone.unitDropIcons[1].isSelecting == true)
        {
            gridBehaviour.dropZone.unitDropIcons[0].isSelecting = false;
            gridBehaviour.dropZone.unitDropIcons[2].isSelecting = false;
        }
        if (gridBehaviour.dropZone.unitDropIcons[2].isSelecting == true)
        {
            gridBehaviour.dropZone.unitDropIcons[0].isSelecting = false;
            gridBehaviour.dropZone.unitDropIcons[1].isSelecting = false;
        }
    }
    //==================== This Function for cancel unit choose ====================
    private void Cancel()
    {
        isSelecting = false; // cancel choose the unit
    }
}