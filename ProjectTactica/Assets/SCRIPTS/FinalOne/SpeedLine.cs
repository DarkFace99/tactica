﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedLine : MonoBehaviour
{
    [SerializeField]
    private Color redColor; //(255, 60, 60, 255)
    [SerializeField]
    private Color blueColor; //(120, 255, 255, 255)
    [SerializeField]
    private float destroyDelay; //1.65
    [SerializeField]
    private Vector2 starting_pos; //(0, -1.7)
    // Start is called before the first frame update
    void Start()
    {
        transform.localPosition = starting_pos;
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        if (GetComponent<PhotonView>().IsMine)
        {
            sr.color = blueColor;
        }
        else
        {
            sr.color = redColor;
        }

        Destroy(gameObject, destroyDelay);
    }
}
