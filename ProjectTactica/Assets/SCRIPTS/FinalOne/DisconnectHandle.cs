﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;

public class DisconnectHandle : MonoBehaviourPunCallbacks
{
    //public int endSceneIndex;
    private SystemController systemController;
    [SerializeField]
    private string endSceneName;
    [SerializeField]
    private int maxPing;
    [SerializeField]
    private float maxOverPingTime;
    private float overPingTime;
    // Scene Trasitions
    public Animator transitions;
    public float transitionTIME = 1.20f;

    private void Start()
    {
        systemController = SystemController.systemController.GetComponent<SystemController>();
    }

    private void Update()
    {
        if(SystemController.currentState == SystemController.GameState.Game_Over)
        {
            if (PhotonNetwork.InRoom)
            {
                StartCoroutine(StartTransition());
                PhotonNetwork.LeaveRoom();
            }
            //PhotonNetwork.LoadLevel(endSceneIndex);
            if (PlayerPrefs.HasKey("END_CONDITION" + DataLogin.Instance.IdIndex))
            {
                StartCoroutine(StartTransition());
                SceneManager.LoadScene(endSceneName);
            }
            else
            {
                systemController.setEndCondition(1);
            }
        }
        pingCheck();
        //Debug.Log("PING = " + PhotonNetwork.GetPing());
    }
    IEnumerator StartTransition()
    {
        transitions.ResetTrigger("StartTrans");
        yield return new WaitForSeconds(transitionTIME);  
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        if(SystemController.currentState != SystemController.GameState.Game_Over)
        {
            systemController.setEndCondition(1);
            SystemController.currentState = SystemController.GameState.Game_Over;
        }
    }

    public override void OnLeftRoom()
    {
        if (SystemController.currentState != SystemController.GameState.Game_Over)
        {
            systemController.setEndCondition(-1);
            SystemController.currentState = SystemController.GameState.Game_Over;
        }
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        if(SystemController.currentState != SystemController.GameState.Game_Over)
        {
            systemController.setEndCondition(-1);
            SystemController.currentState = SystemController.GameState.Game_Over;
        }
    }

    void pingCheck()
    {
        if (PhotonNetwork.GetPing() > maxPing)
        {
            overPingTime += Time.deltaTime;
            if (overPingTime > maxOverPingTime)
            {
                PhotonNetwork.Disconnect();
            }
        }
        else
        {
            overPingTime = 0f;
        }
    }

}
