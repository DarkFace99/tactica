﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class ChangeAlphaDisaster : MonoBehaviour
{
    //[SerializeField]
    //private float red;
    //[SerializeField]
    //private float green;
    //[SerializeField]
    //private float blue;
    //[SerializeField]
    //private float alpha;
    public bool isChangeAlpha;
    // Start is called before the first frame update
    void Start()
    {
        isChangeAlpha = false;
        gameObject.transform.SetParent(GridBehaviour1.gridBehaviour.transform);
        StartCoroutine(waitToChangeAlpha());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator waitToChangeAlpha()
    {
        yield return new WaitUntil(() => (isChangeAlpha));
        if(PhotonNetwork.IsMasterClient == false)
        {
            GetComponent<PhotonView>().RPC("RPC_ChangeAlpha", RpcTarget.Others);
        }
        //this.GetComponent<SpriteRenderer>().color = new Color(red, green, blue, alpha);
    }
    [PunRPC]
    void RPC_ChangeAlpha()
    {
        isChangeAlpha = true;
    }
}