﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Photon.Pun;

public class GridSpawn : MonoBehaviour
{
    public GridBehaviour1 gridBehave;

    public List<GameObject> Desert_Ground;
    public List<GameObject> Desert_Stun;
    public List<GameObject> Desert_Hurt;
    public List<GameObject> Desert_Nerf;

    public List<GameObject> IceAge_Ground;
    public List<GameObject> IceAge_Stun;
    public List<GameObject> IceAge_Hurt;
    public List<GameObject> IceAge_Nerf;

    public List<GameObject> City_Ground;
    public List<GameObject> City_Stun;
    public List<GameObject> City_Hurt;
    public List<GameObject> City_Nerf;

    /*=================== SPAWN GRID =======================*/
    public void GridGenerate(int theme_num, int layout_num)
    {
        for (int i = 0; i < gridBehave.rowTotal; i++)
        {
            for (int j = 0; j < gridBehave.colTotal; j++)
            {
                if ((theme_num == 1) && (layout_num == 1))
                {
                    Desert_Spawn_ONE(i, j);
                }
                else if ((theme_num == 1) && (layout_num == 2))
                {
                    Desert_Spawn_TWO(i, j);
                }
                else if ((theme_num == 1) && (layout_num == 3))
                {
                    Desert_Spawn_Three(i, j);
                }
                else if ((theme_num == 1) && (layout_num == 4))
                {
                    Desert_Spawn_Four(i, j);
                }
                else if ((theme_num == 2) && (layout_num == 1))
                {
                    IceAge_Spawn_ONE(i, j);
                }
                else if ((theme_num == 2) && (layout_num == 2))
                {
                    IceAge_Spawn_TWO(i, j);
                }
                else if ((theme_num == 2) && (layout_num == 3))
                {
                    IceAge_Spawn_Three(i, j);
                }
                else if ((theme_num == 2) && (layout_num == 4))
                {
                    IceAge_Spawn_Four(i, j);
                }
                //else if ((theme_num == 3) && (layout_num == 1))
                //{
                //    City_Spawn_ONE(i, j);
                //}
                //else if ((theme_num == 3) && (layout_num == 2))
                //{

                //}
            }
        }
    }

    /*========= DESERT THEME =========*/
    void Desert_Spawn_ONE(int i, int j) // DESERT => FIRST LAYOUT
    {
        // STUN
        if (((i == 4) && (j == 2)) || ((i == 7) && (j == 2)) || ((i == 0) && (j == 5)) || ((i == 3) && (j == 5)))
        {
            GameObject stun = Instantiate(Desert_Stun[0], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            stun.transform.SetParent(gameObject.transform);
            stun.GetComponent<GridStat1>().row = i;
            stun.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = stun;
        }
        // NERF
        else if (((i == 4) && (j == 0)) || ((i == 1) && (j == 2)) || ((i == 3) && (j == 7)) || ((i == 6) && (j == 5)))
        {
            GameObject nerf = Instantiate(Desert_Nerf[Random.Range(0, Desert_Nerf.Count)], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            nerf.transform.SetParent(gameObject.transform);
            nerf.GetComponent<GridStat1>().row = i;
            nerf.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = nerf;
        }
        // HURT
        else if (((i == 1) && (j == 0)) || ((i == 6) && (j == 7)))
        {
            GameObject hurt = Instantiate(Desert_Hurt[Random.Range(0, Desert_Hurt.Count)], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            hurt.transform.SetParent(gameObject.transform);
            hurt.GetComponent<GridStat1>().row = i;
            hurt.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = hurt;
        }
        // GROUND
        else
        {
            GameObject ground = Instantiate(Desert_Ground[Random.Range(0, Desert_Ground.Count)], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            //GameObject ground = Instantiate(Desert_Ground[0], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            ground.transform.SetParent(gameObject.transform);
            ground.GetComponent<GridStat1>().row = i;
            ground.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = ground;
        }
    }


    void Desert_Spawn_TWO(int i, int j) // DESERT => SECOND LAYOUT
    {
        // STUN
        if (((i == 1) && (j == 2)) || ((i == 2) && (j == 1)) || ((i == 5) && (j == 6)) || ((i == 6) && (j == 5)))
        {
            GameObject stun = Instantiate(Desert_Stun[0], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            stun.transform.SetParent(gameObject.transform);
            stun.GetComponent<GridStat1>().row = i;
            stun.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = stun;
        }
        // NERF
        else if (((i == 4) && (j == 1)) || ((i == 2) && (j == 2)) || ((i == 5) && (j == 6)) || ((i == 3) && (j == 6)))
        {
            GameObject nerf = Instantiate(Desert_Nerf[Random.Range(0, Desert_Nerf.Count)], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            nerf.transform.SetParent(gameObject.transform);
            nerf.GetComponent<GridStat1>().row = i;
            nerf.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = nerf;
        }
        // HURT
        else if (((i == 6) && (j == 0)) || ((i == 6) && (j == 3)) || ((i == 1) && (j == 4)) || ((i == 1) && (j == 7)))
        {
            GameObject hurt = Instantiate(Desert_Hurt[Random.Range(0, Desert_Hurt.Count)], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            hurt.transform.SetParent(gameObject.transform);
            hurt.GetComponent<GridStat1>().row = i;
            hurt.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = hurt;
        }
        // GROUND
        else
        {
            GameObject ground = Instantiate(Desert_Ground[Random.Range(0, Desert_Ground.Count)], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            ground.transform.SetParent(gameObject.transform);
            ground.GetComponent<GridStat1>().row = i;
            ground.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = ground;
        }
    }

    /*========= ICEAGE THEME =========*/
    void IceAge_Spawn_ONE(int i, int j)
    {
        // STUN
        if (((i == 1) && (j == 0)) || ((i == 6) && (j == 0)) || ((i == 4) && (j == 1)) || ((i == 3) && (j == 6)) || ((i == 6) && (j == 7)) || ((i == 1) && (j == 7)))
        {
            GameObject stun = Instantiate(IceAge_Stun[Random.Range(0, IceAge_Stun.Count)], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            stun.transform.SetParent(gameObject.transform);
            stun.GetComponent<GridStat1>().row = i;
            stun.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = stun;
        }
        // NERF
        else if (((i == 5) && (j == 3)) || ((i == 2) && (j == 4)))
        {
            GameObject nerf = Instantiate(IceAge_Nerf[0], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            nerf.transform.SetParent(gameObject.transform);
            nerf.GetComponent<GridStat1>().row = i;
            nerf.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = nerf;
        }
        // HURT
        else if (((i == 2) && (j == 2)) || ((i == 5) && (j == 5)))
        {
            GameObject hurt = Instantiate(IceAge_Hurt[Random.Range(0, IceAge_Hurt.Count)], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            hurt.transform.SetParent(gameObject.transform);
            hurt.GetComponent<GridStat1>().row = i;
            hurt.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = hurt;
        }
        // GROUND
        else
        {
            GameObject ground = Instantiate(IceAge_Ground[Random.Range(0, IceAge_Ground.Count)], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            ground.transform.SetParent(gameObject.transform);
            ground.GetComponent<GridStat1>().row = i;
            ground.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = ground;
        }
    }
    void IceAge_Spawn_TWO(int i, int j)
    {
        // STUN
        if (((i == 3) && (j == 3)) || ((i == 4) && (j == 3)) || ((i == 5) && (j == 3)) || ((i == 2) && (j == 4)) || ((i == 4) && (j == 4)) || ((i == 3) && (j == 4)))
        {
            GameObject stun = Instantiate(IceAge_Stun[Random.Range(0, IceAge_Stun.Count)], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            stun.transform.SetParent(gameObject.transform);
            stun.GetComponent<GridStat1>().row = i;
            stun.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = stun;
        }
        // NERF
        else if (((i == 0) && (j == 2)) || ((i == 1) && (j == 3)) || ((i == 6) && (j == 4)) || ((i == 7) && (j == 5)))
        {
            GameObject nerf = Instantiate(IceAge_Nerf[0], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            nerf.transform.SetParent(gameObject.transform);
            nerf.GetComponent<GridStat1>().row = i;
            nerf.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = nerf;
        }
        // HURT
        else if (((i == 0) && (j == 0)) || ((i == 3) && (j == 0)) || ((i == 7) && (j == 0)) || ((i == 0) && (j == 7)) || ((i == 4) && (j == 7)) || ((i == 7) && (j == 7)))
        {
            GameObject hurt = Instantiate(IceAge_Hurt[Random.Range(0, IceAge_Hurt.Count)], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            hurt.transform.SetParent(gameObject.transform);
            hurt.GetComponent<GridStat1>().row = i;
            hurt.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = hurt;
        }
        // GROUND
        else
        {
            GameObject ground = Instantiate(IceAge_Ground[Random.Range(0, IceAge_Ground.Count)], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            ground.transform.SetParent(gameObject.transform);
            ground.GetComponent<GridStat1>().row = i;
            ground.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = ground;
        }
    }

    /*========= CITY THEME =========*/
    void City_Spawn_ONE(int i, int j)
    {
        ////HURT
        //if((i == 0 && j == 6) || (i == 2 && j == 1) || (i == 5) && (j == 6) || (i == 7 && j == 1))
        //{
        //    GameObject hurt = Instantiate(City_Hurt[0], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
        //    hurt.transform.SetParent(gameObject.transform);
        //    hurt.GetComponent<GridStat1>().row = i;
        //    hurt.GetComponent<GridStat1>().col = j;
        //    gridBehave.gridArr[i, j] = hurt;
        //}
        ////NERF
        //else if((i == 1 && j == 0) || (i == 3) && (j == 0) || (i == 4) && (j == 7) || (i == 6 && j == 7))
        //{
        //    GameObject nerf = Instantiate(City_Nerf[0], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
        //    nerf.transform.SetParent(gameObject.transform);
        //    nerf.GetComponent<GridStat1>().row = i;
        //    nerf.GetComponent<GridStat1>().col = j;
        //    gridBehave.gridArr[i, j] = nerf;
        //}
        ////STUN
        //else if((i == 1 && j == 2) || (i == 1 && j == 5) || (i == 2 && j == 4) || (i == 2 && j == 5) || (i == 5 && j == 2) || (i == 5 && j == 3) || (i == 6 && j == 2) || (i == 6 && j ==5))
        //{
        //    GameObject stun = Instantiate(City_Stun[0], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
        //    stun.transform.SetParent(gameObject.transform);
        //    stun.GetComponent<GridStat1>().row = i;
        //    stun.GetComponent<GridStat1>().col = j;
        //    gridBehave.gridArr[i, j] = stun;
        //}
        //else if((i == 2 && j == 0) || (i == 5 && j == 0) || (i == 1 && j == 5) || (i == 5 && j == 7) || (i == 5 && j == 1))
        //{
        //    GameObject ground = Instantiate(City_Ground[0], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
        //    ground.transform.SetParent(gameObject.transform);
        //    ground.GetComponent<GridStat1>().row = i;
        //    ground.GetComponent<GridStat1>().col = j;
        //    gridBehave.gridArr[i, j] = ground;
        //}
        //else if((i == 0 && j == 2) || (i == 0 && j == 5) || (i == 3 && j == 5) || (i == 4 && j == 2) || (i == 7 && j == 2) || (i == 7 && j == 5))
        //{
        //    GameObject ground = Instantiate(City_Ground[1], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
        //    ground.transform.SetParent(gameObject.transform);
        //    ground.GetComponent<GridStat1>().row = i;
        //    ground.GetComponent<GridStat1>().col = j;
        //    gridBehave.gridArr[i, j] = ground;
        //}
        //else if ((i == 0 && j == 7) || (i == 5 && j == 7))
        //{
        //    GameObject ground = Instantiate(City_Ground[2], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
        //    ground.transform.SetParent(gameObject.transform);
        //    ground.GetComponent<GridStat1>().row = i;
        //    ground.GetComponent<GridStat1>().col = j;
        //    gridBehave.gridArr[i, j] = ground;
        //}
        //else if ((i == 0 && j == 0) || (i == 2 && j == 2) || (i == 3 && j == 1) || (i == 6 && j == 3) || (i == 6 && j == 6))
        //{
        //    GameObject ground = Instantiate(City_Ground[3], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
        //    ground.transform.SetParent(gameObject.transform);
        //    ground.GetComponent<GridStat1>().row = i;
        //    ground.GetComponent<GridStat1>().col = j;
        //    gridBehave.gridArr[i, j] = ground;
        //}
        //else if (i == 7 && j == 0)
        //{
        //    GameObject ground = Instantiate(City_Ground[4], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
        //    ground.transform.SetParent(gameObject.transform);
        //    ground.GetComponent<GridStat1>().row = i;
        //    ground.GetComponent<GridStat1>().col = j;
        //    gridBehave.gridArr[i, j] = ground;
        //}
        //else if ((i == 1 && j == 1) ||(i == 4 && j == 6) || (i == 5 && j == 5) || (i == 7 && j == 7))
        //{
        //    GameObject ground = Instantiate(City_Ground[5], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
        //    ground.transform.SetParent(gameObject.transform);
        //    ground.GetComponent<GridStat1>().row = i;
        //    ground.GetComponent<GridStat1>().col = j;
        //    gridBehave.gridArr[i, j] = ground;
        //}
        //else if ((i == 3 && j == 3) || (i == 3 && j == 6) || (i == 4 && j == 1) || (i == 4 && j == 4))
        //{
        //    GameObject ground = Instantiate(City_Ground[6], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
        //    ground.transform.SetParent(gameObject.transform);
        //    ground.GetComponent<GridStat1>().row = i;
        //    ground.GetComponent<GridStat1>().col = j;
        //    gridBehave.gridArr[i, j] = ground;
        //}
        //else if ((i == 1 && j == 4) || (i == 1 && j == 7) || (i == 2 && j == 3) || (i == 3 && j == 7) || (i == 6 && j == 4)||(i == 2 && j == 7) || (i == 5 && j == 1) || (i == 6 && j == 1))
        //{
        //    GameObject ground = Instantiate(City_Ground[7], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
        //    ground.transform.SetParent(gameObject.transform);
        //    ground.GetComponent<GridStat1>().row = i;
        //    ground.GetComponent<GridStat1>().col = j;
        //    gridBehave.gridArr[i, j] = ground;
        //}
        //else if ((i == 0 && j == 1) || (i == 0 && j == 3) || (i == 0 && j == 4) || (i == 3 && j == 4) || (i == 4 && j == 5))
        //{
        //    GameObject ground = Instantiate(City_Ground[8], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
        //    ground.transform.SetParent(gameObject.transform);
        //    ground.GetComponent<GridStat1>().row = i;
        //    ground.GetComponent<GridStat1>().col = j;
        //    gridBehave.gridArr[i, j] = ground;
        //}
        //else if ((i == 3 && j == 2) || (i == 4 && j == 3) || (i == 7 && j == 3) || (i == 7 && j == 4) || (i == 7 && j == 6))
        //{
        //    GameObject ground = Instantiate(City_Ground[10], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
        //    ground.transform.SetParent(gameObject.transform);
        //    ground.GetComponent<GridStat1>().row = i;
        //    ground.GetComponent<GridStat1>().col = j;
        //    gridBehave.gridArr[i, j] = ground;
        //}
        //else
        //{
        //    GameObject ground = Instantiate(City_Ground[9], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
        //    ground.transform.SetParent(gameObject.transform);
        //    ground.GetComponent<GridStat1>().row = i;
        //    ground.GetComponent<GridStat1>().col = j;
        //    gridBehave.gridArr[i, j] = ground;
        //}
    }
    void Desert_Spawn_Three(int i, int j)
    {
        if ((i == 0 && j == 0) || (i == 0 && j == 7) || (i == 7 && j == 0) || (i == 7 && j == 7))
        {
            GameObject stun = Instantiate(Desert_Stun[0], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            stun.transform.SetParent(gameObject.transform);
            stun.GetComponent<GridStat1>().row = i;
            stun.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = stun;
        }
        else if ((i == 1 && j == 5) || (i == 4 && j == 0))
        {
            GameObject hurt = Instantiate(Desert_Hurt[Random.Range(0, Desert_Hurt.Count)], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            hurt.transform.SetParent(gameObject.transform);
            hurt.GetComponent<GridStat1>().row = i;
            hurt.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = hurt;
        }
        else if ((i == 3 && j == 7) || (i == 6 && j == 2))
        {
            GameObject nerf = Instantiate(Desert_Nerf[Random.Range(0, Desert_Nerf.Count)], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            nerf.transform.SetParent(gameObject.transform);
            nerf.GetComponent<GridStat1>().row = i;
            nerf.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = nerf;
        }
        else
        {
            GameObject ground = Instantiate(Desert_Ground[Random.Range(0, Desert_Ground.Count)], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            ground.transform.SetParent(gameObject.transform);
            ground.GetComponent<GridStat1>().row = i;
            ground.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = ground;
        }
    }
    void IceAge_Spawn_Three(int i, int j)
    {
        //HURT
        if ((i == 0 && j == 6) || (i == 2 && j == 1) || (i == 5) && (j == 6) || (i == 7 && j == 1))
        {
            GameObject hurt = Instantiate(IceAge_Hurt[Random.Range(0, IceAge_Hurt.Count)], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            hurt.transform.SetParent(gameObject.transform);
            hurt.GetComponent<GridStat1>().row = i;
            hurt.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = hurt;
        }
        //NERF
        else if ((i == 1 && j == 0) || (i == 3) && (j == 0) || (i == 4) && (j == 7) || (i == 6 && j == 7))
        {
            GameObject nerf = Instantiate(IceAge_Nerf[0], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            nerf.transform.SetParent(gameObject.transform);
            nerf.GetComponent<GridStat1>().row = i;
            nerf.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = nerf;
        }
        //STUN
        else if ((i == 1 && j == 2) || (i == 1 && j == 5) || (i == 2 && j == 4) || (i == 2 && j == 5) || (i == 5 && j == 2) || (i == 5 && j == 3) || (i == 6 && j == 2) || (i == 6 && j == 5))
        {
            GameObject stun = Instantiate(IceAge_Stun[Random.Range(0, IceAge_Stun.Count)], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            stun.transform.SetParent(gameObject.transform);
            stun.GetComponent<GridStat1>().row = i;
            stun.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = stun;
        }
        else
        {
            GameObject ground = Instantiate(IceAge_Ground[Random.Range(0, IceAge_Ground.Count)], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            ground.transform.SetParent(gameObject.transform);
            ground.GetComponent<GridStat1>().row = i;
            ground.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = ground;
        }
    }
    void IceAge_Spawn_Four(int i, int j)
    {
        //HURT
        if ((i == 3 && j == 0) || (i == 4 && j == 7) || (i == 7 && j == 3) || (i == 0 && j == 4))
        {
            GameObject hurt = Instantiate(IceAge_Hurt[Random.Range(0, IceAge_Hurt.Count)], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            hurt.transform.SetParent(gameObject.transform);
            hurt.GetComponent<GridStat1>().row = i;
            hurt.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = hurt;
        }
        //NERF
        else if ((i == 0 && j == 5) || (i == 2) && (j == 4) || (i == 3) && (j == 7) || (i == 4 && j == 0) || (i == 5 && j == 3) || (i == 7 && j == 2))
        {
            GameObject nerf = Instantiate(IceAge_Nerf[0], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            nerf.transform.SetParent(gameObject.transform);
            nerf.GetComponent<GridStat1>().row = i;
            nerf.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = nerf;
        }
        //STUN
        else if ((i == 2 && j == 0) || (i == 1 && j == 4) || (i == 3 && j == 1) || (i == 4 && j == 6) || (i == 5 && j == 7) || (i == 6 && j == 3))
        {
            GameObject stun = Instantiate(IceAge_Stun[Random.Range(0, IceAge_Stun.Count)], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            stun.transform.SetParent(gameObject.transform);
            stun.GetComponent<GridStat1>().row = i;
            stun.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = stun;
        }
        else
        {
            GameObject ground = Instantiate(IceAge_Ground[Random.Range(0, IceAge_Ground.Count)], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            ground.transform.SetParent(gameObject.transform);
            ground.GetComponent<GridStat1>().row = i;
            ground.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = ground;
        }
    }
    void Desert_Spawn_Four(int i, int j) // DESERT => FIRST LAYOUT
    {
        // STUN
        if ((i == 0 && j == 4) || (i == 0 && j == 7) || (i == 2 && j == 2) || (i == 5 && j == 5) || (i == 7 && j == 0) || (i == 7 && j == 3))
        {
            GameObject stun = Instantiate(Desert_Stun[0], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            stun.transform.SetParent(gameObject.transform);
            stun.GetComponent<GridStat1>().row = i;
            stun.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = stun;
        }
        // NERF
        else if ((i == 0 && j == 0) || (i == 5 && j == 2))
        {
            GameObject nerf = Instantiate(Desert_Nerf[Random.Range(0, Desert_Nerf.Count)], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            nerf.transform.SetParent(gameObject.transform);
            nerf.GetComponent<GridStat1>().row = i;
            nerf.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = nerf;
        }
        // HURT
        else if ((i == 2 && j == 5) || (i == 7 && j == 7))
        {
            GameObject hurt = Instantiate(Desert_Hurt[Random.Range(0, Desert_Hurt.Count)], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            hurt.transform.SetParent(gameObject.transform);
            hurt.GetComponent<GridStat1>().row = i;
            hurt.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = hurt;
        }
        // GROUND
        else
        {
            GameObject ground = Instantiate(Desert_Ground[Random.Range(0, Desert_Ground.Count)], new Vector3(gridBehave.startingPos.x + gridBehave.scaleX * j, gridBehave.startingPos.y - gridBehave.scaleY * i, 0), Quaternion.identity);
            ground.transform.SetParent(gameObject.transform);
            ground.GetComponent<GridStat1>().row = i;
            ground.GetComponent<GridStat1>().col = j;
            gridBehave.gridArr[i, j] = ground;
        }
    }
}