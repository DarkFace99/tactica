﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;

public class Skill_Saveria : MonoBehaviour
{
    //character cannon
    [SerializeField]
    private float effect_delay;
    [SerializeField]
    private GameObject speedLine;
    [SerializeField]
    private GameObject character_cannon;
    [SerializeField]
    private int turnLength;
    [SerializeField]
    private List<Sprite> hurt_terrain_sprite;
    //==================================================
    private int turn_to_stop_skill;
    private BattlePhase battlePhase;
    private GridBehaviour1 gridBehaviour;
    // Start is called before the first frame update
    void Start()
    {
        battlePhase = BattlePhase.battlePhase.GetComponent<BattlePhase>();
        gridBehaviour = GridBehaviour1.gridBehaviour.GetComponent<GridBehaviour1>();
        if (GetComponent<PhotonView>().IsMine)
        {
            StartCoroutine(waitUntilSkill());
        }
    }

    IEnumerator waitUntilSkill()
    {
        CharacterController cc = GetComponent<CharacterController>();
        if (cc.isSkill)
        {
            yield return new WaitUntil(() => !cc.isSkill);
        }
        else
        {
            yield return new WaitUntil(() => cc.isSkill);
            spawnCharacterCannon();
        }
        StartCoroutine(waitUntilSkill());
    }

    void spawnCharacterCannon()
    {
        PhotonNetwork.Instantiate(Path.Combine("Prefab", speedLine.name), Vector2.zero, Quaternion.identity);
        PhotonNetwork.Instantiate(Path.Combine("Prefab", character_cannon.name), Vector2.zero, Quaternion.identity);
        StartCoroutine(waitForCharacterEffect());
    }

    IEnumerator waitForCharacterEffect()
    {
        yield return new WaitForSecondsRealtime(effect_delay);
        //use the skill
        setNewList();
        changing_grid(true);

        turn_to_stop_skill = SystemController.turn + turnLength;
        GetComponent<PhotonView>().RPC("rpc_set_recover_skill", RpcTarget.All, turn_to_stop_skill);
        StartCoroutine(waitUntilStopSkill());
        CharacterController cc = GetComponent<CharacterController>();
        battlePhase.actionCheck_complete(cc.player);
        cc.turnToMove = 0;
    }

    public void changing_grid(bool set)
    {
        CharacterController cc = GetComponent<CharacterController>();
        foreach (GameObject obj in cc.skill_grid_list_confirm)
        {
            GridStat1 gridStat = obj.GetComponent<GridStat1>();
            GetComponent<PhotonView>().RPC("rpc_change_gridStat", RpcTarget.All, gridStat.row, gridStat.col, set);
        }
    }

    void setNewList()
    {
        CharacterController cc = GetComponent<CharacterController>();
        List<GameObject> gridTemp = new List<GameObject>(); //initialize new list
        List<GameObject> markTemp = new List<GameObject>(); //initialize new list
        for (int i = 0; i < cc.skill_grid_list_confirm.Count; i++)
        {
            if (cc.skill_grid_list_confirm[i].GetComponent<GridStat1>().canVisit)
            {
                gridTemp.Add(cc.skill_grid_list_confirm[i]);
                markTemp.Add(cc.skill_mark_list_confirm[i]);
            }
            else
            {
                PhotonNetwork.Destroy(cc.skill_mark_list_confirm[i]);
            }
        }
        cc.skill_grid_list_confirm = gridTemp;
        cc.skill_mark_list_confirm = markTemp;
    }

    [PunRPC]
    void rpc_change_gridStat(int row, int col, bool set)
    {
        GameObject grid = gridBehaviour.gridArr[row, col];
        GridStat1 gridStat = grid.GetComponent<GridStat1>();
        if (set)
        {
            gridStat.canVisit = false;
            grid.tag = "HURT_TERRAIN";
            grid.GetComponent<SpriteRenderer>().sprite = hurt_terrain_sprite[DataSelect.Instance.T - 1];

        }
        else
        {
            gridStat.canVisit = true;
            grid.tag = "grid_basic";
            grid.GetComponent<SpriteRenderer>().sprite = gridStat.starting_sprite;
        }
        gridStat.setOrderInLayer();
    }

    [PunRPC]
    void rpc_set_recover_skill(int turn)
    {
        GetComponent<CharactereAction>().turn_to_recover_skill = turn;
    }

    IEnumerator waitUntilStopSkill()
    {
        CharacterController cc = GetComponent<CharacterController>();
        yield return new WaitUntil(() => SystemController.turn == turn_to_stop_skill || !cc.isSkill);
        if (cc.isSkill)
        {
            cc.clearAllAction();
        }
        GetComponent<PhotonView>().RPC("rpc_set_recover_skill", RpcTarget.All, 0);
    }
}
