﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhereToDrop : MonoBehaviour
{
    private SystemController systemController;
    private GridBehaviour1 gridBehavior;
    //private bool isFree = false;
    // Start is called before the first frame update
    void Start()
    {
        systemController = SystemController.systemController.GetComponent<SystemController>();
        gridBehavior = GridBehaviour1.gridBehaviour.GetComponent<GridBehaviour1>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnMouseOver()
    {
        if(Input.GetKeyDown(KeyCode.Mouse0) && systemController.IconClick() != null)
        {
            UnitDropIcon icon = systemController.IconClick().GetComponent<UnitDropIcon>();
            icon.spawnLocation = transform.localPosition;
            icon.isSelecting = false;
            // isFree = true;
        }
    }
}
