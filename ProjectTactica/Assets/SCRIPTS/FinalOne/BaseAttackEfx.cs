﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;

public class BaseAttackEfx : MonoBehaviour
{
    [SerializeField]
    private GameObject efx;
    private Vector2[] spawn_pos;
    //===========================================
    private CharacterController cc;

    private void Start()
    {
        set_spawn_pos();
        cc = GetComponent<CharacterController>();
        StartCoroutine(waitUntilAttack());
    }

    void set_spawn_pos()
    {
        spawn_pos = new Vector2[4];
        spawn_pos[0] = new Vector2(0f, 0.6f);
        spawn_pos[1] = new Vector2(0f, 0.6f);
        spawn_pos[2] = new Vector2(0f, 0.6f);
        spawn_pos[3] = new Vector2(0f, 0.6f);
    }

    IEnumerator waitUntilAttack()
    {
        if (!cc.isAttack)
        {
            yield return new WaitUntil(() => cc.isAttack);
            spawnEfx();
        }
        else
        {
            yield return new WaitUntil(() => !cc.isAttack);
        }
        StartCoroutine(waitUntilAttack());
    }
    void spawnEfx()
    {
        if (cc.baseAttackType == 1) //melee attack
        {
            foreach (GameObject grid in cc.attacking_grid_list_confirm) //loop through confirm list of attacking grid
            {
                GridStat1 gridStat = grid.GetComponent<GridStat1>();
                if (gridStat.stepOnCharacter != null) //there's character standing on the grid
                {
                    CharacterController stepOn_cc = gridStat.stepOnCharacter.GetComponent<CharacterController>();

                    if (stepOn_cc.player != cc.player) //is it opponent's character
                    {
                        startSpawnEfx(gridStat.stepOnCharacter);
                    }
                }
            }
        }
        else //range attack
        {
            //range attack damage one character which is the nearest one
            for (int i = 0; i < cc.attacking_grid_list_confirm.Count; i++) //loop through confirm list og attacking grid
            {
                GridStat1 gridStat = cc.attacking_grid_list_confirm[i].GetComponent<GridStat1>();
                if (gridStat.stepOnCharacter != null) //if there's character standing on
                {
                    CharacterController stepOn_cc = gridStat.stepOnCharacter.GetComponent<CharacterController>();

                    if (stepOn_cc.player != cc.player && cc.rangeAttackCheck(cc.attacking_grid_list_confirm[i])) //is it opponent's character
                    {
                        startSpawnEfx(gridStat.stepOnCharacter);
                        break; //break for loop when found first character to attack
                    }
                }
            }
        }
    }

    void startSpawnEfx(GameObject obj)
    {
        int attackDirection = cc.attack_direction_confirm;
        Vector2 pos = new Vector2(obj.transform.position.x + spawn_pos[attackDirection - 1].x, obj.transform.position.y + spawn_pos[attackDirection - 1].y);
        if (attackDirection == 1)
        {
            PhotonNetwork.Instantiate(Path.Combine("Prefab", efx.name), pos, Quaternion.identity);
        }
        else if (attackDirection == 2)
        {
            PhotonNetwork.Instantiate(Path.Combine("Prefab", efx.name), pos, Quaternion.identity);
        }
        else if (attackDirection == 3)
        {
            PhotonNetwork.Instantiate(Path.Combine("Prefab", efx.name), pos, Quaternion.identity);
        }
        else if (attackDirection == 4)
        {
            PhotonNetwork.Instantiate(Path.Combine("Prefab", efx.name), pos, Quaternion.identity);
        }
    }
}
