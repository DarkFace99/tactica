﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character_Canon : MonoBehaviour
{
    private bool isMine;
    [SerializeField]
    private Vector2 startin_pos; //(-14, -1.3)
    [SerializeField]
    private float slowDown_pos; //1
    [SerializeField]
    private float end_pos; //14
    [SerializeField]
    private float translateSpeed_normal; //40
    [SerializeField]
    private float translateSpeed_slowDown; //1
    [SerializeField]
    private float slowDown_delay; //1
    private bool isSlowDown;
    private Vector2 direction;
    // Start is called before the first frame update
    void Start()
    {
        isMine = GetComponent<PhotonView>().IsMine;
        setPos();
        setDirection();
        isSlowDown = false;
        transform.localPosition = startin_pos;
        StartCoroutine(waitUntilSlowDown());
        StartCoroutine(waitUntilEnd());
    }

    private void Update()
    {
        if (!isSlowDown)
        {
            transform.Translate(direction * translateSpeed_normal * Time.deltaTime);
        }
        else
        {
            transform.Translate(direction * translateSpeed_slowDown * Time.deltaTime);
        }
    }

    void setDirection()
    {
        if (isMine)
        {
            direction = Vector2.left;
        }
        else
        {
            direction = Vector2.right;
        }
    }

    void setPos()
    {
        if (isMine)
        {
            startin_pos = new Vector2(startin_pos.x * -1f, startin_pos.y);
            end_pos *= -1f;
            slowDown_pos *= -1f;
            GetComponent<SpriteRenderer>().flipX = true;
        }
    }

    IEnumerator waitUntilSlowDown()
    {
        if (!isMine)
        {
            yield return new WaitUntil(() => transform.position.x >= slowDown_pos);
        }
        else
        {
            yield return new WaitUntil(() => transform.position.x <= slowDown_pos);
        }
        isSlowDown = true;
        StartCoroutine(waitForSlowDown());
    }

    IEnumerator waitForSlowDown()
    {
        yield return new WaitForSecondsRealtime(slowDown_delay);
        isSlowDown = false;
    }

    IEnumerator waitUntilEnd()
    {
        if (!isMine)
        {
            yield return new WaitUntil(() => transform.position.x >= end_pos);
        }
        else
        {
            yield return new WaitUntil(() => transform.position.x <= end_pos);
        }
        Destroy(gameObject);
    }
}
