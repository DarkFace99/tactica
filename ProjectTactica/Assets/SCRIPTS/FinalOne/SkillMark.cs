﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class SkillMark : MonoBehaviour
{
    [SerializeField]
    private GameObject skillMark;
    //============================
    public int row;
    public int col;
    public int direction;
    public bool isMouseOver;
    public bool isConfirm;
    private SystemController systemController;
    private GridBehaviour1 gridBehavior1;
    private GameObject controlling_obj;
    private int character_number;

    // Start is called before the first frame update
    void Start()
    {
        systemController = SystemController.systemController.GetComponent<SystemController>(); //SystemController
        gridBehavior1 = GridBehaviour1.gridBehaviour.GetComponent<GridBehaviour1>(); //GridBehavior1
        controlling_obj = systemController.controllingCharacter(); //controlling character
        character_number = controlling_obj.GetComponent<CharacterController>().character_number;
        isConfirm = false;
        if (character_number == 3) //Croc-Boi
        {
            isConfirm = true;
            spawnSkillMark_croc_boi();
        }
        else if (character_number == 6 || character_number == 7)
        {
            isConfirm = true;
            spawnSkillMark_Ekak();
        }
    }

    //when mouse is enter
    public void onMouseEnter()
    {
        if (controlling_obj.GetComponent<CharacterController>().skill_mark_list.Count == 0)
        {
            isMouseOver = true;
            if(character_number == 1) //Anna
            {
                spawnSkillMark_Anna(direction);
            }
            else if (character_number == 2) //Neiga
            {
                spawnSkillMark_Neiga();
            }
            else if (character_number == 5)
            {
                spawnSkillMark_Diora(direction);
            }
            else if (character_number == 9)
            {
                spawnSkillMark_Saveria(direction);
            }
        }
    }

    public void onMouseDown()
    {
        if (!isConfirm)
        {
            isConfirm = true;
        }
    }

    //when mouse is exit
    public void onMouseExit()
    {
        if (controlling_obj.GetComponent<CharacterController>().skill_mark_list.Count > 0)
        {
            isMouseOver = false;
            if (!isConfirm)
            {
                controlling_obj.GetComponent<CharacterController>().clearSkillMark(); //clear spawned attacking mark from CharacterController's list
            }
        }
    }

    void spawnSkillMark_Neiga()
    {
        GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row, col].transform.localPosition, Quaternion.identity);
        setSkillMarkValue(obj, gridBehavior1.gridArr[row, col]);
    }

    void spawnSkillMark_Anna(int direction)
    {
        if (direction == 1)
        {
            if(row - 1 > -1)
            {
                GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row - 1, col].transform.localPosition, Quaternion.identity);
                setSkillMarkValue(obj, gridBehavior1.gridArr[row - 1, col]);
                if (row - 2 > -1)
                {
                    GameObject obj_2 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row - 2, col].transform.localPosition, Quaternion.identity);
                    setSkillMarkValue(obj_2, gridBehavior1.gridArr[row - 2, col]);

                    if (col - 1 > -1)
                    {
                        GameObject obj_3 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row - 2, col - 1].transform.localPosition, Quaternion.identity);
                        setSkillMarkValue(obj_3, gridBehavior1.gridArr[row - 2, col - 1]);
                    }
                    if (col + 1 < gridBehavior1.colTotal)
                    {
                        GameObject obj_3 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row - 2, col + 1].transform.localPosition, Quaternion.identity);
                        setSkillMarkValue(obj_3, gridBehavior1.gridArr[row - 2, col + 1]);
                    }
                }
            }
        }
        else if (direction == 2)
        {
            if (col + 1 < gridBehavior1.colTotal)
            {
                GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row, col + 1].transform.localPosition, Quaternion.identity);
                setSkillMarkValue(obj, gridBehavior1.gridArr[row, col + 1]);
                if (col + 2 < gridBehavior1.colTotal)
                {
                    GameObject obj_2 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row, col + 2].transform.localPosition, Quaternion.identity);
                    setSkillMarkValue(obj_2, gridBehavior1.gridArr[row, col + 2]);

                    if (row - 1 > -1)
                    {
                        GameObject obj_3 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row - 1, col + 2].transform.localPosition, Quaternion.identity);
                        setSkillMarkValue(obj_3, gridBehavior1.gridArr[row - 1, col + 2]);
                    }
                    if (row + 1 < gridBehavior1.colTotal)
                    {
                        GameObject obj_3 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row + 1, col + 2].transform.localPosition, Quaternion.identity);
                        setSkillMarkValue(obj_3, gridBehavior1.gridArr[row + 1, col + 2]);
                    }
                }
            }
        }
        else if (direction == 3)
        {
            if (row + 1 < gridBehavior1.rowTotal)
            {
                GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row + 1, col].transform.localPosition, Quaternion.identity);
                setSkillMarkValue(obj, gridBehavior1.gridArr[row + 1, col]);
                if (row + 2 < gridBehavior1.rowTotal)
                {
                    GameObject obj_2 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row + 2, col].transform.localPosition, Quaternion.identity);
                    setSkillMarkValue(obj_2, gridBehavior1.gridArr[row + 2, col]);

                    if (col - 1 > -1)
                    {
                        GameObject obj_3 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row + 2, col - 1].transform.localPosition, Quaternion.identity);
                        setSkillMarkValue(obj_3, gridBehavior1.gridArr[row + 2, col - 1]);
                    }
                    if (col + 1 < gridBehavior1.colTotal)
                    {
                        GameObject obj_3 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row + 2, col + 1].transform.localPosition, Quaternion.identity);
                        setSkillMarkValue(obj_3, gridBehavior1.gridArr[row + 2, col + 1]);
                    }
                }
            }
        }
        else if (direction == 4)
        {
            if (col - 1 > - 1)
            {
                GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row, col - 1].transform.localPosition, Quaternion.identity);
                setSkillMarkValue(obj, gridBehavior1.gridArr[row, col - 1]);
                if (col - 2 > - 1)
                {
                    GameObject obj_2 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row, col - 2].transform.localPosition, Quaternion.identity);
                    setSkillMarkValue(obj_2, gridBehavior1.gridArr[row, col - 2]);

                    if (row - 1 > -1)
                    {
                        GameObject obj_3 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row - 1, col - 2].transform.localPosition, Quaternion.identity);
                        setSkillMarkValue(obj_3, gridBehavior1.gridArr[row - 1, col - 2]);
                    }
                    if (row + 1 < gridBehavior1.colTotal)
                    {
                        GameObject obj_3 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row + 1, col - 2].transform.localPosition, Quaternion.identity);
                        setSkillMarkValue(obj_3, gridBehavior1.gridArr[row + 1, col - 2]);
                    }
                }
            }
        }
    }

    void spawnSkillMark_croc_boi()
    {
        if (controlling_obj.GetComponent<CharacterController>().attacking_mark_list.Count == 0)
        {
            if (row - 1 > -1)
            {
                GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row - 1, col].transform.localPosition, Quaternion.identity);
                setSkillMarkValue(obj, gridBehavior1.gridArr[row - 1, col]);
                if (col - 1 > -1)
                {
                    GameObject obj_2 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row - 1, col - 1].transform.localPosition, Quaternion.identity);
                    setSkillMarkValue(obj_2, gridBehavior1.gridArr[row - 1, col - 1]);
                }
                if (col + 1 < gridBehavior1.colTotal)
                {
                    GameObject obj_3 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row - 1, col + 1].transform.localPosition, Quaternion.identity);
                    setSkillMarkValue(obj_3, gridBehavior1.gridArr[row - 1, col + 1]);
                }
            }

            if (row + 1 > -1)
            {
                GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row + 1, col].transform.localPosition, Quaternion.identity);
                setSkillMarkValue(obj, gridBehavior1.gridArr[row + 1, col]);
                if (col - 1 > -1)
                {
                    GameObject obj_2 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row + 1, col - 1].transform.localPosition, Quaternion.identity);
                    setSkillMarkValue(obj_2, gridBehavior1.gridArr[row + 1, col - 1]);
                }
                if (col + 1 < gridBehavior1.colTotal)
                {
                    GameObject obj_3 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row + 1, col + 1].transform.localPosition, Quaternion.identity);
                    setSkillMarkValue(obj_3, gridBehavior1.gridArr[row + 1, col + 1]);
                }
            }

            if (col - 1 > -1)
            {
                GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row, col - 1].transform.localPosition, Quaternion.identity);
                setSkillMarkValue(obj, gridBehavior1.gridArr[row, col - 1]);
            }

            if (col + 1 < gridBehavior1.colTotal)
            {
                GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row, col + 1].transform.localPosition, Quaternion.identity);
                setSkillMarkValue(obj, gridBehavior1.gridArr[row, col + 1]);
            }
        }
    }

    void spawnSkillMark_Diora(int direction)
    {
        if (direction == 1)
        {
            if (row - 1 > -1)
            {
                GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row - 1, col].transform.localPosition, Quaternion.identity);
                setSkillMarkValue(obj, gridBehavior1.gridArr[row - 1, col]);
                if (col - 1 > -1)
                {
                    GameObject obj_3 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row - 1, col - 1].transform.localPosition, Quaternion.identity);
                    setSkillMarkValue(obj_3, gridBehavior1.gridArr[row - 1, col - 1]);
                }
                if (col + 1 < gridBehavior1.colTotal)
                {
                    GameObject obj_3 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row - 1, col + 1].transform.localPosition, Quaternion.identity);
                    setSkillMarkValue(obj_3, gridBehavior1.gridArr[row - 1, col + 1]);
                }
                if (row - 2 > -1)
                {
                    GameObject obj_2 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row - 2, col].transform.localPosition, Quaternion.identity);
                    setSkillMarkValue(obj_2, gridBehavior1.gridArr[row - 2, col]);
                }
            }
        }
        else if (direction == 2)
        {
            if (col + 1 < gridBehavior1.colTotal)
            {
                GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row, col + 1].transform.localPosition, Quaternion.identity);
                setSkillMarkValue(obj, gridBehavior1.gridArr[row, col + 1]);
                if (row - 1 > -1)
                {
                    GameObject obj_3 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row - 1, col + 1].transform.localPosition, Quaternion.identity);
                    setSkillMarkValue(obj_3, gridBehavior1.gridArr[row - 1, col + 1]);
                }
                if (row + 1 < gridBehavior1.colTotal)
                {
                    GameObject obj_3 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row + 1, col + 1].transform.localPosition, Quaternion.identity);
                    setSkillMarkValue(obj_3, gridBehavior1.gridArr[row + 1, col + 1]);
                }
                if (col + 2 < gridBehavior1.colTotal)
                {
                    GameObject obj_2 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row, col + 2].transform.localPosition, Quaternion.identity);
                    setSkillMarkValue(obj_2, gridBehavior1.gridArr[row, col + 2]);
                }
            }
        }
        else if (direction == 3)
        {
            if (row + 1 < gridBehavior1.rowTotal)
            {
                GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row + 1, col].transform.localPosition, Quaternion.identity);
                setSkillMarkValue(obj, gridBehavior1.gridArr[row + 1, col]);
                if (col - 1 > -1)
                {
                    GameObject obj_3 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row + 1, col - 1].transform.localPosition, Quaternion.identity);
                    setSkillMarkValue(obj_3, gridBehavior1.gridArr[row + 1, col - 1]);
                }
                if (col + 1 < gridBehavior1.colTotal)
                {
                    GameObject obj_3 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row + 1, col + 1].transform.localPosition, Quaternion.identity);
                    setSkillMarkValue(obj_3, gridBehavior1.gridArr[row + 1, col + 1]);
                }
                if (row + 2 < gridBehavior1.rowTotal)
                {
                    GameObject obj_2 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row + 2, col].transform.localPosition, Quaternion.identity);
                    setSkillMarkValue(obj_2, gridBehavior1.gridArr[row + 2, col]);
                }
            }
        }
        else if (direction == 4)
        {
            if (col - 1 > -1)
            {
                GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row, col - 1].transform.localPosition, Quaternion.identity);
                setSkillMarkValue(obj, gridBehavior1.gridArr[row, col - 1]);
                if (row - 1 > -1)
                {
                    GameObject obj_3 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row - 1, col - 1].transform.localPosition, Quaternion.identity);
                    setSkillMarkValue(obj_3, gridBehavior1.gridArr[row - 1, col - 1]);
                }
                if (row + 1 < gridBehavior1.colTotal)
                {
                    GameObject obj_3 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row + 1, col - 1].transform.localPosition, Quaternion.identity);
                    setSkillMarkValue(obj_3, gridBehavior1.gridArr[row + 1, col - 1]);
                }
                if (col - 2 > -1)
                {
                    GameObject obj_2 = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row, col - 2].transform.localPosition, Quaternion.identity);
                    setSkillMarkValue(obj_2, gridBehavior1.gridArr[row, col - 2]);
                }
            }
        }
    }

    void spawnSkillMark_Ekak()
    {
        GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row, col].transform.localPosition, Quaternion.identity);
        setSkillMarkValue(obj, gridBehavior1.gridArr[row, col]);
    }

    void spawnSkillMark_Saveria(int direction)
    {
        if (direction == 1)
        {
            if (row - 1 > -1)
            {
                for(int i = row - 1; i > -1; i--)
                {
                    if (gridBehavior1.gridArr[i, col].tag == "grid_basic")
                    {
                        GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[i, col].transform.localPosition, Quaternion.identity);
                        setSkillMarkValue(obj, gridBehavior1.gridArr[i, col]);
                    }
                }
            }
        }
        else if (direction == 2)
        {
            if (col + 1 < gridBehavior1.colTotal)
            {
                for (int i = col + 1; i < gridBehavior1.colTotal; i++)
                {
                    if (gridBehavior1.gridArr[row, i].tag == "grid_basic")
                    {
                        GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row, i].transform.localPosition, Quaternion.identity);
                        setSkillMarkValue(obj, gridBehavior1.gridArr[row, i]);
                    }
                }
            }
        }
        else if (direction == 3)
        {
            if (row + 1 < gridBehavior1.rowTotal)
            {
                for (int i = row + 1; i < gridBehavior1.rowTotal; i++)
                {
                    if (gridBehavior1.gridArr[i, col].tag == "grid_basic")
                    {
                        GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[i, col].transform.localPosition, Quaternion.identity);
                        setSkillMarkValue(obj, gridBehavior1.gridArr[i, col]);
                    }
                }
            }
        }
        else if (direction == 4)
        {
            if (col - 1 > -1)
            {
                for (int i = col - 1; i > -1; i--)
                {
                    if (gridBehavior1.gridArr[row, i].tag == "grid_basic")
                    {
                        GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", skillMark.name), gridBehavior1.gridArr[row, i].transform.localPosition, Quaternion.identity);
                        setSkillMarkValue(obj, gridBehavior1.gridArr[row, i]);
                    }
                }
            }
        }
    }

    //add attacking mark data to CharacterController
    void setSkillMarkValue(GameObject mark, GameObject grid)
    {
        CharacterController cc = controlling_obj.GetComponent<CharacterController>();
        cc.skill_mark_list.Add(mark); //add mark to normal attacking mark list
        cc.skill_grid_list.Add(grid); //add grid to normal attacking grid list
        cc.skill_direction = direction; //add attack direction to normal attack direction variable
        mark.GetComponent<RendererDifferent>().characterIndex = cc.characterIndex;

        if (cc.character_number == 2) //character showing their skill mark
        {
            mark.GetComponent<DataHiding>().isHidingData = false;
        }
        else // character not showing their skill mark
        {
            if (!mark.GetComponent<PhotonView>().IsMine)
            {
                mark.GetComponent<SpriteRenderer>().enabled = false; //disable renderer if it's not our object
            }
        }
    }
}
