﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BigIcon : MonoBehaviour
{
    private GridBehaviour1 gridBehaviour;
    public static GameObject bigIcon;
    public List<GameObject> SpawnObj;
    public List<GameObject> selectDropButton;
    public List<GameObject> selectUnitImage;
    public SystemController systemController;
    void Awake()
    {
        bigIcon = gameObject;
    }
    // Start is called before the first frame update
    void Start()
    {
        gridBehaviour = GridBehaviour1.gridBehaviour.GetComponent<GridBehaviour1>();

    }
    public void Slot1()
    {
        if(gridBehaviour.dropZone.unitDropIcons[0].alreadyDeploy == false && gridBehaviour.dropZone.unitDropIcons[0].isSelecting == false && gridBehaviour.dropZone.candrop == true)
        {
            gridBehaviour.dropZone.unitDropIcons[0].isSelecting = true;
            //Debug.Log("isSelecting: " + gridBehaviour.dropZone.unitDropIcons[0].isSelecting);
            gridBehaviour.dropZone.unitDropIcons[0].Selecting();
            //selectDropButton[0].GetComponentInChildren<Text>().text = "Cancel";
        }
        else if (gridBehaviour.dropZone.unitDropIcons[0].alreadyDeploy == true && gridBehaviour.dropZone.unitDropIcons[0].isSelecting == false && gridBehaviour.dropZone.candrop == true)
        {
            //remove data
            gridBehaviour.dropZone.unitDropIcons[0].alreadyDeploy = false; // Set already that grid deploy = false in the List to access
            gridBehaviour.dropZone.spawnDrop[0].gridStat.canDeploy = true; // Set can deploy = true
            //gridBehaviour.dropZone.listDropZone.Add(gridBehaviour.dropZone.spawnDrop[0].gridStat); // Add the list of drop zone to return able to choose
            gridBehaviour.dropZone.spawnDrop[0].reSelect = true;
            //selectDropButton[0].GetComponentInChildren<Text>().text = "Press To Select";
        }
        else if (gridBehaviour.dropZone.unitDropIcons[0].alreadyDeploy == false && gridBehaviour.dropZone.unitDropIcons[0].isSelecting == true && gridBehaviour.dropZone.candrop == true)
        {
            gridBehaviour.dropZone.unitDropIcons[0].isSelecting = false;
            //selectDropButton[0].GetComponentInChildren<Text>().text = "Press To Select";
        }

    }
    public void Slot2()
    {
        if (gridBehaviour.dropZone.unitDropIcons[1].alreadyDeploy == false && gridBehaviour.dropZone.unitDropIcons[1].isSelecting == false && gridBehaviour.dropZone.candrop == true)
        {
            gridBehaviour.dropZone.unitDropIcons[1].isSelecting = true;
            //Debug.Log("isSelecting: " + gridBehaviour.dropZone.unitDropIcons[1].isSelecting);
            gridBehaviour.dropZone.unitDropIcons[1].Selecting();
            //selectDropButton[1].GetComponentInChildren<Text>().text = "Cancel";
        }
        else if (gridBehaviour.dropZone.unitDropIcons[1].alreadyDeploy == true && gridBehaviour.dropZone.unitDropIcons[1].isSelecting == false && gridBehaviour.dropZone.candrop == true)
        {
            //remove data
            gridBehaviour.dropZone.unitDropIcons[1].alreadyDeploy = false; // Set already that grid deploy = false in the List to access
            gridBehaviour.dropZone.spawnDrop[1].gridStat.canDeploy = true; // Set can deploy = true
            //gridBehaviour.dropZone.listDropZone.Add(gridBehaviour.dropZone.spawnDrop[1].gridStat); // Add the list of drop zone to return able to choose
            gridBehaviour.dropZone.spawnDrop[1].reSelect = true;
            //selectDropButton[1].GetComponentInChildren<Text>().text = "Press To Select";
        }
        else if (gridBehaviour.dropZone.unitDropIcons[1].alreadyDeploy == false && gridBehaviour.dropZone.unitDropIcons[1].isSelecting == true && gridBehaviour.dropZone.candrop == true)
        {
            gridBehaviour.dropZone.unitDropIcons[1].isSelecting = false;
            //selectDropButton[1].GetComponentInChildren<Text>().text = "Press To Select";
        }
    }
    public void Slot3()
    {
        if (gridBehaviour.dropZone.unitDropIcons[2].alreadyDeploy == false && gridBehaviour.dropZone.unitDropIcons[2].isSelecting == false && gridBehaviour.dropZone.candrop == true)
        {
            gridBehaviour.dropZone.unitDropIcons[2].isSelecting = true;
            //Debug.Log("isSelecting: " + gridBehaviour.dropZone.unitDropIcons[2].isSelecting);
            gridBehaviour.dropZone.unitDropIcons[1].Selecting();
            //selectDropButton[2].GetComponentInChildren<Text>().text = "Cancel";
        }
        else if (gridBehaviour.dropZone.unitDropIcons[2].alreadyDeploy == true && gridBehaviour.dropZone.unitDropIcons[2].isSelecting == false && gridBehaviour.dropZone.candrop == true)
        {
            //remove data
            gridBehaviour.dropZone.unitDropIcons[2].alreadyDeploy = false; // Set already that grid deploy = false in the List to access
            gridBehaviour.dropZone.spawnDrop[2].gridStat.canDeploy = true; // Set can deploy = true
            //gridBehaviour.dropZone.listDropZone.Add(gridBehaviour.dropZone.spawnDrop[2].gridStat); // Add the list of drop zone to return able to choose
            gridBehaviour.dropZone.spawnDrop[2].reSelect = true;
            //selectDropButton[2].GetComponentInChildren<Text>().text = "Press To Select";
        }
        else if (gridBehaviour.dropZone.unitDropIcons[2].alreadyDeploy == false && gridBehaviour.dropZone.unitDropIcons[2].isSelecting == true && gridBehaviour.dropZone.candrop == true)
        {
            gridBehaviour.dropZone.unitDropIcons[2].isSelecting = false;
            //selectDropButton[2].GetComponentInChildren<Text>().text = "Press To Select";
        }
    }
    public void BigUnitIcon()
    {
        for(int i = 0; i < selectUnitImage.Count; i++)
        {
            selectDropButton[i].SetActive(true);
            selectUnitImage[i].SetActive(true);
            selectUnitImage[i].GetComponent<Image>().sprite = systemController.icon_list[i].GetComponent<SpriteRenderer>().sprite;
        }
    }
    public void CheckCrocBoi()
    {
        for(int i = 0; i < DataSelect.Instance.playerChoose.Count; i++)
        {
            if (DataSelect.Instance.playerChoose[i] == 1)
            {
                selectUnitImage[i].transform.localScale = new Vector3(1.2f, 1.0f, 1.0f);
            }
        }
        
    }
}
