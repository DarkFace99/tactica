﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using TMPro;

public class UI_Controller : MonoBehaviour
{
    [SerializeField]
    private Text turn_text; //turn's number text
    [SerializeField]
    private Text phase_text; //current phase text
    [SerializeField]
    private Text delay_text; //delay checking in battle phase text
    [SerializeField]
    private Image endActionImage; //confirm action button image
    [SerializeField]
    private Sprite endAction_sprite_normal;
    [SerializeField]
    private Sprite endAction_sprite_pressed;
    private int player; //player number
    public static bool canClick; //determine whether player can click confirm action button
    //public List<Text> our_name_list; //list of our character's name
    //public List<Text> opponent_name_list; //list or opponent's character's name
    [SerializeField]
    private List<Text> our_health_list; //list of our character's health
    [SerializeField]
    private List<Text> opponent_health_list; //list of opponent's character's health
    private SystemController systemController;
    [SerializeField]
    private List<int> current_health; //current all character health
    [SerializeField]
    private GameObject askingPanel;
    public static bool isActive_askingPanel; //determine setActive of asking panel
    public static GameObject asking_character;
    public Text asking_text; //what do you wanna ask the player
    [SerializeField]
    private List<GameObject> bigIcon_list;
    [SerializeField]
    private GameObject bigIcon_border;
    [SerializeField]
    private GameObject surrenderButton; //surrender button
    [SerializeField]
    private GameObject fadePanel;
    /*[SerializeField]
    private List<Sprite> endAction_sprite_list;*/
    [SerializeField]
    private Text endActonText;
    [SerializeField]
    private Button endActionButton;
    [SerializeField]
    private Text step_text;
    [SerializeField]
    private List<GameObject> race_icon_list;

    // HealthBar
    [SerializeField]
    private List<Slider> ourHealthBar_List;
    [SerializeField]
    private List<Slider> opponentHealthBar_List;

    // TimerBar
    [SerializeField]
    private Slider timerBar;
    [SerializeField]
    private Image timeBar_image;
    private Color timeBar_starting_color;
    [SerializeField]
    private Color timeBar_warning_color;
    [SerializeField]
    private GameObject time_warning_sign;
    [SerializeField]
    private float actionTime;
    private float timeCount;
    private bool isCountDown;

    // EVent Button
    public GameObject eventFrame;
    [SerializeField]
    private Text youText;
    [SerializeField]
    private Text opponentText;

    // Start is called before the first frame update
    void Start()
    {
        player = SystemController.player; //set player
        canClick = false;
        delay_text.enabled = false;
        systemController = SystemController.systemController.GetComponent<SystemController>();

        isActive_askingPanel = false;
        asking_character = null;

        surrenderButton.SetActive(false);
        StartCoroutine(setActiveSurrender());

        StartCoroutine(waitForStart());

        fadePanel.SetActive(false);
        bigIcon_border.SetActive(false);
        step_text.enabled = false;
        
        StartCoroutine(waitUntilStartSpawn());
        timerBar.gameObject.SetActive(false);
        isCountDown = false;
   
        eventFrame.SetActive(false);
        SetName();
        StartCoroutine(waitUntilFadePanel(SystemController.currentState));
        timeBar_starting_color = timeBar_image.color;

        StartCoroutine(waitUntilSkipTurn());
    }

    // Update is called once per frame
    void Update()
    {
        checkTurn(SystemController.turn);
        checkPhase(SystemController.currentState);
        canClickCheck(SystemController.currentState);
        //showDelay();
        healthCheck();
        askingPanel.SetActive(isActive_askingPanel);
        askingPanelCheck();
        setBigIcon();
        TimeCountCheck();
    }

    public void clickedEvent()
    {
        eventFrame.SetActive(true);
    }
    public void clickedBackEvent()
    {
        eventFrame.SetActive(false);
    }

    void TimeCountCheck()
    {
        if (isCountDown)
        {
            timerBar.gameObject.SetActive(true);
            timeCount -= Time.deltaTime;

            if (timeCount > 0f)
            {
                timerBar.value = timeCount;
                if (timeCount > 10f)
                {
                    timeBar_image.color = timeBar_starting_color;
                    time_warning_sign.SetActive(false);
                }
                else
                {
                    timeBar_image.color = timeBar_warning_color;
                    time_warning_sign.SetActive(true);
                }
            }
            else
            {
                timerBar.value = 0f;
                isCountDown = false;
                timerBar.gameObject.SetActive(false);

                timeBar_image.color = timeBar_starting_color;
                time_warning_sign.SetActive(false);
            }

        }
        else
        {
            timeBar_image.color = timeBar_starting_color;
            time_warning_sign.SetActive(false);
        }
    }

    private void eventClicked()
    {
        eventFrame.SetActive(true);
    }

    void TimeCount(float timeToCount)
    {
        timeCount = timeToCount;
        timerBar.maxValue = timeToCount;
        timerBar.value = timeToCount;
        isCountDown = true;
    }

    IEnumerator TimeCount_2()
    {
        if (SystemController.currentState == SystemController.GameState.P1_Action)
        {
            yield return new WaitUntil(() => SystemController.currentState != SystemController.GameState.P1_Action);
        }
        else if (SystemController.currentState == SystemController.GameState.P2_Action)
        {
            yield return new WaitUntil(() => SystemController.currentState != SystemController.GameState.P2_Action);
        }
        else
        {
            yield return new WaitUntil(() => SystemController.currentState == SystemController.GameState.P1_Action || SystemController.currentState == SystemController.GameState.P2_Action);
        }

        if (SystemController.currentState == SystemController.GameState.P1_Action || SystemController.currentState == SystemController.GameState.P2_Action)
        {
            TimeCount(actionTime);
        }
        else
        {
            timeCount = 0f;
        }
        StartCoroutine(TimeCount_2());
    }

    IEnumerator waitUntilStartSpawn()
    {
        yield return new WaitUntil(() => systemController.isStartSpawn);
        TimeCount(actionTime);
        StartCoroutine(TimeCount_2());
        StartCoroutine(waitUntilTimesOut(player));
    }

    IEnumerator waitUntilTimesOut(int player)
    {
        if (player == 1)
        {
            yield return new WaitUntil(() => !isCountDown && SystemController.currentState == SystemController.GameState.P1_Action);
        }
        else
        {
            yield return new WaitUntil(() => !isCountDown && SystemController.currentState == SystemController.GameState.P2_Action);
        }
        endActionButton.onClick.Invoke();
        StartCoroutine(waitUntilTimesOut(player));
    }

    void checkTurn(int turn)
    {
        turn_text.text = turn.ToString();
    }

    void checkPhase(SystemController.GameState gameState)
    {
        if(gameState == SystemController.GameState.Drop && phase_text.text != "Dropping Phase")
        {
            phase_text.text = "Dropping Phase";
        }
        else if (gameState == SystemController.GameState.P1_Action)
        {
            if (phase_text.text != "YOUR TURN" || phase_text.text != "OPPONENT's TURN")
            {
                setText(1);
            }
        }
        else if (gameState == SystemController.GameState.P2_Action && phase_text.text != "P2's Action Phase")
        {
            if (phase_text.text != "YOUR TURN" || phase_text.text != "OPPONENT's TURN")
            {
                setText(2);
            }
        }
        else if (gameState == SystemController.GameState.Battle && phase_text.text != "Battle Phase")
        {
            phase_text.text = "Battle Phase";
        }
        else if (gameState == SystemController.GameState.Game_Over)
        {
            phase_text.text = "Game Over";
        }
    }

    IEnumerator waitUntilFadePanel(SystemController.GameState gameState)
    {
        yield return new WaitUntil(() => SystemController.currentState != gameState);
        if ((SystemController.currentState == SystemController.GameState.P1_Action && player == 1) || (SystemController.currentState == SystemController.GameState.P2_Action && player == 2) || (SystemController.currentState == SystemController.GameState.Battle))
        {
            fadePanel.SetActive(true);
            fadePanel.GetComponent<FadePanel>().isFade = true;
        }

        StartCoroutine(waitUntilFadePanel(SystemController.currentState));
    }

    void setText(int player_check)
    {
        if (player == player_check)
        {
            phase_text.text = "YOUR TURN";
        }
        else
        {
            phase_text.text = "OPPONENT's TURN";
        }
    }

    void canClickCheck(SystemController.GameState gameState)
    {
        if (systemController.isStartSpawn)
        {
            if (player == 1 && gameState == SystemController.GameState.P1_Action && !fadePanel.GetComponent<FadePanel>().isFade)
            {
                canClick = true;
                endActionImage.sprite = endAction_sprite_normal;
                endActonText.color = new Color(1f, 1f, 1f, 1f);
            }
            else if (player == 2 && gameState == SystemController.GameState.P2_Action && !fadePanel.GetComponent<FadePanel>().isFade)
            {
                canClick = true;
                endActionImage.sprite = endAction_sprite_normal;
                endActonText.color = new Color(1f, 1f, 1f, 1f);
            }
            else
            {
                canClick = false;
                endActionImage.sprite = endAction_sprite_pressed;
                endActonText.color = new Color(0f, 0f, 0f, 1f);
            }
        }
        else
        {
            canClick = false;
            endActionImage.sprite = endAction_sprite_pressed;
            endActonText.color = new Color(0f, 0f, 0f, 1f);
        }
    }

    //used in button
    public void endAction()
    {
        if (canClick)
        {
            GetComponent<PhotonView>().RPC("rpc_changeState", RpcTarget.All, player);
        }
    }

    [PunRPC]
    void rpc_changeState(int player)
    {
        if(player == 1)
        {
            if(SystemController.firstTurnPlayer == 1)
            {
                SystemController.currentState = SystemController.GameState.P2_Action;
            }
            else
            {
                SystemController.currentState = SystemController.GameState.Battle;
            }
        }
        else
        {
            if (SystemController.firstTurnPlayer == 2)
            {
                SystemController.currentState = SystemController.GameState.P1_Action;
            }
            else
            {
                SystemController.currentState = SystemController.GameState.Battle;
            }
        }
    }

    /*void showDelay()
    {
        if(SystemController.currentState == SystemController.GameState.Battle)
        {
            delay_text.enabled = true;
            delay_text.text = "Delay " + BattlePhase.delay;
        }
        else
        {
            delay_text.enabled = false;
        }
    }*/

    void setOurTextList()//Apply to Start
    {
        for(int i = 0; i < systemController.character_list.Count; i++)
        {
            CharacterController cc = systemController.character_list[i].GetComponent<CharacterController>();
            //our_name_list[i].text = cc.name;
            our_health_list[i].text = cc.health.ToString();
            current_health[i] = cc.health;

            ourHealthBar_List[i].maxValue = current_health[i]; // Set MAX Health when Start The Game
            ourHealthBar_List[i].value = current_health[i]; // Set Health when Start The Game
            GetComponent<PhotonView>().RPC("rpc_setOpponentMaxHealth", RpcTarget.Others, i, current_health[i]);
            GetComponent<PhotonView>().RPC("rpc_setOpponentHealthList", RpcTarget.Others, i, current_health[i]);

            GetComponent<PhotonView>().RPC("rpc_setOpponentTextList", RpcTarget.Others, i, cc.name, cc.health);
        }
    }

    [PunRPC]
    void rpc_setOpponentTextList(int i, string name, int health)
    {
        //opponent_name_list[i].text = name;
        opponent_health_list[i].text = health.ToString();
    }

    /*================= SET HEALTHBAR VALUES =================*/
    [PunRPC] // Update Health
    void rpc_setOpponentHealthList(int i, int health)
    {
        opponentHealthBar_List[i].value = health;
    }

    [PunRPC] // Start Health
    void rpc_setOpponentMaxHealth(int i, int maxHealth)
    {
        opponentHealthBar_List[i].maxValue = maxHealth;
    }



    void healthCheck()//Apply to Update
    {
        for(int i = 0; i < systemController.character_list.Count; i++)
        {
            if(current_health[i] != systemController.character_list[i].GetComponent<CharacterController>().health)
            {
                current_health[i] = systemController.character_list[i].GetComponent<CharacterController>().health;
                setHealth(i);
            }
        }
    }

    void setHealth(int index)// HealthCheck
    {
        ourHealthBar_List[index].value = current_health[index];// Update Health 
        GetComponent<PhotonView>().RPC("rpc_setOpponentHealthList", RpcTarget.Others, index, current_health[index]);

        our_health_list[index].text = current_health[index].ToString();
        GetComponent<PhotonView>().RPC("rpc_setOpponentHealth", RpcTarget.Others, index, current_health[index]);
    }

    [PunRPC]
    void rpc_setOpponentHealth(int index, int health)
    {
        opponent_health_list[index].text = health.ToString();
    }

    public void askingPanel_yes()
    {
        if(asking_character!= null)
        {
            CharacterController cc_asking = asking_character.GetComponent<CharacterController>();
            //asking_text.text = "CANCEL STEADY";
            cc_asking.clearAllAction();
            closeAskingPanel();
        }
    }

    public void closeAskingPanel()
    {
        isActive_askingPanel = false;
        asking_character = null;
    }

    void askingPanelCheck()
    {
        if (isActive_askingPanel)
        {
            if((player == 1 && SystemController.currentState != SystemController.GameState.P1_Action) || (player == 2 && SystemController.currentState != SystemController.GameState.P2_Action))
            {
                closeAskingPanel();
            }
        }
    }

    //close active all big icons
    void resetBigIcon()
    {
        bigIcon_border.SetActive(false);
        foreach(GameObject obj in bigIcon_list )
        {
            obj.SetActive(false);
        }
        foreach(GameObject obj in race_icon_list)
        {
            obj.SetActive(false);
        }
    }

    void setBigIcon()
    {
        if(RaycastCheck.clickingCharacter)
        {
            bigIcon_border.SetActive(true);
            CharacterController cc = RaycastCheck.clickingCharacter.GetComponent<CharacterController>();
            int character_number = cc.character_number;
            for(int i = 0; i< bigIcon_list.Count; i++)
            {
                if(i == character_number - 1)
                {
                    bigIcon_list[i].SetActive(true);
                }
                else
                {
                    bigIcon_list[i].SetActive(false);
                }
            }

            step_text.enabled = true;
            setBigIcon_text(cc.gameObject);
            setRaceIcon(cc.race);
        }
        else
        {
            resetBigIcon();
            step_text.enabled = false;
        }
    }

    void setRaceIcon(string race_name)
    {
        int index = 0;
        if(race_name == "Human")
        {
            index = 0;
        }
        else if (race_name == "Elf")
        {
            index = 1;
        }
        else if (race_name == "Beast")
        {
            index = 2;
        }

        for(int i = 0; i < race_icon_list.Count; i++)
        {
            if(index == i)
            {
                race_icon_list[i].SetActive(true);
            }
            else
            {
                race_icon_list[i].SetActive(false);
            }
        }
    }

    void setBigIcon_text(GameObject obj)
    {
        CharacterController cc = obj.GetComponent<CharacterController>();
        CharactereAction ca = obj.GetComponent<CharactereAction>();
        if (ca.turn_to_recover_stun > 0)
        {
            step_text.text = "Stun Left : " + (ca.turn_to_recover_stun - SystemController.turn);
        }
        else if (ca.turn_to_recover_skill > 0)
        {
            step_text.text = "Skill Length : " + (ca.turn_to_recover_skill - SystemController.turn);
        }
        else
        {
            int stepDisplay = cc.maxStep - cc.currentStep;
            if (stepDisplay > 0)
            {
                step_text.text = "Move Left : " + stepDisplay;
            }
            else
            {
                step_text.text = "Move Left : -";
            }
        }
    }

    IEnumerator setActiveSurrender()
    {
        yield return new WaitUntil(() => systemController.canSurrender);
        surrenderButton.SetActive(true);
    }

    public void surrender()
    {
        if (aliveCharacter())
        {
            foreach(GameObject obj in systemController.character_list)
            {
                CharacterController cc = obj.GetComponent<CharacterController>();
                if(cc.health > 0)
                {
                    cc.health = 0;
                    cc.startDie();
                }
            }
            StartCoroutine(waitForSurrender());
        }
    }

    GameObject aliveCharacter()
    {
        foreach (GameObject obj in systemController.character_list)
        {
            CharacterController cc = obj.GetComponent<CharacterController>();
            if (cc.health > 0)
            {
                return obj;
            }
        }
        return null;
    }

    IEnumerator waitForSurrender()
    {
        yield return new WaitForSecondsRealtime(1.5f);
        systemController.setEndCondition(-1);
        SystemController.currentState = SystemController.GameState.Game_Over;
    }

    IEnumerator waitForStart()
    {
        yield return new WaitForSecondsRealtime(systemController.startingDelay);
        setOurTextList();

        TimeCount(systemController.dropTime);
    }
    void SetName()
    {
        youText.text = PhotonNetwork.NickName;
        GetComponent<PhotonView>().RPC("RPC_SetName", RpcTarget.Others, PhotonNetwork.NickName);
    }
    [PunRPC]
    void RPC_SetName(string name)
    {
        opponentText.text = name;
    }

    IEnumerator waitUntilSkipTurn()
    {
        if (canClick)
        {
            yield return new WaitUntil(() => !canClick);
        }
        else
        {
            yield return new WaitUntil(() => canClick);
            if (isSkipTurn())
            {
                timeCount = 0.1f;
            }
        }
        StartCoroutine(waitUntilSkipTurn());
    }

    bool isSkipTurn()
    {
        foreach(GameObject obj in systemController.character_list)
        {
            if (obj.GetComponent<CharacterController>().canMove)
            {
                return false;
            }
        }
        return true;
    }
}
