﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class EyeUI : MonoBehaviour
{
    public bool isP1Action;
    public bool isP2Action;
    private SystemController systemController;
    public static bool isEye;
    [SerializeField]
    private GameObject blackPanel;
    [SerializeField]
    private Vector2 bigScale;
    private Vector2 startingScale;
    private float alpha = 1f;
    [SerializeField]
    private float lerpSpeed;
    private bool isIncrease;
    [SerializeField]
    private GameObject middle_leg;
    [SerializeField]
    private GameObject two_leg;
    private ActionLogController actionLogController;
    // Start is called before the first frame update
    void Start()
    {
        startingScale = transform.localScale;
        isEye = false;
        isIncrease = false;
        isP1Action = false;
        isP2Action = false;
        systemController = SystemController.systemController.GetComponent<SystemController>();
        actionLogController = ActionLogController.actionLogController.GetComponent<ActionLogController>();
        blackPanel.SetActive(false);
        StartCoroutine(waitUntilShowEye());
    }

    // Update is called once per frame
    void Update()
    {
        bool isActive = (isP1Action || isP2Action) && SystemController.currentState != SystemController.GameState.Battle && isLogStayStill();
        setPos();
        GetComponent<SpriteRenderer>().enabled = isActive;
        GetComponent<Collider2D>().enabled = isActive;
        if (isEye)
        {
            lerpingRenderer();
            if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                OnMouseDown();
            }
        }
        if (!isActive)
        {
            two_leg.SetActive(false);
            middle_leg.SetActive(false);
        }
        GetComponent<SpriteRenderer>().color = new Color(alpha, alpha, alpha, 1f);
    }

    IEnumerator waitUntilShowEye()
    {
        if (SystemController.currentState == SystemController.GameState.P1_Action || SystemController.currentState == SystemController.GameState.P2_Action)
        {
            yield return new WaitUntil(() => SystemController.currentState == SystemController.GameState.Battle);
            if (isEye)
            {
                OnMouseDown();
            }
        }
        else
        {
            yield return new WaitUntil(() => SystemController.currentState == SystemController.GameState.P1_Action || SystemController.currentState == SystemController.GameState.P2_Action);
            //Debug.Log("Has Character Action = " + systemController.actionCharacter() != null);
            GetComponent<PhotonView>().RPC("rpc_setAction", RpcTarget.All, SystemController.player, systemController.actionCharacter() != null);
        }

        StartCoroutine(waitUntilShowEye());
    }

    [PunRPC]
    void rpc_setAction(int player, bool set)
    {
        if (player == 1)
        {
            isP1Action = set;
        }
        else if (player == 2)
        {
            isP2Action = set;
        }
    }
    

    private void OnMouseDown()
    {
        if (!isEye)
        {
            //transform.localScale = bigScale;
            isEye = true;
            blackPanel.SetActive(true);
            RaycastCheck.canClickCharacter = false;
        }
        else
        {
            //transform.localScale = startingScale;
            isEye = false;
            blackPanel.SetActive(false);
            RaycastCheck.canClickCharacter = true;
            alpha = 1f;
            isIncrease = false;
        }
    }

    void lerpingRenderer()
    {
        if (isIncrease)
        {
            alpha += lerpSpeed * Time.deltaTime;
            if(alpha > 1f)
            {
                alpha = 1f;
                isIncrease = false;
            }
        }
        else
        {
            alpha -= lerpSpeed * Time.deltaTime;
            if (alpha < 0.4f)
            {
                alpha = 0.4f;
                isIncrease = true;
            }
        }
    }

    void setPos()
    {
        if(isP1Action || isP2Action)
        {
            float y;
            if (isP1Action && isP2Action)
            {
                y = (actionLogController.SpawnPoint[0].y + actionLogController.SpawnPoint[1].y) / 2f;

                two_leg.SetActive(true);
                middle_leg.SetActive(false);
            }
            else
            {
                y = findPos((isP1Action && SystemController.player == 1) || (isP2Action && SystemController.player == 2));

                two_leg.SetActive(false);
                middle_leg.SetActive(true);
            }
            transform.localPosition = new Vector3(transform.position.x, y, transform.position.z);
        }
        else
        {
            two_leg.SetActive(false);
            middle_leg.SetActive(false);
        }
    }

    float findPos(bool isOwner)
    {
        if (isOwner)
        {
            foreach (GameObject obj in actionLogController.actionLog_list)
            {
                if (obj.GetComponent<ActionLog>().owner)
                {
                    return actionLogController.SpawnPoint[obj.GetComponent<ActionLog>().index].y;
                }
            }
        }
        else
        {
            foreach (GameObject obj in actionLogController.actionLog_list)
            {
                if (!obj.GetComponent<ActionLog>().owner)
                {
                    return actionLogController.SpawnPoint[obj.GetComponent<ActionLog>().index].y;
                }
            }
        }
        return 0f;
    }

    bool isLogStayStill()
    {
        foreach (GameObject obj in actionLogController.actionLog_list)
        {
            ActionLog actionLog = obj.GetComponent<ActionLog>();
            if (actionLog.isMoving || actionLog.goMove)
            {
                return false;
            }
        }
        return true;
    }
}
