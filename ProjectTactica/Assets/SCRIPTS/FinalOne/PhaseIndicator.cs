﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhaseIndicator : MonoBehaviour
{
    public SystemController.GameState isState;
    [SerializeField]
    private string battlephase;
    [SerializeField]
    private string DropPhase;
    [SerializeField]
    private string yourRound;
    [SerializeField]
    private string opponentRound;
    [SerializeField]
    public Text indicatorText;
    public int round;
    [SerializeField]
    private GameObject disaster;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    // Update is called once per frame
    void Update()
    {
        disaster.SetActive(isState == SystemController.GameState.Battle && round % 5 == 0 && round != 0);
        if(isState == SystemController.GameState.Battle && indicatorText.text != battlephase)
        {
            indicatorText.text = battlephase;
        }
        else if(isState == SystemController.GameState.P1_Action && (indicatorText.text != yourRound || indicatorText.text != opponentRound))
        {
            if (SystemController.player == 1)
            {
                indicatorText.text = yourRound;
            }
            else
            {
                indicatorText.text = opponentRound;
            }
        }
        else if (isState == SystemController.GameState.P2_Action && (indicatorText.text != yourRound || indicatorText.text != opponentRound))
        {
            if (SystemController.player == 1)
            {
                indicatorText.text = opponentRound;
            }
            else
            {
                indicatorText.text = yourRound;
            }
        }
        else if(isState == SystemController.GameState.Drop && (indicatorText.text != DropPhase))
        {
            indicatorText.text = DropPhase;
        }
    }
}
