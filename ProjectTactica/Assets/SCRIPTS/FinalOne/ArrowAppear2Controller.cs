﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArrowAppear2Controller : MonoBehaviour
{
    private float time = 0f;
    public static float alpha = 1f;
    [SerializeField]
    private float blinkDelay;
    [SerializeField]
    private List<GameObject> arrow;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Checking())
        {
            time += Time.deltaTime;
            if (alpha == 0f)
            {
                if (time >= blinkDelay)
                {
                    time = 0f;
                    alpha = 1f;
                }
            }
            else if (alpha == 1f)
            {
                if (time >= blinkDelay * 2f)
                {
                    time = 0f;
                    alpha = 0f;
                }
            }
        }
        else
        {
            time = 0f;
            alpha = 1f;
        }
    }
    bool Checking()
    {
        for (int i = 0; i < arrow.Count; i++)
        {
            if (arrow[i].GetComponent<Image>().enabled)
            {
                return true;
            }
        }
        return false;
    }
}
