﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;

public class Skill_Cap : MonoBehaviour
{
    private BattlePhase battlePhase;
    private SystemController systemController;
    //character cannon
    [SerializeField]
    private float effect_delay;
    [SerializeField]
    private GameObject speedLine;
    [SerializeField]
    private GameObject character_cannon;
    // Start is called before the first frame update
    void Start()
    {
        battlePhase = BattlePhase.battlePhase.GetComponent<BattlePhase>();
        systemController = SystemController.systemController.GetComponent<SystemController>();
        StartCoroutine(waitUntilSkill());
    }

    IEnumerator waitUntilSkill()
    {
        CharacterController cc = GetComponent<CharacterController>();
        if (cc.isSkill)
        {
            yield return new WaitUntil(() => !cc.isSkill);
        }
        else
        {
            yield return new WaitUntil(() => cc.isSkill);
            spawnCharacterCannon();
        }
        StartCoroutine(waitUntilSkill());
    }

    void spawnCharacterCannon()
    {
        PhotonNetwork.Instantiate(Path.Combine("Prefab", speedLine.name), Vector2.zero, Quaternion.identity);
        PhotonNetwork.Instantiate(Path.Combine("Prefab", character_cannon.name), Vector2.zero, Quaternion.identity);
        StartCoroutine(waitForCharacterEffect());
    }

    IEnumerator waitForCharacterEffect()
    {
        yield return new WaitForSecondsRealtime(effect_delay);
        //use the skill
        clearing();
    }

    void clearing()
    {
        GetComponent<PhotonView>().RPC("rpc_cancelAllAction", RpcTarget.Others);
        finish_clearing();
    }

    void finish_clearing()
    {
        CharacterController cc = GetComponent<CharacterController>();
        cc.clearAllAction();
        battlePhase.actionCheck_complete(cc.player);
    }

    [PunRPC]
    void rpc_cancelAllAction()
    {
        foreach (GameObject obj in systemController.character_list)
        {
            obj.GetComponent<CharacterController>().clearAllAction();
        }
    }
}
