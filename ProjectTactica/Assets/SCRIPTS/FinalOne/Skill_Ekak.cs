﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;

public class Skill_Ekak : MonoBehaviour
{
    [SerializeField]
    private int turnLength;
    //character cannon
    [SerializeField]
    private float effect_delay;
    [SerializeField]
    private GameObject speedLine;
    [SerializeField]
    private GameObject character_cannon;
    //==========================================
    private int turn_to_stop_skill;
    private BattlePhase battlePhase;
    // Start is called before the first frame update
    void Start()
    {
        battlePhase = BattlePhase.battlePhase.GetComponent<BattlePhase>();
        StartCoroutine(waitUntilSkill());
    }

    IEnumerator waitUntilSkill()
    {
        CharacterController cc = GetComponent<CharacterController>();
        if (cc.isSkill)
        {
            yield return new WaitUntil(() => !cc.isSkill);
        }
        else
        {
            yield return new WaitUntil(() => cc.isSkill);
            spawnCharacterCannon();
        }
        StartCoroutine(waitUntilSkill());
    }

    IEnumerator waitUntilStopSkill()
    {
        CharacterController cc = GetComponent<CharacterController>();
        yield return new WaitUntil(() => SystemController.turn == turn_to_stop_skill || !cc.isSkill);
        if (cc.isSkill)
        {
            cc.clearAllAction();
        }
        GetComponent<PhotonView>().RPC("rpc_setIsEkakSkill", RpcTarget.Others, false);
        GetComponent<PhotonView>().RPC("rpc_set_recover_skill", RpcTarget.All, 0);
    }

    [PunRPC]
    void rpc_setIsEkakSkill(bool set)
    {
        SystemController.isEkakSkill = set;
    }

    [PunRPC]
    void rpc_set_recover_skill(int turn)
    {
        GetComponent<CharactereAction>().turn_to_recover_skill = turn;
    }

    void spawnCharacterCannon()
    {
        PhotonNetwork.Instantiate(Path.Combine("Prefab", speedLine.name), Vector2.zero, Quaternion.identity);
        PhotonNetwork.Instantiate(Path.Combine("Prefab", character_cannon.name), Vector2.zero, Quaternion.identity);
        StartCoroutine(waitForCharacterEffect());
    }

    IEnumerator waitForCharacterEffect()
    {
        yield return new WaitForSecondsRealtime(effect_delay);
        CharacterController cc = GetComponent<CharacterController>();
        //use the skill
        GetComponent<PhotonView>().RPC("rpc_setIsEkakSkill", RpcTarget.Others, true);
        turn_to_stop_skill = SystemController.turn + turnLength;
        GetComponent<PhotonView>().RPC("rpc_set_recover_skill", RpcTarget.All, turn_to_stop_skill);
        StartCoroutine(waitUntilStopSkill());
        battlePhase.actionCheck_complete(cc.player);
        cc.turnToMove = 0;
    }
}
