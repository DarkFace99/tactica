﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class BaseAttack : MonoBehaviour
{
    [SerializeField]
    private GameObject attackingMark;
    //=======================================
    public int row; //current row use to mark attack
    public int col; //current column use to mark attack
    private SystemController systemController;
    private GridBehaviour1 gridBehavior1;
    private GameObject controlling_obj;
    private int baseAttackType; // 1 = melee // 2 = range
    public int direction; //direction set by four direction script
    public bool isMouseOver;
    public bool isConfirm; //confirm the attack
    
    void Start()
    {
        systemController = SystemController.systemController.GetComponent<SystemController>(); //SystemController
        gridBehavior1 = GridBehaviour1.gridBehaviour.GetComponent<GridBehaviour1>(); //GridBehavior1
        controlling_obj = systemController.controllingCharacter(); //controlling character
        baseAttackType = controlling_obj.GetComponent<CharacterController>().baseAttackType; //get base attack type
        isConfirm = false;
    }

    //when mouse is enter
    public void onMouseEnter()
    {
        if (controlling_obj.GetComponent<CharacterController>().attacking_mark_list.Count == 0)
        {
            isMouseOver = true;
            if(baseAttackType == 1)
            {
                spawnAttackingMark_melee(direction); //spawn melee attaack's mark
            }
            else if (baseAttackType == 2)
            {
                spawnAttackingMark_range(direction); //spawn range attack's mark
            }
        }
    }

    public void onMouseDown()
    {
        if (!isConfirm)
        {
            isConfirm = true;
        }
    }

    //when mouse is exit
    public void onMouseExit()
    {
        if(controlling_obj.GetComponent<CharacterController>().attacking_mark_list.Count > 0)
        {
            isMouseOver = false;
            if (!isConfirm)
            {
                controlling_obj.GetComponent<CharacterController>().clearAttackingMark(); //clear spawned attacking mark from CharacterController's list
            }
        }
    }

    //======================================SPAWN_MELEE_ATTACK_MARK======================================
    //spawning depend on the direction set by four direction's script
    //spawn as networking object
    //always check whether the grid existed
    void spawnAttackingMark_melee(int direction)
    {
        if (direction == 1) //spawn above
        {
            if (row - 1 > -1 && col - 1 > -1)
            {
                GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", attackingMark.name), gridBehavior1.gridArr[row - 1, col - 1].transform.localPosition, Quaternion.identity);
                setAttackingMarkValue(obj, gridBehavior1.gridArr[row - 1, col - 1]);
            }
            if (row - 1 > -1) 
            {
                GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", attackingMark.name), gridBehavior1.gridArr[row - 1, col].transform.localPosition, Quaternion.identity);
                setAttackingMarkValue(obj, gridBehavior1.gridArr[row - 1, col]);
            }
            if (row - 1 > -1 && col + 1 < gridBehavior1.colTotal)
            {
                GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", attackingMark.name), gridBehavior1.gridArr[row - 1, col + 1].transform.localPosition, Quaternion.identity);
                setAttackingMarkValue(obj, gridBehavior1.gridArr[row - 1, col + 1]);
            }
        }
        else if (direction == 2) //spawn right
        {
            if (row - 1 > -1 && col + 1 < gridBehavior1.colTotal)
            {
                GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", attackingMark.name), gridBehavior1.gridArr[row - 1, col + 1].transform.localPosition, Quaternion.identity);
                setAttackingMarkValue(obj, gridBehavior1.gridArr[row - 1, col + 1]);
            }
            if (col + 1 < gridBehavior1.colTotal)
            {
                GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", attackingMark.name), gridBehavior1.gridArr[row, col + 1].transform.localPosition, Quaternion.identity);
                setAttackingMarkValue(obj, gridBehavior1.gridArr[row, col + 1]);
            }
            if (row + 1 < gridBehavior1.rowTotal && col + 1 < gridBehavior1.colTotal)
            {
                GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", attackingMark.name), gridBehavior1.gridArr[row + 1, col + 1].transform.localPosition, Quaternion.identity);
                setAttackingMarkValue(obj, gridBehavior1.gridArr[row + 1, col + 1]);
            }
        }
        else if (direction == 3) //spawn below
        {
            if (row + 1 < gridBehavior1.colTotal && col - 1 > -1)
            {
                GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", attackingMark.name), gridBehavior1.gridArr[row + 1, col - 1].transform.localPosition, Quaternion.identity);
                setAttackingMarkValue(obj, gridBehavior1.gridArr[row + 1, col - 1]);
            }
            if (row + 1 < gridBehavior1.colTotal)
            {
                GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", attackingMark.name), gridBehavior1.gridArr[row + 1, col].transform.localPosition, Quaternion.identity);
                setAttackingMarkValue(obj, gridBehavior1.gridArr[row + 1, col]);
            }
            if (row + 1 < gridBehavior1.colTotal && col + 1 < gridBehavior1.colTotal)
            {
                GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", attackingMark.name), gridBehavior1.gridArr[row + 1, col + 1].transform.localPosition, Quaternion.identity);
                setAttackingMarkValue(obj, gridBehavior1.gridArr[row + 1, col + 1]);
            }
        }
        else if (direction == 4) //spawn left
        {
            if (row - 1 > -1 && col - 1 > -1)
            {
                GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", attackingMark.name), gridBehavior1.gridArr[row - 1, col - 1].transform.localPosition, Quaternion.identity);
                setAttackingMarkValue(obj, gridBehavior1.gridArr[row - 1, col - 1]);
            }
            if (col - 1 > -1)
            {
                GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", attackingMark.name), gridBehavior1.gridArr[row, col - 1].transform.localPosition, Quaternion.identity);
                setAttackingMarkValue(obj, gridBehavior1.gridArr[row, col - 1]);
            }
            if (row + 1 < gridBehavior1.rowTotal && col - 1 > -1)
            {
                GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", attackingMark.name), gridBehavior1.gridArr[row + 1, col - 1].transform.localPosition, Quaternion.identity);
                setAttackingMarkValue(obj, gridBehavior1.gridArr[row + 1, col - 1]);
            }
        }
    }

    //======================================SPAWN_RANGE_ATTACK_MARK======================================
    //spawning depend on the direction set by four direction's script
    //spawn as networking object
    //always check whether the grid existed
    void spawnAttackingMark_range(int direction)
    {
        if(direction == 1) //spawn above
        {
            for(int i = 1; i < 4; i++)
            {
                if(row - i > -1)
                {
                    GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", attackingMark.name), gridBehavior1.gridArr[row - i, col].transform.localPosition, Quaternion.identity);
                    setAttackingMarkValue(obj, gridBehavior1.gridArr[row - i, col]);
                }
                else
                {
                    break;
                }
            }
        }
        else if(direction == 2) //spawn right
        {
            for (int i = 1; i < 4; i++)
            {
                if (col + i < gridBehavior1.colTotal)
                {
                    GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", attackingMark.name), gridBehavior1.gridArr[row, col + i].transform.localPosition, Quaternion.identity);
                    setAttackingMarkValue(obj, gridBehavior1.gridArr[row, col + i]);
                }
                else
                {
                    break;
                }
            }
        }
        else if(direction == 3) //spawn below
        {
            for (int i = 1; i < 4; i++)
            {
                if (row + i < gridBehavior1.rowTotal)
                {
                    GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", attackingMark.name), gridBehavior1.gridArr[row + i, col].transform.localPosition, Quaternion.identity);
                    setAttackingMarkValue(obj, gridBehavior1.gridArr[row + i, col]);
                }
                else
                {
                    break;
                }
            }
        }
        else if (direction == 4) //spawn left
        {
            for (int i = 1; i < 4; i++)
            {
                if (col - i > -1)
                {
                    GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", attackingMark.name), gridBehavior1.gridArr[row, col - i].transform.localPosition, Quaternion.identity);
                    setAttackingMarkValue(obj, gridBehavior1.gridArr[row, col - i]);
                }
                else
                {
                    break;
                }
            }
        }
    }

    //add attacking mark data to CharacterController
    void setAttackingMarkValue(GameObject mark, GameObject grid)
    {
        CharacterController cc = controlling_obj.GetComponent<CharacterController>();
        cc.attacking_mark_list.Add(mark); //add mark to normal attacking mark list
        cc.attacking_grid_list.Add(grid); //add grid to normal attacking grid list
        cc.attack_direction = direction; //add attack direction to normal attack direction variable
        mark.GetComponent<RendererDifferent>().characterIndex = cc.characterIndex;
        if (!mark.GetComponent<PhotonView>().IsMine)
        {
            mark.GetComponent<SpriteRenderer>().enabled = false; //disable renderer if it's not our object
        }
    }
}
