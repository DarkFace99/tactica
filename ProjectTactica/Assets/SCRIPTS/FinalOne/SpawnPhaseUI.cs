﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPhaseUI : MonoBehaviour
{
    [SerializeField]
    private Vector2 spawnPos;
    [SerializeField]
    private GameObject action_phase;
    [SerializeField]
    private GameObject battle_phase;
    private GameObject spawnedObj = null;
    [SerializeField]
    private AudioController sound;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(waitUntilYourTurn());
    }

    IEnumerator waitUntilYourTurn()
    {
        yield return new WaitUntil(() => (SystemController.currentState == SystemController.GameState.P1_Action && SystemController.player == 1) || (SystemController.currentState == SystemController.GameState.P2_Action && SystemController.player == 2));
        spawnPhaseUI(action_phase);
        sound.audio[3].Play();
        StartCoroutine(waitUntilBattlePhase());
    }

    IEnumerator waitUntilBattlePhase()
    {
        yield return new WaitUntil(() => SystemController.currentState == SystemController.GameState.Battle);
        spawnPhaseUI(battle_phase);
        sound.audio[2].Play();
        StartCoroutine(waitUntilYourTurn());
    }

    void spawnPhaseUI(GameObject obj)
    {
        if (spawnedObj)
        {
            Destroy(spawnedObj);
        }
        spawnedObj = Instantiate(obj, spawnPos, Quaternion.identity);
    }
    
}
