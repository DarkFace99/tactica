﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArrowAppear2 : MonoBehaviour
{
    [SerializeField]
    private int ID;
    [SerializeField]
    private float blinkDelay;
    [SerializeField]
    private DropZone dropZone;
    private SystemController systemController;
    
    // Start is called before the first frame update
    void Start()
    {
        systemController = SystemController.systemController.GetComponent<SystemController>();
        
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Image>().enabled = isChecking();
        //if (GetComponent<Image>().enabled)
        //{
        //    time += Time.deltaTime;
        //    if (alpha == 0f)
        //    {
        //        if (time >= blinkDelay)
        //        {
        //            time = 0f;
        //            alpha = 1f;
        //        }
        //    }
        //    else if (alpha == 1f)
        //    {
        //        if (time >= blinkDelay * 2f)
        //        {
        //            time = 0f;
        //            alpha = 0f;
        //        }
        //    }
        //    GetComponent<Image>().color = new Color(GetComponent<Image>().color.r, GetComponent<Image>().color.g, GetComponent<Image>().color.b, alpha);
        //}
        //else
        //{
        //    time = 0f;
        //    alpha = 1f;
        //}
        if(GetComponent<Image>().enabled)
        {
            GetComponent<Image>().color = new Color(GetComponent<Image>().color.r, GetComponent<Image>().color.g, GetComponent<Image>().color.b, ArrowAppear2Controller.alpha);
        }
    }
    bool isChecking()
    {
        for(int i = 0; i < dropZone.unitDropIcons.Count; i++)
        {
            if(!dropZone.unitDropIcons[i])
            {
                return false;
            }
            else if(dropZone.unitDropIcons[i].isSelecting)
            {
                return false;
            }
        }
        if (dropZone.unitDropIcons[ID].alreadyDeploy)
        {
            return false;
        }
        return true;
    }
    //public void ToSetObject(int i)
    //{
    //    if(dropZone.unitDropIcons[i].alreadyDeploy)
    //    {
    //        systemController.arrow1[i].SetActive(false);
    //    }
    //    else
    //    {
    //        systemController.arrow1[i].SetActive(true);
    //    }
    //}
}