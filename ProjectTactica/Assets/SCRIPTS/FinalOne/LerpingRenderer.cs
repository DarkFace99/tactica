﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpingRenderer : MonoBehaviour
{
    public bool isLerp;
    [SerializeField]
    private float lerpingSpeed;
    [SerializeField]
    private float minAlpha;
    //===========================
    private float maxAlpha;
    private float currentAlpha;
    private bool isIncrease;
    private SpriteRenderer sr;

    private void Start()
    {
        maxAlpha = GetComponent<SpriteRenderer>().color.a;
        currentAlpha = maxAlpha;
        isIncrease = false;
        sr = GetComponent<SpriteRenderer>();
        if (GetComponent<PhotonView>().IsMine && !isLerp)
        {
            StartCoroutine(waitUntilLerp());
        }
    }

    private void Update()
    {
        if (isLerp)
        {
            colorChange();
        }
    }

    void colorChange()
    {
        if (isIncrease)
        {
            currentAlpha += lerpingSpeed * Time.deltaTime;
            if (currentAlpha >= maxAlpha)
            {
                isIncrease = false;
                currentAlpha = maxAlpha;
            }
        }
        else
        {
            currentAlpha -= lerpingSpeed * Time.deltaTime;
            if (currentAlpha <= minAlpha)
            {
                isIncrease = true;
                currentAlpha = minAlpha;
            }
        }
        sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, currentAlpha);
    }

    IEnumerator waitUntilLerp()
    {
        yield return new WaitUntil(() => isLerp);
        GetComponent<PhotonView>().RPC("rpc_setIsLerp", RpcTarget.Others, isLerp);
    }

    [PunRPC]
    void rpc_setIsLerp(bool set)
    {
        isLerp = set;
    }
}
