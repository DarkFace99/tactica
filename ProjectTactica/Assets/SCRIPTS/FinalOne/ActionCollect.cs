﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class ActionCollect : MonoBehaviour
{
    private SystemController systemController;
    private ActionLogController actionLogController;
    // Start is called before the first frame update
    void Start()
    {
        systemController = SystemController.systemController.GetComponent<SystemController>(); //SystemController
        actionLogController = ActionLogController.actionLogController.GetComponent<ActionLogController>();
    }

    //move action data from normal-list to confirm-list
    //used in button
    public void dataCollect()
    {
        if (UI_Controller.canClick)
        {
            GameObject controlling_obj = systemController.controllingCharacter(); //find latest controlling object
            if (controlling_obj != null)
            {
                CharacterController cc = controlling_obj.GetComponent<CharacterController>();
                if (cc.direction_list.Count > 0 || cc.attacking_mark_list.Count > 0 || cc.skill_mark_list.Count > 0)
                {
                    actionLogController.logSpawn = null;
                    cc.total_delay = 0;
                    //collectMove
                    if (cc.direction_list.Count > 0)
                    {
                        for (int i = 0; i < cc.direction_list.Count; i++)
                        {
                            cc.direction_list_confirm.Add(cc.direction_list[i]); //add moving direction from normal list to confirm list
                            cc.character_sim_list_confirm.Add(cc.character_sim_list[i]); //add character sim. from normal list to confirm list
                            setDataHiding(cc.character_sim_list[i]);
                        }
                        cc.total_delay += cc.direction_list_confirm.Count; //set moving delay = amount of moving direction list
                    }

                    //collectAttack
                    if (cc.attacking_mark_list.Count > 0)
                    {
                        for (int i = 0; i < cc.attacking_mark_list.Count; i++)
                        {
                            cc.attacking_mark_list_confirm.Add(cc.attacking_mark_list[i]); //add attacking mark from normal list to confirm list
                            cc.attacking_grid_list_confirm.Add(cc.attacking_grid_list[i]); //add attacking grid from normal list to confirm list
                            setDataHiding(cc.attacking_mark_list[i]);
                        }
                        cc.total_delay += cc.baseAttackDelay;
                        cc.attack_direction_confirm = cc.attack_direction; //assign attacking direction to confirm variable
                    }

                    else if (cc.skill_mark_list.Count > 0)
                    {
                        for (int i = 0; i < cc.skill_mark_list.Count; i++)
                        {
                            cc.skill_mark_list_confirm.Add(cc.skill_mark_list[i]); //add attacking mark from normal list to confirm list
                            cc.skill_grid_list_confirm.Add(cc.skill_grid_list[i]); //add attacking grid from normal list to confirm list
                            setDataHiding(cc.skill_mark_list[i]);
                        }
                        cc.total_delay += cc.skillDelay;
                        cc.skill_direction_confirm = cc.skill_direction;
                    }
                    
                    //set others
                    cc.turnToMove = SystemController.turn + 1; //set turn to run the action in battle phase
                    actionLogController.SetDetail(cc.total_delay,cc.character_number,actionLogController.actionLog_list.Count-1);
                    clearData(controlling_obj);
                }
                else
                {
                    cc.rightClickCancel_call();
                }
            }
        }
    }

    //clear normal-list and data which is not necessary now
    void clearData(GameObject controlling_obj)
    {
        CharacterController cc = controlling_obj.GetComponent<CharacterController>();
        cc.direction_list.Clear(); //clear normal direction list
        cc.character_sim_list.Clear(); //clear normal character sim. list
        cc.path_list.Clear(); //clear moving path list
        cc.resetMovingGuide(); //clear moving guide (if it's spawned)
        cc.attacking_mark_list.Clear(); //clear normal attacking mark list 
        cc.attacking_grid_list.Clear(); //clear normal attacking grid list
        cc.isControl = false; //set is control = false
        cc.attack_direction = 0; //set normal attack direction = 0
        cc.skill_mark_list.Clear();
        cc.skill_grid_list.Clear();
        cc.skill_direction = 0;

        //destroy each attacking ui
        foreach (GameObject obj in cc.character_ui_list)
        {
            Destroy(obj);
        }
        cc.character_ui_list.Clear(); //character ui list
        RaycastCheck.clickingCharacter = null;
    }

    void setDataHiding(GameObject obj)
    {
        obj.GetComponent<DataHiding>().isActionCollect = true;
    }
}
