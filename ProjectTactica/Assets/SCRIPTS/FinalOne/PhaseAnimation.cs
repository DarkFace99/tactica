﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhaseAnimation : MonoBehaviour
{
    public void setReverse()
    {
        GetComponent<Animator>().SetBool("isReverse", true);
    }
}
