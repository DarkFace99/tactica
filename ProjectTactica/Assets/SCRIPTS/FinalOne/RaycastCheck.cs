﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastCheck : MonoBehaviour
{
    public LayerMask layerMask_normal;
    public LayerMask layerMask_ignore_character;
    private GameObject mouseOverObject;
    public static GameObject clickingCharacter;
    public static bool canClickCharacter;
    private SystemController systemController;
    private SystemController.GameState currenState;
    // Start is called before the first frame update
    void Start()
    {
        clickingCharacter = null;
        canClickCharacter = true;
        systemController = SystemController.systemController.GetComponent<SystemController>();
        currenState = SystemController.currentState;
        StartCoroutine(waitUntilChangePhase());

    }

    // Update is called once per frame
    void Update()
    {
        //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        RaycastHit2D hit;
        if (canClickCharacter)
        {
             hit = Physics2D.Raycast(mousePos, Vector3.forward, float.MaxValue, layerMask_normal);
        }
        else
        {
            hit = Physics2D.Raycast(mousePos, Vector3.forward, float.MaxValue, layerMask_ignore_character);
        }
        
        if (hit) //hit character or moving guide blue
        {
            if(mouseOverObject != hit.collider.gameObject)
            {
                mouseExit(mouseOverObject);
                mouseOverObject = hit.collider.gameObject;
                mouseEnter(mouseOverObject);
            }
        }
        else //hit none of them
        {
            mouseExit(mouseOverObject);

            if (mouseOverObject)
            {
                mouseOverObject = null;
            }
        }

        mouseDown(mouseOverObject);
        /*if (clickingCharacter)
        {
            Debug.Log("character = " + clickingCharacter.name);
        }
        Debug.Log("canClick = " + canClickCharacter);*/
        characterClickCheck();
    }

    private void mouseExit(GameObject obj)
    {
        if (obj) //add script to call mouse ext function
        {
            CharacterController cc = obj.GetComponent<CharacterController>();
            BaseAttack ba = obj.GetComponent<BaseAttack>();
            SkillMark sm = obj.GetComponent<SkillMark>();

            //add object to call mouse exit function
            if (cc) 
            {
                cc.onMouseExit();
            }
            else if (ba)
            {
                ba.onMouseExit();
            }
            else if (sm)
            {
                sm.onMouseExit();
            }
        }
    }

    private void mouseEnter(GameObject obj)
    {
        if (obj) //add script to call mouse enter function
        {
            CharacterController cc = obj.GetComponent<CharacterController>();
            BaseAttack ba = obj.GetComponent<BaseAttack>();
            SkillMark sm = obj.GetComponent<SkillMark>();

            //add object to call mouse enter function
            if (cc) 
            {
                cc.onMouseEnter();
            }
            else if (ba)
            {
                ba.onMouseEnter();
            }
            else if (sm)
            {
                sm.onMouseEnter();
            }
        }
    }

    private void mouseDown(GameObject obj)
    {
        if (obj) //add script to call mouseDown function
        {
            CharacterController cc = obj.GetComponent<CharacterController>();
            MovementMark mm = obj.GetComponent<MovementMark>();
            FourDirectionAttackGuide fda = obj.GetComponent<FourDirectionAttackGuide>();
            BaseAttack ba = obj.GetComponent<BaseAttack>();
            SkillMark sm = obj.GetComponent<SkillMark>();

            //add object to call mouse down function
            if (Input.GetKeyDown(KeyCode.Mouse0)) 
            {
                if (cc)
                {
                    cc.onMouseDown();
                }
                else if (mm)
                {
                    mm.onMouseDown();
                }
                else if (fda)
                {
                    fda.onMouseDown();
                }
                else if (ba)
                {
                    ba.onMouseDown();
                }
                else if (sm)
                {
                    sm.onMouseDown();
                }
            }
        }
    }

    bool isOurTurn()
    {
        if ((SystemController.player == 1 && SystemController.currentState == SystemController.GameState.P1_Action) || (SystemController.player == 2 && SystemController.currentState == SystemController.GameState.P2_Action))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    IEnumerator waitUntilChangePhase()
    {
        yield return new WaitUntil(() => currenState != SystemController.currentState);
        currenState = SystemController.currentState;
        clickingCharacter = null;
        StartCoroutine(waitUntilChangePhase());
    }

    void characterClickCheck()
    {
        if (systemController.controllingCharacter())
        {
            if(systemController.controllingCharacter() != clickingCharacter)
            {
                systemController.controllingCharacter().GetComponent<CharacterController>().rightClickCancel_call();
            }
        }
    }
}
