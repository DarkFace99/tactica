﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Skill_John : MonoBehaviour
{
    //character cannon
    [SerializeField]
    private float effect_delay;
    [SerializeField]
    private GameObject speedLine;
    [SerializeField]
    private GameObject character_cannon;
    //=============================================
    public GameObject rival;
    private BattlePhase battlePhase;
    // Start is called before the first frame update
    void Start()
    {
        battlePhase = BattlePhase.battlePhase.GetComponent<BattlePhase>();
        StartCoroutine(waitUntilFoundRival());
    }

    IEnumerator waitUntilFoundRival()
    {
        if (!rival)
        {
            yield return new WaitUntil(() => rival);
            spawnCharacterCannon();
        }
        else
        {
            yield return new WaitUntil(() => !rival);
        }
        StartCoroutine(waitUntilFoundRival());
    }


    void spawnCharacterCannon()
    {
        PhotonNetwork.Instantiate(Path.Combine("Prefab", speedLine.name), Vector2.zero, Quaternion.identity);
        PhotonNetwork.Instantiate(Path.Combine("Prefab", character_cannon.name), Vector2.zero, Quaternion.identity);
        StartCoroutine(waitForCharacterEffect());
    }

    IEnumerator waitForCharacterEffect()
    {
        yield return new WaitForSecondsRealtime(effect_delay);
        skillJohn();
    }

    void skillJohn()
    {
        ActiveSkill activeSkill = rival.GetComponent<ActiveSkill>();
        CharacterController rival_cc = rival.GetComponent<CharacterController>();
        CharacterController cc = GetComponent<CharacterController>();
        if (activeSkill)
        {
            activeSkill.reCooldown_call();
            //Debug.Log("SKILL_JOHN");
        }

        rival_cc.setIsBeingAttack(true); //make opponent's character being attacked
        rival_cc.attacker = gameObject; //set this character as an attacker in opponent CharacterController script
        rival_cc.setDirection(cc.oppositeDirection(cc.current_direction));
        cc.clearAllAction();
        rival = null;
        StartCoroutine(continueSteadyCheck());
    }

    IEnumerator continueSteadyCheck()
    {
        CharacterController cc = GetComponent<CharacterController>();

        yield return new WaitForSecondsRealtime(cc.continueBattleDelay);

        battlePhase.steady_check_complete(cc.player); //complete steady check of this player
    }
}
