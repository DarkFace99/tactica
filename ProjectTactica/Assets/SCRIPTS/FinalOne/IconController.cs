﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class IconController : MonoBehaviour
{
    [SerializeField]
    private int character_index;
    [SerializeField]
    private List<GameObject> all_icon_list_our;
    [SerializeField]
    private List<GameObject> all_icon_list_opponent;
    [SerializeField]
    private List<Sprite> died_sprite_list;
    private SystemController systemController;
    // Start is called before the first frame update
    void Start()
    {
        systemController = SystemController.systemController.GetComponent<SystemController>(); //SystemController
        StartCoroutine(waitForStart());
        StartCoroutine(waitUntilDie());
    }

    void setOurIcon(int index)
    {
        all_icon_list_our[index].SetActive(true);
        systemController.icon_list[character_index] = all_icon_list_our[index];
        GetComponent<PhotonView>().RPC("setOpponentIcon", RpcTarget.Others, index);
    }

    [PunRPC]
    void setOpponentIcon(int index)
    {
        all_icon_list_opponent[index].SetActive(true);
    }

    IEnumerator waitForStart()
    {
        yield return new WaitForSecondsRealtime(systemController.startingDelay);
        setOurIcon(systemController.character_list[character_index].GetComponent<CharacterController>().character_number - 1);
    }

    IEnumerator waitUntilDie()
    {
        yield return new WaitUntil(() => systemController.character_list[character_index].GetComponent<CharacterController>().health <= 0);
        int index = systemController.character_list[character_index].GetComponent<CharacterController>().character_number - 1;
        all_icon_list_our[index].GetComponent<SpriteRenderer>().sprite = died_sprite_list[index];
        GetComponent<PhotonView>().RPC("setOpponentIcon_die", RpcTarget.Others, index);
    }

    [PunRPC]
    void setOpponentIcon_die(int index)
    {
        all_icon_list_opponent[index].GetComponent<SpriteRenderer>().sprite = died_sprite_list[index];
    }

}
