﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class ActionLog : MonoBehaviour
{
    [SerializeField]
    private TextMeshPro TurnToMoveText;
    [SerializeField]
    private TextMeshPro totalDelayText;
    [SerializeField]
    private TextMeshPro increaseDelayText;
    [SerializeField]
    private List<GameObject> Icon_list;
    
    public bool isMoving = false;
    [SerializeField]
    private float speed_up;
    [SerializeField]
    private float speed_left;
    private ActionLogController actionLogController;
    public bool goMove = false;
    public float highest;
    public int index;
    public GameObject owner;
    public int turn;
    public int total_delay;
    public int increase_delay;
    [SerializeField]
    List<float> pos_with_increase;
    [SerializeField]
    List<float> pos_without_increase;
    [SerializeField]
    private GameObject arrow;
    public GameObject actionCanceled;
    public GameObject actionCover;
    [SerializeField]
    private List<Color> delay_text_color;

    // Start is called before the first frame update
    void Start()
    {
        actionLogController = ActionLogController.actionLogController.GetComponent<ActionLogController>();
        turn = SystemController.turn + 1;

        actionLogController.actionLog_list.Add(gameObject);
        TurnToMoveText.SetText(turn.ToString());
        if (owner)
        {
            StartCoroutine(WaitForDestroy());
            StartCoroutine(waitUntilAction());
        }
        StartCoroutine(waitUntilTurnToMove());
        actionCanceled.SetActive(false);
        actionCover.SetActive(true);
        actionLogController.sortActionLog();
    }
    // Update is called once per frame
    void Update()
    {
        if(goMove)
        {
            transform.Translate(Vector2.up * speed_up * Time.deltaTime);
            if(transform.position.y >= highest)
            {
                goMove = false;
                transform.localPosition = new Vector2(transform.position.x, highest);
            }
        }
        else if (isMoving)
        {
            transform.Translate(Vector2.left * speed_left * Time.deltaTime);
        }

    }

    public void setActionLog(int delay_total, int delay_increase)
    {
        setTextColor(delay_total);
        totalDelayText.SetText(delay_total.ToString());
        setDelayIncrease(increaseDelayText, delay_increase);
        if (delay_increase > 0)
        {
            setPos(pos_with_increase);
        }
        else
        {
            setPos(pos_without_increase);
        }
        total_delay = delay_total;
        increase_delay = delay_increase;
        if (actionLogController)
        {
            actionLogController.sortActionLog();
        }
    }

    void setPos(List<float> list)
    {
        arrow.transform.localPosition = new Vector2(list[1], 0f);
        totalDelayText.rectTransform.localPosition = new Vector2(list[2], 0f);
    }

    void setDelayIncrease(TextMeshPro tmp, int number)
    {
        if (number == 0)
        {
            tmp.SetText("");
        }
        else
        {
            tmp.SetText("+" + number.ToString());
        }
    }

    public void setActionLog_SetCharacter(int characterNumber)
    {
        Icon_list[characterNumber - 1].SetActive(true);
    }

    IEnumerator WaitForDestroy()
    {
        yield return new WaitUntil(() => SystemController.turn > turn);
        actionLogController.YAY(index);
    }

    IEnumerator waitUntilAction()
    {
        if (!bingActionCheck())
        {
            yield return new WaitUntil(() => bingActionCheck());
            actionLogController.bing_call(index);
        }
        else
        {
            yield return new WaitUntil(() => !bingActionCheck());
        }
        StartCoroutine(waitUntilAction());
    }

    bool bingActionCheck()
    {
        CharacterController cc = owner.GetComponent<CharacterController>();
        return cc.isMove || cc.is_steady_base || cc.is_steady_skill || cc.isSkill;
    }

    void setTextColor(int delay)
    {
        if (delay > 7)
        {
            totalDelayText.color = delay_text_color[2];
        }
        else if (delay > 4)
        {
            totalDelayText.color = delay_text_color[1];
        }
        else if (delay > 0)
        {
            totalDelayText.color = delay_text_color[0];
        }
    }

    IEnumerator waitUntilTurnToMove()
    {
        yield return new WaitUntil(() => turn == SystemController.turn);
        actionCover.SetActive(false);
    }
}
