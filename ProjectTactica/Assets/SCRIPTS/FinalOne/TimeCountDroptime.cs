﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeCountDroptime : MonoBehaviour
{
    public Text text;
    public GameObject text2;
    private float currentTime;
    private float startingTime;
    private SystemController systemController;
    private bool isDecrease;
    // Start is called before the first frame update
    void Start()
    {
        systemController = SystemController.systemController.GetComponent<SystemController>();
        startingTime = systemController.dropTime;
        currentTime = startingTime;
        isDecrease = false;
        StartCoroutine(waitForStart());
    }

    // Update is called once per frame
    void Update()
    {
        if (isDecrease && currentTime > 0f)
        {
            currentTime -= Time.deltaTime;
            text.text = currentTime.ToString("0");
            if (currentTime <= 0f)
            {
                gameObject.SetActive(false);
                text2.SetActive(false);
            }
        }

        if(currentTime < 4f)
        {
            text.color = new Color(0.9f, 0f, 0f, 1f);
        }
    }

    IEnumerator waitForStart()
    {
        yield return new WaitForSecondsRealtime(systemController.startingDelay);
        isDecrease = true;
    }
}