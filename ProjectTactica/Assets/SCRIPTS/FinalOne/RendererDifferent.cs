﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class RendererDifferent : MonoBehaviour
{
    [SerializeField]
    private bool isSetAlpha;
    //=====================================================
    private List<Color> color_list; //set to private later
    public int characterIndex;
    private SystemController systemController;
    private PhotonView pv;
    //=========================================
    // Start is called before the first frame update
    void Start()
    {
        pv = GetComponent<PhotonView>();

        if (pv.IsMine)
        {
            pv.RPC("rpc_setCharacterIndex", RpcTarget.All, characterIndex);
            GetComponent<SpriteRenderer>().sortingOrder = 0;
        }
        else
        {
            GetComponent<SpriteRenderer>().sortingOrder = 1;
        }

        systemController = SystemController.systemController.GetComponent<SystemController>();
        setInitialColor();
        //set opponent color differently
        setColorDifferent();
    }

    void setInitialColor()
    {
        color_list = new List<Color>();
        int listCount = systemController.character_list.Count * 2;
        for (int i = 0; i < listCount; i++)
        {
            Color addingColor;
            if (i < listCount / 2)
            {
                addingColor = new Color(0, 0.8f - (0.12f * (float)(i + 1)), 1f, 0.75f);
                //Color addingColor = new Color(0, 1f, 1f, 0.5f); same color for each character
            }
            else
            {
                addingColor = new Color(1f, 0f + (0.15f * (float)(i - systemController.character_list.Count)), 0f, 0.75f);
                //Color addingColor = new Color(1f, 0f, 0f, 0.5f); same color for each character
            }

            if (!isSetAlpha)
            {
                addingColor.a = 1f;
            }
            color_list.Add(addingColor);
        }
    }

    void setColorDifferent()
    {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        if (pv != null)
        {
            if (pv.IsMine)
            {
                sr.color = color_list[characterIndex];
            }
            else
            {
                sr.color = color_list[characterIndex + systemController.character_list.Count];
            }
        }
        else
        {
            sr.color = color_list[characterIndex];
        }
    }

    [PunRPC]
    void rpc_setCharacterIndex(int index)
    {
        characterIndex = index;
    }
}
