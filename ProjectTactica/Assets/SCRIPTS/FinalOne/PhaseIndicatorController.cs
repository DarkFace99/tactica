﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhaseIndicatorController : MonoBehaviour
{
    [SerializeField]
    private Text text;
    [SerializeField]
    private List<GameObject> indicator;
    private SystemController systemController;
    private SystemController.GameState lastState;
    private int firstTurn;
    private int turn;
    [SerializeField]
    private Sprite[] indicatorSprite;
    void Awake()
    {
        
    }
    // Start is called before the first frame update
    void Start()
    {
        lastState = SystemController.currentState;
        firstTurn = 2;
        turn = SystemController.turn;
        for(int i = 0; i < indicator.Count; i++)
        {
            indicator[i].GetComponent<PhaseIndicator>().isState = lastState;
            indicator[i].GetComponent<PhaseIndicator>().round = turn;
            lastState = GetNextPhase();
        }
        ChangeSprite();
        StartCoroutine(CheckGameState(SystemController.currentState));
    }

    // Update is called once per frame
    void Update()
    {
        if (indicator[0].GetComponent<PhaseIndicator>().isState == SystemController.GameState.Drop)
        {
            text.color = new Color(77f / 255f, 86f / 255f, 100f / 255f, 1f);
        }
        else if ((indicator[0].GetComponent<PhaseIndicator>().isState == SystemController.GameState.P2_Action && SystemController.player == 1) || (indicator[0].GetComponent<PhaseIndicator>().isState == SystemController.GameState.P1_Action && SystemController.player == 2))
        {
            text.color = new Color(115f / 255f, 23f / 255f, 45f / 255f, 1f);
        }
        else if ((indicator[0].GetComponent<PhaseIndicator>().isState == SystemController.GameState.P1_Action && SystemController.player == 1) || (indicator[0].GetComponent<PhaseIndicator>().isState == SystemController.GameState.P2_Action && SystemController.player == 2))
        {
            text.color = new Color(40f / 255f, 92f / 255f, 196f / 255f, 1f);
        }
        else if (indicator[0].GetComponent<PhaseIndicator>().isState == SystemController.GameState.Battle)
        {
            text.color = new Color(59f / 255f, 23f / 255f, 37f / 255f, 1f);
        }
        //Debug.Log(turn);
    }
    SystemController.GameState GetNextPhase()
    {
        if(lastState == SystemController.GameState.Drop)
        {
            if(firstTurn == 1)
            {
                //Debug.Log("Go to P1_Action");
                return SystemController.GameState.P1_Action;
            }
            else
            {
                //Debug.Log("Go to P2_Action");
                return SystemController.GameState.P2_Action;
            }
        }
        else if(lastState == SystemController.GameState.P1_Action)
        {
            if(firstTurn == 1)
            {
                //Debug.Log("Go to P2_Action");
                return SystemController.GameState.P2_Action;
            }
            else
            {
                //Debug.Log("Go to Battle");
                ToBattlePhase();
                return SystemController.GameState.Battle;
            }
        }
        else if(lastState == SystemController.GameState.P2_Action)
        {
            if(firstTurn == 1)
            {
                //Debug.Log("Go to Battle");
                ToBattlePhase();
                return SystemController.GameState.Battle;
            }
            else
            {
                //Debug.Log("Go to P1_Action");
                return SystemController.GameState.P1_Action;
            }
        }
        else if(lastState == SystemController.GameState.Battle)
        {
            turn++;
            if (firstTurn == 1)
            {
                //Debug.Log("Go to P1_Action");
                return SystemController.GameState.P1_Action;
            }
            else
            {
                //Debug.Log("Go to P2_Action");
                return SystemController.GameState.P2_Action;
            }
        }
        else
        {
            //Debug.Log("Go to GameOver");
            return SystemController.GameState.Game_Over;
        }
    }
    IEnumerator CheckGameState(SystemController.GameState nowState)
    {
        yield return new WaitUntil(() => SystemController.currentState != nowState);
        for(int i = 0; i < indicator.Count - 1; i++)
        {
            indicator[i].GetComponent<PhaseIndicator>().isState = indicator[i + 1].GetComponent<PhaseIndicator>().isState;
            indicator[i].GetComponent<PhaseIndicator>().round = indicator[i + 1].GetComponent<PhaseIndicator>().round;
        }
        indicator[indicator.Count - 1].GetComponent<PhaseIndicator>().isState = lastState;
        indicator[indicator.Count - 1].GetComponent<PhaseIndicator>().round = turn;
        lastState = GetNextPhase();
        ChangeSprite();
        StartCoroutine(CheckGameState(SystemController.currentState));
    }
    void ToBattlePhase()
    {
        if (firstTurn == 1)
        {
            firstTurn = 2;
        }
        else
        {
            firstTurn = 1;
        }
    }
    void ChangeSprite()
    {
        for(int i = 0; i < indicator.Count; i++)
        {
            if(indicator[i].GetComponent<PhaseIndicator>().isState == SystemController.GameState.Drop)
            {
                indicator[i].GetComponent<Image>().sprite = indicatorSprite[0];
            }
            else if (indicator[i].GetComponent<PhaseIndicator>().isState == SystemController.GameState.P1_Action)
            {
                if(SystemController.player == 1)
                {
                    indicator[i].GetComponent<Image>().sprite = indicatorSprite[1];
                }
                else
                {
                    indicator[i].GetComponent<Image>().sprite = indicatorSprite[2];
                }
            }
            else if (indicator[i].GetComponent<PhaseIndicator>().isState == SystemController.GameState.P2_Action)
            {
                if (SystemController.player == 1)
                {
                    indicator[i].GetComponent<Image>().sprite = indicatorSprite[2];
                }
                else
                {
                    indicator[i].GetComponent<Image>().sprite = indicatorSprite[1];
                }
            }
            else if (indicator[i].GetComponent<PhaseIndicator>().isState == SystemController.GameState.Battle)
            {
                indicator[i].GetComponent<Image>().sprite = indicatorSprite[3];
            }
        }
    }
}
