﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropSpawn : MonoBehaviour
{
    public int ID; // Use for assign value of unitDropIcon with ID in Array
    private GridBehaviour1 gridBehaviour;
    public GridStat1 gridStat;
    public int row; // To assign girdArr row position
    public int col; // To assign girdArr column position
    public SpriteRenderer rend;
    public bool reSelect = false;
    private SystemController systemController;
    // Start is called before the first frame update
    void Start()
    {
        gridStat = transform.parent.GetComponent<GridStat1>();
        systemController = SystemController.systemController.GetComponent<SystemController>();
        gridBehaviour = GridBehaviour1.gridBehaviour.GetComponent<GridBehaviour1>();
        StartCoroutine(WaitForReclick());
    }

    // Update is called once per frame
    void Update()
    {

    }
    //=================== This Function is about when you use cursor to point on Something and cancel the unit that already deploy ====================
    private void OnMouseDown()
    {
        //if (Input.GetMouseButtonDown(1)) // When Click right mouse on the unit
        //{
        //gridStat.theUnitInGrid--; // Decrease unit in the grid
        //gridBehaviour.unitDropIcons[ID].alreadyDeploy = false; // Set already that grid deploy = false in the List to access
        //gridStat.canDeploy = true; // Set can deploy = true
        //gridBehaviour.listDropZone.Add(gridStat); // Add the list of drop zone to return able to choose
        //Destroy(gameObject); // Destroy This object
        //}
    }
    IEnumerator WaitForReclick()
    {
        yield return new WaitUntil(() => reSelect == true);
        if(reSelect == true)
        {
            gridStat.theUnitInGrid--; // Decrease unit in the grid
            gridBehaviour.dropZone.unitDropIcons[ID].alreadyDeploy = false; // Set already that grid deploy = false in the List to access
            gridStat.canDeploy = true; // Set can deploy = true
            gridBehaviour.dropZone.listDropZone.Add(gridStat); // Add the list of drop zone to return able to choose
            Destroy(gameObject); // Destroy This object
            //for(int i = 0; i < systemController.arrow1.Count; i++)
            //{
            //    systemController.arrow1[i].GetComponent<ArrowAppear2>().ToSetObject(i);
            //}
            
        }
    }
}