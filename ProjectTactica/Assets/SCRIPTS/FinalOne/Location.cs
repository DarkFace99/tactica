﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Location : MonoBehaviour
{
    public Vector2 direction;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        if(direction.x != 0f && direction.y != 0f)
        {
            speed *= 2f / 3f;
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(direction * speed * Time.deltaTime);
    }
}
