﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EfxAnimation : MonoBehaviour
{
    [SerializeField]
    private float translateSpeed;
    [SerializeField]
    private float fadeSpeed;
    [SerializeField]
    private float fadeDelay;
    //===========================
    private float alpha;
    private bool isFade;

    // Start is called before the first frame update
    void Start()
    {
        alpha = 255f;
        isFade = false;
        StartCoroutine(setIsFade());
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.up * translateSpeed * Time.deltaTime);
        if (isFade)
        {
            alpha -= fadeSpeed * Time.deltaTime;
            alphaCheck();
            fading();
        }

    }

    void fading()
    {
        if (GetComponent<TextMeshPro>())
        {
            TextMeshPro text = GetComponent<TextMeshPro>();
            text.color = new Color32((byte)text.color.r, (byte)text.color.b, (byte)text.color.a, (byte)alpha);
        }
        else
        {
            SpriteRenderer sr = GetComponent<SpriteRenderer>();
            sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, alpha / 255f);
        }
    }

    void alphaCheck()
    {
        if (alpha <= 0f)
        {
            Destroy(gameObject);
        }
    }

    IEnumerator setIsFade()
    {
        yield return new WaitForSecondsRealtime(fadeDelay);
        isFade = true;
    }
}