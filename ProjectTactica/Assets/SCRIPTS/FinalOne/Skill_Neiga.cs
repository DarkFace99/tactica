﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;

public class Skill_Neiga : MonoBehaviour
{
    private BattlePhase battlePhase;
    //character cannon
    [SerializeField]
    private float effect_delay;
    [SerializeField]
    private GameObject speedLine;
    [SerializeField]
    private GameObject character_cannon;
    // Start is called before the first frame update
    void Start()
    {
        battlePhase = BattlePhase.battlePhase.GetComponent<BattlePhase>();
        StartCoroutine(waitUntilSkill());
    }

    IEnumerator waitUntilSkill()
    {
        CharacterController cc = GetComponent<CharacterController>();
        if (cc.isSkill)
        {
            GetComponent<PhotonTransformView>().m_SynchronizePosition = false;
            yield return new WaitUntil(() => !cc.isSkill);
        }
        else
        {
            yield return new WaitUntil(() => cc.isSkill);
            spawnCharacterCannon();
        }
        StartCoroutine(waitUntilSkill());
    }

    void warping()
    {
        CharacterController cc = GetComponent<CharacterController>();
        GameObject grid_to_warp = cc.skill_grid_list_confirm[0];
        if (grid_to_warp.GetComponent<GridStat1>().canVisit)
        {
            transform.localPosition = grid_to_warp.transform.localPosition;
        }
        finish_warp(cc);
    }

    void finish_warp(CharacterController cc)
    {
        GetComponent<PhotonView>().RPC("rpc_setPos", RpcTarget.Others, transform.position.x, transform.position.y);
        cc.clearAllAction();
        battlePhase.actionCheck_complete(cc.player);
        GetComponent<PhotonTransformView>().m_SynchronizePosition = true;
    }

    [PunRPC]
    void rpc_setPos(float x, float y)
    {
        transform.localPosition = new Vector2(x, y);
    }

    void spawnCharacterCannon()
    {
        PhotonNetwork.Instantiate(Path.Combine("Prefab", speedLine.name), Vector2.zero, Quaternion.identity);
        PhotonNetwork.Instantiate(Path.Combine("Prefab", character_cannon.name), Vector2.zero, Quaternion.identity);
        StartCoroutine(waitForCharacterEffect());
    }

    IEnumerator waitForCharacterEffect()
    {
        yield return new WaitForSecondsRealtime(effect_delay);
        //use the skill
        warping();
    }

}
