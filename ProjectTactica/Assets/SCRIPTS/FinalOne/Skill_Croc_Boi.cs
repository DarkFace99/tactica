﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Skill_Croc_Boi : MonoBehaviour
{
    //character cannon
    [SerializeField]
    private float effect_delay;
    [SerializeField]
    private GameObject speedLine;
    [SerializeField]
    private GameObject character_cannon;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(waitUntilSkill());
    }

    IEnumerator waitUntilSkill()
    {
        CharacterController cc = GetComponent<CharacterController>();

        yield return new WaitUntil(() => cc.isSkill);
        
        spawnCharacterCannon();
    }

    void skill_finish()
    {
        GetComponent<CharacterController>().clearAllAction();
        GetComponent<ActiveSkill>().SteadyCheck_call();
    }

    void skill_start()
    {
        CharacterController cc = GetComponent<CharacterController>();
        foreach (GameObject grid in cc.skill_grid_list_confirm)
        {
            GridStat1 gridStat = grid.GetComponent<GridStat1>();
            if (gridStat.stepOnCharacter)
            {
                CharacterController stepOnCC = gridStat.stepOnCharacter.GetComponent<CharacterController>();
                if (stepOnCC.player != cc.player)
                {
                    stepOnCC.attacker = gameObject;
                    stepOnCC.setIsBeingAttack(true);
                }
            }
        }
    }

    void spawnCharacterCannon()
    {
        PhotonNetwork.Instantiate(Path.Combine("Prefab", speedLine.name), Vector2.zero, Quaternion.identity);
        PhotonNetwork.Instantiate(Path.Combine("Prefab", character_cannon.name), Vector2.zero, Quaternion.identity);
        StartCoroutine(waitForCharacterEffect());
    }

    IEnumerator waitForCharacterEffect()
    {
        yield return new WaitForSecondsRealtime(effect_delay);
        skill_start();
        skill_finish();
        StartCoroutine(waitUntilSkill());
    }
}
