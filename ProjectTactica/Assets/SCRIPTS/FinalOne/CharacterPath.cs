﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterPath : MonoBehaviour
{
    [SerializeField]
    private GameObject parent;
    [SerializeField]
    private GameObject characterAlpha;
    [SerializeField]
    private Vector2 smallScale;
    [SerializeField]
    private Vector2 bigScale;
    /*[SerializeField]
    private float newScale;*/
    // Start is called before the first frame update
    void Awake()
    {
        transform.localScale = smallScale;
        GetComponent<RendererDifferent>().characterIndex = parent.GetComponent<RendererDifferent>().characterIndex;
        if (GetComponent<LerpingRenderer>() != null)
        {
            StartCoroutine(waitUntilLerp());
        }
    }

    // Update is called once per frame
    void Update()
    {
        dataHidingCheck(parent.GetComponent<DataHiding>(), GetComponent<DataHiding>());
        setCharacterAlpha();
    }

    IEnumerator waitUntilLerp()
    {    
        yield return new WaitUntil(() => parent.GetComponent<LerpingRenderer>().isLerp);
        GetComponent<LerpingRenderer>().isLerp = parent.GetComponent<LerpingRenderer>().isLerp;
    }

    void dataHidingCheck(DataHiding parent_data, DataHiding data)
    {

        if (data.isActionCollect != parent_data.isActionCollect)
        {
            data.isActionCollect = parent_data.isActionCollect;
            transform.localScale = bigScale;
        }

        if (data.isMouseOver_our != parent_data.isMouseOver_our)
        {
            data.isMouseOver_our = parent_data.isMouseOver_our;
        }

        if (data.isMouseOver_opponent != parent_data.isMouseOver_opponent)
        {
            data.isMouseOver_opponent = parent_data.isMouseOver_opponent;
        }

        if (data.isShow != parent_data.isShow)
        {
            data.isShow = parent_data.isShow;
        }
    }

    void setCharacterAlpha()
    {
        if(characterAlpha.GetComponent<SpriteRenderer>().enabled != GetComponent<SpriteRenderer>().enabled)
        {
            characterAlpha.GetComponent<SpriteRenderer>().enabled = GetComponent<SpriteRenderer>().enabled;
        }
    }
}
