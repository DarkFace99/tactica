﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class ActionLogController : MonoBehaviour
{
    public List<GameObject> actionLog_list;
    public List<Vector2> SpawnPoint;
    private SystemController systemController;
    public GameObject actionLog;
    public GameObject actionLog2;
    public GameObject logSpawn;
    private int current_character_number;
    public static GameObject actionLogController;
    public GameObject canceledCharacter;

    // Start is called before the first frame update
    private void Awake()
    {
        actionLogController = gameObject;
    }
    void Start()
    {
        systemController = SystemController.systemController.GetComponent<SystemController>();
        StartCoroutine(WaitForMove());
        StartCoroutine(waitUntilCancel());
    }

    public void sortActionLog()
    {
        if (actionLog_list.Count > 0)
        {
            for (int i = actionLog_list.Count - 1; i > 0; i--)
            {
                ActionLog actionLog_1 = actionLog_list[i].GetComponent<ActionLog>();
                ActionLog actionLog_2 = actionLog_list[i - 1].GetComponent<ActionLog>();
                if (actionLog_1.turn == actionLog_2.turn)
                {
                    if ((actionLog_1.total_delay + actionLog_1.increase_delay < actionLog_2.total_delay + actionLog_2.increase_delay) || (actionLog_1.total_delay + actionLog_1.increase_delay == actionLog_2.total_delay + actionLog_2.increase_delay && actionLog_2.GetComponent<UpdateActionLog>()))
                    {
                        swapActionLog(i, i - 1);
                    }
                }
            }
        }
    }

    IEnumerator WaitForMove()
    {
        if(!logSpawn)
        {
            yield return new WaitUntil(() => systemController.controllingCharacter());
            logSpawn = Instantiate(actionLog, SpawnPoint[actionLog_list.Count], Quaternion.identity);
            ActionLog action_log = logSpawn.GetComponent<ActionLog>();
            action_log.index = actionLog_list.Count;
            action_log.setActionLog(0,0);
            action_log.owner = systemController.controllingCharacter();
            current_character_number = action_log.owner.GetComponent<CharacterController>().character_number;
            action_log.setActionLog_SetCharacter(current_character_number);
        }
        else
        {
            //will change later in order to prevent destroying when action is collected after end action button invoked
            yield return new WaitUntil(() => !systemController.controllingCharacter() || systemController.controllingCharacter().GetComponent<CharacterController>().character_number != current_character_number);
            if(logSpawn)
            {
                ActionLog action_log = logSpawn.GetComponent<ActionLog>();
                if(action_log.index < actionLog_list.Count - 1)
                {
                    swapActionLog(action_log.index, action_log.index + 1);
                }
                actionLog_list.Remove(logSpawn);
                Destroy(logSpawn);
            }
        }
        StartCoroutine(WaitForMove());
    }

    public void YAY(int index)
    {
        GetComponent<PhotonView>().RPC("rpc_Yay", RpcTarget.All, index);
        
    }

    void assign_list(List<GameObject>list1,List<GameObject>list2)
    {
        list1.Clear();
        foreach(GameObject obj in list2)
        {
            list1.Add(obj);
        }
    }

    [PunRPC]
    void RPC_SetDetail(int total_delay, int cnumber, int index)
    {
        GameObject obj = Instantiate(actionLog2,SpawnPoint[index],Quaternion.identity);
        ActionLog actionLog = obj.GetComponent<ActionLog>();
        actionLog.setActionLog(total_delay, 0);
        actionLog.setActionLog_SetCharacter(cnumber);
        actionLog.index = index;
    }

    public void SetDetail(int total_delay, int cnumber, int index)
    {
        GetComponent<PhotonView>().RPC("RPC_SetDetail", RpcTarget.Others,total_delay, cnumber, index);
    }

    [PunRPC]
    void rpc_Yay(int index)
    {
        ActionLog action_log = actionLog_list[index].GetComponent<ActionLog>();
        action_log.isMoving = true;
        Destroy(actionLog_list[index], 1f);

        List<GameObject> list = new List<GameObject>();
        assign_list(list, actionLog_list);
        actionLog_list.Clear();

        for(int i = 0; i < list.Count; i++)
        {
            if (i < index)
            {
                actionLog_list.Add(list[i]);
            }
            else if (i > index)
            {
                list[i - 1] = list[i];
                actionLog_list.Add(list[i - 1]);
                ActionLog actionLog = list[i - 1].GetComponent<ActionLog>();
                actionLog.goMove = true;
                actionLog.highest = SpawnPoint[i - 1].y;
                actionLog.index--;
            }
        }
    }

    public void bing_call(int index)
    {
        GetComponent<PhotonView>().RPC("rpc_bing", RpcTarget.All, index);
    }

    [PunRPC]
    void rpc_bing(int index)
    {
        actionLog_list[index].GetComponent<ColorBing>().isBing = true;
    }

    IEnumerator waitUntilCancel()
    {
        yield return new WaitUntil(() => canceledCharacter);
        foreach(GameObject obj in actionLog_list)
        {
            ActionLog actionLog = obj.GetComponent<ActionLog>();
            if(actionLog.owner == canceledCharacter)
            {
                GetComponent<PhotonView>().RPC("rpc_setCanceledActionLog", RpcTarget.All, actionLog.index);
            }
        }
        canceledCharacter = null;
        StartCoroutine(waitUntilCancel());
    }

    [PunRPC]
    void rpc_setCanceledActionLog(int index)
    {
        actionLog_list[index].GetComponent<ActionLog>().actionCanceled.SetActive(true);
        actionLog_list[index].GetComponent<ActionLog>().actionCover.SetActive(false);
    }

    public void swapActionLog(int index_1, int index_2)
    {
        GameObject obj = actionLog_list[index_1];
        actionLog_list[index_1] = actionLog_list[index_2];
        actionLog_list[index_2] = obj;

        int temp;
        temp = actionLog_list[index_1].GetComponent<ActionLog>().index;
        actionLog_list[index_1].GetComponent<ActionLog>().index = actionLog_list[index_2].GetComponent<ActionLog>().index;
        actionLog_list[index_2].GetComponent<ActionLog>().index = temp;

        Vector3 v = actionLog_list[index_1].transform.localPosition;
        actionLog_list[index_1].transform.localPosition = actionLog_list[index_2].transform.localPosition;
        actionLog_list[index_2].transform.localPosition = v;
    }
}