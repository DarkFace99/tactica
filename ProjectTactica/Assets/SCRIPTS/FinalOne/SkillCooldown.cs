﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SkillCooldown : MonoBehaviour
{
    public TextMeshPro skillCooldownText;
    private SystemController system;
    private ActiveSkill activeSkill;
    public int skillCoodown;

    void Start()
    {
        system = SystemController.systemController.GetComponent<SystemController>();
        activeSkill = RaycastCheck.clickingCharacter.GetComponent<ActiveSkill>();
        skillCoodown = 0;
        StartCoroutine(setCooldown());
    }

    IEnumerator setCooldown()
    {
        yield return new WaitUntil(() => skillCoodown != activeSkill.current_cooldown);
        skillCoodown = activeSkill.current_cooldown;
        setCooldownText(skillCoodown);
        StartCoroutine(setCooldown());
    }

    private void setCooldownText(int cooldown)
    {
        if (cooldown > 0)
        {
            skillCooldownText.SetText(cooldown.ToString());
        }
        else
        {
            skillCooldownText.SetText(" ");
        }
    }

}
