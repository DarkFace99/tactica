﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarningSign : MonoBehaviour
{
    [SerializeField]
    private float delay;
    private int number;
    [SerializeField]
    private GameObject warning_1;
    [SerializeField]
    private GameObject warning_2;
    //Sound
    [SerializeField]
    private AudioController sound;
    // Start is called before the first frame update
    void OnEnable()
    {
        number = 0;
        sound.audio[0].Play();
        StartCoroutine(waitForSetActive());
    }

    void OnDisable()
    {
        sound.audio[0].Stop();
        StopCoroutine(waitForSetActive());
    }

    void setActive()
    {
        number++;
        warning_1.SetActive(number % 2 == 0);
        warning_2.SetActive(number % 2 != 0);
    }

    IEnumerator waitForSetActive()
    {
        yield return new WaitForSecondsRealtime(delay);
        setActive();
        StartCoroutine(waitForSetActive());
    }
}
