﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CountDownUI : MonoBehaviour
{
    [SerializeField]
    private float maxScaleX;
    [SerializeField]
    private GameObject slider;
    [SerializeField]
    private GameObject slider_bg;
    [SerializeField]
    private TextMeshPro displayingText;
    [SerializeField]
    private float scalingSpeed;
    private float currentScaleX;
    private float partAmount;
    private SystemController systemController;
    private bool isScale;
    [SerializeField]
    private float min_fontSize;
    private float starting_fontSize;
    // Start is called before the first frame update
    void OnEnable()
    {
        isScale = false;
        slider_bg.GetComponent<SpriteRenderer>().enabled = false;
        displayingText.SetText("");
        starting_fontSize = displayingText.fontSize;
        currentScaleX = 0f;
        systemController = SystemController.systemController.GetComponent<SystemController>();
        partAmount = maxScaleX / (float)(systemController.maxCountDown);
        StartCoroutine(waitUntilFirstTurn());
    }

    private void Update()
    {
        scalingCheck();
    }

    bool isFirstTurnBegin()
    {
        return (SystemController.firstTurnPlayer == 1 && SystemController.currentState == SystemController.GameState.P1_Action) || (SystemController.firstTurnPlayer == 2 && SystemController.currentState == SystemController.GameState.P2_Action);
    }

    IEnumerator waitUntilFirstTurn()
    {
        yield return new WaitUntil(() => isFirstTurnBegin());
        StartCoroutine(waitForChangeSlider());
    }

    IEnumerator waitForChangeSlider()
    {
        yield return new WaitForSecondsRealtime(1.5f);
        maxScaleX = partAmount * systemController.currentCountDown;
        isScale = true;
        StartCoroutine(waitUntilNotFirstTurn());
        if (!slider_bg.GetComponent<SpriteRenderer>().enabled)
        {
            slider_bg.GetComponent<SpriteRenderer>().enabled = true;
        }
        setText();
    }

    IEnumerator waitUntilNotFirstTurn()
    {
        yield return new WaitUntil(() => !isFirstTurnBegin());
        StartCoroutine(waitUntilFirstTurn());
    }

    void scalingCheck()
    {
        if (isScale)
        {
            if(systemController.currentCountDown == systemController.maxCountDown)
            {
                currentScaleX += scalingSpeed * Time.deltaTime;
                if(currentScaleX >= maxScaleX)
                {
                    currentScaleX = maxScaleX;
                    isScale = false;
                }
            }
            else
            {
                currentScaleX -= scalingSpeed * Time.deltaTime;
                if (currentScaleX <= maxScaleX)
                {
                    currentScaleX = maxScaleX;
                    isScale = false;
                }
            }
            slider.transform.localScale = new Vector2(currentScaleX, slider.transform.localScale.y);
        }
    }

    void setText()
    {
        if(systemController.currentCountDown == 0)
        {
            displayingText.SetText("LAST ROUND");
        }
        else
        {
            displayingText.SetText((systemController.currentCountDown + 1) + " ROUNDS LEFT");
        }
    }

    private void OnMouseEnter()
    {
        displayingText.fontSize = min_fontSize;
        if (!systemController.isLosing)
        {
            string text = "ELIMINATE OPPONENT WITHIN ";
            if (systemController.currentCountDown == 0)
            {
                displayingText.SetText("LAST ROUND TO ELIMINATE OPPONENT");
            }
            else
            {
                displayingText.SetText(text + (systemController.currentCountDown + 1) + " ROUNDS");
            }
        }
        else
        {
            string text = "SURVIVE FOR ";
            if (systemController.currentCountDown == 0)
            {
                displayingText.SetText("SURVIVE ON THIS ROUND");
            }
            else
            {
                displayingText.SetText(text + (systemController.currentCountDown + 1) + " ROUNDS");
            }
        }
    }

    private void OnMouseExit()
    {
        displayingText.fontSize = starting_fontSize;
        setText();
    }
}
