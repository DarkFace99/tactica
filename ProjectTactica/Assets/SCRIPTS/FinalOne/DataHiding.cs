﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class DataHiding : MonoBehaviour
{
    public bool isHidingData;
    public bool isActionCollect;
    public bool isMouseOver_our;
    public bool isMouseOver_opponent;
    public bool isShow;
    private bool isFirstShow;
    //public bool isOwnerMouse;


    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(waitUntilEkakSkill());
        setInitial();
        StartCoroutine(waitUntilActionCollect());
        StartCoroutine(waitUntilMouseEnter_our());
        StartCoroutine(waitUntilMouseEnter_opponent());
        StartCoroutine(waitUntilShow());
    }

    void setInitial()
    {
        if (isHidingData && GetComponent<PhotonView>().IsMine && !SystemController.isEkakSkill)
        {
            setRenderer(2, false);
        }
        isActionCollect = false;
        isMouseOver_our = false;
        isMouseOver_opponent = false;
        isShow = false;
        isFirstShow = false;
    }

    IEnumerator waitUntilShow()
    {
        if (isShow)
        {
            yield return new WaitUntil(() => !isShow);
        }
        else
        {
            yield return new WaitUntil(() => isShow);
            if (!isFirstShow)
            {
                isFirstShow = true;
            }
        }
        setRenderer(1, isShow);
        StartCoroutine(waitUntilShow());
    }

    IEnumerator waitUntilActionCollect()
    {
        yield return new WaitUntil(() => isActionCollect);
        setRenderer(1, false);
    }

    IEnumerator waitUntilMouseEnter_our()
    {
        if (isMouseOver_our)
        {
            yield return new WaitUntil(() => !isMouseOver_our);
        }
        else
        {
            yield return new WaitUntil(() => isMouseOver_our);
        }

        if (!isShow)
        {
            setRenderer(3, isMouseOver_our);
        }
        StartCoroutine(waitUntilMouseEnter_our());
    }

    IEnumerator waitUntilMouseEnter_opponent()
    {
        if (isMouseOver_opponent)
        {
            yield return new WaitUntil(() => !isMouseOver_opponent);
        }
        else
        {
            yield return new WaitUntil(() => isMouseOver_opponent);
        }

        if (!isShow)
        {
            if (!isHidingData || (isHidingData && isFirstShow))
            {
                setRenderer(2, isMouseOver_opponent);
            }
        }

        StartCoroutine(waitUntilMouseEnter_opponent());
    }

    IEnumerator waitUntilEkakSkill()
    {
        if (!SystemController.isEkakSkill)
        {
            yield return new WaitUntil(() => SystemController.isEkakSkill);
        }
        else
        {
            yield return null;
        }
        isHidingData = false;

    }

    public void setRenderer(int target, bool set) // 1 -> All // 2 -> Others // else -> Only Mine
    {
        if(target == 1)
        {
            GetComponent<PhotonView>().RPC("rpc_setRenderer", RpcTarget.All, set);
        }
        else if (target == 2)
        {
            GetComponent<PhotonView>().RPC("rpc_setRenderer", RpcTarget.Others, set);
        }
        else
        {
            rpc_setRenderer(set);
        }
    }

    [PunRPC]
    void rpc_setRenderer(bool set)
    {
        GetComponent<SpriteRenderer>().enabled = set;
    }

}
