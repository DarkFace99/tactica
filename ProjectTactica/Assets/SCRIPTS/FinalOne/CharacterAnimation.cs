﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimation : MonoBehaviour
{
    private Animator anim;
    private CharacterController cc;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        cc = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        setDirection();
    }

    void setDirection()
    {
        int direction = cc.current_direction;
        if (direction == 1)
        {
            anim.SetFloat("x", 0f);
            anim.SetFloat("y", 1f);
        }
        else if (direction == 2)
        {
            anim.SetFloat("x", 1f);
            anim.SetFloat("y", 0f);
        }
        else if (direction == 3)
        {
            anim.SetFloat("x", 0f);
            anim.SetFloat("y", -1f);
        }
        else if (direction == 4)
        {
            anim.SetFloat("x", -1f);
            anim.SetFloat("y", 0f);
        }
    }
}
