﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowAppear : MonoBehaviour
{
    private GridBehaviour1 gridBehaviour;
    private SystemController systemController;
    private SpriteRenderer rend;
    [SerializeField]
    private float blinkDelay;
    private float time = 0f;
    private float alpha = 1f;
    // Start is called before the first frame update
    void Start()
    {

        gridBehaviour = GridBehaviour1.gridBehaviour.GetComponent<GridBehaviour1>();
        systemController = SystemController.systemController.GetComponent<SystemController>();

        rend = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        rend.enabled = isChecking();

        if (rend.enabled)
        {
            time += Time.deltaTime;
            if (alpha == 0f)
            {
                if (time >= blinkDelay)
                {
                    time = 0f;
                    alpha = 1f;
                }
            }
            else if (alpha == 1f)
            {
                if (time >= blinkDelay * 2f)
                {
                    time = 0f;
                    alpha = 0f;
                }
            }
            rend.color = new Color(rend.color.r, rend.color.g, rend.color.b, alpha);
        }
        else
        {
            time = 0f;
            alpha = 1f;
        }
    }
    bool isChecking()
    {
        for (int i = 0; i < gridBehaviour.dropZone.unitDropIcons.Count; i++)
        {
            if (!gridBehaviour.dropZone.unitDropIcons[i])
            {
                return false;
            }
            else if (gridBehaviour.dropZone.unitDropIcons[i].isSelecting)
            {
                return true;
            }
        }
        return false;
    }
}