﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateOption : MonoBehaviour
{
    [SerializeField]
    private GameObject optionUI;
    public void ActiveTrue()
    {
        optionUI.SetActive(true);
    }
    public void ActiveFalse()
    {
        optionUI.SetActive(false);
    }
}
