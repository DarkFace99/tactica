﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class BattlePhase : MonoBehaviour
{
    public float actionCompleteDelayTime; //delay a bit before send value after complete action
    [SerializeField]
    private float sequence_delay;
    [SerializeField]
    private float fade_delay;
    //=======================================
    public bool isSetP1;
    public bool isSetP2;
    public bool isCheckP1_move; //is already check P1's movement or steady
    public bool isCheckP2_move; //is already check P2's movement or steady
    public bool isCheckP1_attack; //is already check P1's attack
    public bool isCheckP2_attack; //is already check P2's attack

    public int player;
    private bool oneTimeBattle;
    private bool oneTimeSteadyCheck;
    private SystemController systemController;
    public static GameObject battlePhase;
    public GridBehaviour1 gridBehaviour;
    
    //win&lose condition
    public bool isCheckP1_gameOver; //is P1 already gameover
    public bool isCheckP2_gameOver; //is P2 already gameover
    public bool isP1_GameOver;
    public bool isP2_GameOver;
    
    //Disaster
    public bool disaster_mark_complete; //is already mark disaster
    public bool disaster_action_complete; //is already complete disaster
    public List<int> disaster_turn_list; //list of turn to occur disaster
    public int disaster_turn_amount;

    private GameObject character;
    public int opponent_total_delay;

    // Start is called before the first frame update
    void Awake()
    {
        battlePhase = gameObject;
    }

    void Start()
    {
        opponent_total_delay = 0;
        isSetP1 = false;
        isSetP2 = false;
        isCheckP1_move = false;
        isCheckP2_move = false;
        isCheckP1_attack = true;
        isCheckP2_attack = true;
        isCheckP1_gameOver = true;
        isCheckP2_gameOver = true;
        isP1_GameOver = false;
        isP1_GameOver = false;
        oneTimeBattle = true;
        disaster_mark_complete = true;
        disaster_action_complete = true;
        player = SystemController.player; //set player
        systemController = SystemController.systemController.GetComponent<SystemController>(); //SystemController
    }

    // Update is called once per frame
    void Update()
    {
        //start Battle Phase
        if(SystemController.currentState == SystemController.GameState.Battle && oneTimeBattle)
        {
            oneTimeBattle = false;
            isCheckP1_gameOver = false;
            isCheckP2_gameOver = false;

            character = systemController.actionCharacter();
            int delay_to_set;
            if (character)
            {
                delay_to_set = character.GetComponent<CharacterController>().total_delay;
            }
            else
            {
                delay_to_set = -1;
            }
            GetComponent<PhotonView>().RPC("rpc_setOpponentDelay", RpcTarget.Others, delay_to_set, player);
            set_isSet(player);
            //change firstTurnPlayer 
            SystemController.firstTurnPlayer = changeFirstTurn(SystemController.firstTurnPlayer);

            setDisasterBool(SystemController.turn); //set disaster_mark and disaster_action
            StartCoroutine(waitUntilSetDelay());
            
        }

        //check steady of all character
        if(SystemController.currentState == SystemController.GameState.Battle && oneTimeSteadyCheck && ((!isCheckP1_attack && player == 1) || (!isCheckP2_attack && player == 2)))
        {
            oneTimeSteadyCheck = false;
            checkSteady();
        }

    }

    IEnumerator waitUntilSetDelay()
    {
        yield return new WaitUntil(() => isSetP1 && isSetP2);
        StartCoroutine(setStartBattlePhase());
    }

    IEnumerator setStartBattlePhase()
    {
        yield return new WaitForSecondsRealtime(fade_delay);
        StartCoroutine(checkingOrder()); //set checking order of each player
        StartCoroutine(continueBattlePhase()); //wait to continue battle phase of each delay
    }

    IEnumerator continueBattlePhase()
    {
        yield return new WaitUntil(() => isCheckP1_move && isCheckP2_move && isCheckP1_attack && isCheckP2_attack);
        isSetP1 = false;
        isSetP2 = false;
        isCheckP1_move = false;
        isCheckP2_move = false;
        isCheckP1_attack = true;
        isCheckP2_attack = true;
        StartCoroutine(gameOverCheckingOrder(player));
        StartCoroutine(endConditionCheck(player));
        if (!PhotonNetwork.IsMasterClient)
        {
            StartCoroutine(beforeChangePhase());
        }
    }

    [PunRPC]
    void rpc_setOpponentDelay(int delay, int player_set)
    {
        opponent_total_delay = delay;
        set_isSet(player_set);
    }

    void set_isSet(int player_set)
    {
        if (player_set == 1)
        {
            isSetP1 = true;
        }
        else
        {
            isSetP2 = true;
        }
    }

    [PunRPC]
    void rpc_changePhase()
    {
        //delay = startingDelay; //reset delay
        oneTimeBattle = true;
        SystemController.turn++; //increase turn
        systemController.decreaseCooldown();
        //check game over condition
        if (isP1_GameOver || isP2_GameOver)
        {
            SystemController.currentState = SystemController.GameState.Game_Over;
        }
        else
        {
            //check first turn player before change phase
            if (SystemController.firstTurnPlayer == 1)
            {
                SystemController.currentState = SystemController.GameState.P1_Action;
            }
            else
            {
                SystemController.currentState = SystemController.GameState.P2_Action;
            }
        }
    }

    int changeFirstTurn(int firstTurnPlayer)
    {
        if (firstTurnPlayer == 1)
        {
            return 2;
        }
        else
        {
            return 1;
        }
    }

    //check movement action and steady action
    void actionCheck()
    {

        CharacterController cc = character.GetComponent<CharacterController>();
        if (cc.character_sim_list_confirm.Count > 0)
        {
            StartCoroutine(waitForSequence(cc, 1));
            return;
        }
        else if (cc.attacking_mark_list_confirm.Count > 0 || cc.skill_mark_list_confirm.Count > 0)
        {
            checkAttackOrSkill(cc);
            return;
        }
        
        actionCheck_complete(player);
    }

    void checkAttackOrSkill(CharacterController cc)
    {
        if (cc.attacking_mark_list_confirm.Count > 0)
        {
            StartCoroutine(waitForSequence(cc, 2));
        }
        else if (cc.skill_mark_list_confirm.Count > 0)
        {
            StartCoroutine(waitForSequence(cc, 3));
        }
    }

    IEnumerator waitUntilStopMoving(CharacterController cc)
    {
        yield return new WaitUntil(() => !cc.isMove);
        if ((cc.attacking_mark_list_confirm.Count ==  0 && cc.skill_mark_list_confirm.Count == 0) || gridBehaviour.gridArr[cc.row, cc.col].GetComponent<GridStat1>().isSteady)
        {
            actionCheck_complete(player); //complete action check
        }
        else
        {
            StartCoroutine(waitForCheckAttackOrSkill(cc));
        }
    }

    IEnumerator waitForCheckAttackOrSkill(CharacterController cc)
    {
        yield return new WaitForSecondsRealtime(sequence_delay/2f);
        checkAttackOrSkill(cc);
    }

    //==========================================SEQUENCE==========================================//

    IEnumerator waitForSequence(CharacterController cc, int type) // 1-> move // 2-> steady base attack // 3-> steady skill
    {
        beforeSequence(cc, type);
        yield return new WaitForSecondsRealtime(sequence_delay);
        afterSequence(cc, type);
    }

    void beforeSequence(CharacterController cc, int type)
    {
        if(type == 1)
        {
            foreach(GameObject c_sim in cc.character_sim_list_confirm)
            {
                cc.setDataHiding_Show(c_sim, true);
            }
        }
        else if(type == 2)
        {
            if (cc.attacking_mark_list_confirm.Count > 0)
            {
                if (cc.baseAttackType == 1)
                {
                    foreach (GameObject attacking_mark in cc.attacking_mark_list_confirm)
                    {
                        cc.setDataHiding_Show(attacking_mark, true);
                    }
                }
                else
                {
                    for (int i = 0; i < cc.attacking_grid_list_confirm.Count; i++)
                    {
                        cc.setDataHiding_Show(cc.attacking_mark_list_confirm[i], true);
                        if (cc.isDisableMark(cc.attacking_grid_list_confirm[i]))
                        {
                            if (i < cc.attacking_grid_list_confirm.Count - 1)
                            {
                                break;
                            }
                        }
                    }
                }
            }
        }
        else if (type == 3)
        {
            foreach (GameObject skill_mark in cc.skill_mark_list_confirm)
            {
                cc.setDataHiding_Show(skill_mark, true);
            }
        }

    }

    void afterSequence(CharacterController cc, int type)
    {
        if (type == 1)
        {
            cc.isMove = true;
            StartCoroutine(waitUntilStopMoving(cc));
        }
        else if(type == 2)
        {
            cc.is_steady_base = true;
        }
        else if (type == 3)
        {
            cc.is_steady_skill = true;
        }
    }

    //==========================================SEQUENCE==========================================//

    //public function called IEnumerator function
    public void actionCheck_complete(int checking_player)
    {
        StartCoroutine(actionCheck_complete_delay(checking_player));
    }

    //IEnumerator function called rpc function
    IEnumerator actionCheck_complete_delay(int checking_player)
    {
        yield return new WaitForSecondsRealtime(actionCompleteDelayTime);
        actionCompleteDelayTime = 0f;
        GetComponent<PhotonView>().RPC("rpc_actionCheck_complete", RpcTarget.All, checking_player);
    }

    //check player number and set isCheck player move by rpc
    [PunRPC]
    void rpc_actionCheck_complete(int checkingPlayer)
    {
        //after action check -> set checkAttack to false to check steady and attack
        isCheckP1_attack = false;
        isCheckP2_attack = false;
        oneTimeSteadyCheck = true;

        if (checkingPlayer == 1)
        {
            isCheckP1_move = true;
        }
        else
        {
            isCheckP2_move = true;
        }
    }

    //set order of action checking depends on player number
    IEnumerator checkingOrder()
    {
        if (!character)
        {
            yield return null;
            actionCheck_complete(player);
        }
        else
        {
            CharacterController cc = character.GetComponent<CharacterController>();
            if (cc.total_delay < opponent_total_delay || (cc.total_delay == opponent_total_delay && player == SystemController.firstTurnPlayer))
            {
                yield return null;
            }
            else
            {
                if (player == 1)
                {
                    yield return new WaitUntil(() => isCheckP2_move && isCheckP1_attack && isCheckP2_attack);
                }
                else
                {
                    yield return new WaitUntil(() => isCheckP1_move && isCheckP1_attack && isCheckP2_attack);
                }
            }
            actionCheck();
        }
    }
    
    //public function called rpc function
    public void steady_check_complete(int checking_player)
    {
        GetComponent<PhotonView>().RPC("rpc_steady_check_complete", RpcTarget.All, checking_player);
    }

    //rpc function to set isCheck P1's or P2's attack
    [PunRPC]
    void rpc_steady_check_complete(int checking_player)
    {
        if (checking_player == 1)
        {
            isCheckP1_attack = true;
        }
        else
        {
            isCheckP2_attack = true;
        }
    }

    //check steady of all characters
    void checkSteady()
    {
        if (character)
        {
            CharacterController cc = character.GetComponent<CharacterController>();
            if (!cc.is_steady_base && !cc.is_steady_skill) //no steady status character
            {
                steady_check_complete(player); //complete steady check
            }
            else //has steady status character
            {
                if (cc.isEnemyInRange())
                {
                    if (cc.attacking_grid_list_confirm.Count > 0)
                    {
                        cc.isAttack = true;
                    }
                    else if (cc.skill_grid_list_confirm.Count > 0)
                    {
                        cc.isSkill = true;
                    }
                }
                else
                {
                    steady_check_complete(cc.player);
                }
            }
        }
        else
        {
            steady_check_complete(player);
        }
    }

    
    void setDisasterBool(int turn) //set bool relate to disaster
    {
        if(turn % 5 == 0 && turn % 10 != 0) //turn to mark disaster
        {
            disaster_mark_complete = false;
        }
        else if(turn % 10 == 0) //add turn integer to list
        {
            for (int i = 0; i < disaster_turn_amount; i++) 
            {
                disaster_turn_list.Add(turn + i);
            }
        }

        if(disaster_turn_list.Count != 0) //turn to take disaster action
        {
            disaster_action_complete = false;
            if(turn == disaster_turn_list[disaster_turn_list.Count - 1] && disaster_turn_amount > 1)
            {
                disaster_turn_list.Clear();
            }
        }
    }

    IEnumerator beforeChangePhase()
    {
        if (!disaster_mark_complete) //disaster mark need to be activated
        {
            //mark the grid
            gridBehaviour.RandomRowOrColumn();
            gridBehaviour.Disaster();
            //use below function when finish marking
            //you can make public function over this rpc funtion if u want
            GetComponent<PhotonView>().RPC("rpc_setDisasterMark", RpcTarget.All, true);
        }
        else if (!disaster_action_complete) //disaster action need to be activated
        {
            //disaster action on the marked grid
            if (disaster_turn_list.Count > 0)
            {
                gridBehaviour.CheckCancel(disaster_turn_list[0] + disaster_turn_list.Count);
                if (disaster_turn_amount == 1)
                {
                    GetComponent<PhotonView>().RPC("rpc_clear_disaster_turn_list", RpcTarget.All);
                }
            }
            //use below function when finish disaster appearance
            //you can make public function over this rpc funtion if u want
            GetComponent<PhotonView>().RPC("rpc_setDisasterAction", RpcTarget.All, true);
            if(disaster_turn_list.Count == 0)
            {
                //what happend when it is the last turn of disaster occuring
                gridBehaviour.AfterDisaster();
                gridBehaviour.RemoveDisasterGrid();
            }
        }

        yield return new WaitUntil(() => isCheckP1_gameOver && isCheckP2_gameOver); //wait until checking win condition is complete
        GetComponent<PhotonView>().RPC("rpc_changePhase", RpcTarget.All); //change phase using rpc
    }

    [PunRPC]
    void rpc_clear_disaster_turn_list()
    {
        disaster_turn_list.Clear();
    }

    //activate in update in starting battle phase if statement
    //wait to check win condition
    IEnumerator gameOverCheckingOrder(int checking_player)
    {
        yield return new WaitUntil(() => disaster_mark_complete && disaster_action_complete);
        if (systemController.isCountDown)
        {
            systemController.currentCountDown--;
        }
        gameOverCheck(checking_player);
        //Debug.Log("COUNTDOWN " + systemController.currentCountDown);
        
    }

    void gameOverCheck(int checking_player)
    {
        GetComponent<PhotonView>().RPC("rpc_gameOverCheck", RpcTarget.All, checking_player, systemController.isGameOverCheck());
    }

    [PunRPC]
    void rpc_gameOverCheck(int player, bool isGameOver)
    {
        if(player == 1)
        {
            if (isGameOver)
            {
                isP1_GameOver = true;
            }
            isCheckP1_gameOver = true;
        }
        else
        {
            if (isGameOver)
            {
                isP2_GameOver = true;
            }
            isCheckP2_gameOver = true;
        }
    }

    IEnumerator endConditionCheck(int checking_player)
    {
        yield return new WaitUntil(() => isCheckP1_gameOver && isCheckP2_gameOver);
        if (isP1_GameOver || isP2_GameOver)
        {
            if (isP1_GameOver && isP2_GameOver)
            {
                //Debug.Log("Draw");
                systemController.setEndCondition(0);
            }
            else
            {
                if (checking_player == 1)
                {
                    if (isP1_GameOver && !isP2_GameOver)
                    {
                        //Debug.Log("Lose");
                        systemController.setEndCondition(-1);
                    }
                    else if (!isP1_GameOver && isP2_GameOver)
                    {
                        //Debug.Log("Win");
                        systemController.setEndCondition(1);
                    }
                }
                else
                {
                    if (isP1_GameOver && !isP2_GameOver)
                    {
                        //Debug.Log("Win");
                        systemController.setEndCondition(1);
                    }
                    else if (!isP1_GameOver && isP2_GameOver)
                    {
                        //Debug.Log("Lose");
                        systemController.setEndCondition(-1);
                    }
                }
            }                                                                                                                                                                                                                                                                               
        }
        else if (systemController.currentCountDown < 0)
        {
            systemController.setEndCondition(0);
            isP1_GameOver = true;
            isP2_GameOver = true;
        }
    }

    //rpc function to set disaster_mark_complete
    [PunRPC]
    void rpc_setDisasterMark(bool set)
    {
        disaster_mark_complete = set;
    }

    //rpc function to set disaster_action_complete
    [PunRPC]
    void rpc_setDisasterAction(bool set)
    {
        disaster_action_complete = set;
    }
}
