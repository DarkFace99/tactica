﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;

public class Skill_Anna : MonoBehaviour
{
    [SerializeField]
    private int stunLength;
    //character cannon
    [SerializeField]
    private float effect_delay;
    [SerializeField]
    private GameObject speedLine;
    [SerializeField]
    private GameObject character_cannon;
    private GridBehaviour1 gridBehaviour;
    // Start is called before the first frame update
    void Start()
    {
        gridBehaviour = GridBehaviour1.gridBehaviour.GetComponent<GridBehaviour1>();
        StartCoroutine(waitUntilSkill());
    }

    IEnumerator waitUntilSkill()
    {
        CharacterController cc = GetComponent<CharacterController>();

        yield return new WaitUntil(() => cc.isSkill);
        
        setSkillEffect_starting();
        spawnCharacterCannon();
    }

    void anna_skill_finish()
    {
        setSkillEffect();
        GetComponent<CharacterController>().clearAllAction();
        GetComponent<ActiveSkill>().SteadyCheck_call();
    }

    void setSkillEffect()
    {
        foreach (GameObject grid in GetComponent<CharacterController>().skill_grid_list_confirm)
        {
            GridStat1 gridStat = grid.GetComponent<GridStat1>();
            if (gridStat.stepOnCharacter)
            {
                CharacterController stepOnCC = gridStat.stepOnCharacter.GetComponent<CharacterController>();
                CharacterController cc = GetComponent<CharacterController>();
                if (stepOnCC.player != cc.player)
                {
                    stepOnCC.setDirection(cc.oppositeDirection(cc.current_direction));
                    stepOnCC.setKnockBack();
                    stepOnCC.setStun_call(stunLength);
                }
            }
        }
    }

    void setSkillEffect_starting()
    {
        CharacterController cc = GetComponent<CharacterController>();
        foreach (GameObject grid in cc.skill_grid_list_confirm)
        {
            GridStat1 gridStat = grid.GetComponent<GridStat1>();
            if (gridStat.stepOnCharacter)
            {
                CharacterController stepOnCC = gridStat.stepOnCharacter.GetComponent<CharacterController>();
                if (stepOnCC.player != cc.player && goingToKnockBackCheck(cc.skill_direction_confirm , stepOnCC.row, stepOnCC.col))
                {
                    stepOnCC.setGoingToKnockBack_call(true);
                }
            }
        }
    }

    void spawnCharacterCannon()
    {
        PhotonNetwork.Instantiate(Path.Combine("Prefab", speedLine.name), Vector2.zero, Quaternion.identity);
        PhotonNetwork.Instantiate(Path.Combine("Prefab", character_cannon.name), Vector2.zero, Quaternion.identity);
        StartCoroutine(waitForCharacterEffect());
    }

    IEnumerator waitForCharacterEffect()
    {
        yield return new WaitForSecondsRealtime(effect_delay);
        anna_skill_finish();
        StartCoroutine(waitUntilSkill());
    }

    bool goingToKnockBackCheck(int direction, int row, int col)
    {
        GameObject knockBackGrid = null;

        if (direction == 1 && row -1 > -1)
        {
            knockBackGrid = gridBehaviour.gridArr[row - 1, col];
        }
        else if (direction == 2 && col + 1 < gridBehaviour.colTotal)
        {
            knockBackGrid = gridBehaviour.gridArr[row, col + 1];
        }
        else if (direction == 3 && row + 1 < gridBehaviour.rowTotal)
        {
            knockBackGrid = gridBehaviour.gridArr[row + 1, col];
        }
        else if (direction == 4 && col -1 > -1)
        {
            knockBackGrid = gridBehaviour.gridArr[row, col - 1];
        }

        if (knockBackGrid)
        {
            return knockBackGrid.tag != "HURT_TERRAIN" && knockBackGrid.tag !=  "STUN_TERRAIN";
        }
        else
        {
            return false;
        }

    }
    

}
