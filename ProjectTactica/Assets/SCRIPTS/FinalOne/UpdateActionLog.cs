﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateActionLog : MonoBehaviour
{
    private SystemController systemController;
    private CharacterController cc;
    // Start is called before the first frame update
    void Start()
    {
        systemController = SystemController.systemController.GetComponent<SystemController>();
        cc = systemController.controllingCharacter().GetComponent<CharacterController>();
        StartCoroutine(waitUntilNotControl());
    }

    // Update is called once per frame
    void Update()
    {
        if (RaycastCheck.canClickCharacter)
        {
            GetComponent<ActionLog>().setActionLog(0, GetDelayMoved());
        }
        else
        {
            GetComponent<ActionLog>().setActionLog(GetDelayMoved(), getDelayAttackOrSkill());
        }
    }

    IEnumerator waitUntilNotControl()
    {
        yield return new WaitUntil(() => !systemController.controllingCharacter());
        GetComponent<ActionLog>().setActionLog(GetDelayMoved() + getDelayAttackOrSkill(), 0);
        Destroy(this);
    }

    int getDelayAttackOrSkill()
    {
        if (cc.attacking_mark_list.Count > 0 || cc.attacking_mark_list_confirm.Count > 0)
        {
            return cc.baseAttackDelay;
        }
        else if (cc.skill_mark_list.Count > 0 || cc.skill_mark_list_confirm.Count > 0)
        {
            return cc.skillDelay;
        }
        else
        {
            return 0;
        }
    }
    int GetDelayMoved()
    {
        if(cc.direction_list_confirm.Count > 0)
        {
            return cc.direction_list_confirm.Count;
        }
        else
        {
            return cc.direction_list.Count;
        }
    }
}
