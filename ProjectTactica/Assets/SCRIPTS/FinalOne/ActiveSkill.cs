﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class ActiveSkill : MonoBehaviour
{
    [SerializeField]
    private bool isSteadySkill;
    [SerializeField]
    private int max_cooldown;
    [SerializeField]
    private bool isSetDirection;
    //======================================
    public int current_cooldown;
    private int current_cooldown_copy;
    private BattlePhase battlePhase;
    // Start is called before the first frame update
    void Start()
    {
        battlePhase = BattlePhase.battlePhase.GetComponent<BattlePhase>();
        current_cooldown = max_cooldown;
        current_cooldown_copy = current_cooldown;

        if (GetComponent<PhotonView>().IsMine)
        {
            if (!isSteadySkill)
            {
                StartCoroutine(waitUntilSteady_1());
            }
            else
            {
                StartCoroutine(waitUntilSteady_2());
            }

            StartCoroutine(waitUntilSkill());
            StartCoroutine(waitUntilChangeCooldown());
        }
    }

    IEnumerator waitUntilSteady_1()
    {
        CharacterController cc = GetComponent<CharacterController>();
        yield return new WaitUntil(() => cc.is_steady_skill);
        cc.isSkill = cc.is_steady_skill;
        cc.is_steady_skill = false;
        setDirection(cc);

        StartCoroutine(waitUntilSteady_1());
    }

    IEnumerator waitUntilSteady_2()
    {
        CharacterController cc = GetComponent<CharacterController>();
        if (!cc.is_steady_skill)
        {
            yield return new WaitUntil(() => cc.is_steady_skill);
            setGridSteady();
            cc.turnToMove = 0;
            setDirection(cc);
            battlePhase.actionCompleteDelayTime = cc.continueBattleDelay;
            battlePhase.actionCheck_complete(cc.player);
        }
        else
        {
            yield return new WaitUntil(() => !cc.is_steady_skill);
        }
        StartCoroutine(waitUntilSteady_2());
    }

    void setDirection(CharacterController cc)
    {
        if (isSetDirection)
        {
            cc.setDirection(cc.skill_direction_confirm);
        }
    }
    
    public void SteadyCheck_call()
    {
        StartCoroutine(waitForSteadyCheck());
    }

    void setGridSteady()
    {
        CharacterController cc = GetComponent<CharacterController>();
        foreach (GameObject grid in cc.skill_grid_list_confirm)
        {
            int setRow = grid.GetComponent<GridStat1>().row;
            int setCol = grid.GetComponent<GridStat1>().col;
            cc.setGridSteady_call(setRow, setCol, true);
        }
    }

    IEnumerator waitForSteadyCheck()
    {
        CharacterController cc = GetComponent<CharacterController>();

        yield return new WaitForSecondsRealtime(cc.continueBattleDelay);
        
        battlePhase.steady_check_complete(cc.player); //complete steady check of this player
    }

    IEnumerator waitUntilSkill()
    {
        CharacterController cc = GetComponent<CharacterController>();
        yield return new WaitUntil(() => cc.isSkill || cc.is_steady_skill);
        StartCoroutine(waitUntilFinishSkill());
    }

    IEnumerator waitUntilFinishSkill()
    {
        CharacterController cc = GetComponent<CharacterController>();
        yield return new WaitUntil(() => !cc.isSkill && !cc.is_steady_skill);
        current_cooldown = max_cooldown;
        StartCoroutine(waitUntilSkill());
    }

    //john Skill Efx
    public void reCooldown_call()
    {
        GetComponent<PhotonView>().RPC("rpc_reCooldown", RpcTarget.All); //change to others
    }

    [PunRPC]
    void rpc_reCooldown()
    {
        current_cooldown = max_cooldown;
    }

    IEnumerator waitUntilChangeCooldown()
    {
        yield return new WaitUntil(() => current_cooldown != current_cooldown_copy);
        current_cooldown_copy = current_cooldown;
        GetComponent<PhotonView>().RPC("rpc_set_current_cooldown", RpcTarget.Others, current_cooldown);
        StartCoroutine(waitUntilChangeCooldown());
    }

    [PunRPC]
    void rpc_set_current_cooldown(int num)
    {
        current_cooldown = num;
    }
}
