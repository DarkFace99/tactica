﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonText : MonoBehaviour
{
    [SerializeField]
    private DropZone dropZone;
    [SerializeField]
    private BigIcon bigIcon;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        for(int i = 0; i < dropZone.unitDropIcons.Count; i++)
        {
            if (!dropZone.unitDropIcons[i].isSelecting && dropZone.unitDropIcons[i].alreadyDeploy)
            {
                bigIcon.selectDropButton[i].GetComponentInChildren<Text>().text = "CANCEL";
            }
            else if (!dropZone.unitDropIcons[i].isSelecting && !dropZone.unitDropIcons[i].alreadyDeploy)
            {
                bigIcon.selectDropButton[i].GetComponentInChildren<Text>().text = "DROP";
            }
            else if (dropZone.unitDropIcons[i].isSelecting && !dropZone.unitDropIcons[i].alreadyDeploy)
            {
                bigIcon.selectDropButton[i].GetComponentInChildren<Text>().text = "CANCEL";
            }
        }
        
    }
}
