﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FourDirectionAttackGuide : MonoBehaviour
{
    public float posX; //set initial position of x publicly
    public float posY; //set initial position of y publicly
    [SerializeField]
    private bool isPassiveSkill;
    [SerializeField]
    private GameObject attackingGuide; //four direction attacking guide
    public bool isBaseAttack; //is it base attack
    public Sprite fadeSprite;
    [SerializeField]
    private bool isNoDirection;
    //=================================================
    public int row;
    public int col;
    public bool isClick;
    private bool canClick;
    public List<GameObject> attacking_guide_list;
    private GridBehaviour1 gridBehavior1;
    private SystemController systemController;
    public bool isUnmoveClick = false;

    // Start is called before the first frame update
    void Start()
    {
        transform.localPosition = new Vector3(posX, posY, transform.position.z);
        if (isPassiveSkill)
        {
            Destroy(this);
        }
        isClick = false;
        gridBehavior1 = GridBehaviour1.gridBehaviour.GetComponent<GridBehaviour1>();
        systemController = SystemController.systemController.GetComponent<SystemController>();
        setCanClick();
    }

    // Update is called once per frame
    void Update()
    {
        setRowCol(systemController.controllingCharacter());

        if (isMouseOverCheck() || isConfirmCheck())
        {
            toggleRenderer(false);
            if (isConfirmCheck())
            {
                setConfirm();
            }
        }
        else
        {
            toggleRenderer(true);
        }
    }


    void setCanClick()
    {
        if (isBaseAttack)
        {
            canClick = true;
        }
        else
        {
            GameObject obj = systemController.controllingCharacter();
            ActiveSkill activeSkill = obj.GetComponent<ActiveSkill>();
            if (activeSkill)
            {
                if(activeSkill.current_cooldown == 0)
                {
                    canClick = true;
                }
                else
                {
                    canClick = false;
                    GetComponent<SpriteRenderer>().sprite = fadeSprite;
                }
            }
            else
            {
                canClick = false;
            }
        }
    }

    public void onMouseDown()
    {
        if (!isClick && canClick)
        {
            CharacterController cc = systemController.controllingCharacter().GetComponent<CharacterController>();
            cc.resetMovingGuide();
            cc.resetSkillClicking(this);
            isClick = true;
            if (!isNoDirection)
            {
                spawnAttackingGuide(row, col);
            }
            else
            {
                spawnAttackingGuide_noDirection();
            }
            systemController.setColliider(false);
        }
    }

    void spawnAttackingGuide(int row, int col)
    {
        if (row - 1 > -1)
        {
            if (isBaseAttack)
            {
                GameObject obj = Instantiate(attackingGuide, gridBehavior1.gridArr[row - 1, col].transform.localPosition, Quaternion.identity);
                setAttackGuideValue_base(obj, 1);
            }
            else
            {
                CharacterController cc = systemController.controllingCharacter().GetComponent<CharacterController>();

                if (cc.character_number == 2)
                {
                    for (int i = 1; row - i > -1; i++)
                    {
                        if (gridBehavior1.gridArr[row - i, col].GetComponent<GridStat1>().canVisit)
                        {
                            GameObject obj = Instantiate(attackingGuide, gridBehavior1.gridArr[row - i, col].transform.localPosition, Quaternion.identity);
                            setSkillGuideValue(obj, 1, row - i, col);
                        }
                    }
                }
                else
                {
                    GameObject obj = Instantiate(attackingGuide, gridBehavior1.gridArr[row - 1, col].transform.localPosition, Quaternion.identity);
                    setSkillGuideValue(obj, 1, row, col);
                }
            }
        }
        if (col + 1 < gridBehavior1.colTotal)
        {
            if (isBaseAttack)
            {
                GameObject obj = Instantiate(attackingGuide, gridBehavior1.gridArr[row, col + 1].transform.localPosition, Quaternion.identity);
                setAttackGuideValue_base(obj, 2);
            }
            else
            {
                CharacterController cc = systemController.controllingCharacter().GetComponent<CharacterController>();

                if (cc.character_number == 2)
                {
                    for (int i = 1; col + i < gridBehavior1.colTotal; i++)
                    {
                        if (gridBehavior1.gridArr[row, col + i].GetComponent<GridStat1>().canVisit)
                        {
                            GameObject obj = Instantiate(attackingGuide, gridBehavior1.gridArr[row, col + i].transform.localPosition, Quaternion.identity);
                            setSkillGuideValue(obj, 2, row, col + i);
                        }
                    }
                }
                else
                {
                    GameObject obj = Instantiate(attackingGuide, gridBehavior1.gridArr[row, col + 1].transform.localPosition, Quaternion.identity);
                    setSkillGuideValue(obj, 2, row, col);
                }
            }
        }
        if (row + 1 < gridBehavior1.rowTotal)
        {
            if (isBaseAttack)
            {
                GameObject obj = Instantiate(attackingGuide, gridBehavior1.gridArr[row + 1, col].transform.localPosition, Quaternion.identity);
                setAttackGuideValue_base(obj, 3);
            }
            else
            {
                CharacterController cc = systemController.controllingCharacter().GetComponent<CharacterController>();

                if (cc.character_number == 2)
                {
                    for (int i = 1; row + i < gridBehavior1.rowTotal; i++)
                    {
                        if (gridBehavior1.gridArr[row + i, col].GetComponent<GridStat1>().canVisit)
                        {
                            GameObject obj = Instantiate(attackingGuide, gridBehavior1.gridArr[row + i, col].transform.localPosition, Quaternion.identity);
                            setSkillGuideValue(obj, 3, row + i, col);
                        }
                    }
                }
                else
                {
                    GameObject obj = Instantiate(attackingGuide, gridBehavior1.gridArr[row + 1, col].transform.localPosition, Quaternion.identity);
                    setSkillGuideValue(obj, 3, row, col);
                }
            }
        }
        if (col - 1 > -1)
        {
            if (isBaseAttack)
            {
                GameObject obj = Instantiate(attackingGuide, gridBehavior1.gridArr[row, col - 1].transform.localPosition, Quaternion.identity);
                setAttackGuideValue_base(obj, 4);
            }
            else
            {
                CharacterController cc = systemController.controllingCharacter().GetComponent<CharacterController>();

                if (cc.character_number == 2)
                {
                    for (int i = 1; col - i > -1; i++)
                    {
                        if (gridBehavior1.gridArr[row, col - i].GetComponent<GridStat1>().canVisit)
                        {
                            GameObject obj = Instantiate(attackingGuide, gridBehavior1.gridArr[row, col - i].transform.localPosition, Quaternion.identity);
                            setSkillGuideValue(obj, 4, row, col - i);
                        }
                    }
                }
                else
                {
                    GameObject obj = Instantiate(attackingGuide, gridBehavior1.gridArr[row, col - 1].transform.localPosition, Quaternion.identity);
                    setSkillGuideValue(obj, 4, row, col);
                }
            }
        }
    }

    void spawnAttackingGuide_noDirection()
    {
        GameObject obj = Instantiate(attackingGuide, gridBehavior1.gridArr[row, col].transform.localPosition, Quaternion.identity);
        setSkillGuideValue(obj, 4, row, col);
    }


    public void clearAttackingGuide()
    {
        foreach(GameObject obj in attacking_guide_list)
        {
            Destroy(obj);
        }
        attacking_guide_list.Clear();
        systemController.setColliider(true);
    }

    void setRowCol(GameObject obj)
    {
        if(obj.GetComponent<CharacterController>().path_list.Count == 0)
        {
            row = obj.GetComponent<CharacterController>().row;
            col = obj.GetComponent<CharacterController>().col;
        }
        else
        {
            row = obj.GetComponent<CharacterController>().path_list[obj.GetComponent<CharacterController>().path_list.Count - 1].GetComponent<GridStat1>().row;
            col = obj.GetComponent<CharacterController>().path_list[obj.GetComponent<CharacterController>().path_list.Count - 1].GetComponent<GridStat1>().col;
        }
    }

    void setAttackGuideValue_base(GameObject obj, int direction)
    {
        attacking_guide_list.Add(obj);
        obj.GetComponent<BaseAttack>().direction = direction;
        obj.GetComponent<BaseAttack>().row = row;
        obj.GetComponent<BaseAttack>().col = col;
    }

    void setSkillGuideValue(GameObject obj, int direction, int row, int col)
    {
        attacking_guide_list.Add(obj);
        obj.GetComponent<SkillMark>().direction = direction;
        obj.GetComponent<SkillMark>().row = row;
        obj.GetComponent<SkillMark>().col = col;
    }

    bool isMouseOverCheck()
    {
        foreach(GameObject obj in attacking_guide_list)
        {
            BaseAttack ba = obj.GetComponent<BaseAttack>();
            if (ba)
            {
                if (ba.isMouseOver)
                {
                    return true;
                }
            }
            else
            {
                SkillMark sm = obj.GetComponent<SkillMark>();
                if (sm.isMouseOver)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public bool isConfirmCheck()
    {
        foreach (GameObject obj in attacking_guide_list)
        {
            BaseAttack ba = obj.GetComponent<BaseAttack>();
            if (ba)
            {
                if (ba.isConfirm)
                {
                    GetComponent<SpriteRenderer>().sprite = fadeSprite;
                    systemController.controllingCharacter().GetComponent<CharacterController>().confirmSkillClick();
                    return true;
                }
            }
            else
            {
                SkillMark sm = obj.GetComponent<SkillMark>();
                if (sm.isConfirm)
                {
                    GetComponent<SpriteRenderer>().sprite = fadeSprite;
                    systemController.controllingCharacter().GetComponent<CharacterController>().confirmSkillClick();
                    return true;
                }
            }
        }
        return false;
    }

    void toggleRenderer(bool set)
    {
        foreach(GameObject obj in attacking_guide_list)
        {
            obj.GetComponent<SpriteRenderer>().enabled = set;
        }
    }

    void setConfirm()
    {
        foreach (GameObject obj in attacking_guide_list)
        {
            BaseAttack ba = obj.GetComponent<BaseAttack>();
            if (ba)
            {
                ba.isConfirm = true;
            }
            else
            {
                SkillMark sm = obj.GetComponent<SkillMark>();
                sm.isConfirm = true;
            }
        }
    }

    private void OnDestroy()
    {
        if (!isUnmoveClick)
        {
            clearAttackingGuide();
        }
    }
}
