﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropZone : MonoBehaviour
{
    [SerializeField] GridBehaviour1 gridBehave;
    public SystemController systemController;
    public List<GameObject> number;
    public List<Transform> number2;
    public List<UnitDropIcon> unitDropIcons;
    public List<GridStat1> listDropZone;
    public List<DropSpawn> spawnDrop;
    public BigIcon bigIcon;
    private float wait = 3.5f;
    public bool candrop = false;
    public GameObject dropUnit;
    private int randomNumber;
    [SerializeField]
    private AudioController sound;
    public void DropPoint(int theme, int layout)
    {
        /*=================== DESERT THEME ====================*/
        if ((theme == 1) && (layout == 1))
        {
            SetDropGrid_Desert_ONE();
        }
        else if ((theme == 1) && (layout == 2))
        {
            SetDropGrid_Desert_TWO();
        }
        else if(theme == 1 && layout == 3)
        {
            SetDropGrid_Desert_Three();
        }
        else if (theme == 1 && layout == 4)
        {
            SetDropGrid_Desert_Four();
        }
        /*=================== ICEAGE THEME ====================*/
        else if ((theme == 2) && (layout == 1))
        {
            SetDropGrid_IceAge_ONE();
        }
        else if ((theme == 2) && (layout == 2))
        {
            SetDropGrid_IceAge_TWO();
        }
        else if ((theme == 2) && (layout == 3))
        {
            SetDropGrid_IceAge_Three();
        }
        else if ((theme == 2) && (layout == 4))
        {
            SetDropGrid_IceAge_Four();
        }
        /*=================== CITY THEME ====================*/
        //else if ((theme == 3) && (layout == 1))
        //{
        //    SetDropGrid_City_ONE();
        //}
        //else if ((theme == 3) && (layout == 2))
        //{
        //    SetDropGrid_City_TWO();
        //}
    }

    /*======== SET DropZoneGrid =====*/
    void SetDropGrid_Desert_ONE()
    {
        if (SystemController.player == 1)
        {
            for (int i = 0; i < gridBehave.colTotal; i++)
            {
                for (int j = 0; j < gridBehave.rowTotal; j++)
                {
                    if (i == 0 && j == 6)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 0 && j == 7)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 1 && j == 5)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 1 && j == 6)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 2 && j == 4)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 2 && j == 5)
                    {
                        setDropZone(i, j);
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < gridBehave.colTotal; i++)
            {
                for (int j = 0; j < gridBehave.rowTotal; j++)
                {
                    if (i == 5 && j == 2)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 5 && j == 3)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 6 && j == 1)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 6 && j == 2)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 7 && j == 0)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 7 && j == 1)
                    {
                        setDropZone(i, j);
                    }
                }
            }
        }
    }
    void SetDropGrid_Desert_TWO()
    {
        if (SystemController.player == 1)
        {
            for (int i = 0; i < gridBehave.colTotal; i++)
            {
                for (int j = 0; j < gridBehave.rowTotal; j++)
                {
                    if (i == 0 && j == 0)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 0 && j == 1)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 0 && j == 5)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 0 && j == 6)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 0 && j == 7)
                    {
                        setDropZone(i, j);
                    }

                    else if (i == 1 && j == 0)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 1 && j == 1)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 1 && j == 5)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 1 && j == 6)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 2 && j == 0)
                    {
                        setDropZone(i, j);
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < gridBehave.colTotal; i++)
            {
                for (int j = 0; j < gridBehave.rowTotal; j++)
                {
                    if (i == 5 && j == 7)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 6 && j == 1)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 6 && j == 2)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 6 && j == 6)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 6 && j == 7)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 7 && j == 0)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 7 && j == 1)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 7 && j == 2)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 7 && j == 6)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 7 && j == 7)
                    {
                        setDropZone(i, j);
                    }
                }
            }
        }
    }
    void SetDropGrid_IceAge_ONE()
    {
        if(SystemController.player == 1)
        {
            for (int i = 0; i < gridBehave.colTotal; i++)
            {
                setDropZone(7, i);
            }
        }
        else
        {
            for (int i = 0; i < gridBehave.colTotal; i++)
            {
                setDropZone(0, i);
            }
        }
        
    }
    void SetDropGrid_IceAge_TWO()
    {
        if(SystemController.player == 1)
        {
            for(int i = 0; i < gridBehave.rowTotal; i++)
            {
                for(int j = 0; j < gridBehave.colTotal; j++)
                {
                    if(i == 2 && j == 5)
                    {
                        setDropZone(i, j);
                    }
                    else if(i == 3 && j == 5)
                    {
                        setDropZone(i, j);
                    }
                    else if(i == 4 && j == 5)
                    {
                        setDropZone(i, j);
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < gridBehave.rowTotal; i++)
            {
                for (int j = 0; j < gridBehave.colTotal; j++)
                {
                    if (i == 3 && j == 2)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 4 && j == 2)
                    {
                        setDropZone(i, j);
                    }
                    else if (i == 5 && j == 2)
                    {
                        setDropZone(i, j);
                    }
                }
            }
        }
    }
    void SetDropGrid_IceAge_Three()
    {
        if (SystemController.player == 1)
        {
            for (int i = 0; i < gridBehave.rowTotal; i++)
            {
                for (int j = 0; j < gridBehave.colTotal; j++)
                {
                    if (i == 5)
                    {
                        if (j == 0)
                        {
                            setDropZone(i, j);
                        }
                    }
                    else if (i == 6)
                    {
                        if (j == 0 || j == 1)
                        {
                            setDropZone(i, j);
                        }
                    }
                    else if (i == 7)
                    {
                        if (j == 0 || j == 2)
                        {
                            setDropZone(i, j);
                        }
                    }

                }
            }
        }
        else
        {
            for (int i = 0; i < gridBehave.rowTotal; i++)
            {
                for (int j = 0; j < gridBehave.colTotal; j++)
                {
                    if (i == 0)
                    {
                        if (j == 5 || j == 7)
                        {
                            setDropZone(i, j);
                        }
                    }
                    else if (i == 1)
                    {
                        if (j == 6 || j == 7)
                        {
                            setDropZone(i, j);
                        }
                    }
                    else if (i == 2 && j == 7)
                    {
                        setDropZone(i, j);
                    }

                }
            }
        }
    }
    void SetDropGrid_Desert_Four()
    {
        if (SystemController.player == 1)
        {
            for (int i = 0; i < gridBehave.rowTotal; i++)
            {
                for (int j = 0; j < gridBehave.colTotal; j++)
                {
                    if (i == 5)
                    {
                        if (j == 6 || j == 7)
                        {
                            setDropZone(i, j);
                        }
                    }
                    else if (i == 6)
                    {
                        if (j == 5 || j == 6 || j == 7)
                        {
                            setDropZone(i, j);
                        }
                    }
                    else if (i == 7)
                    {
                        if (j == 5 || j == 6)
                        {
                            setDropZone(i, j);
                        }
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < gridBehave.rowTotal; i++)
            {
                for (int j = 0; j < gridBehave.colTotal; j++)
                {
                    if (i == 0)
                    {
                        if (j == 1 || j == 2)
                        {
                            setDropZone(i, j);
                        }
                    }
                    else if (i == 1)
                    {
                        if (j == 0 || j == 1 || j == 2)
                        {
                            setDropZone(i, j);
                        }
                    }
                    else if (i == 2)
                    {
                        if (j == 0 || j == 1)
                        {
                            setDropZone(i, j);
                        }
                    }
                }
            }
        }
    }
    void SetDropGrid_Desert_Three()
    {
        if(SystemController.player == 1)
        {
            for(int i = 0; i < gridBehave.rowTotal; i++)
            {
                for(int j = 0; j < gridBehave.colTotal; j++)
                {
                    if (i == 0)
                    {
                        if (j == 2 || j == 3 || j == 4 || j == 5)
                        {
                            setDropZone(i, j);
                        }
                    }
                    else if (i == 1)
                    {
                        if (j == 3 || j == 4)
                        {
                            setDropZone(i, j);
                        }
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < gridBehave.rowTotal; i++)
            {
                for (int j = 0; j < gridBehave.colTotal; j++)
                {
                    if (i == 7)
                    {
                        if (j == 2 || j == 3 || j == 4 || j == 5)
                        {
                            setDropZone(i, j);
                        }
                    }
                    else if (i == 6)
                    {
                        if (j == 3 || j == 4)
                        {
                            setDropZone(i, j);
                        }
                    }
                }
            }
               
        }
    }
    void SetDropGrid_IceAge_Four()
    {
        if (SystemController.player == 1)
        {
            for (int i = 0; i < gridBehave.rowTotal; i++)
            {
                for (int j = 0; j < gridBehave.colTotal; j++)
                {
                    if (i == 6 || i == 7)
                    {
                        if (j == 0 || j == 1 || j == 6 || j == 7)
                        {
                            setDropZone(i, j);
                        }
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < gridBehave.rowTotal; i++)
            {
                for (int j = 0; j < gridBehave.colTotal; j++)
                {
                    if (i == 0 || i == 1)
                    {
                        if (j == 0 || j == 1 || j == 6 || j == 7)
                        {
                            setDropZone(i, j);
                        }
                    }
                }
            }
        }
    }

    /*============== OTHER ==============*/
    void setDropZone(int row, int col)
    {
        listDropZone.Add(gridBehave.gridArr[row, col].GetComponent<GridStat1>()); // Call the gridStat1 from the gridArr that is the drop zone and Add member to the List
        gridBehave.gridArr[row, col].GetComponent<GridStat1>().isDropZone = true; // Call the gridStat1 from the gridArr that is the drop zone and Set isDropZone and assign this grid isDropZone to true
        gridBehave.gridArr[row, col].GetComponent<GridStat1>().canDeploy = true; // Call the gridStat1 from the gridArr that is the drop zone 
        gridBehave.gridArr[row, col].GetComponent<GridStat1>().rend.color = new Color(11f/255f, 255f/255f, 0f / 255f, 1f); // Call the gridStat1 from the gridArr that is the drop zone to change color to pink if it is drop zone
        //Debug.Log("Color: " + gridBehave.gridArr[row, col].GetComponent<SpriteRenderer>().color);
    }
    void ChangeSprite()
    {
        for (int i = 0; i < gridBehave.colTotal; i++) // Lope in number of columns
        {
            for (int j = 0; j < gridBehave.rowTotal; j++) // Lope in number of rows
            {
                if (gridBehave.gridArr[i, j].GetComponent<GridStat1>().isDropZone == true)
                {
                    SetColor(i, j); // Set 2D array in another function
                }
            }
        }
    }
    //==================== Function to set the 2D array that grid ====================
    void SetColor(int row, int col)
    {
        if (gridBehave.gridArr[row, col].GetComponent<GridStat1>().isDropZone == true) // Set Condition Check that grid is Drop zone
        {
            gridBehave.gridArr[row, col].GetComponent<GridStat1>().rend.color = new Color(1f, 1f, 1f, 1f); // Change color back to white
            //gridBehave.gridArr[row, col].GetComponent<GridStat1>().rend.sprite = normal;
        }
    }
    public IEnumerator ChangeColourBack()
    {
        yield return new WaitUntil(() => SystemController.currentState != SystemController.GameState.Drop); // Set that is not the Drop phase
        ChangeSprite(); // Call function change colour
    }
    public IEnumerator WaitAtFirst()
    {
        yield return new WaitForSecondsRealtime(wait);
        for (int i = 0; i < number.Count; i++)
        {
            for (int j = 0; j < number[i].transform.childCount; j++)
            {
                if (number[i].transform.GetChild(j).gameObject.activeSelf == true)
                {
                    //Debug.Log("Yay");
                    number2.Add(number[i].transform.GetChild(j));
                }
                // Debug.Log("I is " + i + " Name is " + number[i].transform.GetChild(j).name + " is " + number[i].transform.GetChild(j).gameObject.activeSelf);
            }
        }
        for (int i = 0; i < number2.Count; i++)
        {
            unitDropIcons.Add(number2[i].GetComponent<UnitDropIcon>());
        }
    }
    //==================== This IEnumerator use for Waiting after DropPhase ====================
    public IEnumerator Waiting()
    {
        yield return new WaitForSeconds(systemController.dropTime - 0.9f); // For the reason that cannot run in time I choose to run it before go to new GameState
        for (int i = 0; i < unitDropIcons.Count; i++)
        {
            unitDropIcons[i].isSelecting = false;
            unitDropIcons[i].alreadyDeploy = true;
        }
        RandomEndDrop(); // Start function name it random
    }
    //==================== This Function use for deploy unit and Random when player doesn't select grid to deploy ====================
    void RandomEndDrop()
    {
        if (SystemController.player == 1) // For player 1 because of the RPC and send data it is seperate if it is not show for 2 screen
        {
            for (int i = 0; i < systemController.spawn_point.Count; i++) // The lope that run in size of this array
            {
                if (systemController.dropZone.spawnDrop[i] != null) // Check condition if there is spawnDrop list that keep in the gridBehaviour and if it is not null which is means has already select
                {
                    systemController.characterControllers[i].current_direction = 4; // Set current direction of player 1's units the directon point left from CharacterController Line 59
                    systemController.spawn_point[i] = gridBehave.gridArr[gridBehave.dropZone.spawnDrop[i].row, gridBehave.dropZone.spawnDrop[i].col].transform.localPosition; // Set Spawn point at that grid which player select
                }
                else if (systemController.spawn_point[i].x == 0 || systemController.spawn_point[i].y == 0) // Set condition that is the default when player doesn't select where to drop units
                {
                    randomNumber = Random.Range(0, gridBehave.dropZone.listDropZone.Count); // Random number of array that from list
                    systemController.characterControllers[i].current_direction = 4; // Set current direction of player 1's units the directon point left
                    systemController.spawn_point[i] = gridBehave.gridArr[gridBehave.dropZone.listDropZone[randomNumber].row, gridBehave.dropZone.listDropZone[randomNumber].col].transform.localPosition; // Set the spawn point use the Vector2 of gridArr from "gridBehaviour" to set position of the random unit
                    gridBehave.dropZone.listDropZone.Remove(gridBehave.dropZone.listDropZone[randomNumber]); // Remove drop zone after random
                }
            }
        }
        else // For player 2
        {
            for (int i = 0; i < systemController.spawn_point.Count; i++) // The lope that run in size of this array
            {
                if (gridBehave.dropZone.spawnDrop[i] != null) // Check condition if there is spawnDrop list that keep in the gridBehaviour and if it is not null which is means has already select
                {
                    systemController.characterControllers[i].current_direction = 2; // Set current direction of player 2's units the directon point right from CharacterController Line 59
                    systemController.spawn_point[i] = gridBehave.gridArr[gridBehave.dropZone.spawnDrop[i].row, gridBehave.dropZone.spawnDrop[i].col].transform.localPosition; // Set Spawn point at that grid which player select
                }
                else if (systemController.spawn_point[i].x == 0 || systemController.spawn_point[i].y == 0) // Set condition that is the default when player doesn't select where to drop units
                {
                    randomNumber = Random.Range(0, gridBehave.dropZone.listDropZone.Count); // Random number of array that from list
                    systemController.characterControllers[i].current_direction = 2; // Set current direction of player 1's units the directon point right
                    systemController.spawn_point[i] = gridBehave.gridArr[gridBehave.dropZone.listDropZone[randomNumber].row, gridBehave.dropZone.listDropZone[randomNumber].col].transform.localPosition; // Set the spawn point use the Vector2 of gridArr from "gridBehaviour" to set position of the random unit
                    gridBehave.dropZone.listDropZone.Remove(gridBehave.dropZone.listDropZone[randomNumber]); // Remove drop zone after random
                }
            }
        }
        systemController.setReadyCall(SystemController.player);
    }
    [PunRPC]
    void RPC_CanDrop1()
    {
        candrop = true;
    }
    [PunRPC]
    public void CanDrop1()
    {
        GetComponent<PhotonView>().RPC("RPC_CanDrop1", RpcTarget.All);
    }
    public IEnumerator DestroyAll()
    {
        yield return new WaitForSeconds(systemController.dropTime);
        RemoveSelectGrid(); // This function Line 257 use for Destroy deploy sample that Instantiate after deploy on grid
    }
    //==================== This function use to Remove Deploy sample ====================
    void RemoveSelectGrid()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Deploy"); // Add GameObject array that FindGameObjectWithTag string name "Deploy"
        for (int i = 0; i < objs.Length; i++) // Run lope by the number of the objs array
        {
            Destroy(objs[i]); // Destroy all of the objs array
        }
    }
    [PunRPC]
    void RPC_DisableButton(int i)
    {
        bigIcon.selectDropButton[i].SetActive(false);
        bigIcon.selectUnitImage[i].SetActive(false);
        systemController.arrow1[i].SetActive(false);
        if (unitDropIcons[i].isSelecting == true)
        {
            unitDropIcons[i].isSelecting = false;
        }
    }
    public IEnumerator GoToNextPhase()
    {
        yield return new WaitUntil(() => SystemController.currentState != SystemController.GameState.Drop);
        for (int i = 0; i < bigIcon.selectDropButton.Count; i++)
        {
            GetComponent<PhotonView>().RPC("RPC_DisableButton", RpcTarget.All, i);
        }
        if (SystemController.player == 1)
        {
            sound.audio[1].Play();
        }
        if (sound.audio[0].isPlaying)
        {
            sound.audio[0].Stop();
        }
    }
    public IEnumerator GetSprite()
    {
        yield return new WaitForSecondsRealtime(wait + 1.0f);
        bigIcon.BigUnitIcon();
        bigIcon.CheckCrocBoi();
        for (int i = 0; i < systemController.arrow1.Count; i++)
        {
            systemController.arrow1[i].SetActive(true);
        }
    }
}