﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class RegionSelect : MonoBehaviour
{
    [SerializeField]
    private Dropdown dropDown;
    [SerializeField]
    private Text warning_text;
    [SerializeField]
    private float warning_text_delay;
    private string[] regionArr;

    private void Awake()
    {
        setRegionArr();
        setStartDropDown();
        warning_text.enabled = false;
    }

    void setRegionArr()
    {
        regionArr = new string[14];
        regionArr[0] = "asia";
        regionArr[1] = "au";
        regionArr[2] = "cae";
        regionArr[3] = "cn";
        regionArr[4] = "eu";
        regionArr[5] = "in";
        regionArr[6] = "jp";
        regionArr[7] = "ru";
        regionArr[8] = "rue";
        regionArr[9] = "za";
        regionArr[10] = "sa";
        regionArr[11] = "kr";
        regionArr[12] = "us";
        regionArr[13] = "usw";
    }

    int getIndex(string sumPref)
    {
        int index = 0;
        foreach (char c in sumPref)
        {
            if(c == ';')
            {
                return index;
            }
            index++;
        }
        return index;
    }

    string getBestRegionToken()
    {
        string best_region = PhotonNetwork.BestRegionSummaryInPreferences;
        return best_region.Remove(getIndex(best_region));
    }

    void setStartDropDown()
    {
        string starting_region;
        if (PlayerPrefs.HasKey("REGION"))
        {
            starting_region = PlayerPrefs.GetString("REGION");
        }
        else
        {
            starting_region = getBestRegionToken();
            PlayerPrefs.SetString("REGION", starting_region);
        }

        for (int i = 0; i < 14; i++)
        {
            if (starting_region == regionArr[i])
            {
                dropDown.value = i;
            }
        }
    }

    public void dropdown_region(int index)
    {
        //PlayerPrefs.SetString("REGION", regionArr[index]);
        if(regionArr[index] != getBestRegionToken())
        {
            warning_text.enabled = true;
            StartCoroutine(waitForWarningDisappear());
        }
    }

    IEnumerator waitForWarningDisappear()
    {
        yield return new WaitForSecondsRealtime(warning_text_delay);
        warning_text.enabled = false;
    }
    
    public void saveRegion()
    {
        PlayerPrefs.SetString("REGION", regionArr[dropDown.value]);
    }

}
