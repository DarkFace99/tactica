﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;

public class PressAnyKey : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private Text text;
    [SerializeField]
    private float blinkDelay;
    [SerializeField]
    private string nextSceneName;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(waitForBlinkText());
        StartCoroutine(waitForStart());
    }

    IEnumerator waitForBlinkText()
    {
        if (text.enabled)
        {
            yield return new WaitForSecondsRealtime(blinkDelay * 2f);
        }
        else
        {
            yield return new WaitForSecondsRealtime(blinkDelay);
        }
        text.enabled = !text.enabled;
        StartCoroutine(waitForBlinkText());
    }
    

    IEnumerator waitUntilKeyDown()
    {
        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Space));
        connectToRegion();
    }

    IEnumerator waitForStart()
    {
        yield return new WaitForSecondsRealtime(0.1f);
        StartCoroutine(waitUntilKeyDown());
    }

    void connectToRegion()
    {
        PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = PlayerPrefs.GetString("REGION");
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        GetComponent<RegionSelect>().saveRegion();
        SceneManager.LoadScene(nextSceneName);
    }

}
