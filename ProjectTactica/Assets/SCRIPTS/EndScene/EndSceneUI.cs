﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Photon.Pun;

public class EndSceneUI : MonoBehaviour
{
    [SerializeField]
    private string menuSceneName;
    [SerializeField]
    private string pressAnySceneName;
    [SerializeField]
    private Text rewardText;
    [SerializeField]
    private Text finalMoneyText;
    [SerializeField]
    private Image bgImage;
    [SerializeField]
    private Sprite lose_bg;
    
    [SerializeField]
    private List<GameObject> display_list;
    [SerializeField]
    private List<int> reward_list;
    [SerializeField]
    private List<int> reward_list2;
    private int reward;
    private int final_money;
    private int display_reward;
    private float waitNum;
    [SerializeField]
    private Animator textAnim;
    [SerializeField]
    private GameObject[] MoneyObject;
    private string mode;

    void Start()
    {
        mode = ModeSelect.Instance.gameMode;
        //mode = "HARDCORE";
        final_money = 0;
        display_reward = 0;
        reward = 0;
        waitNum = 3.30f;
        setEndConditionText();
        if(ModeSelect.Instance.gameMode == "PRIVATE")
        {
            for(int i = 0; i < MoneyObject.Length; i++)
            {
                MoneyObject[i].SetActive(false);
            }
        }
    }

    void setEndConditionText()
    {
        int dataIndex = DataLogin.Instance.IdIndex;
        //int dataIndex = 0;
        //PlayerPrefs.SetInt("END_CONDITION" + dataIndex, 0);
        int endCondition = PlayerPrefs.GetInt("END_CONDITION" + dataIndex);
        if (endCondition < 1) // DEFEAT OR DRAW
        {
            bgImage.sprite = lose_bg;
        }
        
        //reward = reward_list[endCondition + 1];
        final_money = PlayerPrefs.GetInt("Money" + dataIndex, PlayerPrefs.GetInt("Money" + dataIndex));
        finalMoneyText.text = final_money.ToString();
        rewardText.text = "";
        if (mode != "PRIVATE")
        {
            if(mode == "QUICKPLAY")
            {
                reward = reward_list[endCondition + 1];
                if (endCondition == -1)
                {
                    PlayerPrefs.SetInt("Money" + dataIndex, PlayerPrefs.GetInt("Money" + dataIndex) + reward_list[0]);
                }
                else if (endCondition == 1)
                {
                    PlayerPrefs.SetInt("Money" + dataIndex, PlayerPrefs.GetInt("Money" + dataIndex) + reward_list[2]);
                }
                else
                {
                    PlayerPrefs.SetInt("Money" + dataIndex, PlayerPrefs.GetInt("Money" + dataIndex) + reward_list[1]);
                }
            }
            else if(mode == "HARDCORE")
            {
                reward = reward_list2[endCondition + 1];
                if (endCondition == -1)
                {
                    rewardText.color = new Color(1f, 0f, 0f, 1f);
                    PlayerPrefs.SetInt("Money" + dataIndex, PlayerPrefs.GetInt("Money" + dataIndex) + reward_list2[0]);
                }
                else if (endCondition == 1)
                {
                    PlayerPrefs.SetInt("Money" + dataIndex, PlayerPrefs.GetInt("Money" + dataIndex) + reward_list2[2]);
                }
                else
                {
                    PlayerPrefs.SetInt("Money" + dataIndex, PlayerPrefs.GetInt("Money" + dataIndex) + reward_list2[1]);
                }
            }
        }
        PlayerPrefs.DeleteKey("END_CONDITION" + dataIndex); //clear memory
        
        StartCoroutine(waitForPlayAnim(endCondition + 1));
    }

    public void goToMenu()
    {
        if (PhotonNetwork.IsConnected)
        {
            SceneManager.LoadScene(menuSceneName);
        }
        else
        {
            SceneManager.LoadScene(pressAnySceneName);
        }
    }

    private IEnumerator MoneyUpdater()
    {
        while (true)
        {
            if (display_reward != reward)
            {
                if(display_reward < reward)
                {
                    display_reward++;
                    rewardText.text = "+ " + display_reward.ToString();
                }
                else
                {
                    display_reward--;
                    rewardText.text = "- " + (display_reward * -1).ToString();
                }
            }
            else
            {
                StartCoroutine(atTheEnd());
                break;
            }
            yield return new WaitForSecondsRealtime(0.01f);
        }
    }

    private IEnumerator atTheEnd()
    {
        yield return new WaitForSecondsRealtime(1.5f);
        textAnim.SetTrigger("FinalSum");
        rewardText.text = "";
        finalMoneyText.text = (final_money + reward).ToString();
    }

    IEnumerator waitForPlayAnim(int index)
    {
        yield return new WaitForSecondsRealtime(3.8f);
        display_list[index].SetActive(true);
        StartCoroutine(waitForChangeCurrency());
    }

    IEnumerator waitForChangeCurrency()
    {
        yield return new WaitForSecondsRealtime(1.2f);
        StartCoroutine(MoneyUpdater());
    }
}