﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundEndScene : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        SoundController.Instance.audio[1].Stop();
        SoundController.Instance.audio[0].Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
