﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TestingScript : MonoBehaviour
{
    [SerializeField]
    private GameObject damageText;
    private int damage = 1;
    // Start is called before the first frame update
    void Start()
    {
        GameObject obj = Instantiate(damageText, new Vector2(transform.position.x, transform.position.y + 1.1f), Quaternion.identity);
        obj.GetComponent<TextMeshPro>().SetText(damage.ToString());
    }
}
