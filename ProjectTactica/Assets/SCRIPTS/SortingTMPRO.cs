﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SortingTMPRO : MonoBehaviour
{
    [SerializeField] private string layerName;
    private MeshRenderer mesh;
    // Start is called before the first frame update
    void Start()
    {
        mesh = GetComponent<MeshRenderer>();
        mesh.sortingLayerName = layerName;
    }
}
