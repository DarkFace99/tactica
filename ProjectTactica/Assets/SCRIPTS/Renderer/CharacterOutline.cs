﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class CharacterOutline : MonoBehaviour
{
    [SerializeField]
    private Material firstOne;
    [SerializeField]
    private Material secondOne;
    private SpriteRenderer spriteRender;

    private void Awake()
    {
        spriteRender = GetComponent<SpriteRenderer>();

        if (GetComponent<PhotonView>().IsMine)
        {
            setMaterial(firstOne);
        }
        else
        {
            setMaterial(secondOne);
        }
    }

    void setMaterial(Material mat)
    {
        spriteRender.material = mat;
    }
}
