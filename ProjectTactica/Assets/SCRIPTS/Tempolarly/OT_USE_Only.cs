﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OT_USE_Only : MonoBehaviour
{
    public List<GameObject> testSubject;
    [SerializeField] bool isChecked;

    // Start is called before the first frame update
    void Start()
    {
        isChecked = false;
        testSubject[0].SetActive(true);
        testSubject[1].SetActive(false);

        StartCoroutine(Example());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator Example()
    {
        Debug.Log("Still Didn't pressed anything");
        yield return new WaitUntil(() => isChecked == true);
        testSubject[0].SetActive(false);
        testSubject[1].SetActive(true);
        isChecked = false;
    }

    public void PressedBut() {
        isChecked = true;
    }

}
