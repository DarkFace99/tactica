﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempoSettings : MonoBehaviour
{
    public int width, height;
    void Awake()
    {
        //bool start = true;
        //SetFull(start);
        if(PlayerPrefs.HasKey("Width") == false || PlayerPrefs.HasKey("Height") == false)
        {
            PlayerPrefs.SetInt("Width", 1920);
            PlayerPrefs.SetInt("Height", 1080);
        }
        if(PlayerPrefs.HasKey("FullScreen") == false)
        {
            PlayerPrefs.SetInt("FullScreen", 1);
            
        }
        if((PlayerPrefs.HasKey("Width") || PlayerPrefs.HasKey("Height") && (PlayerPrefs.HasKey("FullScreen") == false)))
        {
            Screen.SetResolution(PlayerPrefs.GetInt("Width"), PlayerPrefs.GetInt("Height"), !Screen.fullScreen);
        }
        if ((PlayerPrefs.HasKey("Width") || PlayerPrefs.HasKey("Height") && (PlayerPrefs.HasKey("FullScreen") == true)))
        {
            Screen.SetResolution(PlayerPrefs.GetInt("Width"), PlayerPrefs.GetInt("Height"), Screen.fullScreen);
        }
    }

    

    public void SetWidt(int W)
    {
        width = W;
    }
    public void SetHeight(int H)
    {
        height = H;
    }

    public void SetReso()
    {
        Screen.SetResolution(width, height, Screen.fullScreen);
        PlayerPrefs.SetInt("Width", width);
        PlayerPrefs.SetInt("Height", height);
    }

    public void SetFull(bool isfull)
    {
        Screen.fullScreen = isfull;
        if(isfull)
        {
            PlayerPrefs.SetInt("FullScreen", 1);
        }
        else
        {
            PlayerPrefs.SetInt("FullScreen", 0);
        }
    }
}
