﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    [SerializeField]
    private GameObject firstButton;
    [SerializeField]
    private GameObject secondButton;
    [SerializeField]
    private string TestScene;
    [SerializeField]
    private string CharacterMenuScene;
    [SerializeField]
    private string Credit;
    [SerializeField]
    private GameObject graphicPanel;
    [SerializeField]
    private GameObject volumnPanel;
    [SerializeField]
    private GameObject optionButton;
    [SerializeField]
    private GameObject returnButton;
    [SerializeField]
    private GameObject graphicButton;
    [SerializeField]
    private GameObject volumnButton;
    [SerializeField]
    private GameObject LogoutButton;

    public void goHOWTO()
    {
        SceneManager.LoadScene("HowToPlay");
    }
    public void GoToTestScene()
    {
        SceneManager.LoadScene(TestScene);
    }
    public void GoToMenuCharacterScene()
    {
        SceneManager.LoadScene(CharacterMenuScene);
    }
    public void GoToCredit()
    {
        SceneManager.LoadScene(Credit);
    }
    public void Option()
    {
        firstButton.SetActive(false);
        secondButton.SetActive(true);
        optionButton.GetComponent<Image>().color = new Color(0f, 0f, 0f, 0f);
        LogoutButton.SetActive(false);
    }
    public void Return()
    {
        secondButton.SetActive(false);
        firstButton.SetActive(true);
        returnButton.GetComponent<Image>().color = new Color(0f, 0f, 0f, 0f);
        LogoutButton.SetActive(true);
    }
    public void Graphic()
    {
        graphicPanel.SetActive(true);
        secondButton.SetActive(false);
        graphicButton.GetComponent<Image>().color = new Color(0f, 0f, 0f, 0f);
        LogoutButton.SetActive(false);
    }
    public void Volumn()
    {
        volumnPanel.SetActive(true);
        secondButton.SetActive(false);
        volumnButton.GetComponent<Image>().color = new Color(0f, 0f, 0f, 0f);
        LogoutButton.SetActive(false);
    }
    public void Back1()
    {
        graphicPanel.SetActive(false);
        secondButton.SetActive(true);
    }
    public void Back2()
    {
        volumnPanel.SetActive(false);
        secondButton.SetActive(true);
    }
}
