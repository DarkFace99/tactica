﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputEvent : MonoBehaviour
{
    [SerializeField]
    private float width_expanded;
    [SerializeField]
    private float height_expanded;
    private float width_start;
    private float height_start;
    private RectTransform rect;
    // Start is called before the first frame update
    private void Awake()
    {
        rect = GetComponent<RectTransform>();
        width_start = rect.rect.width;
        height_start = rect.rect.height;
    }

    private void OnEnable()
    {
        mouseExit();
    }

    public void mouseEnter()
    {
        rect.sizeDelta = new Vector2(width_expanded, height_expanded);
    }

    public void mouseExit()
    {
        rect.sizeDelta = new Vector2(width_start, height_start);
    }
}
