﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModeSelect : MonoBehaviour
{
    public static ModeSelect Instance { get; private set; }
    public string gameMode;
    private void Awake()
    {
        //==================== Singleton for Not spawn more than 1 and the keep value between scene ====================
        //_characterSelectManager2 = FindObjectOfType<CharacterSelectManager2>();
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
