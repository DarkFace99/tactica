﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CheatButton : MonoBehaviour
{
    [SerializeField]
    private int userIndex;
    [SerializeField]
    private string nameScene;
    public Resolution[] resolutions;
    private bool isFull;
    private void Awake()
    {
        

        if (PlayerPrefs.GetInt("FullScreen") == 0)
        {
            isFull = false;
        }
        else
        {
            isFull = true;
        }
        Screen.SetResolution(PlayerPrefs.GetInt("Width"), PlayerPrefs.GetInt("Height"), isFull);
        QualitySettings.SetQualityLevel(PlayerPrefs.GetInt("Quality"));
        if (PlayerPrefs.HasKey("master"))
        {
            PlayerPrefs.SetFloat("master", PlayerPrefs.GetFloat("master"));
        }
        else
        {
            PlayerPrefs.SetFloat("master", 0);
        }
        if (PlayerPrefs.HasKey("music"))
        {
            PlayerPrefs.SetFloat("music", PlayerPrefs.GetFloat("music"));
        }
        else
        {
            PlayerPrefs.SetFloat("music", 0);
        }
        if (PlayerPrefs.HasKey("sfx"))
        {
            PlayerPrefs.SetFloat("sfx", PlayerPrefs.GetFloat("sfx"));
        }
        else
        {
            PlayerPrefs.SetFloat("sfx", 0);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        userIndex = DataLogin.Instance.IdIndex;
        StartCoroutine(Cheater());
    }
    void Update()
    {
        //if(Input.GetKeyDown(KeyCode.Space))
        //{
        //    CheatAllUnit();
        //    Debug.Log("You got all units");
        //}
        //if (Input.GetKey(KeyCode.RightShift) && Input.GetKey(KeyCode.RightShift))
        //{
        //    CheatMoney();
        //    Debug.Log("Cheat Activated");
        //}
    }
    public void CheatAllUnit()
    {
        PlayerPrefs.SetInt("UnlockAnna" + userIndex, 1);
        PlayerPrefs.SetInt("UnlockCroc-Boi" + userIndex, 1);
        PlayerPrefs.SetInt("UnlockNeiga" + userIndex, 1);
        PlayerPrefs.SetInt("UnlockCharacter4" + userIndex, 1);
        PlayerPrefs.SetInt("UnlockCharacter5" + userIndex, 1);
        PlayerPrefs.SetInt("UnlockCharacter6" + userIndex, 1);
        PlayerPrefs.SetInt("UnlockCharacter7" + userIndex, 1);
        PlayerPrefs.SetInt("UnlockCharacter8" + userIndex, 1);
        PlayerPrefs.SetInt("UnlockCharacter9" + userIndex, 1);
        PlayerPrefs.SetInt("CharacterCount" + userIndex, 9);
    }
    public void CheatMoney()
    {
        PlayerPrefs.SetInt("Money" + userIndex, 50000);
    }
    public void Logout()
    {
        SceneManager.LoadScene(nameScene);
    }
    IEnumerator Cheater()
    {
        yield return new WaitUntil(() =>(Input.GetKey(KeyCode.F) && Input.GetKey(KeyCode.O) && Input.GetKey(KeyCode.N)));
        CheatMoney();
        Debug.Log("Cheat Activated");
    }
}
