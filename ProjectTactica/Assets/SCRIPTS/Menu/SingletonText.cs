﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;

public class SingletonText : MonoBehaviour
{
    public static SingletonText singletonText;
    [SerializeField]
    private TextMeshProUGUI textPing;
    [SerializeField]
    private TextMeshProUGUI textFps;
    [SerializeField]
    private float changeTextDelay;
    [SerializeField]
    private GameObject wifi_signal;
    [SerializeField]
    private List<Sprite> wifi_signal_sprite;
    [SerializeField]
    private List<int> ping_level;
    
    void Awake()
    {
        if (!singletonText)
        {
            singletonText = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        setText_ping();
        setText_fps();
        StartCoroutine(waitForChangeText());
    }

    void setText_ping()
    {
        if (PhotonNetwork.IsConnected)
        {
            int ping = PhotonNetwork.GetPing();
            textPing.SetText(ping + "ms");
            setWifiSignal(ping);
        }
        else
        {
            textPing.SetText("");
        }
    }

    void setText_fps()
    {
        textFps.SetText("FPS : " + (1f/Time.deltaTime).ToString("0"));
    }

    IEnumerator waitForChangeText()
    {
        yield return new WaitForSecondsRealtime(changeTextDelay);
        setText_ping();
        setText_fps();
        StartCoroutine(waitForChangeText());
    }

    void setWifiSignal(int ping)
    {
        for(int i = wifi_signal_sprite.Count - 1; i > -1; i--)
        {
            if(ping >= ping_level[i])
            {
                wifi_signal.GetComponent<Image>().sprite = wifi_signal_sprite[i];
                if(i == wifi_signal_sprite.Count - 1)
                {
                    transform.localScale = new Vector3(0.2f, 0.2f, 1f);
                }
                else
                {
                    transform.localScale = new Vector3(0.1f, 0.1f, 1f);
                }
                break;
            }
        }
    }

}
