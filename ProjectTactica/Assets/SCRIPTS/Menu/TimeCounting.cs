﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeCounting : MonoBehaviour
{
    [SerializeField]
    private Text display_text;
    private float sec = 0f;
    private float min = 0f;

    private void Update()
    {
        timeCheck();
    }

    private void OnDisable()
    {
        sec = 0f;
        min = 0f;
    }

    void timeCheck()
    {
        sec += Time.deltaTime;
        if(sec >= 59f)
        {
            sec = 0f;
            min++;
        }
        display_text.text = timeDisplay(min) + ":" + timeDisplay(sec);
    }

    string timeDisplay(float time)
    {

        if(time.ToString("0").Length < 2)
        {
            return "0" + time.ToString("0");
        }
        else
        {
            return time.ToString("0");
        }
    }
}
