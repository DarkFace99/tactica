﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonMenu : MonoBehaviour
{
    [SerializeField] GameObject menuGameObject;
    [SerializeField] GameObject option;

    public void CharacterGO()
    {
        SceneManager.LoadScene("MenuCharacters");
    }

    public void QuitGO()
    {
        Debug.Log("Problem Quit");
        Application.Quit();
    }

    public void OptionGO()
    {
        //menuGameObject.SetActive(false); 
        GameObject optionGameObject = Instantiate(option, transform.position, Quaternion.identity);
        optionGameObject.transform.SetParent(this.transform, false);
        optionGameObject.transform.localPosition = Vector3.zero;
        
    }
}
