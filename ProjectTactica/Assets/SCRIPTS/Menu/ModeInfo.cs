﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModeInfo : MonoBehaviour
{
    int number = 0;
    [SerializeField]
    private GameObject info;

    public void click()
    {
        number++;
        info.SetActive(number % 2 != 0);
    }
}
