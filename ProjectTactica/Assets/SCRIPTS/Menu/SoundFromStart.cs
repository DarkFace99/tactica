﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundFromStart : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if(SoundController.Instance.audio[1].isPlaying)
        {
            SoundController.Instance.audio[1].Stop();
        }
        if(!SoundController.Instance.audio[0].isPlaying)
        {
            SoundController.Instance.audio[0].Play();
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
