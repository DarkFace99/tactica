﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class SoundController : MonoBehaviour
{
    [SerializeField]
    private AudioMixer mixer;
    [SerializeField]
    private int lastSceneIndex;
    [SerializeField]
    private int menuSceneIndex;
    [SerializeField]
    private int startingSceneIndex;
    [SerializeField]
    private int firstSceneIndex;
    [SerializeField]
    private int characterSelectIndex;
    public static SoundController Instance { get; private set; }
    public AudioSource[] audio;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

    }
    void Start()
    {
        //StartCoroutine(waitUntilFinalOne());
        //StartCoroutine(FirstPlay());
        //StartCoroutine(waitForCharacterSelect());
        if (PlayerPrefs.HasKey("master"))
        {
            mixer.SetFloat("volume", PlayerPrefs.GetFloat("master"));
            //PlayerPrefs.SetFloat("master", PlayerPrefs.GetFloat("master"));
        }
        else
        {
            PlayerPrefs.SetFloat("master", -15);
        }
        if (PlayerPrefs.HasKey("music"))
        {
            mixer.SetFloat("music", PlayerPrefs.GetFloat("music"));
            //PlayerPrefs.SetFloat("music", PlayerPrefs.GetFloat("music"));
        }
        else
        {
            PlayerPrefs.SetFloat("music", -15);
        }
        if (PlayerPrefs.HasKey("sfx"))
        {
            //PlayerPrefs.SetFloat("sfx", PlayerPrefs.GetFloat("sfx"));
            mixer.SetFloat("sfx", PlayerPrefs.GetFloat("sfx"));
        }
        else
        {
            PlayerPrefs.SetFloat("sfx", -15);
        }
       // Debug.Log(PlayerPrefs.GetFloat("master") + " " + PlayerPrefs.GetFloat("music") + " " + PlayerPrefs.GetFloat("sfx"));

    }
    //IEnumerator waitForCharacterSelect()
    //{
    //    yield return new WaitUntil(() => SceneManager.GetActiveScene().buildIndex == characterSelectIndex);
    //    audio[0].Stop();
    //    audio[1].Play();
    //}
    //IEnumerator waitUntilFinalOne()
    //{
    //    yield return new WaitUntil(() => SceneManager.GetActiveScene().buildIndex == lastSceneIndex);
    //    audio[0].Play();
    //    audio[1].Stop();
    //}
    //IEnumerator FirstPlay()
    //{
    //    yield return new WaitUntil(() => SceneManager.GetActiveScene().buildIndex == firstSceneIndex || SceneManager.GetActiveScene().buildIndex == menuSceneIndex);
    //    {
    //        if (!audio[0].isPlaying)
    //        {
    //            audio[0].Play();
    //        }
    //        if(audio[1].isPlaying)
    //        {
    //            audio[1].Stop();
    //        }
    //    }
    //}
    //private void Update()
    //{
    //    if(Input.GetKeyDown(KeyCode.Return))
    //    {
    //        PlayerPrefs.DeleteAll();
    //    }
    //}
}
