﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIstate : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> first_state_ui_list;
    [SerializeField]
    private List<GameObject> second_state_ui_list;
    private int state = 1;
    [SerializeField]
    private Text text_1;
    
    public void setState(int set)
    {
        if (clickingCheck())
        {
            state = set;
        }
    }

    public void setUI(int index)
    {
        if (clickingCheck())
        {
            if (state == 1)
            {
                first_state_ui_list[index].SetActive(true);
                second_state_ui_list[index].SetActive(false);
            }
            else if (state == 2)
            {
                setUI_list(first_state_ui_list, true);
                setUI_list(second_state_ui_list, false);
                first_state_ui_list[index].SetActive(false);
                second_state_ui_list[index].SetActive(true);
            }
        }
        else
        {
            LobbyControl lobbyControl = GetComponent<LobbyControl>();
            lobbyControl.current_mode = lobbyControl.lastMode;
        }
    }

    void setUI_list(List<GameObject> list, bool set)
    {
        foreach(GameObject obj in list)
        {
            obj.SetActive(set);
        }
    }

    bool clickingCheck()
    {
        LobbyControl lobbyControl = GetComponent<LobbyControl>();
        if(lobbyControl.current_mode == "QUICKPLAY")
        {
            return true;
        }
        else if (lobbyControl.current_mode == "PRIVATE")
        {
            return lobbyControl.private_input.text.Length >= lobbyControl.minimunLength;
        }
        else if (lobbyControl.current_mode == "HARDCORE")
        {
            return PlayerPrefs.GetInt("Money" + DataLogin.Instance.IdIndex) >= 100;
        }
        else
        {
            return false;
        }
    }
    
}
