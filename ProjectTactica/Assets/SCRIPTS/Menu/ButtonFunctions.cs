﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonFunctions : MonoBehaviour
{
    [SerializeField] ButtonController buttonControl;
    [SerializeField] bool disable;
   

    public void ActivateSound(AudioClip whichSound)
    {
        if (!disable)
        {
            buttonControl.audioSource.PlayOneShot(whichSound);
        }
        else
        {
            disable = false;
        }
    }
}
