﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class RoomControl : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private int multiplayerSceneIndex;
    private bool inRoom;
    private bool oneTime;
    // Start is called before the first frame update
    public CanvasGroup transObject;
    public Animator transitions;
    public float transitionTIME = 1.20f;

    void Awake()
    {
        transObject.gameObject.SetActive(false);
    }

    void Start()
    {
        if (DataSelect.Instance)
        {
            DataSelect.Instance.playerChoose.Clear();
            //Debug.Log("Clearly.......");
        }
        inRoom = false;
        oneTime = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (inRoom)
        {
            if (PhotonNetwork.CurrentRoom.PlayerCount == PhotonNetwork.CurrentRoom.MaxPlayers && oneTime)
            {
                oneTime = false;
                ModeSelect.Instance.gameMode = GetComponent<LobbyControl>().current_mode;
                transObject.gameObject.SetActive(false);
                PhotonNetwork.CurrentRoom.IsOpen = false;
                PhotonNetwork.CurrentRoom.IsVisible = false;
                StartCoroutine(StartTransition());
            }
        }
    }

    IEnumerator StartTransition()
    {
        transitions.ResetTrigger("StartTrans");
        yield return new WaitForSeconds(transitionTIME);
        startGame();
    }

    public override void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);
    }

    public override void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    public override void OnJoinedRoom()
    {
        //Debug.Log("Join Room");
        inRoom = true;
    }

    public override void OnLeftRoom()
    {
        inRoom = false;
    }

    void startGame()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            //Debug.Log("LOAD_1");
            PhotonNetwork.LoadLevel(multiplayerSceneIndex);
        }
    }
}
