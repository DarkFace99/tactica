﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class ButtonEvent : MonoBehaviour
{
    [SerializeField]
    private Text text;
    [SerializeField]
    private string text_mouseEnter;
    private string text_start;
    private int fontSize_start;
    [SerializeField]
    private int fontSize_big;
    [SerializeField]
    private AudioSource audio;
    private void Awake()
    {
        fontSize_start = text.fontSize;
        text_start = text.text;
    }

    private void OnEnable()
    {
        mouseExit();
    }

    public void mouseEnter()
    {
        audio.Play();
        text.fontSize = fontSize_big;
        if (text_mouseEnter != "")
        {
            text.text = text_mouseEnter;
        }
    }

    public void mouseExit()
    {
        text.fontSize = fontSize_start;
        text.text = text_start;
    }
}
