﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;

public class LobbyControl : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private int roomSize;
    public string current_mode;
    public string lastMode;
    public InputField private_input;
    public int minimunLength;
    private bool isTryJoining;
    [SerializeField]
    private Button hardcore_info;
    [SerializeField]
    private Text coin;

    // Start is called before the first frame update
    void Start()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        isTryJoining = false;
        coin.text = PlayerPrefs.GetInt("Money" + DataLogin.Instance.IdIndex).ToString();
    }
    
    void setUI_List(List<GameObject> list, bool set)
    {
        foreach(GameObject obj in list)
        {
            obj.SetActive(set);
        }
    }

    public void quickStart(string mode)
    {
        quickCancel();
        lastMode = current_mode;
        current_mode = mode;
        //Debug.Log(PlayerPrefs.GetInt("money" + DataLogin.Instance.IdIndex));
        if (mode == "PRIVATE")
        {
            if (private_input.text.Length < minimunLength)
            {
                return;
            }
            isTryJoining = true;
            PhotonNetwork.JoinRoom("TACTICA_" + mode + "_" + private_input.text);
            return;
        }
        else if (mode == "HARDCORE" && PlayerPrefs.GetInt("Money" + DataLogin.Instance.IdIndex) < 100)
        {
            hardcore_info.onClick.Invoke();
            return;
        }

        ExitGames.Client.Photon.Hashtable roomProp = new ExitGames.Client.Photon.Hashtable() { { "MODE", mode } };
        string[] lobbyProp = new string[] { "MODE" };
        PhotonNetwork.JoinRandomRoom(roomProp, (byte)roomSize, MatchmakingMode.FillRoom, TypedLobby.Default, null);        
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        createRoom(current_mode);
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        if (isTryJoining)
        {
            quickStart(current_mode);
        }
    }

    public void createRoom(string mode)
    {
        quickCancel();
        string roomName;
        if (mode == "PRIVATE")
        {
            lastMode = current_mode;
            current_mode = mode;
            if (private_input.text.Length < 3)
            {
                return;
            }
            roomName = "TACTICA_" + mode + "_" + private_input.text;
        }
        else
        {
            roomName = "TACTICA_" + mode + "_" + Random.Range(1000, 10000);
        }
        ExitGames.Client.Photon.Hashtable roomProp = new ExitGames.Client.Photon.Hashtable() { { "MODE", mode} };
        string[] lobbyProp = new string[] { "MODE" };
        RoomOptions roomOption = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = (byte)roomSize, CustomRoomProperties = roomProp, CustomRoomPropertiesForLobby = lobbyProp};
        PhotonNetwork.CreateRoom(roomName, roomOption, TypedLobby.Default);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        createRoom(current_mode);
    }

    public void quickCancel()
    {
        isTryJoining = false;
        if (PhotonNetwork.InRoom)
        {
            if (PhotonNetwork.CurrentRoom.PlayerCount != PhotonNetwork.CurrentRoom.MaxPlayers)
            {
                PhotonNetwork.LeaveRoom();
            }
        }
    }
}
