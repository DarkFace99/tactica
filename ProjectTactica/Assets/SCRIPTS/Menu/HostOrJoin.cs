﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class HostOrJoin : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private Text display_text;
    [SerializeField]
    private float delay;
    [SerializeField]
    private Text roomName_text;
    private int dotCount = 0;
    [SerializeField]
    private InputField private_input;
    private float timeCount = 0f;

    private void Update()
    {
        if (display_text.enabled)
        {
            timeCount += Time.deltaTime;
            if (timeCount >= delay)
            {
                timeCount = 0f;
                dotCount++;
                if(dotCount > 3)
                {
                    dotCount = 0;
                }
            }
            display_text.text = getDisplayText(PhotonNetwork.IsMasterClient, dotCount);
        }
    }

    public override void OnDisable()
    {
        dotCount = 0;
        display_text.text = "";
        roomName_text.text = "";
        timeCount = 0f;
        display_text.enabled = false;
    }

    public override void OnCreatedRoom()
    {
        roomName_text.text = "RoomName:" + private_input.text;
        display_text.enabled = true;
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        if (display_text.text.Length == 0)
        {
            roomName_text.text = "Finding... " + private_input.text;
            display_text.enabled = true;
        }
    }
    /*public override void OnJoinedRoom()
    {
        if (display_text.text.Length == 0)
        {
            roomName_text.text = "Finding... " + private_input.text;
            display_text.enabled = true;
        }
    }*/
    

    string getDisplayText(bool isMaster, int count)
    {
        string dot = new string('.', count);
        if (isMaster)
        {
            return "Hosting" + dot;
        }
        else
        {
            return "Joining" + dot;
        }
    }
}
