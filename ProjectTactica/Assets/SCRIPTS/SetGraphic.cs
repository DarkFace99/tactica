﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetGraphic : MonoBehaviour
{
    private bool isFull;
    private void Awake()
    {
        if (PlayerPrefs.HasKey("Width") == false || PlayerPrefs.HasKey("Height") == false)
        {
            PlayerPrefs.SetInt("Width", 1920);
            PlayerPrefs.SetInt("Height", 1080);
        }
        if (PlayerPrefs.HasKey("FullScreen") == false)
        {
            PlayerPrefs.SetInt("FullScreen", 1);

        }
        if ((PlayerPrefs.HasKey("Width") || PlayerPrefs.HasKey("Height") && (PlayerPrefs.HasKey("FullScreen") == false)))
        {
            Screen.SetResolution(PlayerPrefs.GetInt("Width"), PlayerPrefs.GetInt("Height"), !Screen.fullScreen);
        }
        if ((PlayerPrefs.HasKey("Width") || PlayerPrefs.HasKey("Height") && (PlayerPrefs.HasKey("FullScreen") == true)))
        {
            Screen.SetResolution(PlayerPrefs.GetInt("Width"), PlayerPrefs.GetInt("Height"), Screen.fullScreen);
        }
        if (PlayerPrefs.GetInt("FullScreen") == 0)
        {
            isFull = false;
        }
        else
        {
            isFull = true;
        }
        QualitySettings.SetQualityLevel(PlayerPrefs.GetInt("Quality"),true);
        if (PlayerPrefs.HasKey("master"))
        {
            PlayerPrefs.SetFloat("master", PlayerPrefs.GetFloat("master"));
        }
        else
        {
            PlayerPrefs.SetFloat("master", -10);
        }
        if (PlayerPrefs.HasKey("music"))
        {
            PlayerPrefs.SetFloat("music", PlayerPrefs.GetFloat("music"));
        }
        else
        {
            PlayerPrefs.SetFloat("music", -10);
        }
        if (PlayerPrefs.HasKey("sfx"))
        {
            PlayerPrefs.SetFloat("sfx", PlayerPrefs.GetFloat("sfx"));
        }
        else
        {
            PlayerPrefs.SetFloat("sfx", -10);
        }
    }
}
