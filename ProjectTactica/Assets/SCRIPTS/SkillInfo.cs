﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillInfo : MonoBehaviour
{
    [SerializeField]
    private GameObject panel;
    [SerializeField]
    private Vector2 offsetPos; //( +- 0.5f, - 1.95f)
    private GameObject spawn_panel;
    private float mouseOverTime = 0;
    private float mouseOverTime_max = 0.5f;

    private void OnMouseEnter()
    {
        StartCoroutine(waitUntilSpawnPanel());
    }

    private void OnMouseOver()
    {
        mouseOverTime += Time.deltaTime;
    }

    private void OnMouseExit()
    {
        mouseOverTime = 0f;
        StopCoroutine(waitUntilSpawnPanel());
        if (spawn_panel)
        {
            Destroy(spawn_panel);
            spawn_panel = null;
        }
    }

    IEnumerator waitUntilSpawnPanel()
    {
        yield return new WaitUntil(() => mouseOverTime >= mouseOverTime_max);
        if (spawn_panel)
        {
            Destroy(spawn_panel);
        }
        spawn_panel = Instantiate(panel, new Vector2(transform.position.x + offsetPos.x, transform.position.y + offsetPos.y), Quaternion.identity);
    }

    private void OnDestroy()
    {
        if (spawn_panel)
        {
            Destroy(spawn_panel);
        }
    }
}
