﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Photon.Pun;

public class EndSceneUI : MonoBehaviour
{
    [SerializeField]
    private Text endConditionText;
    [SerializeField]
    private string menuSceneName;
    [SerializeField]
    private string pressAnySceneName;
    [SerializeField]
    private Text rewardText;
    [SerializeField]
    private Image bgImage;
    [SerializeField]
    private Sprite lose_bg;

    private void Awake()
    {
        setEndConditionText();
    }

    void setEndConditionText()
    {
        if (PlayerPrefs.GetInt("END_CONDITION" + DataLogin.Instance.IdIndex) == -1)
        {
            Debug.Log("Before add " + PlayerPrefs.GetString("User" + DataLogin.Instance.IdIndex) + " Money:" + PlayerPrefs.GetInt("Money" + DataLogin.Instance.IdIndex));
            endConditionText.text = "DEFEAT";
            rewardText.text = "10";
            PlayerPrefs.SetInt("Money" + DataLogin.Instance.IdIndex, PlayerPrefs.GetInt("Money" + DataLogin.Instance.IdIndex) + 100);
            Debug.Log(PlayerPrefs.GetString("User" + DataLogin.Instance.IdIndex) + " Money:" + PlayerPrefs.GetInt("Money" + DataLogin.Instance.IdIndex));
            //lose background
            bgImage.sprite = lose_bg;
        }
        else if (PlayerPrefs.GetInt("END_CONDITION" + DataLogin.Instance.IdIndex) == 0)
        {
            endConditionText.text = "DRAW";
        }
        else if (PlayerPrefs.GetInt("END_CONDITION" + DataLogin.Instance.IdIndex) == 1)
        {
            Debug.Log("Before add " + PlayerPrefs.GetString("User" + DataLogin.Instance.IdIndex) + " Money:" + PlayerPrefs.GetInt("Money" + DataLogin.Instance.IdIndex));
            endConditionText.text = "VICTORY";
            rewardText.text = "100";
            PlayerPrefs.SetInt("Money" + DataLogin.Instance.IdIndex, PlayerPrefs.GetInt("Money" + DataLogin.Instance.IdIndex) + 10);
            Debug.Log(PlayerPrefs.GetString("User" + DataLogin.Instance.IdIndex) + " Money:" + PlayerPrefs.GetInt("Money" + DataLogin.Instance.IdIndex));
        }

        PlayerPrefs.DeleteKey("END_CONDITION" + DataLogin.Instance.IdIndex); //clear memory
    }

    public void goToMenu()
    {
        if (PhotonNetwork.IsConnected)
        {
            SceneManager.LoadScene(menuSceneName);
        }
        else
        {
            SceneManager.LoadScene(pressAnySceneName);
        }
    }
}
