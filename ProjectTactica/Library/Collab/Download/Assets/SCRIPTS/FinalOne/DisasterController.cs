﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisasterController : MonoBehaviour
{
    [SerializeField] private GameObject warning;
    [SerializeField] private GameObject disaster;
    private bool FUCK;

    // Start is called before the first frame update
    void Start()
    {
        warning.SetActive(false);
        disaster.SetActive(false);
        StartCoroutine(onlyInActionPhase());
        StartCoroutine(onlyInBattlePhase());
    }

    // Update is called once per frame
    IEnumerator w8Warning()
    {
        yield return new WaitUntil(() => GetComponent<ChangeAlphaDisaster>().isChangeAlpha);
        warning.SetActive(false);
        disaster.SetActive(true);
    }
    IEnumerator onlyInBattlePhase()
    {
        yield return new WaitUntil(() => SystemController.currentState == SystemController.GameState.Battle);
        warning.SetActive(true);
        disaster.SetActive(false);
        StartCoroutine(onlyInBattlePhase());
    }
    IEnumerator onlyInActionPhase()
    {
        yield return new WaitUntil(() => (SystemController.currentState == SystemController.GameState.P1_Action) || (SystemController.currentState == SystemController.GameState.P2_Action));
        StartCoroutine(waitA_sec());  
    }
    IEnumerator waitA_sec()
    {
        yield return new WaitForSeconds(3f);
        warning.SetActive(false);
        disaster.SetActive(false);
        StartCoroutine(onlyInActionPhase());
    }
}
