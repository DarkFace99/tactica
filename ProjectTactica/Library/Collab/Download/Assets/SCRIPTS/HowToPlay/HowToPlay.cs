﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HowToPlay : MonoBehaviour
{
    [SerializeField] List<GameObject> characterSelectPannel; // 3
    [SerializeField] List<GameObject> droppingPannel; // 2
    [SerializeField] List<GameObject> actionPannel; // 3
    [SerializeField] List<GameObject> battlePannel; // 1
    [SerializeField] List<GameObject> attackPannel; // 1
    [SerializeField] List<GameObject> terrainPannel; // 2

    [SerializeField] GameObject Back;
    [SerializeField] GameObject Next;

    [SerializeField] Text page;
    public int pageNum;
    public int pannelNum;
    public int current_pannel;
    public int current_page;

    [SerializeField]
    private Color darkColor;
    [SerializeField]
    private Color normalColor;
    [SerializeField]
    private List<Image> image_list;

    void Start()
    {
        startThisScene();
        StartCoroutine(checkPannel());

        image_list[pannelNum - 1].color = darkColor;
        StartCoroutine(waitUntilChangePanel(pannelNum));
    }
    

    IEnumerator waitUntilChangePanel(int panel)
    {
        yield return new WaitUntil(() => panel != pannelNum);
        image_list[panel - 1].color = normalColor;
        image_list[pannelNum - 1].color = darkColor;
        StartCoroutine(waitUntilChangePanel(pannelNum));
    }

    // START FUNCTION
    void startThisScene()
    {
        pannelNum = 1;
        current_pannel = pannelNum;

        pageNum = 1;
        current_page = pageNum;

        characterSelectPannel[0].SetActive(true);

        Back.SetActive(false);
        Next.SetActive(true);
        page.text = pageNum.ToString();

        for (int i = 1; i < characterSelectPannel.Count; i++)
        {
            characterSelectPannel[i].SetActive(false);
        }
        for (int i = 1; i < droppingPannel.Count; i++)
        {
            droppingPannel[i].SetActive(false);
        }
        for (int i = 1; i < actionPannel.Count; i++)
        {
            actionPannel[i].SetActive(false);
        }
        for (int i = 1; i < battlePannel.Count; i++)
        {
            battlePannel[i].SetActive(false);
        }
        for (int i = 1; i < attackPannel.Count; i++)
        {
            attackPannel[i].SetActive(false);
        }
        for (int i = 1; i < terrainPannel.Count; i++)
        {
            terrainPannel[i].SetActive(false);
        }
    }

    // START OPTIONS PANNEL

    public void C_Button()
    {
        pannelNum = 1;
        pageNum = 1;
        page.text = pageNum.ToString();
    }
    public void D_Button()
    {
        pannelNum = 2;
        pageNum = 1;
        page.text = pageNum.ToString();
    }
    public void Ac_Button()
    {
        pannelNum = 3;
        pageNum = 1;
        page.text = pageNum.ToString();
    }
    public void B_Button()
    {
        pannelNum = 4;
        pageNum = 1;
        page.text = "          1";
    }
    public void At_Button()
    {
        pannelNum = 5;
        pageNum = 1;
        page.text = "          1";
    }
    public void T_Button()
    {
        pannelNum = 6;
        pageNum = 1;
        page.text = pageNum.ToString();
    }

    // BUTTON GO BACK AND NEXT IN PAGE
    public void backPage()
    {
        if (pageNum > 1)
        {
            pageNum -= 1;
            page.text = pageNum.ToString();
        }
    }
    public void nextPage()
    {
        if (pannelNum == 1)
        {
            if (pageNum < characterSelectPannel.Count)
            {
                pageNum += 1;
                page.text = pageNum.ToString();
            }
        }
        if (pannelNum == 2)
        {
            if (pageNum < droppingPannel.Count)
            {
                pageNum += 1;
                page.text = pageNum.ToString();
            }
        }
        if (pannelNum == 3)
        {
            if (pageNum < actionPannel.Count)
            {
                pageNum += 1;
                page.text = pageNum.ToString();
            }
        }
        if (pannelNum == 6)
        {
            if (pageNum < terrainPannel.Count)
            {
                pageNum += 1;
                page.text = pageNum.ToString();
            }
        }
    }

    // IEnumerator TO CHECK IF PANNEL OR NUM CHANGED
    IEnumerator checkPannel()
    {
        yield return new WaitUntil(() => (current_pannel != pannelNum) || (current_page != pageNum));
        clearAll();
        if (pannelNum == 1)
        {
            panONE();
        }
        else if(pannelNum == 2)
        {
            panTWO();
        }
        else if (pannelNum == 3)
        {
            panTHREE();
        }
        else if (pannelNum == 4)
        {
            panFOUR();
        }
        else if (pannelNum == 5)
        {
            panFIVE();
        }
        else if (pannelNum == 6)
        {
            panSIX();
        }

    }












    // FOR CHECKING 
    void panONE() // PANNEL ONE
    {
        if (pageNum == 1)
        {
            characterSelectPannel[0].SetActive(true);
            characterSelectPannel[1].SetActive(false);
            characterSelectPannel[2].SetActive(false);
            current_pannel = pannelNum;
            current_page = pageNum;
            Back.SetActive(false);
            Next.SetActive(true);
        }
        else if (pageNum == 2)
        {
            characterSelectPannel[0].SetActive(false);
            characterSelectPannel[1].SetActive(true);
            characterSelectPannel[2].SetActive(false);
            current_pannel = pannelNum;
            current_page = pageNum;
            Back.SetActive(true);
            Next.SetActive(true);
        }
        else if (pageNum == 3)
        {
            characterSelectPannel[0].SetActive(false);
            characterSelectPannel[1].SetActive(false);
            characterSelectPannel[2].SetActive(true);
            current_pannel = pannelNum;
            current_page = pageNum;
            Back.SetActive(true);
            Next.SetActive(false);
        }
        StartCoroutine(checkPannel());
    }
    void panTWO() // PANNEL TWO
    {
        if (pageNum == 1)
        {
            droppingPannel[0].SetActive(true);
            droppingPannel[1].SetActive(false);
            current_pannel = pannelNum;
            current_page = pageNum;
            Back.SetActive(false);
            Next.SetActive(true);
        }
        else if(pageNum == 2)
        {
            droppingPannel[0].SetActive(false);
            droppingPannel[1].SetActive(true);
            current_pannel = pannelNum;
            current_page = pageNum;
            Back.SetActive(true);
            Next.SetActive(false);
        }
        StartCoroutine(checkPannel());
    }
    void panTHREE() // PANNEL THREE
    {
        if (pageNum == 1)
        {
            actionPannel[0].SetActive(true);
            actionPannel[1].SetActive(false);
            actionPannel[2].SetActive(false);
            actionPannel[3].SetActive(false);
            current_pannel = pannelNum;
            current_page = pageNum;
            Back.SetActive(false);
            Next.SetActive(true);
        }
        else if (pageNum == 2)
        {
            actionPannel[0].SetActive(false);
            actionPannel[1].SetActive(true);
            actionPannel[2].SetActive(false);
            actionPannel[3].SetActive(false);
            current_pannel = pannelNum;
            current_page = pageNum;
            Back.SetActive(true);
            Next.SetActive(true);
        }
        else if (pageNum == 3)
        {
            actionPannel[0].SetActive(false);
            actionPannel[1].SetActive(false);
            actionPannel[2].SetActive(true);
            actionPannel[3].SetActive(false);
            current_pannel = pannelNum;
            current_page = pageNum;
            Back.SetActive(true);
            Next.SetActive(true);
        }
        else if (pageNum == 4)
        {
            actionPannel[0].SetActive(false);
            actionPannel[1].SetActive(false);
            actionPannel[2].SetActive(false);
            actionPannel[3].SetActive(true);
            current_pannel = pannelNum;
            current_page = pageNum;
            Back.SetActive(true);
            Next.SetActive(false);
        }
        StartCoroutine(checkPannel());
    }
    void panFOUR() // PANNEL FOUR
    {
        if (pageNum == 1)
        {
            battlePannel[0].SetActive(true);
            current_pannel = pannelNum;
            current_page = pageNum;
            Back.SetActive(false);
            Next.SetActive(false);
        }
        StartCoroutine(checkPannel());
    }
    void panFIVE() // PANNEL FOUR
    {
        if (pageNum == 1)
        {
            attackPannel[0].SetActive(true);
            current_pannel = pannelNum;
            current_page = pageNum;
            Back.SetActive(false);
            Next.SetActive(false);
        }
        StartCoroutine(checkPannel());
    }
    void panSIX() // PANNEL FOUR
    {
        if (pageNum == 1)
        {
            terrainPannel[0].SetActive(true);
            terrainPannel[1].SetActive(false);
            current_pannel = pannelNum;
            current_page = pageNum;
            Back.SetActive(false);
            Next.SetActive(true);
        }
        else if (pageNum == 2)
        {
            terrainPannel[0].SetActive(false);
            terrainPannel[1].SetActive(true);
            current_pannel = pannelNum;
            current_page = pageNum;
            Back.SetActive(true);
            Next.SetActive(false);
        }
        StartCoroutine(checkPannel());
    }

    public void LoadBack()
    {
        SceneManager.LoadScene("MENU");
    }

    // CLEAR ALL
    void clearAll()
    {
        for (int i = 0; i < characterSelectPannel.Count; i++)
        {
            characterSelectPannel[i].SetActive(false);
        }
        for (int i = 0; i < droppingPannel.Count; i++)
        {
            droppingPannel[i].SetActive(false);
        }
        for (int i = 0; i < actionPannel.Count; i++)
        {
            actionPannel[i].SetActive(false);
        }
        for (int i = 0; i < battlePannel.Count; i++)
        {
            battlePannel[i].SetActive(false);
        }
        for (int i = 0; i < attackPannel.Count; i++)
        {
            attackPannel[i].SetActive(false);
        }
        for (int i = 0; i < terrainPannel.Count; i++)
        {
            terrainPannel[i].SetActive(false);
        }
    }
}
