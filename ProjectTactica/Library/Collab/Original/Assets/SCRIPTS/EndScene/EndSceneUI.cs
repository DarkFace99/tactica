﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Photon.Pun;

public class EndSceneUI : MonoBehaviour
{
    [SerializeField]
    private Text endConditionText;
    [SerializeField]
    private string menuSceneName;
    [SerializeField]
    private string pressAnySceneName;
    [SerializeField]
    private Text rewardText;
    [SerializeField]
    private Image bgImage;
    [SerializeField]
    private Sprite lose_bg;

    public int money;
    public int display_money;

    void Start()
    {
        money = 0;
        display_money = 0;
        if (PlayerPrefs.GetInt("END_CONDITION" + DataLogin.Instance.IdIndex) == -1)
        {
            money = 100;
            endConditionText.text = "DEFEAT";
            PlayerPrefs.SetInt("Money" + DataLogin.Instance.IdIndex, PlayerPrefs.GetInt("Money" + DataLogin.Instance.IdIndex) + 100);
            bgImage.sprite = lose_bg;
            StartCoroutine(ScoreUpdater());
        }
        else if (PlayerPrefs.GetInt("END_CONDITION" + DataLogin.Instance.IdIndex) == 1)
        {
            money = 10;
            endConditionText.text = "VICTORY";
            PlayerPrefs.SetInt("Money" + DataLogin.Instance.IdIndex, PlayerPrefs.GetInt("Money" + DataLogin.Instance.IdIndex) + 10);
            StartCoroutine(ScoreUpdater());
        }
    }
    

    public void goToMenu()
    {
        if (PhotonNetwork.IsConnected)
        {
            SceneManager.LoadScene(menuSceneName);
        }
        else
        {
            SceneManager.LoadScene(pressAnySceneName);
        }
    }

    private IEnumerator ScoreUpdater()
    {
        while (true)
        {
            if (display_money < money)
            {
                display_money++;
                rewardText.text = display_money.ToString();
            }
            yield return new WaitForSeconds(0.2f);
        }
    }
}
