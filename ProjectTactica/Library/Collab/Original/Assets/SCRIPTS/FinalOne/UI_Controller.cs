﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class UI_Controller : MonoBehaviour
{
    [SerializeField]
    private Text turn_text; //turn's number text
    [SerializeField]
    private Text phase_text; //current phase text
    [SerializeField]
    private Text delay_text; //delay checking in battle phase text
    [SerializeField]
    private Image endActionImage; //confirm action button image
    private int player; //player number
    public static bool canClick; //determine whether player can click confirm action button
    //public List<Text> our_name_list; //list of our character's name
    //public List<Text> opponent_name_list; //list or opponent's character's name
    [SerializeField]
    private List<Text> our_health_list; //list of our character's health
    [SerializeField]
    private List<Text> opponent_health_list; //list of opponent's character's health
    private SystemController systemController;
    [SerializeField]
    private List<int> current_health; //current all character health
    [SerializeField]
    private GameObject askingPanel;
    public static bool isActive_askingPanel; //determine setActive of asking panel
    public static GameObject asking_character;
    public Text asking_text; //what do you wanna ask the player
    [SerializeField]
    private List<GameObject> bigIcon_list;
    [SerializeField]
    private GameObject surrenderButton; //surrender button
    [SerializeField]
    private GameObject fadePanel;
    /*[SerializeField]
    private List<Sprite> endAction_sprite_list;*/
    [SerializeField]
    private Text endActonText;

    // HealthBar
    [SerializeField]
    private List<Slider> ourHealthBar_List;
    [SerializeField]
    private List<Slider> opponentHealthBar_List;

    // TimerBar
    [SerializeField]
    private Slider timerBar;
    private float Time;
    private bool countDown = false;
    private float time;

    // Start is called before the first frame update
    void Start()
    {
        player = SystemController.player; //set player
        canClick = false;
        delay_text.enabled = false;
        systemController = SystemController.systemController.GetComponent<SystemController>();

        isActive_askingPanel = false;
        asking_character = null;

        surrenderButton.SetActive(false);
        StartCoroutine(setActiveSurrender());

        StartCoroutine(waitForStart());

        fadePanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        checkTurn(SystemController.turn);
        checkPhase(SystemController.currentState);
        canClickCheck(SystemController.currentState);
        showDelay();
        healthCheck();
        askingPanel.SetActive(isActive_askingPanel);
        setBigIcon();
        if (countDown == true)
        {
            if (timerBar.value > 0)
            {
                timerBar.value -= UnityEngine.Time.deltaTime;
            }
            else
            {
                countDown = false;
            }
        }
    }

    void TimerBar()
    {
        if (SystemController.currentState == SystemController.GameState.Drop)
        {
            Time = 10;
            timerBar.maxValue = Time;
            timerBar.value = Time;
            countDown = true;
        }
        else if ((SystemController.currentState == SystemController.GameState.P1_Action) && (SystemController.currentState == SystemController.GameState.P2_Action))
        {
            Time = 20;
            timerBar.maxValue = Time;
            timerBar.value = Time;
            countDown = true;
        }
        else
        {
            Time = 1;
            timerBar.maxValue = Time;
            timerBar.value = Time;
        }
    }

    void checkTurn(int turn)
    {
        turn_text.text = "T " + turn;
    }

    void checkPhase(SystemController.GameState gameState)
    {
        if(gameState == SystemController.GameState.Drop && phase_text.text != "Dropping Phase")
        {
            phase_text.text = "Dropping Phase";
            //setFadePanel();
        }
        else if (gameState == SystemController.GameState.P1_Action && phase_text.text != "P1's Action Phase")
        {
            phase_text.text = "P1's Action Phase";
            //setFadePanel();
        }
        else if (gameState == SystemController.GameState.P2_Action && phase_text.text != "P2's Action Phase")
        {
            phase_text.text = "P2's Action Phase";
            //setFadePanel();
        }
        else if (gameState == SystemController.GameState.Battle && phase_text.text != "Battle Phase")
        {
            phase_text.text = "Battle Phase";
            //setFadePanel();
        }
        else if (gameState == SystemController.GameState.Game_Over)
        {
            phase_text.text = "Game Over";
        }
    }

    void setFadePanel()
    {
        fadePanel.SetActive(true);
        fadePanel.GetComponent<FadePanel>().isFade = true;
    }

    void canClickCheck(SystemController.GameState gameState)
    {
        if (player == 1 && gameState == SystemController.GameState.P1_Action)
        {
            canClick = true;
            Color color = new Color(1f, 1f, 1f, 1f);
            endActionImage.color = color;
            endActonText.color = color;
        }
        else if (player == 2 && gameState == SystemController.GameState.P2_Action)
        {
            canClick = true;
            Color color = new Color(1f, 1f, 1f, 1f);
            endActionImage.color = color;
            endActonText.color = color;
        }
        else 
        {
            canClick = false;
            Color color = new Color(0.5f, 0.5f, 0.5f, 1f);
            endActionImage.color = color;
            endActonText.color = new Color(0f, 0f, 0f, 1f);
        }
    }

    //used in button
    public void endAction()
    {
        if (canClick)
        {
            GetComponent<PhotonView>().RPC("rpc_changeState", RpcTarget.All, player);
        }
    }

    [PunRPC]
    void rpc_changeState(int player)
    {
        if(player == 1)
        {
            if(SystemController.firstTurnPlayer == 1)
            {
                SystemController.currentState = SystemController.GameState.P2_Action;
            }
            else
            {
                SystemController.currentState = SystemController.GameState.Battle;
            }
        }
        else
        {
            if (SystemController.firstTurnPlayer == 2)
            {
                SystemController.currentState = SystemController.GameState.P1_Action;
            }
            else
            {
                SystemController.currentState = SystemController.GameState.Battle;
            }
        }
    }

    void showDelay()
    {
        if(SystemController.currentState == SystemController.GameState.Battle)
        {
            delay_text.enabled = true;
            delay_text.text = "Delay " + BattlePhase.delay;
        }
        else
        {
            delay_text.enabled = false;
        }
    }

    void setOurTextList()//Apply to Start
    {
        for(int i = 0; i < systemController.character_list.Count; i++)
        {
            CharacterController cc = systemController.character_list[i].GetComponent<CharacterController>();
            //our_name_list[i].text = cc.name;
            our_health_list[i].text = cc.health.ToString();
            current_health[i] = cc.health;

            ourHealthBar_List[i].maxValue = current_health[i]; // Set MAX Health when Start The Game
            ourHealthBar_List[i].value = current_health[i]; // Set Health when Start The Game
            GetComponent<PhotonView>().RPC("rpc_setOpponentMaxHealth", RpcTarget.Others, i, current_health[i]);
            GetComponent<PhotonView>().RPC("rpc_setOpponentHealthList", RpcTarget.Others, i, current_health[i]);

            GetComponent<PhotonView>().RPC("rpc_setOpponentTextList", RpcTarget.Others, i, cc.name, cc.health);
        }
    }

    [PunRPC]
    void rpc_setOpponentTextList(int i, string name, int health)
    {
        //opponent_name_list[i].text = name;
        opponent_health_list[i].text = health.ToString();
    }

    /*================= SET HEALTHBAR VALUES =================*/
    [PunRPC] // Update Health
    void rpc_setOpponentHealthList(int i, int health)
    {
        opponentHealthBar_List[i].value = health;
    }

    [PunRPC] // Start Health
    void rpc_setOpponentMaxHealth(int i, int maxHealth)
    {
        opponentHealthBar_List[i].maxValue = maxHealth;
    }



    void healthCheck()//Apply to Update
    {
        for(int i = 0; i < systemController.character_list.Count; i++)
        {
            if(current_health[i] != systemController.character_list[i].GetComponent<CharacterController>().health)
            {
                current_health[i] = systemController.character_list[i].GetComponent<CharacterController>().health;
                setHealth(i);
            }
        }
    }

    void setHealth(int index)// HealthCheck
    {
        ourHealthBar_List[index].value = current_health[index];// Update Health 
        GetComponent<PhotonView>().RPC("rpc_setOpponentHealthList", RpcTarget.Others, index, current_health[index]);

        our_health_list[index].text = current_health[index].ToString();
        GetComponent<PhotonView>().RPC("rpc_setOpponentHealth", RpcTarget.Others, index, current_health[index]);
    }

    [PunRPC]
    void rpc_setOpponentHealth(int index, int health)
    {
        opponent_health_list[index].text = health.ToString();
    }

    public void askingPanel_yes()
    {
        if(asking_character!= null)
        {
            CharacterController cc_asking = asking_character.GetComponent<CharacterController>();
            asking_text.text = "Cancel " +  cc_asking.name + "'s Steady";
            cc_asking.clearAllAction();
            closeAskingPanel();
        }
    }

    public void closeAskingPanel()
    {
        isActive_askingPanel = false;
        asking_character = null;
    }

    //close active all big icons
    void resetBigIcon()
    {
        foreach(GameObject obj in bigIcon_list)
        {
            obj.SetActive(false);
        }
    }

    void setBigIcon()
    {
        if(systemController.controllingCharacter() != null)
        {
            int character_number = systemController.controllingCharacter().GetComponent<CharacterController>().character_number;
            for(int i = 0; i< bigIcon_list.Count; i++)
            {
                if(i == character_number - 1)
                {
                    bigIcon_list[i].SetActive(true);
                }
                else
                {
                    bigIcon_list[i].SetActive(false);
                }
            }
        }
        else
        {
            resetBigIcon();
        }
    }

    IEnumerator setActiveSurrender()
    {
        yield return new WaitUntil(() => systemController.canSurrender);
        surrenderButton.SetActive(true);
    }

    public void surrender()
    {
        systemController.setEndCondition(-1);
        SystemController.currentState = SystemController.GameState.Game_Over;
    }

    IEnumerator waitForStart()
    {
        yield return new WaitForSecondsRealtime(systemController.startingDelay);
        setOurTextList();
    }
}
