﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;

public class SystemController : MonoBehaviour
{
    public float dropTime; //length of dropping phase (realtime seconds)

    //============================================================
    public static GameObject systemController; 
    // Put in here
    public List<GameObject> character_list; //list of all characters as GameObject
    public List<GameObject> icon_list; //list of all character's icons
    public List<Vector2> spawn_point; //list of all character's spawn points
    public List<CharacterController> characterControllers; //list of all character's CharacterController scripts
    public enum GameState {Drop, P1_Action, P2_Action, Battle, Game_Over} //enum variable type for declaring game state
    public static GameState currentState; //tells current game state
    public static int player; //player 1 or 2 
    public static int turn; //cuurent turn
    public static int firstTurnPlayer; //player number take an action first
    private bool oneTimeSpawn;
    public static int attackingOrder; //use to sort list of attacking order
    public GameObject canvas;
    public GridBehaviour1 gridBehaviour;
    private int randomNumber;
    public bool canSurrender;
    //public List<GameObject> attack_order_list; //activate to check list of characters sorted by attackingOrder, also activate in sortAttackingOrder() function

    // Start is called before the first frame update
    void Awake()
    {
        attackingOrder = 1;
        systemController = gameObject;
        oneTimeSpawn = true;
        canSurrender = false;
        turn = 1;
        currentState = GameState.Drop;
        setPlayer(PhotonNetwork.IsMasterClient); // master client -> 1 // non master client -> 2
        if (!PhotonNetwork.IsMasterClient) //countdown ending drop phase
        {
            StartCoroutine(endDropPhase());
        }
        
        StartCoroutine(Waiting()); // Run only a time to wait for time

    }
    private void Start()
    {
        SetCharacter();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("TURN" + turn);
        if(currentState != GameState.Drop && oneTimeSpawn) //end drop phase
        {
            oneTimeSpawn = false;
            //loop through list of character and spawn as networking object
            for(int i = 0; i < character_list.Count; i++)
            {
                GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefab", character_list[i].name), spawn_point[i], Quaternion.identity);
                character_list[i] = obj; //set character list to be clone object in scene instead
            }
        }
    }

    //set player number // master client -> 1 // non master client -> 2
    void setPlayer(bool isMaster)
    {
        if (isMaster)
        {
            player = 1;
        }
        else
        {
            player = 2;
        }
    }

    //countdown for ending drop phase
    IEnumerator endDropPhase()
    {
        yield return new WaitForSecondsRealtime(dropTime);
        GetComponent<PhotonView>().RPC("rpc_endDropPhase", RpcTarget.All);
    }
    
    //use rpc to change game state
    [PunRPC]
    void rpc_endDropPhase()
    {
        //firstTurnPlayer = Random.Range(1, 3);
        firstTurnPlayer = 2;
        if (firstTurnPlayer == 1)
        {
            currentState = GameState.P1_Action;
        }
        else
        {
            currentState = GameState.P2_Action;
        }
    }
    //==================== This Function use for deploy unit and Random when player doesn't select grid to deploy ====================
    void RandomEndDrop()
    {
        if (player == 1) // For player 1 because of the RPC and send data it is seperate if it is not show for 2 screen
        {
            for (int i = 0; i < spawn_point.Count; i++) // The lope that run in size of this array
            {
                if (gridBehaviour.spawnDrop[i] != null) // Check condition if there is spawnDrop list that keep in the gridBehaviour and if it is not null which is means has already select
                {
                    characterControllers[i].current_direction = 1; // Set current direction of player 1's units the directon point down from CharacterController Line 59
                    spawn_point[i] = gridBehaviour.gridArr[gridBehaviour.spawnDrop[i].row, gridBehaviour.spawnDrop[i].col].transform.localPosition; // Set Spawn point at that grid which player select
                }
                else if (spawn_point[i].x == 0 || spawn_point[i].y == 0) // Set condition that is the default when player doesn't select where to drop units
                {
                    randomNumber = Random.Range(0, gridBehaviour.listDropZone.Count); // Random number of array that from list
                    characterControllers[i].current_direction = 1; // Set current direction of player 1's units the directon point down
                    spawn_point[i] = gridBehaviour.gridArr[gridBehaviour.listDropZone[randomNumber].row, gridBehaviour.listDropZone[randomNumber].col].transform.localPosition; // Set the spawn point use the Vector2 of gridArr from "gridBehaviour" to set position of the random unit
                    gridBehaviour.listDropZone.Remove(gridBehaviour.listDropZone[randomNumber]); // Remove drop zone after random
                }
            }
        }
        else // For player 2
        {
            for (int i = 0; i < spawn_point.Count; i++) // The lope that run in size of this array
            {
                if (gridBehaviour.spawnDrop[i] != null) // Check condition if there is spawnDrop list that keep in the gridBehaviour and if it is not null which is means has already select
                {
                    characterControllers[i].current_direction = 3; // Set current direction of player 2's units the directon point Up from CharacterController Line 59
                    spawn_point[i] = gridBehaviour.gridArr[gridBehaviour.spawnDrop[i].row, gridBehaviour.spawnDrop[i].col].transform.localPosition; // Set Spawn point at that grid which player select
                }
                else if (spawn_point[i].x == 0 || spawn_point[i].y == 0) // Set condition that is the default when player doesn't select where to drop units
                {
                    randomNumber = Random.Range(0, gridBehaviour.listDropZone.Count); // Random number of array that from list
                    characterControllers[i].current_direction = 3; // Set current direction of player 1's units the directon point up
                    spawn_point[i] = gridBehaviour.gridArr[gridBehaviour.listDropZone[randomNumber].row, gridBehaviour.listDropZone[randomNumber].col].transform.localPosition; // Set the spawn point use the Vector2 of gridArr from "gridBehaviour" to set position of the random unit
                    gridBehaviour.listDropZone.Remove(gridBehaviour.listDropZone[randomNumber]); // Remove drop zone after random
                }
            }
        }
        RemoveSelectGrid(); // This function Line 257 use for Destroy deploy sample that Instantiate after deploy on grid
    }

    //public function returning controlling character as GameObject
    //if there's non -> return null
    public GameObject controllingCharacter()
    {
        for (int i = 0; i < character_list.Count; i++)
        {
            if (character_list[i].GetComponent<CharacterController>().isControl)
            {
                return character_list[i];
            }
        }
        return null;
    }

    //set all charaters' collider = set
    public void setColiider(bool set)
    {
        for (int i = 0; i < character_list.Count; i++)
        {
            character_list[i].GetComponent<CharacterController>().GetComponent<Collider2D>().enabled = set;
        }
    }
  
    //public function return list of GameObject sorted by attacking order
    public List<GameObject> sortAttackingOrder()
    {
        //attack_order_list.Clear();
        List<GameObject> order = new List<GameObject>(); //initialize new list

        foreach (GameObject character in character_list) //loop through list of characters
        {
            if (character.GetComponent<CharacterController>().is_steady_base) //if character is entering steady status
            {
                order.Add(character); //add character to the list
            }
        }

        if (order.Count > 1) //if there's steady character more than 1 character
        {
            //use bubble sort to sort the list depends on attackingOrder of each chracter
            for (int time = 0; time < order.Count; time++)
            {
                for (int i = 0; i < order.Count; i++)
                {
                    if (i + 1 < order.Count)
                    {
                        if (order[i].GetComponent<CharacterController>().attackingOrder > order[i + 1].GetComponent<CharacterController>().attackingOrder)
                        {
                            GameObject obj_temp = order[i];
                            order[i] = order[i + 1];
                            order[i + 1] = obj_temp;
                        }
                    }
                }
            }
        }

        //attack_order_list = order;

        if (order.Count > 0) //if there's steady character
        {
            return order;
        }
        else // no steady character
        {
            return null;
        }
    }

    //==================== Click at Icon to select and choose grid ====================
    public GameObject IconClick()
    {
        foreach(GameObject icon in icon_list)
        {
            if(icon.GetComponent<UnitDropIcon>().isSelecting)
            {
                return icon;
            }
        }
        return null;
    }
    //==================== This IEnumerator use for Waiting after DropPhase ====================
    IEnumerator Waiting()
    {
        yield return new WaitForSeconds(dropTime - 0.7f); // For the reason that cannot run in time I choose to run it before go to new GameState
        RandomEndDrop(); // Start function name it random
    }
    void SetCharacter()
    {
        for (int i = 0; i < character_list.Count; i++)
        {
            characterControllers.Add(character_list[i].GetComponent<CharacterController>());
        }
    }
    //==================== This function use to Remove Deploy sample ====================
    void RemoveSelectGrid()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Deploy"); // Add GameObject array that FindGameObjectWithTag string name "Deploy"
        for(int i = 0; i < objs.Length; i++) // Run lope by the number of the objs array
        {
            Destroy(objs[i]); // Destroy all of the objs array
        }
    }

    //check game over
    public bool isGameOverCheck()
    {
        int diedCharacter = 0;
        for(int i = 0; i < character_list.Count; i++)
        {
            CharacterController cc = character_list[i].GetComponent<CharacterController>();
            if (cc.health <= 0)
            {
                diedCharacter++;
            }
        }

        if(diedCharacter == character_list.Count)
        {
            return true;
        }
        else
        {
            if (diedCharacter == character_list.Count - 1)
            {
                canSurrender = true;
            }
            return false;
        }
    }

    //set end condition as int // -1 -> lose // 0 -> draw // 1 -> win
    public void setEndCondition(int endCondition)
    {
        PlayerPrefs.SetInt("END_CONDITION", endCondition);
    }
}