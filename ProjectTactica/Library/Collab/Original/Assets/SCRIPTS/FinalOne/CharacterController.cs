﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class CharacterController : MonoBehaviour
{
    public int character_number; //No. of character
    //public string name; //character's name
    public string race; //character's race
    public int health; //character's starting health
    public int baseAttackType; //type of base attack // 1 -> melee // 2-> range
    [SerializeField]
    private GameObject baseAttack; //base attack icon
    [SerializeField]
    private GameObject moving_guide_blue; //grid which is able to move on
    [SerializeField]
    private GameObject moving_guide_red; // grid which is not able to move on
    public GameObject character_sim; // character moving simulator
    public int maxStep; //max step character able to move (normally, it's 3)
    private int maxStepTemp;
    [SerializeField]
    private float translateSpeed; //walking speed
    public int baseAttackDelay; //delay of base attack (normally, it's 3)
    public int skillDelay;
    public float continueBattleDelay; //wait for secs realtime to continue the battle
    [SerializeField]
    private GameObject skill_icon;

    //======================================
    public int player; //player number
    public int row; //current row of grid, player's standing on
    public int col; //current column of grid, player's standing on
    public bool isControl; //is character controlled by the player
    public bool canMove; //can player move in the action phase
    public bool isMove; //enable to make player moving
    public int currentStep; //how many step player has already moved
    private int index_move; //use to run the lists of movement
    private bool oneTimeCanVisitCheck;
    public List<GameObject> path_list; //list of grid character gonna move to
    public List<int> direction_list; //list of direction character gonna move to
    public List<GameObject> character_sim_list; //list of character_sim as GameObject
    public List<GameObject> moving_guide_list; //list of moving guide
    private SystemController systemController;
    private GridBehaviour1 gridBehavior1;
    private BattlePhase battlePhase;
    public List<GameObject> character_ui_list; //list of ui spawned by character
    public List<GameObject> attacking_mark_list; //list of attacking mark
    public List<GameObject> attacking_grid_list; //lsit of grid in area of attack
    public int attack_direction; //direction character gonna attack
    public bool is_steady_base; //is character in steady status
    public bool isAttack; //is character attacking
    public bool isBeingAttacked; //is character being attacked
    public bool oneTimeSteady;
    public bool isCheckSteady; //is character already checked steady status (whether is enemy in range)
    public bool isLastSteadyCheck; //is this character the last one to check steady status
    public GameObject attacker; //attacker attackes this character as GameObject
    public bool isKnockBack; //set to private later
    private GameObject knockBackGrid; //grid which character knocks back to
    private bool isReKnockBack; //is character gonna knock back again
    public bool isStun; //make character unable to move
    public int turn_to_recover_stun; //turn to recover from stun
    private bool oneTimeAttack;
    private Vector2 oldPos;

    public int current_direction; //determine the directional facing of the character // 1 -> UP // 2 -> RIGHT // 3 -> DOWN // 4 -> LEFT //

    public int characterIndex; //index of character in character_list in SystemController script

    //skill
    public List<GameObject> skill_mark_list;
    public List<GameObject> skill_grid_list;
    public int skill_direction;
    public bool isSkill;
    public bool is_steady_skill;

    //Data used for data collecting to run the battle in battle phase
    //Confirm type of above variable
    public List<int> direction_list_confirm; //confirm type list of direction_list
    public List<GameObject> character_sim_list_confirm; //confirm type list of character_sim_list
    public List<GameObject> attacking_mark_list_confirm; //confirm type list of attacking_mark_list
    public List<GameObject> attacking_grid_list_confirm; //confirm type list of attacking_grid_list
    public int attack_direction_confirm; //confirm of attack direction
    public int turnToMove; //turn to runs character action
    public int delay_move; //delay of movement
    public int delay_baseAttack; //delay of base attack
    public int attackingOrder; //order of steady checking when entering steady status
    public List<GameObject> skill_mark_list_confirm;
    public List<GameObject> skill_grid_list_confirm;
    public int skill_direction_confirm;
    public int delay_skill;


    //Characters UI
    public Text delayText;
    public Text turnToMoveText;

    private void Awake()
    {
        setPlayer(); //set player number
    }
    void Start()
    { 
        isControl = false;
        canMove = false;
        isMove = false;
        is_steady_base = false;
        is_steady_skill = false;
        oneTimeCanVisitCheck = true;
        oneTimeSteady = true;
        oneTimeAttack = true;
        isCheckSteady = false;
        isLastSteadyCheck = false;
        isAttack = false;
        isSkill = false;
        isKnockBack = false;
        isReKnockBack = false;
        currentStep = 0;
        maxStepTemp = maxStep;
        index_move = 0;
        systemController = SystemController.systemController.GetComponent<SystemController>(); //SystemController
        gridBehavior1 = GridBehaviour1.gridBehaviour.GetComponent<GridBehaviour1>(); //GridBehaviour1
        battlePhase = BattlePhase.battlePhase.GetComponent<BattlePhase>(); //BattlePhase
        delay_baseAttack = 0;
        delay_skill = 0;
        attackingOrder = 0;
        isStun = false;
        turn_to_recover_stun = 0;
        StartCoroutine(Stun(false));
        if (GetComponent<PhotonView>().IsMine)
        {
            setDirection(current_direction);
        }
    }

    // Update is called once per frame
    void Update()
    {
        canMoveCheck(); //check can move variable
        rightClickCancel(); //use right click to cancel controlling
        characterMoving(); //character movement
        start_steady(); //entering steady status
        setIsCheckSteady(); //reset isCheckSteady value
        start_attack(); //start attacking
        start_beingAttacked(); //start being attacked
        knockingBack(oppositeDirection(current_direction), 1.5f); //control knocking back
        dataLossCheck(); //checking data loss and clear the data again
    }

    void dataLossCheck()
    {
        if(!isOurTurn())
        {
            if (moving_guide_list.Count > 0)
            {
                resetMovingGuide();
            }
            if (character_ui_list.Count > 0)
            {
                foreach (GameObject obj in character_ui_list)
                {
                    Destroy(obj);
                }
                character_ui_list.Clear();
            }
            if (isControl)
            {
                isControl = false;
            }
        }
    }

    //set player number
    void setPlayer()
    {
        if (GetComponent<PhotonView>().IsMine) //set our character
        {
            player = SystemController.player;
        }
        else
        {
            if(SystemController.player == 1) //set opponent's character
            {
                player = 2;
            }
            else
            {
                player = 1;
            }
        }
    }
    
    //reset controlling character
    void resetControl()
    {
        if(systemController.controllingCharacter() != null)
        {
            CharacterController cc = systemController.controllingCharacter().GetComponent<CharacterController>();
            //clear unconfirm data
            cc.isControl = false;
            cc.path_list.Clear();
            cc.direction_list.Clear();
            foreach(GameObject obj in cc.character_sim_list)
            {
                PhotonNetwork.Destroy(obj);
            }
            cc.character_sim_list.Clear();
            cc.resetMovingGuide();
            cc.currentStep = 0;

            //clear character ui
            foreach(GameObject obj in cc.character_ui_list)
            {
                Destroy(obj);
            }
            cc.clearAttackingMark();
            cc.clearSkillMark();
        }
    }

    //press right click to cancel controlling
    void rightClickCancel()
    {
        if(Input.GetKeyDown(KeyCode.Mouse1))
        {
            if (GetComponent<PhotonView>().IsMine && isOurTurn())
            {
                resetControl();
            }

            //apply to every CharacterController obj
            /*if (health > 0)
            {
                GetComponent<Collider2D>().enabled = false;
                GetComponent<Collider2D>().enabled = true;
            }*/
        }
    }

    //return boolean whether neighbor grid is able to move on
    bool canVisitCheck(int current_row, int current_col, int direction)
    {
        if(direction == 1)
        {
            if(gridBehavior1.gridArr[current_row - 1, current_col].GetComponent<GridStat1>().canVisit)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if(direction == 2)
        {
            if (gridBehavior1.gridArr[current_row, current_col + 1].GetComponent<GridStat1>().canVisit)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if (direction == 3)
        {
            if (gridBehavior1.gridArr[current_row + 1, current_col].GetComponent<GridStat1>().canVisit)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if (direction == 4)
        {
            if (gridBehavior1.gridArr[current_row, current_col - 1].GetComponent<GridStat1>().canVisit)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
    }

    //set canMove variable
    void canMoveCheck()
    {
        //can move when... // entering player's action phase according to player number // must be client's object // not moving more than step limit // no confirm list data
        if((isOurTurn() && GetComponent<PhotonView>().IsMine && currentStep < maxStep  && direction_list_confirm.Count == 0 && attacking_mark_list_confirm.Count == 0  && skill_mark_list_confirm.Count == 0 && !isStun))
        {
            canMove = true;
        }
        else
        {
            canMove = false;
        }
    }
    IEnumerator Stun(bool set)
    {
        if (set)
        {
            yield return new WaitUntil(() => isOurTurn());
            if(turn_to_recover_stun == SystemController.turn && isStun == true)
            {
                isStun = false;
                turn_to_recover_stun = 0;
            }
                StartCoroutine(Stun(false));
        }
        else
        {
            yield return new WaitUntil(() => !isOurTurn());

            StartCoroutine(Stun(true));
        }
    }
    bool isOurTurn()
    {
        if ((player == 1 && SystemController.currentState == SystemController.GameState.P1_Action) || (player == 2 && SystemController.currentState == SystemController.GameState.P2_Action))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //clear all spawned moving guide
    public void resetMovingGuide()
    {
        for (int i = 0; i < systemController.character_list.Count; i++)
        {
            foreach(GameObject obj in systemController.character_list[i].GetComponent<CharacterController>().moving_guide_list)
            {
                Destroy(obj);
            }
            systemController.character_list[i].GetComponent<CharacterController>().moving_guide_list.Clear();
        }
    }

    //spawn moving guide on neighbor grid
    public void spawnMovingGuide(int current_row, int current_col)
    {
        if(current_row - 1 > -1) //above grid is existed
        {
            if(canVisitCheck(current_row, current_col, 1) && alreadyChooseCheck(current_row - 1, current_col)) //grid is able to visit and not already chosen to move on
            {
                //spawn BLUE moving guide as GameObject
                GameObject obj = Instantiate(moving_guide_blue, gridBehavior1.gridArr[current_row - 1, current_col].transform.localPosition, Quaternion.identity);
                setMovingGuide(obj, current_row - 1, current_col, 1); //set moving guide data
            }
            else //grid unable to move on
            {
                //spawn RED moving guide as GameObject
                GameObject obj = Instantiate(moving_guide_red, gridBehavior1.gridArr[current_row - 1, current_col].transform.localPosition, Quaternion.identity);
                setMovingGuide(obj, current_row - 1, current_col, 1);
            }
        }
        if (current_col + 1 < gridBehavior1.colTotal) //right grid is existed
        {
            if (canVisitCheck(current_row, current_col, 2) && alreadyChooseCheck(current_row, current_col + 1))
            {
                GameObject obj = Instantiate(moving_guide_blue, gridBehavior1.gridArr[current_row, current_col + 1].transform.localPosition, Quaternion.identity);
                setMovingGuide(obj, current_row, current_col + 1, 2);
            }
            else
            {
                GameObject obj = Instantiate(moving_guide_red, gridBehavior1.gridArr[current_row, current_col + 1].transform.localPosition, Quaternion.identity);
                setMovingGuide(obj, current_row, current_col + 1, 2);
            }
        }
        if(current_row + 1 < gridBehavior1.rowTotal) //below grid is existed
        {
            if (canVisitCheck(current_row, current_col, 3) && alreadyChooseCheck(current_row + 1, current_col))
            {
                GameObject obj = Instantiate(moving_guide_blue, gridBehavior1.gridArr[current_row + 1, current_col].transform.localPosition, Quaternion.identity);
                setMovingGuide(obj, current_row + 1, current_col, 3);
            }
            else
            {
                GameObject obj = Instantiate(moving_guide_red, gridBehavior1.gridArr[current_row + 1, current_col].transform.localPosition, Quaternion.identity);
                setMovingGuide(obj, current_row + 1, current_col, 3);
            }
        }
        if (current_col - 1 > -1) //left grid is existed
        {
            if (canVisitCheck(current_row, current_col, 4) && alreadyChooseCheck(current_row, current_col - 1))
            {
                GameObject obj = Instantiate(moving_guide_blue, gridBehavior1.gridArr[current_row, current_col - 1].transform.localPosition, Quaternion.identity);
                setMovingGuide(obj, current_row, current_col - 1, 4);
            }
            else
            {
                GameObject obj = Instantiate(moving_guide_red, gridBehavior1.gridArr[current_row, current_col - 1].transform.localPosition, Quaternion.identity);
                setMovingGuide(obj, current_row, current_col - 1, 4);
            }
        }
    }

    //set moving gudie data
    public void setMovingGuide(GameObject movingGuide, int current_row, int current_col, int direction)
    {
        moving_guide_list.Add(movingGuide); //add moving guide to list
        movingGuide.GetComponent<MovementMark>().row = current_row; //set row of moving guide in MovingMark script
        movingGuide.GetComponent<MovementMark>().col = current_col; //set column of moving guide in MovingMark script
        movingGuide.GetComponent<MovementMark>().direction = direction; //set direction of moving guide
        RendererDifferent rd = movingGuide.GetComponent<RendererDifferent>();
        if (rd != null)
        {
            rd.characterIndex = characterIndex;
        }
    }

    //check whether the grid is already chosen
    bool alreadyChooseCheck(int current_row, int current_col)
    {
        for(int i = 0; i < path_list.Count; i++) //loop through list of chosen grid to move on
        {
            if(gridBehavior1.gridArr[current_row, current_col] == path_list[i]) //if already chosen
            {
                return false;
            }
        }
        return true;
    }

    //======================================================MOVING=============================================================//
    void characterMoving() //check whether character's gonna move
    {
        if (isMove)
        {
            startMoving(); //start moving
            if (index_move < direction_list_confirm.Count)
            {
                whileMoving(direction_list_confirm[index_move]); //move with the direction order in the list
            }
        }
    }

    void startMoving()
    {
        if(oneTimeCanVisitCheck) //check can visit only one time
        {
            oneTimeCanVisitCheck = false;
            //if the grid is unable to move on // or // grid is in steady range
            if (!canVisitCheck(row, col, direction_list_confirm[index_move]) || gridBehavior1.gridArr[row,col].GetComponent<GridStat1>().isSteady)
            {
                finishAction_move(); //stop action move
                GetComponent<PhotonView>().RPC("rpc_clearAllAction", RpcTarget.All); //clear all action data
            }
        }
    }

    //control the direction while character's moving
    void whileMoving(int direction)
    {
        setDirection(direction); //set character direction to moving direction
        if(direction == 1) //moving up
        {
            transform.Translate(Vector2.up * translateSpeed * Time.deltaTime);
            if(transform.position.y >= character_sim_list_confirm[index_move].transform.position.y) //character moving to target position
            {
                changeDirection(); //change the direction of moving
            }
        }
        else if(direction == 2) //moving right
        {
            transform.Translate(Vector2.right * translateSpeed * Time.deltaTime);
            if (transform.position.x >= character_sim_list_confirm[index_move].transform.position.x)
            {
                changeDirection();
            }
        }
        else if(direction == 3) //moving down
        {
            transform.Translate(Vector2.down * translateSpeed * Time.deltaTime);
            if (transform.position.y <= character_sim_list_confirm[index_move].transform.position.y)
            {
                changeDirection();
            }
        }
        else if(direction == 4) //moving left
        {
            transform.Translate(Vector2.left * translateSpeed * Time.deltaTime);
            if (transform.position.x <= character_sim_list_confirm[index_move].transform.position.x)
            {
                changeDirection();
            }
        }
    }

    //change moving direction
    void changeDirection()
    {
        transform.localPosition = character_sim_list_confirm[index_move].transform.localPosition; //set position = moving target's position
        PhotonNetwork.Destroy(character_sim_list_confirm[index_move]); //destroy networking character_sim
        index_move++; //increase index move
        oneTimeCanVisitCheck = true; //can check condition before moving again
        if(index_move == direction_list_confirm.Count) //if index out of range
        {
            finishAction_move(); //stop moving
        }
    }

    //stop moving
    void finishAction_move()
    {
        isMove = false; //disable isMove
        //clear all data
        if(index_move < character_sim_list_confirm.Count)
        {
            for(int i = index_move; i < character_sim_list_confirm.Count; i++)
            {
                PhotonNetwork.Destroy(character_sim_list_confirm[i]);
            }
        }
        character_sim_list_confirm.Clear();
        direction_list_confirm.Clear();
        delay_move = 0;
        index_move = 0;
        currentStep = 0;
        oneTimeCanVisitCheck = true;
        if(attacking_mark_list_confirm.Count == 0 && skill_mark_list_confirm.Count == 0) //if character also has attack action
        {
            turnToMove = 0; //dont yet clear turn to move data
        }

        battlePhase.actionCheck_complete(player); //complete action check of this character and continur battle phase
        
    }

    //======================================================MOVING=============================================================//

    //spawn ui for attacking
    void spawnSkillUi()
    {
        GameObject baseAtk = Instantiate(baseAttack, Vector2.zero, Quaternion.identity); //spawn base attack ui
        character_ui_list.Add(baseAtk); //add to list of character ui
        GameObject skillIcon = Instantiate(skill_icon, Vector2.zero, Quaternion.identity); //spawn skill ui
        character_ui_list.Add(skillIcon); //add to list character ui
    }

    //clear all spawned attacking mark
    public void clearAttackingMark()
    {
        if (attacking_mark_list.Count > 0) //if character spawns attacking mark
        {
            foreach (GameObject mark in attacking_mark_list) //loop throgh list of attacking mark
            {
                PhotonNetwork.Destroy(mark); //destroy mark networkingly
            }

            //clear attacking list
            attacking_mark_list.Clear();
            attacking_grid_list.Clear();
        }
    }

    public void clearSkillMark()
    {
        if (skill_mark_list.Count > 0) //if character spawns attacking mark
        {
            foreach (GameObject mark in skill_mark_list) //loop throgh list of attacking mark
            {
                PhotonNetwork.Destroy(mark); //destroy mark networkingly
            }

            //clear attacking list
            skill_mark_list.Clear();
            skill_grid_list.Clear();
        }
    }


    //===========================================Steady // Attack // Knockback===========================================

    //public function to clear all action of this character
    public void clearAllAction()
    {
        direction_list_confirm.Clear();
        foreach(GameObject obj in character_sim_list_confirm)
        {
            PhotonNetwork.Destroy(obj);
        }
        character_sim_list_confirm.Clear();
        foreach(GameObject obj in attacking_mark_list_confirm)
        {
            PhotonNetwork.Destroy(obj);
        }
        clearAttackingGrid();
        attacking_mark_list_confirm.Clear();
        attacking_grid_list_confirm.Clear();
        attack_direction_confirm = 0;
        turnToMove = 0;
        delay_baseAttack = 0;
        delay_move = 0;
        oneTimeSteady = true;
        oneTimeCanVisitCheck = true;
        is_steady_base = false;

        //clear skill
        foreach (GameObject obj in skill_mark_list_confirm)
        {
            PhotonNetwork.Destroy(obj);
        }
        skill_mark_list_confirm.Clear();
        skill_grid_list_confirm.Clear();
        is_steady_skill = false;
        isSkill = false;
        delay_skill = 0;
    }

    //reset isSteady value in GridStat1 of other display
    void clearAttackingGrid()
    {
        if (attacking_grid_list_confirm.Count > 0)
        {
            foreach (GameObject grid in attacking_grid_list_confirm) //loop through confirm list of attacking grid
            {
                int setRow = grid.GetComponent<GridStat1>().row;
                int setCol = grid.GetComponent<GridStat1>().col;
                if (clearAttackGridCheck(grid)) //check whether gonna reset isSteady value
                {
                    GetComponent<PhotonView>().RPC("setGridSteady", RpcTarget.Others, setRow, setCol, false); //set target to other player
                }
            }
        }
    }

    //check whether gonna reset isSteady value
    bool clearAttackGridCheck(GameObject grid_check)
    {
        foreach(GameObject character in systemController.character_list) //loop through all character
        {
            CharacterController cc = character.GetComponent<CharacterController>();
            if (character != gameObject && cc.is_steady_base) //if character that loop through isn't this character
            {
                foreach(GameObject grid in cc.attacking_grid_list_confirm) //loop confirm list of attacking grid
                {
                    if(grid == grid_check) //if the grid is also in steady range of other character
                    {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    //start entering steady status
    void start_steady()
    {
        if (is_steady_base && oneTimeSteady)
        {
            oneTimeSteady = false;
            setDirection(attack_direction_confirm); //set character's direction =  attack direction
            attackingOrder = SystemController.attackingOrder; //set attacking order
            SystemController.attackingOrder++; //increase static attacking order

            foreach(GameObject grid in attacking_grid_list_confirm)
            {
                int setRow = grid.GetComponent<GridStat1>().row;
                int setCol = grid.GetComponent<GridStat1>().col;
                GetComponent<PhotonView>().RPC("setGridSteady", RpcTarget.Others, setRow, setCol, true); //set all in-range grid's isSteady value = true 
            }
            finish_steady(); //steady finish
        }
    }

    [PunRPC]
    void setGridSteady(int row, int col, bool set)
    {
        gridBehavior1.gridArr[row, col].GetComponent<GridStat1>().isSteady = set;
    }

    void finish_steady()
    {
        battlePhase.actionCheck_complete(player); //end action checking of this player and continue battle phase
    }

    //check whether enemy is in-range of attack or not
    public bool isEnemyInRange()
    {
        foreach(GameObject grid in attacking_grid_list_confirm) //loop through confirm list of attacking grid
        {
            GridStat1 gridStat = grid.GetComponent<GridStat1>();
            //there's character on the grid and character is opponent's
            if (gridStat.stepOnCharacter != null && gridStat.stepOnCharacter.GetComponent<CharacterController>().player != player)
            {
                return true;
            }
        }
        return false;
    }

    //reset isCheckSteady value (make it able to check steady status in battle phase)
    void setIsCheckSteady()
    {
        if(SystemController.currentState == SystemController.GameState.P1_Action && player == 1)
        {
            isCheckSteady = false;
        }
        else if (SystemController.currentState == SystemController.GameState.P2_Action && player == 2)
        {
            isCheckSteady = false;
        }
    }

    //start attack
    void start_attack()
    {
        if (isAttack)
        {
            if (oneTimeAttack)
            {
                oneTimeAttack = false;
                oldPos = transform.localPosition;
                StartCoroutine(waitForAttackComplete());
                //finishAttack(); //comment this out later
                GetComponent<Collider2D>().enabled = false;
            }
            attacking_default(attack_direction_confirm);
        }
    }
    public void finishAttack() //set this function as an event system in animator
    {
        isAttack = false;
        oneTimeAttack = true;
        attacking_default_return(oppositeDirection(attack_direction_confirm), oldPos);
        //melee attack damage all characters in range of attack
        if (baseAttackType == 1) //melee attack
        {
            foreach (GameObject grid in attacking_grid_list_confirm) //loop through confirm list of attacking grid
            {
                GridStat1 gridStat = grid.GetComponent<GridStat1>();
                if (gridStat.stepOnCharacter != null) //there's character standing on the grid
                {
                    CharacterController cc = gridStat.stepOnCharacter.GetComponent<CharacterController>();

                    if (cc.player != player) //is it opponent's character
                    {
                        cc.setIsBeingAttack(true); //make opponent character being attacked
                        cc.attacker = gameObject; //set this character as an attacker in opponent CharacterController script
                        cc.setDirection(oppositeDirection(current_direction)); //set direction of opponent's character to be the opposite of attacker character's direction
                        cc.setKnockBack(); //make oppoenent character knockback one tile
                    }
                }
            }
        }
        else //range attack
        {
            //range attack damage one character which is the nearest one
            for(int i = 0; i < attacking_grid_list_confirm.Count; i++) //loop through confirm list og attacking grid
            {
                GridStat1 gridStat = attacking_grid_list_confirm[i].GetComponent<GridStat1>();
                if(gridStat.stepOnCharacter != null) //if there's character standing on
                {
                    CharacterController cc = gridStat.stepOnCharacter.GetComponent<CharacterController>();

                    if (cc.player != player && rangeAttackCheck(attacking_grid_list_confirm[i])) //is it opponent's character
                    {
                        cc.setIsBeingAttack(true); //make opponent's character being attacked
                        cc.attacker = gameObject; //set this character as an attacker in opponent CharacterController script
                        break; //break for loop when found first character to attack
                    }
                }
            }
        }
        GetComponent<PhotonView>().RPC("rpc_clearAllAction", RpcTarget.All); //clear all action data after attack
        StartCoroutine(continueSteadyCheck()); //delay a bit after attack
    }

    //checking range attack whether the attack is gonna complete
    bool rangeAttackCheck(GameObject grid)
    {
        for(int i = 0 ;i < attacking_grid_list_confirm.Count; i++)
        {
            GameObject checking_grid = attacking_grid_list_confirm[i];
            if (checking_grid == grid)
            {
                break;
            }
            else
            {
                if(checking_grid.tag == "HURT_TERRAIN" || checking_grid.tag == "STUN_TERRAIN") //if it is covering terrain
                {
                    return false;
                }
            }
        }
        return true;
    }

    public void setIsBeingAttack(bool set)
    {
        GetComponent<PhotonView>().RPC("rpc_setIsBeingAttack", RpcTarget.All, set);
    }

    [PunRPC]
    void rpc_setIsBeingAttack(bool set)
    {
        isBeingAttacked = set;
    }


    //continue steady checking in battle phase
    IEnumerator continueSteadyCheck()
    {
        if (baseAttackType == 1)
        {
            yield return new WaitForSecondsRealtime(continueBattleDelay * 1.5f); //a bit longer to wait knockbacking character before continue battle phase
        }
        else
        {
            yield return new WaitForSecondsRealtime(continueBattleDelay);
        }
        isCheckSteady = true;
        if (isLastSteadyCheck) //if it is the last character to check steady status
        {
            isLastSteadyCheck = false; //reset value
            battlePhase.steady_check_complete(player); //complete steady check of this player
        }
    }

    //start being attacked by others
    void start_beingAttacked()
    {
        if (isBeingAttacked)
        {
            isBeingAttacked = false;
            blinkRenderer(8);
            if (!GetComponent<PhotonView>().IsMine)
            {
                finish_beingAttacked();
            }
        }
    }

    //finish being attacked by others
    void finish_beingAttacked()
    {
        string attacker_race = attacker.GetComponent<CharacterController>().race; //get race of attacker
        //check effective and decrease character's health via rpc
        GetComponent<PhotonView>().RPC("rpc_beingAttacked", RpcTarget.All, isEffectiveAttack(attacker_race, race)); 
        //clear all action via rpc
        GetComponent<PhotonView>().RPC("rpc_clearAllAction", RpcTarget.All);
    }

    [PunRPC]
    void rpc_beingAttacked(bool isEffective)
    {
        if (!isEffective)
        {
            health--;
            Debug.Log("Damage = 1");
        }
        else
        {
            health -= 2;
            Debug.Log("Damage = 2");
        }

        startDie(); //check whether health <= 0 is already in function
    }

    [PunRPC]
    void rpc_clearAllAction()
    {
        clearAllAction();
    }

    //check is effective
    bool isEffectiveAttack(string attacker_race, string enemy_race)
    {
        if(attacker_race == "Beast" && enemy_race == "Human") //Beast > Human
        {
            return true;
        }
        else if(attacker_race == "Human" && enemy_race == "Elf") //Human > Elf
        {
            return true;
        }
        else if(attacker_race == "Elf" && enemy_race == "Beast") //Elf > Beast
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //public function called rpc function
    public void setKnockBack()
    {
        GetComponent<PhotonView>().RPC("rpc_setKnockBack_others", RpcTarget.Others);
        //GetComponent<PhotonView>().RPC("rpc_setKnockBack_all", RpcTarget.All);
    }

    [PunRPC]
    void rpc_setKnockBack_others()
    {
        isKnockBack = true;
        start_knockBack(); //start knocking back
    }

    /*[PunRPC]
    void rpc_setKnockBack_all()
    {
        isKnockBack = true; //knock back
    }*/

    void start_knockBack()
    {
        int knockBackDirection = oppositeDirection(current_direction); //set knock back direction to the opposite of current direction
        if(knockBackDirection == 1 && row - 1 > -1) //check above grid is existed
        {
            set_start_knockBack(gridBehavior1.gridArr[row - 1, col]); //set value before start knocking back
        }
        else if(knockBackDirection == 2 && col + 1 < gridBehavior1.colTotal) //check right grid is existed
        {
            set_start_knockBack(gridBehavior1.gridArr[row, col + 1]);
        }
        else if(knockBackDirection == 3 && row + 1 < gridBehavior1.rowTotal) //check below grid is existed
        {
            set_start_knockBack(gridBehavior1.gridArr[row + 1, col]); 
        }
        else if(knockBackDirection == 4 && col - 1 > -1) //check left grid is existed
        {
            set_start_knockBack(gridBehavior1.gridArr[row, col - 1]);
        }
    }

    void set_start_knockBack(GameObject grid)
    {
        knockBackGrid = grid;
        reKnockBackCheck(knockBackGrid); //check whether to knock back again
    }

    void reKnockBackCheck(GameObject grid)
    {
        if (grid.GetComponent<GridStat1>().canVisit) //if the grid is visitable -> dont have to knock back again
        {
            isReKnockBack = false;
        }
        else //otherwise, knock back again
        {
            isReKnockBack = true;
        }
    }

    //while knocking back
    void knockingBack(int direction, float multiAmount)
    {
        if (isKnockBack /*&& GetComponent<PhotonView>().IsMine*/ && knockBackGrid != null)
        {
            if(direction == 1) //knock back to above grid
            {
                transform.Translate(Vector2.up * translateSpeed * multiAmount * Time.deltaTime);
                if(transform.position.y >= knockBackGrid.transform.position.y) //if character reaches the grid target
                {
                    finishKnockBack(); //stop knocking back
                }
            }
            else if(direction == 2) //knock back to right grid
            {
                transform.Translate(Vector2.right * translateSpeed * multiAmount * Time.deltaTime);
                if (transform.position.x >= knockBackGrid.transform.position.x)
                {
                    finishKnockBack();
                }
            }
            else if(direction == 3) //knock back to below grid
            {
                transform.Translate(Vector2.down * translateSpeed * multiAmount * Time.deltaTime);
                if (transform.position.y <= knockBackGrid.transform.position.y)
                {
                    finishKnockBack();
                }
            }
            else if(direction == 4) //knock back to left grid
            {
                transform.Translate(Vector2.left * translateSpeed * multiAmount * Time.deltaTime);
                if (transform.position.x <= knockBackGrid.transform.position.x)
                {
                    finishKnockBack();
                }
            }
        }
    }

    //stop knocking back
    void finishKnockBack()
    {
        transform.localPosition = knockBackGrid.transform.localPosition; //set postion = target grid position
        isKnockBack = false; //stop knock back
        if (isReKnockBack) //check whether has to knock back again
        {
            //check what grid standing on
            //if it is cactus -> 
            if (gridBehavior1.gridArr[row, col].tag == "HURT_TERRAIN")
            {
                GetComponent<PhotonView>().RPC("rpc_beingAttacked", RpcTarget.All, false);
                GetComponent<PhotonView>().RPC("rpc_clearAllAction", RpcTarget.All);
            }

            //if it is mountain ->
            //stun function will be provide later
            if (gridBehavior1.gridArr[row, col].tag == "STUN_TERRAIN")
            {
                isStun = true;
                turn_to_recover_stun = SystemController.turn + 2;
                GetComponent<PhotonView>().RPC("rpc_clearAllAction", RpcTarget.All);
            }

            start_reKnockBack(); //start knocking back again
        }
    }

    void start_reKnockBack()
    {
        isReKnockBack = false; //reset value

        //set knock back target grid according to the direction
        if(current_direction == 1)
        {
            knockBackGrid = gridBehavior1.gridArr[row - 1, col];
        }
        else if(current_direction == 2)
        {
            knockBackGrid = gridBehavior1.gridArr[row, col + 1];
        }
        else if(current_direction == 3)
        {
            knockBackGrid = gridBehavior1.gridArr[row + 1, col];
        }
        else if(current_direction == 4)
        {
            knockBackGrid = gridBehavior1.gridArr[row, col - 1];
        }

        setDirection(oppositeDirection(current_direction)); //set direction = opposite direction
        isKnockBack = true; //knock bace

    }
    

    //===========================================Steady // Attack // Knockback===========================================
    
    //find opposite direction
    int oppositeDirection(int direction)
    {
        if(direction == 1)
        {
            return 3;
        }
        else if (direction == 2)
        {
            return 4;
        }
        else if (direction == 3)
        {
            return 1;
        }
        else
        {
            return 2;
        }
    }

    //public function calls rpc function
    public void setDirection(int direction)
    {
        GetComponent<PhotonView>().RPC("rpc_setDirection", RpcTarget.All, direction);
    }

    //ask canceling steady
    void start_cancel_steady()
    {
        UI_Controller.asking_character = gameObject; //set asking object in UI_Controller script
        UI_Controller.isActive_askingPanel = true; //set active asking ui panel
    }

    //set character direction via rpc
    [PunRPC]
    void rpc_setDirection(int direction)
    {
        current_direction = direction;
    }

    //====================================DIE=====================================
    void startDie()
    {
        GetComponent<PhotonView>().RPC("rpc_startDie", RpcTarget.All);
    }

    [PunRPC]
    void rpc_startDie()
    {
        if(health <= 0)
        {
            health = 0;
            GetComponent<Collider2D>().enabled = false;
            //die animation
            finishDie(); //delete this line later
        }
    }

    //add in the end of die animation
    public void finishDie()
    {
        GetComponent<PhotonView>().RPC("rpc_finishDie", RpcTarget.All);
    }

    [PunRPC]
    void rpc_finishDie()
    {
        GetComponent<SpriteRenderer>().enabled = false;
    }
    //====================================DIE=====================================


    //===================================SET_DATA_HIDING===============================//

    void setDataHiding_MouseOver(GameObject obj, bool set, bool isOwner)
    {
        if (isOwner)
        {
            obj.GetComponent<DataHiding>().isMouseOver_our = set;
        }
        else
        {
            obj.GetComponent<DataHiding>().isMouseOver_opponent = set;
        }
    }

    public void setDataHiding_Show(GameObject obj, bool set)
    {
        obj.GetComponent<DataHiding>().isShow = set;
        LerpingRenderer lr = obj.GetComponent<LerpingRenderer>();
        if (lr != null)
        {
            if (lr.isLerp != set)
            {
                lr.isLerp = set;
            }
        }
    }

    [PunRPC]
    void mouseEnter_dataHiding(bool isOwner)
    {
        if (character_sim_list_confirm.Count > 0)
        {
            foreach (GameObject c_sim in character_sim_list_confirm)
            {
                setDataHiding_MouseOver(c_sim, true, isOwner);
            }
        }

        if (attacking_mark_list_confirm.Count > 0)
        {
            foreach (GameObject attacking_mark in attacking_mark_list_confirm)
            {
                setDataHiding_MouseOver(attacking_mark, true, isOwner);
            }
        }

        if (skill_mark_list_confirm.Count > 0)
        {
            foreach (GameObject skill_mark in skill_mark_list_confirm)
            {
                setDataHiding_MouseOver(skill_mark, true, isOwner);
            }
        }
    }

    [PunRPC]
    void mouseExit_dataHiding(bool isOwner)
    {
        if (character_sim_list_confirm.Count > 0)
        {
            foreach (GameObject c_sim in character_sim_list_confirm)
            {
                setDataHiding_MouseOver(c_sim, false, isOwner);
            }
        }

        if (attacking_mark_list_confirm.Count > 0)
        {
            foreach (GameObject mark in attacking_mark_list_confirm)
            {

                setDataHiding_MouseOver(mark, false, isOwner);
            }
        }

        if (skill_mark_list_confirm.Count > 0)
        {
            foreach (GameObject skill_mark in skill_mark_list_confirm)
            {
                setDataHiding_MouseOver(skill_mark, false, isOwner);
            }
        }
    }

    //===================================SET_DATA_HIDING===============================//

    //===================================ATTACKING_ANIMATION_DEFAULT===============================//

    void attacking_default(int direction)
    {
        if (direction == 1)
        {
            transform.Translate(Vector2.up * translateSpeed / 3f * Time.deltaTime);
        }
        else if (direction == 2)
        {
            transform.Translate(Vector2.right * translateSpeed / 3f * Time.deltaTime);
        }
        else if (direction == 3)
        {
            transform.Translate(Vector2.down * translateSpeed / 3f * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector2.left * translateSpeed / 3f * Time.deltaTime);
        }
    }

    void attacking_default_return(int direction, Vector2 pos)
    {
        if (direction == 1)
        {
            transform.Translate(Vector2.up * translateSpeed * Time.deltaTime);
            if (transform.position.y >= pos.y)
            {
                transform.localPosition = pos;
                GetComponent<Collider2D>().enabled = true;
            }
            else
            {
                attacking_default_return(direction, pos);
            }
        }
        else if (direction == 2)
        {
            transform.Translate(Vector2.right * translateSpeed * Time.deltaTime);
            if (transform.position.x >= pos.x)
            {
                transform.localPosition = pos;
                GetComponent<Collider2D>().enabled = true;
            }
            else
            {
                attacking_default_return(direction, pos);
            }
        }
        else if (direction == 3)
        {
            transform.Translate(Vector2.down * translateSpeed * Time.deltaTime);
            if (transform.position.y <= pos.y)
            {
                transform.localPosition = pos;
                GetComponent<Collider2D>().enabled = true;
            }
            else
            {
                attacking_default_return(direction, pos);
            }
        }
        else
        {
            transform.Translate(Vector2.left * translateSpeed * Time.deltaTime);
            if (transform.position.x <= pos.x)
            {
                transform.localPosition = pos;
                GetComponent<Collider2D>().enabled = true;
            }
            else
            {
                attacking_default_return(direction, pos);
            }
        }
    }

    IEnumerator waitForAttackComplete()
    {
        yield return new WaitForSecondsRealtime(0.15f);
        finishAttack();
    }

    IEnumerator toggleRenderer(float second, bool set)
    {
        yield return new WaitForSecondsRealtime(second);
        if (health > 0 || (health <= 0 && !set))
        {
            GetComponent<SpriteRenderer>().enabled = set;
        }
    }

    void blinkRenderer(int time) //should be odd number
    {
        for (int i = 0; i < time; i++)
        {
            bool set;
            if(i % 2 == 0)
            {
                set = false;
            }
            else
            {
                set = true;
            }
            StartCoroutine(toggleRenderer(0.2f * i, set));
        }
    }

    //===================================ATTACKING_ANIMATION_DEFAULT===============================//

    void nerfTerrainCheck()
    {
        if(gridBehavior1.gridArr[row, col].tag == "NERF_TERRAIN")
        {
            maxStep = 1;
        }
        else
        {
            maxStep = maxStepTemp;
        }
    }
    
    //when cursor hover on character's collider
    public void onMouseDown()
    {
        if (canMove && !isControl) //if player click on character and able to move the character
        {
            //reset all charactes controlling data and set this character as controlling character
            resetControl();
            spawnMovingGuide(row, col);
            isControl = true;
            spawnSkillUi();
            nerfTerrainCheck();
        }
        //if is in steady status but wanna cancel the steady status and move the character
        else if(!canMove && is_steady_base && !UI_Controller.isActive_askingPanel && isOurTurn()) 
        {
            start_cancel_steady();
        }
    }

    public void onMouseEnter()
    {

        //set data hiding value
        PhotonView pv = GetComponent<PhotonView>();
        if (pv.IsMine)
        {
            mouseEnter_dataHiding(pv.IsMine);
        }
        else
        {
            pv.RPC("mouseEnter_dataHiding", RpcTarget.Others, pv.IsMine);
        }
    }

    public void onMouseExit()
    {
        //set data hiding value
        PhotonView pv = GetComponent<PhotonView>();
        if (pv.IsMine)
        {
            mouseExit_dataHiding(pv.IsMine);
        }
        else
        {
            pv.RPC("mouseExit_dataHiding", RpcTarget.Others, pv.IsMine);
        }
    }

    //collision checking
    private void OnTriggerEnter2D(Collider2D other)
    {
        //set row and column of the character = row and column of grid in GridStat1 script
        if(other.GetComponent<GridStat1>()!= null)
        {
            row = other.GetComponent<GridStat1>().row;
            col = other.GetComponent<GridStat1>().col;
        }

        //clear all actiion when collide with knocking back character
        if(other.tag == "character")
        {
            CharacterController cc = other.GetComponent<CharacterController>();
            if (cc.isKnockBack)
            {
                GetComponent<PhotonView>().RPC("rpc_clearAllAction", RpcTarget.All);
            }
        }
    }
}
